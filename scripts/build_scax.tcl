# Set the reference directory for source file relative paths (by default the value is script directory path)
#set origin_dir [file dirname [info script]]
set origin_dir /home/nate/christos_crap/sca_extension/sources/tcl/

puts $origin_dir/../

# top
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/sca_extension.vhd

# scax_main
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/controller.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/dna_reader.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/master_handler.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/scax_watchdog.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/s_reply_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/debug_buffer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_master.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_router.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_readWrite_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_cdc_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/registerFile.vhd

# elinks
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/centralRouter_package.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/Elink2FIFO.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/elinkRXfifo_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc_8b10.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc8b10_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_DEC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_direct.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/FIFO2Elink.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX2_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX4_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX8_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_pdxx_pwxx.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_fall_pw01.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/upstreamEpathFifoWrap.vhd

# elinks/userLogic
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/crc_1byte.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/deframer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/elink_driver.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/elink_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/fcs_wrapperRX.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/fcs_wrapperTX.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/framer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/interface_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/flx_tester.vhd

# common
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/pipeline.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/CDCC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/scax_package.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/registerFile_package.vhd

# user_support
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/user_support/scax_mem_ctrl.vhd

# IP cores
 read_ip $origin_dir/../sources/ip/fh_epath_fifo2K_18bit_wide.xcix
 read_ip $origin_dir/../sources/ip/upstreamFIFO.xcix
 read_ip $origin_dir/../sources/ip/hdlc_bist_fifo.xcix
 read_ip $origin_dir/../sources/ip/i2c_cdc_buffer.xcix
 read_ip $origin_dir/../sources/ip/ila_overview.xcix
 read_ip $origin_dir/../sources/ip/vio_debug.xcix
 read_ip $origin_dir/../sources/ip/dna_buffer.xcix

# constraints
read_xdc -verbose $origin_dir/../sources/xdc/scax.xdc
#read_xdc -verbose $origin_dir/../sources/xdc/vc709.xdc

# Set 'sources_1' fileset properties
puts "Build Succesful. Enjoy :)"
