# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir/.. $::origin_dir_loc
}

puts $origin_dir/../

# Set the project name
set project_name "scax_testbench"

# Use project name variable, if specified in the tcl shell
if { [info exists ::user_project_name] } {
  set project_name $::user_project_name
}

variable script_file
set script_file "build_scax_testbench.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir/.. <path>\]"
  puts "$script_file -tclargs \[--project_name <name>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir/.. <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir/.. path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--project_name <name>\] Create project with the specified name. Default"
  puts "                       name is the name of the project from where this"
  puts "                       script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir/.."   { incr i; set origin_dir/.. [lindex $::argv $i] }
      "--project_name" { incr i; set project_name [lindex $::argv $i] }
      "--help"         { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Create project
create_project $project_name $origin_dir/../$project_name -part xc7a200tfbg484-2

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# top
read_vhdl -vhdl2008 -library work $origin_dir/../sources/sim/scax_testbench_top.vhd

# simulation files (other)
read_vhdl -vhdl2008 -library work $origin_dir/../sources/sim/opcUA_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/sim/scax_package_simul.vhd

# scax_main
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/sca_extension.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/controller.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/dna_reader.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/master_handler.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/scax_watchdog.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/s_reply_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/debug_buffer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_master.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_router.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_readWrite_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/i2c_cdc_manager.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/scax_main/registerFile.vhd

# elinks
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/centralRouter_package.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/Elink2FIFO.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/elinkRXfifo_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc_8b10.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc8b10_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_DEC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_direct.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/FIFO2Elink.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX2_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX4_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX8_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_pdxx_pwxx.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_fall_pw01.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/upstreamEpathFifoWrap.vhd

# elinks/userLogic
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/crc_1byte.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/deframer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/elink_driver.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/elink_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/fcs_wrapperRX.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/fcs_wrapperTX.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/framer.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/interface_wrapper.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/userLogic/flx_tester.vhd


# common
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/pipeline.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/CDCC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/scax_package.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/common/registerFile_package.vhd

# user_support
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/user_support/scax_mem_ctrl.vhd

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$origin_dir/../sources/sim/scax_testbench_top_tb.vhd"]"\
 "[file normalize "$origin_dir/../sources/ip/dna_buffer.xcix"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$origin_dir/../sources/sim/scax_testbench_top_tb.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

# Set 'sim_1' fileset file properties for remote files
# set file "$origin_dir/../sources/sim/scax_package_simul.vhd"
# set file [file normalize $file]
# set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
# set_property "file_type" "VHDL" $file_obj

# IP cores
set obj [get_filesets sources_1]   
set files [list \
 "[file normalize "$origin_dir/../sources/sim/mmcm_master.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/fh_epath_fifo2K_18bit_wide.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/upstreamFIFO.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/hdlc_bist_fifo.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/i2c_cdc_buffer.xcix"]"\
]

add_files -norecurse -fileset $obj $files

# constraints
#read_xdc -verbose $origin_dir/../sources/xdc/scax.xdc
#read_xdc -verbose $origin_dir/../sources/xdc/vc709.xdc

set_property target_language VHDL [current_project]

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "scax_testbench_top" $obj

puts "############################################################################"
puts "Copyright Notice/Copying Permission:
    Copyright 2018 Christos Bakalis (christos.bakalis@cern.ch)\n

    This file is part of SCA_eXtension_firmware (SCAX).\n

    SCAX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SCAX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.\n"
puts "############################################################################"

puts "###################################"
puts "INFO: Project created:$project_name"
puts "###################################"
puts "Build Succesful. Enjoy :)"
