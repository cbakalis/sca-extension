-- for testing

library UNISIM; 
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 
use work.scax_package_simul.all; 
use work.registerFile_package.all; 


entity scax_testbench_top is
  port(
    ------------------------------
    ------ General Interface -----
    elink_clk     : in std_logic; 
    user_clk      : in std_logic; 
    bypass        : in std_logic; 
    rst           : in std_logic; 
    ena_flx_test  : in std_logic; 
    timeout       : in std_logic_vector(15 downto 0); 
    ------------------------------
    ------ OPC-UA Interface ------
    -- rx
    frame_new     : out std_logic;                      -- new frame available
    frame_rx_opc  : out frame_rx_opcUA;                 -- frame contents
    frame_error   : out std_logic_vector(1 downto 0);   -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_sizeRX  : out std_logic_vector(1 downto 0);   -- only 4 size types
    -- tx
    frame_tx_opc  : in frame_tx_opcUA;                  -- frame contents
    tx_request    : in std_logic;                       -- tx new frame 
    frame_sizeTX  : in std_logic_vector(1 downto 0);    -- only 4 size types
    ------------------------------
    ------ Memory Interface ------
    wrEn_toMem    : out std_logic;                     -- to mem wr_en
    ramAddr_toMem : out std_logic_vector(31 downto 0); -- to ram addr
    din_toMem     : out std_logic_vector(31 downto 0); -- to mem din
    ------------------------------
    ------ Bypass Interface ------
    wr_en_elink   : in std_logic; 
    din_elink     : in std_logic_vector(17 downto 0) 
  ); 
end scax_testbench_top; 

architecture Behavioral of scax_testbench_top is
  
component mmcm_master
  port(
    -- Clock in ports
    clk_in1 : in std_logic; 
    -- Clock out ports
    clk_out_40  : out std_logic; 
    clk_out_80  : out std_logic; 
    clk_out_160 : out std_logic; 
    clk_out_320 : out std_logic; 
    -- Status and control signals
    reset  : in  std_logic; 
    locked : out std_logic
  ); 
end component; 

component sca_extension
  generic(
    debug_mode    : std_logic := '0'; 
    TX_DataRate   : integer   := 80;   -- RX is only 80, TX can be 80 or 320
    elinkEncoding : std_logic_vector(1 downto 0)  := "01"; -- only "01" (8b10b)
    cdc_bitmask   : std_logic_vector(15 downto 0) := x"0000"; 
    ser_input     : boolean   := false
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk_scax      : in  std_logic; -- scax main clock
    clk_regFile   : in  std_logic_vector(15 downto 0); -- all possible destination clocks
    clk_40        : in  std_logic; -- 40Mhz e-link clock
    clk_80        : in  std_logic; -- 80Mhz e-link clock
    clk_160       : in  std_logic; -- 160Mhz e-link clock
    clk_320       : in  std_logic; -- 320Mhz e-link clock
    rst           : in  std_logic; -- reset the modules
    -- dbg
    ena_flx_test  : in  std_logic; 
    dbg_fifo_rd   : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX : in  UFLtoSCAX;      -- used when reading
    regs_SCAXtoUFL : out SCAXtoUFL;      -- used when writing
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink      : in  std_logic;-- 1-bit elink rx
    rx_elink2bit  : in  std_logic_vector(1 downto 0); -- 2-bit elink rx
    rx_swap       : in  std_logic; -- swap input bits
    -- tx
    tx_swap       : in  std_logic; -- swap output bits
    tx_elink      : out std_logic; -- 1-bit elink tx
    tx_elink8bit  : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit  : out std_logic_vector(1 downto 0) -- 2-bit elink tx
  ); 
end component; 

component opcUA_wrapper
  generic(
    TX_dataRate     : integer                       := 80;   -- only 80
    elinkEncoding   : std_logic_vector (1 downto 0) := "01"; -- only "01" (8b10b)
    ser_input       : boolean                       := false
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in std_logic; -- user clock
    clk_40          : in std_logic; -- 40Mhz e-link clock
    clk_80          : in std_logic; -- 80Mhz e-link clock
    clk_160         : in std_logic; -- 160Mhz e-link clock
    clk_320         : in std_logic; -- 320Mhz e-link clock
    rst             : in std_logic; -- reset the modules
    reverse_rx      : in std_logic; 
    reverse_tx      : in std_logic; 
    bypass          : in std_logic; 
    wr_en_elink     : in std_logic; 
    din_elink       : in std_logic_vector(17 downto 0); 
    -------         --------------
    -- Master Handler Interface --
    -- rx
    handler_busy    : in  std_logic;                    -- handler not in idle
    deframer_busy   : out std_logic;                    -- busy
    frame_new       : out std_logic;                    -- new frame available
    frame_fieldsRX  : out frame_rx;                     -- frame contents
    frame_error     : out std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_sizeRX    : out std_logic_vector(1 downto 0); -- only 4 size types
                                                        -- tx
    frame_fieldsTX  : in  frame_tx;                     -- frame contents
    tx_request      : in  std_logic;                    -- tx new frame 
    frame_sizeTX    : in  std_logic_vector(1 downto 0); -- only 4 size types
    framer_busy     : out std_logic;                    -- busy
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        : in std_logic;                       -- 1-bit elink rx
    rx_elink2bit    : in std_logic_vector(1 downto 0);    -- 2-bit elink rx
    rx_swap         : in std_logic;                       -- swap input bits
    -- tx
    tx_swap         : in  std_logic;                      -- swap output bits
    tx_elink        : out std_logic;                      -- 1-bit elink tx
    tx_elink2bit    : out std_logic_vector(1 downto 0)    -- 2-bit elink tx
  ); 
end component; 
  
component scax_mem_ctrl
  generic(
    Nbits_addr_W      : natural := 12;  -- address width (write operation)
    Nbits_data_W      : natural := 32;  -- data width (write operation)
    Nbits_addr_R      : natural := 12;  -- address width (read operation) 
    Nbits_data_R      : natural := 32); -- data width (read operation)
  port(
    -------------------------------
    ------ General Interface ------
    wr_clk            : in std_logic; -- regFile wr_clk + mem element wr_clk
    rd_clk            : in std_logic; -- regFile wr_clk + mem element rd_clk
    rst               : in std_logic; -- reset
    -------------------------------
    -- Register File Interface ----
    -- write
    wrEn_fromSCAX     : in  std_logic;                                 -- write-pulse the memDin into the mem ctrl
    wr_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_W-1 downto 0); -- addr to write into
    din_fromSCAX      : in  std_logic_vector(Nbits_data_W-1 downto 0); -- data to write
    wr_addr_toSCAX    : out std_logic_vector(Nbits_addr_W-1 downto 0); -- current addr (back to scax)
    -- read
    rdEn_fromSCAX     : in  std_logic;                                 -- read-pulse the memDout into the mem ctrl
    rd_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_R-1 downto 0); -- addr to read from
    dout_toSCAX       : out std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read (back to scax)
    rd_addr_toSCAX    : out std_logic_vector(Nbits_addr_R-1 downto 0); -- current addr (back to scax)
    -------------------------------
    -- Memory Element Interface ---
    -- write
    wrEn_toMem        : out std_logic;                                 -- to ram/fifo wr_en
    wr_addr_toMem     : out std_logic_vector(Nbits_addr_W-1 downto 0); -- to ram/fifo addr
    din_toMem         : out std_logic_vector(Nbits_data_W-1 downto 0); -- to ram/fifo din
    --read
    dout_fromMem      : in  std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read
    rdEn_toMem        : out std_logic;
    rd_addr_toMem     : out std_logic_vector(Nbits_addr_R-1 downto 0) -- to ram/fifo addr
  ); 
end component; 
  
  signal clk_40             : std_logic := '0'; 
  signal clk_80             : std_logic := '0'; 
  signal clk_160            : std_logic := '0'; 
  signal clk_320            : std_logic := '0'; 
  signal clk_regFile_i      : std_logic_vector(15 downto 0) := (others => '0'); 
  signal elink_2bit_toSCAX   : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal elink_2bit_fromSCAX : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal frame_fieldsRX_i   : frame_rx; 
  signal frame_fieldsTX_i   : frame_tx; 
  
  signal reverse_rx_scax     : std_logic := '0'; 
  signal reverse_tx_scax     : std_logic := '0'; 
  signal reverse_rx_opc     : std_logic := '0'; 
  signal reverse_tx_opc     : std_logic := '0'; 
  
  signal swap_rx_scax        : std_logic := '0'; 
  signal swap_tx_scax        : std_logic := '0'; 
  signal swap_rx_opc        : std_logic := '0'; 
  signal swap_tx_opc        : std_logic := '0'; 
  
  signal elink_1bit_toSCAX   : std_logic := '0'; 
  signal elink_1bit_fromSCAX : std_logic := '0'; 
  signal dbg_fifo_rd        : std_logic := '0'; 
  
  signal rst_i              : std_logic := '0'; 
  signal mmcm_locked        : std_logic := '0'; 

  signal  wrEn_2Mem         : std_logic := '0';
  signal  wr_addr_toMem     : std_logic_vector(11 downto 0) := (others => '0');
  signal  din_2Mem          : std_logic_vector(31 downto 0) := (others => '0');
  signal  dout_fromMem      : std_logic_vector(31 downto 0) := (others => '0');
  signal  rdEn_toMem        : std_logic := '0';
  signal  rd_addr_toMem     : std_logic_vector(11 downto 0) := (others => '0');

  signal regs_UFLtoSCAX_i    : UFLtoSCAX; 
  signal regs_SCAXtoUFL_i    : SCAXtoUFL; 
  
begin
  
mmcm_master_inst : mmcm_master
  port map( 
    -- Clock out ports  
    clk_out_40  => clk_40,
    clk_out_80  => clk_80,
    clk_out_160 => clk_160,
    clk_out_320 => clk_320,
    -- Status and control signals                
    reset       => rst,
    locked      => mmcm_locked,
    -- Clock in ports
    clk_in1     => elink_clk
  ); 

sca_extension_inst : sca_extension
  generic map(
    debug_mode    => '0',
    TX_DataRate   => 80,
    elinkEncoding => elinkEncoding,
    cdc_bitmask   => cdc_bitmask,
    ser_input     => ser_input)
  port map(
    ------------------------------
    ------ General Interface -----
    clk_scax      => user_clk,
    clk_regFile   => clk_regFile_i,
    clk_40        => clk_40,
    clk_80        => clk_80,
    clk_160       => clk_160,
    clk_320       => clk_320,
    rst           => rst_i,
    -- dbg
    ena_flx_test  => ena_flx_test,
    dbg_fifo_rd   => dbg_fifo_rd,
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX => regs_UFLtoSCAX_i,
    regs_SCAXtoUFL => regs_SCAXtoUFL_i,
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink      => elink_1bit_toSCAX,
    rx_elink2bit  => elink_2bit_toSCAX,
    rx_swap       => swap_rx_scax,
    -- tx
    tx_swap       => swap_tx_scax,
    tx_elink      => elink_1bit_fromSCAX,
    tx_elink8bit  => open,
    tx_elink2bit  => elink_2bit_fromSCAX
  ); 
  
opcUA_wrapper_inst : opcUA_wrapper
  generic map(
    TX_dataRate   => 80,
    elinkEncoding => elinkEncoding,
    ser_input     => ser_input)
  port map(
    ------------------------------
    ------ General Interface -----
    clk             => user_clk,
    clk_40          => clk_40,
    clk_80          => clk_80,
    clk_160         => clk_160,
    clk_320         => clk_320,
    rst             => rst_i,
    reverse_rx      => reverse_rx_opc,
    reverse_tx      => reverse_tx_opc,
    bypass          => bypass,
    wr_en_elink     => wr_en_elink,
    din_elink       => din_elink,
    -------         --------------
    -- Master Handler Interface --
    -- rx
    handler_busy    => clk_40,
    deframer_busy   => open,
    frame_new       => frame_new,
    frame_fieldsRX  => frame_fieldsRX_i,
    frame_error     => frame_error,
    frame_sizeRX    => frame_sizeRX,
    -- tx
    frame_fieldsTX  => frame_fieldsTX_i,
    tx_request      => tx_request,
    frame_sizeTX    => frame_sizeTX,
    framer_busy     => open,
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        => elink_1bit_fromSCAX,
    rx_elink2bit    => elink_2bit_fromSCAX,
    rx_swap         => swap_rx_opc,
    -- tx
    tx_swap         => swap_tx_opc,
    tx_elink        => elink_1bit_toSCAX,
    tx_elink2bit    => elink_2bit_toSCAX
  ); 
  
scax_mem_ctrl_inst: scax_mem_ctrl
  generic map(
    Nbits_addr_W  => 12,
    Nbits_data_W  => 32,
    Nbits_addr_R  => 12,
    Nbits_data_R  => 32)
  port map(
    -------------------------------
    ------ General Interface ------
    wr_clk            => user_clk,
    rd_clk            => user_clk,
    rst               => rst,
    -------------------------------
    -- Register File Interface ----
    -- write
    wrEn_fromSCAX     => regs_SCAXtoUFL_i.master01_wrEn_003,
    wr_addr_fromSCAX  => regs_SCAXtoUFL_i.master01_wrAddr32_002,
    din_fromSCAX      => regs_SCAXtoUFL_i.master01_wrData32_003,
    wr_addr_toSCAX    => regs_UFLtoSCAX_i.master01_wrAddr32_002,
    -- read
    rdEn_fromSCAX     => regs_SCAXtoUFL_i.master01_rdEn_001,
    rd_addr_fromSCAX  => regs_SCAXtoUFL_i.master01_rdAddr12_000,
    dout_toSCAX       => regs_UFLtoSCAX_i.master01_rdData32_001,
    rd_addr_toSCAX    => regs_UFLtoSCAX_i.master01_rdAddr12_000,
    -------------------------------
    -- Memory Element Interface ---
    -- write
    wrEn_toMem        => wrEn_2Mem,
    wr_addr_toMem     => wr_addr_toMem,
    din_toMem         => din_2Mem,
    --read
    dout_fromMem      => dout_fromMem,
    rdEn_toMem        => rdEn_toMem,
    rd_addr_toMem     => rd_addr_toMem
  ); 
  
  frame_rx_opc.address      <= frame_fieldsRX_i.address; 
  frame_rx_opc.control      <= frame_fieldsRX_i.control; 
  frame_rx_opc.tr_id        <= frame_fieldsRX_i.tr_id; 
  frame_rx_opc.channel      <= frame_fieldsRX_i.channel; 
  frame_rx_opc.err          <= frame_fieldsRX_i.length; 
  frame_rx_opc.length       <= frame_fieldsRX_i.command; 
  frame_rx_opc.din_a        <= frame_fieldsRX_i.din_a; 
  frame_rx_opc.din_b        <= frame_fieldsRX_i.din_b; 
  frame_rx_opc.fcs          <= frame_fieldsRX_i.fcs; 
  
  
  frame_fieldsTX_i.address  <= frame_tx_opc.address; 
  frame_fieldsTX_i.control  <= frame_tx_opc.control; 
  frame_fieldsTX_i.tr_id    <= frame_tx_opc.tr_id; 
  frame_fieldsTX_i.channel  <= frame_tx_opc.channel; 
  frame_fieldsTX_i.err      <= frame_tx_opc.length; 
  frame_fieldsTX_i.length   <= frame_tx_opc.command; 
  frame_fieldsTX_i.dout_a   <= frame_tx_opc.dout_a; 
  frame_fieldsTX_i.dout_b   <= frame_tx_opc.dout_b; 
  frame_fieldsTX_i.fcs      <= frame_tx_opc.fcs; 
  
  rst_i                     <= rst or (not mmcm_locked); 
  clk_regFile_i             <= (others => clk_160); 
  
-- dummy loopback proc
    regs_UFLtoSCAX_i.master00_dummy4of32_003(3) <= x"01234567";
    regs_UFLtoSCAX_i.master00_dummy32_00a       <= x"abcdef01";

  
end Behavioral; 
