----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 28.08.2018 00:12:10
-- Design Name: Frame Package
-- Module Name: scax_package_simul - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Package for logical grouping of frame contents
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 

package scax_package_simul is
  
type frame_rx_opcUA is record
  address : std_logic_vector(7 downto 0); 
  control : std_logic_vector(7 downto 0); 
  ---------------------------------------
  tr_id   : std_logic_vector(7 downto 0); 
  channel : std_logic_vector(7 downto 0); 
  err     : std_logic_vector(7 downto 0); 
  length  : std_logic_vector(7 downto 0); 
  din_a   : std_logic_vector(15 downto 0); 
  din_b   : std_logic_vector(15 downto 0); 
  ---------------------------------------
  fcs : std_logic_vector(15 downto 0); 
end record; 
  
type frame_tx_opcUA is record
  address : std_logic_vector(7 downto 0); 
  control : std_logic_vector(7 downto 0); 
  ---------------------------------------
  tr_id   : std_logic_vector(7 downto 0); 
  channel : std_logic_vector(7 downto 0); 
  length  : std_logic_vector(7 downto 0); 
  command : std_logic_vector(7 downto 0); 
  dout_a  : std_logic_vector(15 downto 0); 
  dout_b  : std_logic_vector(15 downto 0); 
  ---------------------------------------
  fcs : std_logic_vector(15 downto 0); 
end record; 
  
  -- i2c commands
  constant I2C_W_CTRL  : std_logic_vector(7 downto 0) := x"30"; 
  constant I2C_R_CTRL  : std_logic_vector(7 downto 0) := x"31"; 
  constant I2C_R_STR   : std_logic_vector(7 downto 0) := x"11"; 
  constant I2C_W_DATA0 : std_logic_vector(7 downto 0) := x"40"; 
  constant I2C_R_DATA0 : std_logic_vector(7 downto 0) := x"41"; 
  constant I2C_W_DATA1 : std_logic_vector(7 downto 0) := x"50"; 
  constant I2C_R_DATA1 : std_logic_vector(7 downto 0) := x"51"; 
  constant I2C_S_10B_W : std_logic_vector(7 downto 0) := x"8A"; 
  constant I2C_S_10B_R : std_logic_vector(7 downto 0) := x"8E"; 
  constant I2C_M_10B_W : std_logic_vector(7 downto 0) := x"E2"; 
  constant I2C_M_10B_R : std_logic_vector(7 downto 0) := x"E6"; 
  
  -- i2c channels
  constant I2C_CH_00 : std_logic_vector(7 downto 0) := x"03"; 
  constant I2C_CH_01 : std_logic_vector(7 downto 0) := x"04"; 
  constant I2C_CH_02 : std_logic_vector(7 downto 0) := x"05"; 
  constant I2C_CH_03 : std_logic_vector(7 downto 0) := x"06"; 
  constant I2C_CH_04 : std_logic_vector(7 downto 0) := x"07"; 
  constant I2C_CH_05 : std_logic_vector(7 downto 0) := x"08"; 
  constant I2C_CH_06 : std_logic_vector(7 downto 0) := x"09"; 
  constant I2C_CH_07 : std_logic_vector(7 downto 0) := x"0A"; 
  constant I2C_CH_08 : std_logic_vector(7 downto 0) := x"0B"; 
  constant I2C_CH_09 : std_logic_vector(7 downto 0) := x"0C"; 
  constant I2C_CH_10 : std_logic_vector(7 downto 0) := x"0D"; 
  constant I2C_CH_11 : std_logic_vector(7 downto 0) := x"0E"; 
  constant I2C_CH_12 : std_logic_vector(7 downto 0) := x"0F"; 
  constant I2C_CH_13 : std_logic_vector(7 downto 0) := x"10"; 
  constant I2C_CH_14 : std_logic_vector(7 downto 0) := x"11"; 
  constant I2C_CH_15 : std_logic_vector(7 downto 0) := x"12"; 
  
  
end scax_package_simul; 


