library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.scax_package_simul.all;

entity scax_testbench_top_tb is
end;

architecture bench of scax_testbench_top_tb is

  component scax_testbench_top
      port(
          elink_clk       : in  std_logic;
          user_clk        : in  std_logic;
          bypass          : in  std_logic;
          rst             : in  std_logic;
          timeout         : in  std_logic_vector(15 downto 0);
          ena_flx_test    : in  std_logic;
          frame_new       : out std_logic;
          frame_rx_opc    : out frame_rx_opcUA;
          frame_error     : out std_logic_vector(1 downto 0);
          frame_sizeRX    : out std_logic_vector(1 downto 0);
          frame_tx_opc    : in  frame_tx_opcUA;
          tx_request      : in  std_logic;
          frame_sizeTX    : in  std_logic_vector(1 downto 0);
          wrEn_toMem      : out std_logic; -- to mem wr_en
          ramAddr_toMem   : out std_logic_vector(31 downto 0); -- to ram addr
          din_toMem       : out std_logic_vector(31 downto 0);  -- to mem din
          wr_en_elink     : in  std_logic;
          din_elink       : in  std_logic_vector(17 downto 0)  
      );
  end component;

  signal elink_clk: std_logic := '0';
  signal user_clk: std_logic := '0';
  signal bypass: std_logic := '0';
  signal ena_flx_test : std_logic := '0';
  signal rst: std_logic := '0';
  signal frame_new: std_logic := '0';
  signal timeout : std_logic_vector(15 downto 0) := (others => '0');
  signal frame_rx_opc: frame_rx_opcUA;
  signal frame_error: std_logic_vector(1 downto 0) := (others => '0');
  signal frame_sizeRX: std_logic_vector(1 downto 0)  := (others => '0');
  signal frame_tx_opc: frame_tx_opcUA;
  signal tx_request: std_logic := '0';
  signal frame_sizeTX: std_logic_vector(1 downto 0) := (others => '0');
  signal wr_en_elink: std_logic := '0';
  signal wrEn_toMem: std_logic := '0';
  signal din_elink: std_logic_vector(17 downto 0)  := (others => '0');
  signal ramAddr_toMem: std_logic_vector(31 downto 0)  := (others => '0');
  signal din_toMem: std_logic_vector(31 downto 0)  := (others => '0');

  constant clock_period: time := 3.125 ns;
  constant elink_period : time := 25 ns;
  signal stop_the_clock: boolean;

begin

  uut: scax_testbench_top port map ( elink_clk    => elink_clk,
                                    user_clk     => user_clk,
                                    bypass       => bypass,
                                    rst          => rst,
                                    timeout      => timeout,
                                    ena_flx_test => ena_flx_test,
                                    frame_new    => frame_new,
                                    frame_rx_opc => frame_rx_opc,
                                    frame_error  => frame_error,
                                    frame_sizeRX => frame_sizeRX,
                                    frame_tx_opc => frame_tx_opc,
                                    tx_request   => tx_request,
                                    frame_sizeTX => frame_sizeTX,
                                    wrEn_toMem    => wrEn_toMem, -- to mem wr_en
                                    ramAddr_toMem => ramAddr_toMem, -- to ram addr
                                    din_toMem     => din_toMem, -- to mem din
                                    wr_en_elink  => wr_en_elink,
                                    din_elink    => din_elink );

  stimulus: process
  begin

  -- init the signals
  wait for clock_period*2600;
  frame_tx_opc.address <= x"00";
  frame_tx_opc.control <= x"00";
  frame_tx_opc.tr_id <= x"00";
  frame_tx_opc.length <= x"00";
  frame_tx_opc.command <= x"00";
  frame_tx_opc.dout_a <= x"0000";
  frame_tx_opc.dout_b <= x"0000";
  
  wait for clock_period*2600;
  ena_flx_test <= '1';
  
  wait for clock_period*500;
  ena_flx_test <= '0';

-- reset
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"8f";
  frame_tx_opc.tr_id    <= x"00";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"00";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "00";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"03";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"04";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"0010";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"05";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"66";
  frame_tx_opc.tr_id    <= x"06";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"0002";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"88";
  frame_tx_opc.tr_id    <= x"07";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"aa";
  frame_tx_opc.tr_id    <= x"08";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"0006";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"cc";
  frame_tx_opc.tr_id    <= x"09";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"0a";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"0030";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"0b";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"0c";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"0038";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"0d";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"66";
  frame_tx_opc.tr_id    <= x"0e";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"000e";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"88";
  frame_tx_opc.tr_id    <= x"0f";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"aa";
  frame_tx_opc.tr_id    <= x"10";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"001e";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"cc";
  frame_tx_opc.tr_id    <= x"11";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"12";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"003e";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"13";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"14";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"007e";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"15";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"03";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRB
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"66";
  frame_tx_opc.tr_id    <= x"16";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"02";
  frame_tx_opc.dout_a   <= x"00fe";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"88";
  frame_tx_opc.tr_id    <= x"17";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"aa";
  frame_tx_opc.tr_id    <= x"18";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"0001";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"cc";
  frame_tx_opc.tr_id    <= x"19";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"1a";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"0003";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"1b";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"1c";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"0007";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"1d";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"66";
  frame_tx_opc.tr_id    <= x"1e";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"000f";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"88";
  frame_tx_opc.tr_id    <= x"1f";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"aa";
  frame_tx_opc.tr_id    <= x"20";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"001f";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"cc";
  frame_tx_opc.tr_id    <= x"21";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"22";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"003f";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"23";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"24";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"007f";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"25";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"05";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- write CRC
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"66";
  frame_tx_opc.tr_id    <= x"26";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"04";
  frame_tx_opc.dout_a   <= x"00ff";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"88";
  frame_tx_opc.tr_id    <= x"27";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"aa";
  frame_tx_opc.tr_id    <= x"28";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"0039";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"cc";
  frame_tx_opc.tr_id    <= x"29";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"2a";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"003b";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- read CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"00";
  frame_tx_opc.tr_id    <= x"2b";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"07";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "01";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end

-- write CRD
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"22";
  frame_tx_opc.tr_id    <= x"2c";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"02";
  frame_tx_opc.command  <= x"06";
  frame_tx_opc.dout_a   <= x"003f";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "10";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end


-- read SCAX ID
  wait for clock_period*2_600;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"44";
  frame_tx_opc.tr_id    <= x"03";
  frame_tx_opc.channel  <= x"14";
  frame_tx_opc.length   <= x"04";
  frame_tx_opc.command  <= x"d1";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "11";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end
  
-- write GPIO DIRECTION
    wait for clock_period*2_600;
  
    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"04";
    frame_tx_opc.channel  <= x"02";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"20";
    frame_tx_opc.dout_a   <= x"2400";
    frame_tx_opc.dout_b   <= x"0000";
  
    frame_sizeTX          <= "11";
    tx_request            <= '1';
  
    wait for clock_period;
    tx_request <= '0';
    -- end

-- write ADC INSEL
    wait for clock_period*2_600;
  
    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"05";
    frame_tx_opc.channel  <= x"14";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"50";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"1A1A";
  
    frame_sizeTX          <= "11";
    tx_request            <= '1';
  
    wait for clock_period;
    tx_request <= '0';
    -- end

-- write ADC_GO
    wait for clock_period*2_600;
  
    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"AA";
    frame_tx_opc.tr_id    <= x"06";
    frame_tx_opc.channel  <= x"14";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"02";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"0100";
  
    frame_sizeTX          <= "11";
    tx_request            <= '1';
  
    wait for clock_period;
    tx_request <= '0';
    -- end

  -- 18. write JTAG_CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"CC";
    frame_tx_opc.tr_id    <= x"07";
    frame_tx_opc.channel  <= x"13";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"80";
    frame_tx_opc.dout_a   <= x"dead";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

  -- 18. read JTAG_CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"EE";
    frame_tx_opc.tr_id    <= x"08";
    frame_tx_opc.channel  <= x"13";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"81";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

  -- 18. read JTAG_TDI1
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"09";
    frame_tx_opc.channel  <= x"13";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"11";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

  
    -------------------------
    -------------------------
    -- WRITE TO ADDRESS 0x6
    -------------------------
    -------------------------
  
-- 1. read I2C CTRL
    wait for clock_period*10_000;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"CC";
    frame_tx_opc.tr_id    <= x"0a";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request           <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end
    

-- 2. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"EE";
    frame_tx_opc.tr_id    <= x"0b";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 3. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"0c";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 4. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"22";
    frame_tx_opc.tr_id    <= x"06";
    frame_tx_opc.channel  <= x"0d";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 5. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"44";
    frame_tx_opc.tr_id    <= x"0e";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 6. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"0f";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 7. write DATA0 (this is the address)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"10";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"40";
    frame_tx_opc.dout_a   <= x"0006"; --0006 for addr 6
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 8. write DATA1 (these are the data)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"AA";
    frame_tx_opc.tr_id    <= x"11";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"50";
    frame_tx_opc.dout_a   <= x"c01a";
    frame_tx_opc.dout_b   <= x"c0ca";

    frame_sizeTX          <= "11";
    tx_request           <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


    wait for clock_period*2_600;
-- 9. write I2C_M_10B_W
    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"CC";
    frame_tx_opc.tr_id    <= x"12";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E2";
    frame_tx_opc.dout_a     <= x"0078";
    frame_tx_opc.dout_b     <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


    -------------------------
    -------------------------
    -- WRITE TO ADDRESS 0xA
    -------------------------
    -------------------------
  
-- 1. read I2C CTRL
    wait for clock_period*10_000;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"44";
    frame_tx_opc.tr_id    <= x"1e";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request           <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end
    

-- 2. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"1f";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 3. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"20";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 4. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"aa";
    frame_tx_opc.tr_id    <= x"21";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 5. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"cc";
    frame_tx_opc.tr_id    <= x"22";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 6. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"ee";
    frame_tx_opc.tr_id    <= x"23";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"00A3";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 7. write DATA0 (this is the address)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"24";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"40";
    frame_tx_opc.dout_a   <= x"000A"; --0006 for addr 6
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 8. write DATA1 (these are the data)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"22";
    frame_tx_opc.tr_id    <= x"25";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"50";
    frame_tx_opc.dout_a   <= x"beef";
    frame_tx_opc.dout_b   <= x"dead";

    frame_sizeTX          <= "11";
    tx_request           <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


    wait for clock_period*2_600;
-- 9. write I2C_M_10B_W
    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"44";
    frame_tx_opc.tr_id    <= x"26";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E2";
    frame_tx_opc.dout_a     <= x"0078";
    frame_tx_opc.dout_b     <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


    
    -------------------------
    -------------------------
    -- READ FROM ADDRESS 0x6
    -------------------------
    -------------------------


-- 10. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"EE";
    frame_tx_opc.tr_id    <= x"13";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 11. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"14";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 12. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"22";
    frame_tx_opc.tr_id    <= x"15";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 13. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"44";
    frame_tx_opc.tr_id    <= x"16";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 14. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"17";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

    -- 14. send I2C_M_10B_W (because why the fuck not)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"18";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E2";
    frame_tx_opc.dout_a   <= x"0078";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 11. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"aa";
    frame_tx_opc.tr_id    <= x"19";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 12. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"cc";
    frame_tx_opc.tr_id    <= x"1a";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 7. write DATA0 (this is the address)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"ee";
    frame_tx_opc.tr_id    <= x"1b";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"40";
    frame_tx_opc.dout_a   <= x"0006"; --0006 for addr 6 
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

    
-- 15. write I2C_M_10B_R 
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"1c";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E6";
    frame_tx_opc.dout_a   <= x"0078";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 16. read DATA3
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"22";
    frame_tx_opc.tr_id    <= x"1d";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"71";
    frame_tx_opc.dout_a   <= x"0010";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


    


    
    -------------------------
    -------------------------
    -- READ FROM ADDRESS 0xa
    -------------------------
    -------------------------


-- 10. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"27";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0010";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 11. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"28";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 12. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"aa";
    frame_tx_opc.tr_id    <= x"29";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 13. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"cc";
    frame_tx_opc.tr_id    <= x"30";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 14. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"ee";
    frame_tx_opc.tr_id    <= x"31";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

    -- 14. send I2C_M_10B_W (because why the fuck not)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"00";
    frame_tx_opc.tr_id    <= x"32";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E2";
    frame_tx_opc.dout_a   <= x"0078";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 11. read I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"22";
    frame_tx_opc.tr_id    <= x"33";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"31";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


-- 12. write I2C CTRL
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"44";
    frame_tx_opc.tr_id    <= x"34";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"02";
    frame_tx_opc.command  <= x"30";
    frame_tx_opc.dout_a   <= x"0093";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "10";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 7. write DATA0 (this is the address)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"66";
    frame_tx_opc.tr_id    <= x"35";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"40";
    frame_tx_opc.dout_a   <= x"000A"; --0006 for addr 6 
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "11";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

    
-- 15. write I2C_M_10B_R 
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"88";
    frame_tx_opc.tr_id    <= x"36";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"04";
    frame_tx_opc.command  <= x"E6";
    frame_tx_opc.dout_a   <= x"0010";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

-- 16. read DATA3
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"AA";
    frame_tx_opc.tr_id    <= x"37";
    frame_tx_opc.channel  <= x"03";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"71";
    frame_tx_opc.dout_a   <= x"0010";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end


  -- 18. read CRB (ping)
    wait for clock_period*2_600;

    frame_tx_opc.address  <= x"00";
    frame_tx_opc.control  <= x"CC";
    frame_tx_opc.tr_id    <= x"38";
    frame_tx_opc.channel  <= x"00";
    frame_tx_opc.length   <= x"00";
    frame_tx_opc.command  <= x"03";
    frame_tx_opc.dout_a   <= x"0000";
    frame_tx_opc.dout_b   <= x"0000";

    frame_sizeTX          <= "01";
    tx_request            <= '1';

    wait for clock_period;
    tx_request <= '0';
    -- end

  -- *******************************************
  -- ******* WORKSHOP COMMANDS END *************
  -- *******************************************

  -- FINAL RESET
-- reset
  wait for clock_period*10_000;

  frame_tx_opc.address  <= x"00";
  frame_tx_opc.control  <= x"ee";
  frame_tx_opc.tr_id    <= x"00";
  frame_tx_opc.channel  <= x"00";
  frame_tx_opc.length   <= x"00";
  frame_tx_opc.command  <= x"00";
  frame_tx_opc.dout_a   <= x"0000";
  frame_tx_opc.dout_b   <= x"0000";

  frame_sizeTX          <= "00";
  tx_request            <= '1';

  wait for clock_period;
  tx_request <= '0';
  -- end



  -- I2C I2C I2C
  ---------------------
  -- I2C_W_CTRL  
  -- I2C_R_CTRL  
  -- I2C_R_STR   
  -- I2C_W_DATA0 
  -- I2C_R_DATA0 
  -- I2C_W_DATA1 
  -- I2C_R_DATA1 
  -- I2C_S_10B_W 
  -- I2C_S_10B_R 
  -- I2C_M_10B_W 
  -- I2C_M_10B_R 


  -- *******************************************
  -- ******* TEST I2C COMMANDS BEGIN ***********
  -- *******************************************

   -- write nBytes
   -- wait for clock_period*5_000;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"88";
   -- frame_tx_opc.tr_id    <= x"04";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_CTRL;
   -- frame_tx_opc.dout_a   <= x"00" & "00000100"; -- single-byte
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "10";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
   -- -- end

   -- -- read the nBytes
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"aa";
   -- frame_tx_opc.tr_id    <= x"05";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"01";
   -- frame_tx_opc.command  <= I2C_R_CTRL;
   -- frame_tx_opc.dout_a   <= x"0000";
   -- frame_tx_opc.dout_b   <= x"0007";
  
   -- frame_sizeTX          <= "01";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';


   -- -- load up the data0 register
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"cc";
   -- frame_tx_opc.tr_id    <= x"06";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_DATA0;
   -- frame_tx_opc.dout_a   <= x"1234";
   -- frame_tx_opc.dout_b   <= x"5678";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';

   -- -- initiate a single-byte write to a valid address.
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"ee";
   -- frame_tx_opc.tr_id    <= x"07";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_S_10B_W;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00001010"; --00001010
   -- frame_tx_opc.dout_b   <= "00110011" & "00000000";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';


   ---- write nBytes
   -- wait for clock_period*5_000;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"00";
   -- frame_tx_opc.tr_id    <= x"08";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_CTRL;
   -- frame_tx_opc.dout_a   <= x"00" & "00001000"; -- 4-bytes
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "10";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
   -- -- end

   --   -- initiate a multi-byte write to a valid address. this will write 0x12345678 to dummy
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"22";
   -- frame_tx_opc.tr_id    <= x"09";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_M_10B_W;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00000000"; --0000000000
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "10";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';

   -- -- load up the data0 register with crap
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"cc";
   -- frame_tx_opc.tr_id    <= x"0a";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_DATA0;
   -- frame_tx_opc.dout_a   <= x"c0ca";
   -- frame_tx_opc.dout_b   <= x"c01a";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';

   --   -- initiate a multi-byte read to a valid address. we will receive just a good status at the OPC
   --   -- and internally the data0 register will be loaded with the 0x12345678
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"44";
   -- frame_tx_opc.tr_id    <= x"0b";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_M_10B_R;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00000000"; --0000000000
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "10";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';

   -- -- read the data0 register, we should receive the 0x12345678 value
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"ee";
   -- frame_tx_opc.tr_id    <= x"0c";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_R_DATA0;
   -- frame_tx_opc.dout_a   <= x"0000";
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "01";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';


   -- -- initiate a single-byte read to a valid address. we should receive a good status and what we wrote
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"00";
   -- frame_tx_opc.tr_id    <= x"0d";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_S_10B_R;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00001010"; --00001010
   -- frame_tx_opc.dout_b   <= "00000000" & "00000000";
  
   -- frame_sizeTX          <= "10";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- write the address for the RAM access
   -- wait for clock_period*4_000;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"22";
   -- frame_tx_opc.tr_id    <= x"0e";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_DATA0;
   -- frame_tx_opc.dout_a   <= x"0000";
   -- frame_tx_opc.dout_b   <= x"000a";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- send address for the RAM access
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"44";
   -- frame_tx_opc.tr_id    <= x"0f";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_M_10B_W;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00001011"; --0000000000
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- write the data for the RAM access
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"66";
   -- frame_tx_opc.tr_id    <= x"10";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_DATA0;
   -- frame_tx_opc.dout_a   <= x"1234";
   -- frame_tx_opc.dout_b   <= x"5678";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- send data for the RAM access
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"88";
   -- frame_tx_opc.tr_id    <= x"11";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_M_10B_W;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00001100"; --0000000000
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- write the data for the RAM access
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"aa";
   -- frame_tx_opc.tr_id    <= x"12";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_W_DATA0;
   -- frame_tx_opc.dout_a   <= x"c0ca";
   -- frame_tx_opc.dout_b   <= x"c01a";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';
    
   -- -- send data for the RAM access
   -- wait for clock_period*2_600;
  
   -- frame_tx_opc.address  <= x"00";
   -- frame_tx_opc.control  <= x"cc";
   -- frame_tx_opc.tr_id    <= x"13";
   -- frame_tx_opc.channel  <= I2C_CH_00;
   -- frame_tx_opc.length   <= x"04";
   -- frame_tx_opc.command  <= I2C_M_10B_W;
   -- frame_tx_opc.dout_a   <= "111110" & "00" & "00001100"; --0000000000
   -- frame_tx_opc.dout_b   <= x"0000";
  
   -- frame_sizeTX          <= "11";
   -- tx_request            <= '1';
  
   -- wait for clock_period;
   -- tx_request <= '0';


    -- Put test bench stimulus code here

  -- *******************************************
  -- ******* TEST I2C COMMANDS END *************
  -- *******************************************

    stop_the_clock <= false;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      user_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

  clocking0: process
  begin
    while not stop_the_clock loop
      elink_clk <= '0', '1' after elink_period / 2;
      wait for elink_period;
    end loop;
    wait;
  end process;

end;