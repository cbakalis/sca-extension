
#======================= TIMING ASSERTIONS SECTION ====================
#============================= Primary Clocks =========================

#============================= Virtual Clocks =========================
#============================= Generated Clocks =======================


#======================= TIMING EXCEPTIONS SECTION ====================

#============================== Clock Groups ==========================

#========================== Multicycle Paths ==========================
set NmultiSetup 53
set NmultiHold 52

# i2c_router to i2c_masters
set_multicycle_path 20 -setup -from [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frameRX_i_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frameRX_i_reg[*][*]}]
set_multicycle_path 19 -hold -from  [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frameRX_i_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frameRX_i_reg[*][*]}]

# frame_sizeRX is not used by the i2c_router

# i2c_masters to i2c_router
set_multicycle_path 20 -setup -from [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frame_sizeTX_reg[*]}] -to [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frame_sizeTX_i_reg[*]}]
set_multicycle_path 19 -hold -from  [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frame_sizeTX_reg[*]}] -to [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frame_sizeTX_i_reg[*]}]

# i2c_masters to i2c_router
set_multicycle_path 20 -setup -from [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frameTX_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frameTX_i_reg[*][*]}]
set_multicycle_path 19 -hold -from  [get_cells -hier -filter {NAME =~ */*/conf_instances[*].i2c_master_inst/frameTX_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */*/i2c_router_inst/frameTX_i_reg[*][*]}]


#------------------------------------------
#------------------------------------------
#I2C Master * <--> Register File * - data
#------------------------------------------
# write
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_wr_data_reg[*]}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}] 
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_wr_data_reg[*]}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}]

# read
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/regs_UFLtoSCAX_i_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/regs_UFLtoSCAX_i_reg[*][*]}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
#------------------------------------------

#------------------------------------------
#I2C Master * <--> Register File * - addr
#------------------------------------------
# write
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_addr_reg[*]}
] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}]
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_addr_reg[*]}
] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}]

# read
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_addr_reg[*]}
] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_addr_reg[*]}
] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
#------------------------------------------

#------------------------------------------
#I2C Master * <--> Register File * - wrEn/rdEn
#------------------------------------------
# write
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_wr_reg}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}]
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_wr_reg}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.regs_SCAXtoUFL_reg[*][*]}]

# read
set_multicycle_path $NmultiSetup -setup -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_rd_reg}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
set_multicycle_path $NmultiHold  -hold  -from [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].i2c_master_inst/i2c_readWrite_manager_inst/reg_rd_reg}] -to [get_cells -hier -filter {NAME =~ */i2c_wrapper_inst/conf_instances[*].registerFile_inst/*.reg_rd_data_reg[*]}]
#------------------------------------------
#------------------------------------------


#=============================== False Paths ==========================
# resets
set_false_path -reset_path -from [get_cells -hier -filter {NAME =~ */*/rst_out_reg}]
set_false_path -reset_path -from [get_cells -hier -filter {NAME =~ */*/ena_s2_reg[*]}]

#================================= Other ==============================
