#--------- General ---------

#D89 -> LOCKED
set_property PACKAGE_PIN P20            [get_ports GPIO_LED[0]]
set_property IOSTANDARD LVCMOS25        [get_ports GPIO_LED[0]]

set_property PACKAGE_PIN F4             [get_ports GPIO_LED[1]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[1]]

set_property PACKAGE_PIN T3             [get_ports GPIO_LED[2]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[2]]

#D86 -> PACKET_FORMATION_BUSY
set_property PACKAGE_PIN J16            [get_ports GPIO_LED[3]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[3]]

set_property PACKAGE_PIN Y17            [get_ports GPIO_LED[4]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[4]]

set_property PACKAGE_PIN F15            [get_ports GPIO_LED[5]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[5]]

#--------- E-Links ---------

#J2
set_property PACKAGE_PIN V13            [get_ports ELINK_CLK_P]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_CLK_P]
set_property PACKAGE_PIN V14            [get_ports ELINK_CLK_N]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_CLK_N]
set_property DIFF_TERM TRUE             [get_ports ELINK_CLK_P]

set_property PACKAGE_PIN AA9            [get_ports ELINK_RX_P]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_RX_P]
set_property PACKAGE_PIN AB10           [get_ports ELINK_RX_N]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_RX_N]
set_property DIFF_TERM TRUE             [get_ports ELINK_RX_P]

set_property PACKAGE_PIN T16            [get_ports ELINK_TX_P]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_TX_P]
set_property PACKAGE_PIN U16            [get_ports ELINK_TX_N]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ELINK_TX_N]