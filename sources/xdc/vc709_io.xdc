#--------- General ---------

# DS2.2
set_property PACKAGE_PIN AM39 [get_ports {GPIO_LED[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[0]}]
# DS3.2
set_property PACKAGE_PIN AN39 [get_ports {GPIO_LED[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[1]}]
# DS4.2
set_property PACKAGE_PIN AR37 [get_ports {GPIO_LED[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[2]}]
# DS5.2
set_property PACKAGE_PIN AT37 [get_ports {GPIO_LED[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[3]}]
# DS6.2
set_property PACKAGE_PIN AR35 [get_ports {GPIO_LED[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[4]}]
# DS7.2
set_property PACKAGE_PIN AP41 [get_ports {GPIO_LED[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[5]}]
# DS8.2
set_property PACKAGE_PIN AP42 [get_ports {GPIO_LED[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[6]}]
# DS9.2
set_property PACKAGE_PIN AU39 [get_ports {GPIO_LED[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_LED[7]}]

#GPIO P.B. SW
# SW3
set_property PACKAGE_PIN AR40 [get_ports {GPIO_SW[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW[0]}]
# SW4
set_property PACKAGE_PIN AU38 [get_ports {GPIO_SW[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW[1]}]
# SW5
set_property PACKAGE_PIN AP40 [get_ports {GPIO_SW[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW[2]}]
# SW6
set_property PACKAGE_PIN AV39 [get_ports {GPIO_SW[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW[3]}]
# SW7
set_property PACKAGE_PIN AW40 [get_ports {GPIO_SW[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW[4]}]

#GPIO DIP SW
set_property PACKAGE_PIN AV30 [get_ports {GPIO_DIP[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[0]}]
set_property PACKAGE_PIN AY33 [get_ports {GPIO_DIP[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[1]}]
set_property PACKAGE_PIN BA31 [get_ports {GPIO_DIP[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[2]}]
set_property PACKAGE_PIN BA32 [get_ports {GPIO_DIP[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[3]}]
set_property PACKAGE_PIN AW30 [get_ports {GPIO_DIP[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[4]}]
set_property PACKAGE_PIN AY30 [get_ports {GPIO_DIP[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[5]}]
set_property PACKAGE_PIN BA30 [get_ports {GPIO_DIP[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[6]}]
set_property PACKAGE_PIN BB31 [get_ports {GPIO_DIP[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_DIP[7]}]

set_property PACKAGE_PIN AV40 [get_ports CPU_RST]
set_property IOSTANDARD LVCMOS18 [get_ports CPU_RST]

# 156.250 clock from on-board oscillator
set_property PACKAGE_PIN AK34 [get_ports USER_CLOCK_P]
set_property PACKAGE_PIN AL34 [get_ports USER_CLOCK_N]

set_property IOSTANDARD LVDS [get_ports USER_CLOCK_P]
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_N]

# 120.237 clean clock from clock board
set_property PACKAGE_PIN AK8 [get_ports SMA_MGT_REFCLK_P]
set_property PACKAGE_PIN AK7 [get_ports SMA_MGT_REFCLK_N]

# SFP cage P4
set_property PACKAGE_PIN AL5 [get_ports SFP_RX_N]
set_property PACKAGE_PIN AL6 [get_ports SFP_RX_P]
set_property PACKAGE_PIN AM3 [get_ports SFP_TX_N]
set_property PACKAGE_PIN AM4 [get_ports SFP_TX_P]

set_property PACKAGE_PIN AC38 [get_ports SFP_TX_DISABLE]
set_property IOSTANDARD LVCMOS18 [get_ports SFP_TX_DISABLE]

#---- misc -----
set_false_path -from [get_cells vio_scax_inst/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[*].PROBE_OUT0_INST/Probe_out_reg[*]]