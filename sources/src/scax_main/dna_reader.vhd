----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 04.08.2019 18:08:12
-- Design Name: DNA Reader
-- Module Name: dna_reader - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: FPGA-DNA Reader
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity dna_reader is
  port(
    clk     : in  std_logic;
    clk_40  : in  std_logic;
    rst     : in  std_logic;
    dna     : out std_logic_vector(56 downto 0)
   );
end dna_reader;

architecture RTL of dna_reader is

component dna_buffer
  port(
    rst         : in  std_logic; 
    wr_clk      : in  std_logic; 
    rd_clk      : in  std_logic; 
    din         : in  std_logic_vector(56 downto 0); 
    wr_en       : in  std_logic; 
    rd_en       : in  std_logic; 
    dout        : out std_logic_vector(56 downto 0); 
    full        : out std_logic; 
    empty       : out std_logic; 
    wr_rst_busy : out std_logic; 
    rd_rst_busy : out std_logic
  ); 
end component; 

  signal dna_dout   : std_logic := '0';
  signal dna_rd     : std_logic := '0';
  signal dna_shift  : std_logic := '0';
  signal is_loaded  : std_logic := '0';
  signal dna_i      : std_logic_vector(56 downto 0) := (others => '0');

  signal fifo_wr_en : std_logic := '0';
  signal fifo_rd_en : std_logic := '0';
  signal fifo_din   : std_logic_vector(31 downto 0) := (others => '0');
  signal fifo_dout  : std_logic_vector(31 downto 0) := (others => '0');
  signal fifo_empty : std_logic := '0';
  signal fifo_full  : std_logic := '0';
  
  signal cnt_shift  : unsigned(5 downto 0) := (others => '0');

begin

proc_rd: process(clk_40)
begin
  if(rising_edge(clk_40))then
    if(rst = '0' and is_loaded = '0')then
      dna_rd    <= '1';
      is_loaded <= '1';
    elsif(rst = '0' and is_loaded = '1')then
      dna_rd    <= '0';
      is_loaded <= '1';
    else
      dna_rd    <= '0';
      is_loaded <= '0';
    end if;
  end if;
end process;

proc_shift: process(clk_40)
begin
  if(rising_edge(clk_40))then
    if(is_loaded = '1' and cnt_shift = "111001")then -- stay here and rot
      fifo_wr_en  <= '0';
      dna_shift   <= '0';
      cnt_shift   <= cnt_shift;
    elsif(is_loaded = '1' and cnt_shift = "111000")then -- stay here and rot
      fifo_wr_en  <= '1';
      dna_shift   <= '0';
      cnt_shift   <= cnt_shift + 1;
    elsif(is_loaded = '1' and dna_shift = '0' and cnt_shift /= "111000")then
      fifo_wr_en  <= '0';
      dna_shift   <= '1';
      cnt_shift   <= cnt_shift + 1;
    elsif(is_loaded = '1' and dna_shift = '1' and cnt_shift /= "111000")then
      fifo_wr_en  <= '0';
      dna_shift   <= '0';
      cnt_shift   <= cnt_shift;
    else
      fifo_wr_en  <= '0';
      dna_shift   <= '0';
      cnt_shift   <= (others => '0'); -- reset
    end if;
  end if;
end process;

proc_shiftToDout: process(clk_40)
begin
  if(rising_edge(clk_40))then
    if(is_loaded = '1' and dna_shift = '1')then
      dna_i <= dna_dout & dna_i(56 downto 1);
    elsif(is_loaded = '1' and dna_shift = '0')then
      dna_i <= dna_i;
    else
      dna_i <= (others => '0');
    end if;
  end if;
end process;

rdFifo_proc: process(clk)
begin
  if(rising_edge(clk))then
    if(is_loaded = '1' and fifo_empty = '0')then
      fifo_rd_en <= '1';
    else
      fifo_rd_en <= '0';
    end if;
  end if;
end process;
                            
FPGA_DNA_inst: DNA_PORT 
generic map (SIM_DNA_VALUE => X"123456854635211")
port map (
  DOUT => dna_dout,
  clk => clk_40,
  DIN => '0',
  READ => dna_rd,
  SHIFT => dna_shift
);

dna_buffer_inst: dna_buffer
  port map(
    rst         => rst,
    wr_clk      => clk_40,
    rd_clk      => clk,
    din         => dna_i,
    wr_en       => fifo_wr_en,
    rd_en       => fifo_rd_en,
    dout        => dna,
    full        => fifo_full,
    empty       => fifo_empty,
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 

end RTL;
