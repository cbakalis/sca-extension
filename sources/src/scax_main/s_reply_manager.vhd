----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 23.09.2018 11:38:01
-- Design Name: S-Reply manager
-- Module Name: s_reply_manager - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Builds supervisory-frame replies
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity s_reply_manager is
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    rst           : in  std_logic; 
    rst_req       : out std_logic; 
    ------------------------------
    ------ Handler Interface -----
    frame_newRX   : in  std_logic; -- busy
    frame_type    : in  std_logic_vector(6 downto 0); 
    frameRX       : in  frame_rx; -- frame contents
    ------------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; -- tx new frame 
    frame_sizeTX  : out std_logic_vector(1 downto 0)
  ); 
end s_reply_manager; 

architecture RTL of s_reply_manager is
  
  signal wait_cnt   : unsigned(1 downto 0)         := (others => '0'); 
  signal srej_reply : std_logic_vector(7 downto 0) := "000" & '1' & "1101"; 
  --     N(R) & P/F & constant

  type stateType is (ST_IDLE, ST_SEND_REPLY); 
  signal state : stateType := ST_IDLE; 
  
begin
  
-- s-reply manager FSM
sReply_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      frame_newTX <= '0'; 
      wait_cnt    <= (others => '0'); 
      state       <= ST_IDLE; 
    else
      case state is
          
        -- wait for new frame
        when ST_IDLE => 
          frame_newTX <= '0'; 
          if(frame_newRX = '1')then
            wait_cnt <= wait_cnt + 1; 
            if(wait_cnt = "11")then
              state <= ST_SEND_REPLY; 
            else
              state <= ST_IDLE; 
            end if; 
          else
            wait_cnt <= (others => '0'); 
            state    <= ST_IDLE; 
          end if; 
          
        -- send the reply, until master_handler 
        when ST_SEND_REPLY => 
          frame_newTX <= '1'; 
          if(frame_newRX = '0')then
            state <= ST_IDLE; 
          else
            state <= ST_SEND_REPLY; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- reply demux
replyDemux_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      rst_req <= '0'; 
    else
      frameTX.address <= x"FF"; 
      frameTX.length  <= x"00"; -- unused
      frameTX.dout_a  <= x"0000"; 
      frameTX.dout_b  <= x"0000"; 
      
      case frame_type is
        when "1000000" => -- sabm
          frameTX.control <= uACK_reply; 
          frame_sizeTX    <= "00"; 
          rst_req         <= '0'; 
        when "0100000" => -- rst
          frameTX.control <= uACK_reply; 
          frame_sizeTX    <= "00"; 
          rst_req         <= '1'; 
        when "0010000" => -- test
          frameTX.control <= uACK_reply; 
          frame_sizeTX    <= "00"; 
          rst_req         <= '0'; 
        when "0001000" => -- unsupported s-frame
          frameTX.control <= frameReject_reply; 
          frame_sizeTX    <= "00"; 
          rst_req         <= '0'; 
        when "0000100" =>                     -- invalid channel
          frameTX.control <= frameRX.control; -- maybe swap some bits here?
          frameTX.tr_id   <= frameRX.tr_id; 
          frameTX.channel <= frameRX.channel; 
          frameTX.err     <= error_invChReq; 
          frame_sizeTX    <= "01"; 
          rst_req         <= '0'; 
        when "0000010" =>                     -- bad length
          frameTX.control <= frameRX.control; -- maybe swap some bits here?
          frameTX.tr_id   <= frameRX.tr_id; 
          frameTX.channel <= frameRX.channel; 
          frameTX.err     <= error_invLen; 
          frame_sizeTX    <= "01"; 
          rst_req         <= '0'; 
        when "0000001" =>                     -- generic
          frameTX.control <= frameRX.control; -- maybe swap some bits here?
          frameTX.tr_id   <= frameRX.tr_id; 
          frameTX.channel <= frameRX.channel; 
          frameTX.err     <= error_generic; 
          frame_sizeTX    <= "01"; 
          rst_req         <= '0'; 
        when "0000011" => -- selective-reject
          frameTX.control <= (frameRX.control(6 downto 4) or srej_reply(7 downto 5)) & srej_reply(4 downto 0); 
          frame_sizeTX    <= "00"; 
          rst_req         <= '0'; 
        when others => 
          frameTX.control <= uACK_reply; 
          frameTX.tr_id   <= x"00"; 
          frameTX.channel <= x"00"; 
          frameTX.err     <= x"00"; 
          frame_sizeTX    <= "00"; 
          rst_req         <= '0'; 
      end case; 
    end if; 
  end if; 
end process; 

end RTL; 
