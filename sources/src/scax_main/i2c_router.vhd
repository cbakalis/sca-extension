----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.10.2018 18:08:12
-- Design Name: I2C Router
-- Module Name: i2c_router - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: I2C Router. Decides which master to forward a command to.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity i2c_router is
  port(
    ------------------------------
    ------ General Interface -----
    clk               : in  std_logic; -- user clock (SCAX)
    rst               : in  std_logic; 
    impl_i2c          : in  std_logic_vector(15 downto 0); 
    ena_i2c           : in  std_logic_vector(15 downto 0); 
    ------------------------------
    ------ Handler Interface -----
    frameRX           : in  frame_rx; 
    frame_newRX       : in  std_logic; 
    frame_sizeRX      : in  std_logic_vector(1 downto 0); 
    ------------------------------
    ------ I2C Interface ---------
    -- from i2c
    frameTX_i2c       : in  i2c_array_tx; 
    frame_newTX_i2c   : in  std_logic_vector(15 downto 0); 
    frame_sizeTX_i2c  : in  i2c_array_size; 
    -- to i2c
    frameRX_i2c       : out i2c_array_rx; 
    frame_newRX_i2c   : out std_logic_vector(15 downto 0); 
    frame_sizeRX_i2c  : out i2c_array_size; 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX           : out frame_tx; 
    frame_newTX       : out std_logic; 
    frame_sizeTX      : out std_logic_vector(1 downto 0)
  ); 
end i2c_router; 

architecture RTL of i2c_router is
  
  signal i2c_enable           : std_logic := '0'; 
  signal i2c_error            : std_logic := '0'; 
  signal wait_cnt             : unsigned(3 downto 0) := (others => '0'); 
  signal frame_sizeRX_i       : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal i2c_enable_bitmask   : std_logic_vector(15 downto 0) := (others => '0'); 
  signal i2c_enable_bitmask_i : std_logic_vector(15 downto 0) := (others => '0'); 
  signal i2c_enable_bitmask_s : std_logic_vector(15 downto 0) := (others => '0');
  signal frameRX_i            : frame_rx; 
  
  signal frameTX_i            : frame_tx; 
  signal frame_newTX_i        : std_logic := '0'; 
  signal frame_sizeTX_i       : std_logic_vector(1 downto 0) := (others => '0'); 
  
  type stateType is (ST_IDLE, ST_REG_FRM, ST_CHK_CHAN, ST_ENA_CHAN, ST_ERROR); 
  signal state : stateType := ST_IDLE; 
  
begin
  
-- I2C router FSM
i2c_router_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      i2c_enable <= '0'; 
      i2c_error  <= '0'; 
      wait_cnt   <= (others => '0'); 
      state      <= ST_IDLE; 
    else
      case state is
          
        -- wait for a new frame to come
        when ST_IDLE => 
          i2c_enable <= '0'; 
          i2c_error  <= '0'; 
          
          if(frame_newRX = '1')then
            wait_cnt <= wait_cnt + 1; 
            if(wait_cnt = "1111")then
              state <= ST_REG_FRM; 
            else
              state <= ST_IDLE; 
            end if; 
          else
            wait_cnt <= (others => '0'); 
            state    <= ST_IDLE; 
          end if; 
          
        -- reg the frame
        when ST_REG_FRM => 
          wait_cnt       <= wait_cnt + 1; 
          frameRX_i      <= frameRX; 
          frame_sizeRX_i <= frame_sizeRX; 
          
          if(wait_cnt = "1111")then
            state <= ST_CHK_CHAN; 
          else
            state <= ST_REG_FRM; 
          end if; 
          
        -- check the channel (i.e. I2C master) against the rstn and the bitmask, also calc the control field
        when ST_CHK_CHAN => 
          frameTX_i.control(7 downto 4) <= std_logic_vector(unsigned(frameRX_i.control(7 downto 4)) + 2); 
          frameTX_i.control(3 downto 0) <= frameRX_i.control(3 downto 0); 
          
          if(i2c_enable_bitmask_s(0) = '1')then
            if(ena_i2c(0) = '0' or impl_i2c(0) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(1) = '1')then
            if(ena_i2c(1) = '0' or impl_i2c(1) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(2) = '1')then
            if(ena_i2c(2) = '0' or impl_i2c(2) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(3) = '1')then
            if(ena_i2c(3) = '0' or impl_i2c(3) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(4) = '1')then
            if(ena_i2c(4) = '0' or impl_i2c(4) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(5) = '1')then
            if(ena_i2c(5) = '0' or impl_i2c(5) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(6) = '1')then
            if(ena_i2c(6) = '0' or impl_i2c(6) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(7) = '1')then
            if(ena_i2c(7) = '0' or impl_i2c(7) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(8) = '1')then
            if(ena_i2c(8) = '0' or impl_i2c(8) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(9) = '1')then
            if(ena_i2c(9) = '0' or impl_i2c(9) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(10) = '1')then
            if(ena_i2c(10) = '0' or impl_i2c(10) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(11) = '1')then
            if(ena_i2c(11) = '0' or impl_i2c(11) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(12) = '1')then
            if(ena_i2c(12) = '0' or impl_i2c(12) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(13) = '1')then
            if(ena_i2c(13) = '0' or impl_i2c(13) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(14) = '1')then
            if(ena_i2c(14) = '0' or impl_i2c(14) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          elsif(i2c_enable_bitmask_s(15) = '1')then
            if(ena_i2c(15) = '0' or impl_i2c(15) = '0')then
              state <= ST_ERROR; 
            else
              state <= ST_ENA_CHAN; 
            end if; 
          else 
            state <= ST_ERROR; 
          end if; 
          
        -- enable the channel, stay here until master_handler releases
        when ST_ENA_CHAN => 
          i2c_enable <= '1'; 
          i2c_error  <= '0'; 
          
          if(frame_newRX = '0')then
            state <= ST_IDLE; 
          else
            state <= ST_ENA_CHAN; 
          end if; 
          
        -- send a "channel not enabled" error
        when ST_ERROR => 
          i2c_enable <= '0'; 
          i2c_error  <= '1'; 
          
          if(frame_newRX = '0')then
            state <= ST_IDLE; 
          else
            state <= ST_ERROR; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- i2c demux
i2c_demux_proc : process(clk)
begin
  if(rising_edge(clk))then
    case frameRX_i.channel is
      when x"03"  => i2c_enable_bitmask <= "0000000000000001"; 
      when x"04"  => i2c_enable_bitmask <= "0000000000000010"; 
      when x"05"  => i2c_enable_bitmask <= "0000000000000100"; 
      when x"06"  => i2c_enable_bitmask <= "0000000000001000"; 
      when x"07"  => i2c_enable_bitmask <= "0000000000010000"; 
      when x"08"  => i2c_enable_bitmask <= "0000000000100000"; 
      when x"09"  => i2c_enable_bitmask <= "0000000001000000"; 
      when x"0a"  => i2c_enable_bitmask <= "0000000010000000"; 
      when x"0b"  => i2c_enable_bitmask <= "0000000100000000"; 
      when x"0c"  => i2c_enable_bitmask <= "0000001000000000"; 
      when x"0d"  => i2c_enable_bitmask <= "0000010000000000"; 
      when x"0e"  => i2c_enable_bitmask <= "0000100000000000"; 
      when x"0f"  => i2c_enable_bitmask <= "0001000000000000"; 
      when x"10"  => i2c_enable_bitmask <= "0010000000000000"; 
      when x"11"  => i2c_enable_bitmask <= "0100000000000000"; 
      when x"12"  => i2c_enable_bitmask <= "1000000000000000"; 
      when others => i2c_enable_bitmask <= "0000000000000000"; 
    end case; 
      
  i2c_enable_bitmask_i <= i2c_enable_bitmask;   -- for the mux
  i2c_enable_bitmask_s <= i2c_enable_bitmask_i; -- for the FSM
  end if; 
end process; 
  
-- i2c frame mux
i2c_mux_proc : process(clk)
begin
  if(rising_edge(clk))then
    frameTX_i.address <= x"FF"; 
    -- control has been calculated
    frameTX_i.tr_id   <= frameRX_i.tr_id; 
    frameTX_i.channel <= frameRX_i.channel; 
    -- error will be calculated
    frameTX_i.length  <= x"03"; -- fixed
    
    if(i2c_enable = '1')then
      if(i2c_enable_bitmask_i(00) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(00); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(00); 
        frameTX_i.err    <= frameTX_i2c(00).err; 
        frameTX_i.dout_a <= frameTX_i2c(00).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(00).dout_b; 
      elsif(i2c_enable_bitmask_i(01) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(01); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(01); 
        frameTX_i.err    <= frameTX_i2c(01).err; 
        frameTX_i.dout_a <= frameTX_i2c(01).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(01).dout_b; 
      elsif(i2c_enable_bitmask_i(02) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(02); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(02); 
        frameTX_i.err    <= frameTX_i2c(02).err; 
        frameTX_i.dout_a <= frameTX_i2c(02).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(02).dout_b; 
      elsif(i2c_enable_bitmask_i(03) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(03); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(03); 
        frameTX_i.err    <= frameTX_i2c(03).err; 
        frameTX_i.dout_a <= frameTX_i2c(03).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(03).dout_b; 
      elsif(i2c_enable_bitmask_i(04) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(04); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(04); 
        frameTX_i.err    <= frameTX_i2c(04).err; 
        frameTX_i.dout_a <= frameTX_i2c(04).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(04).dout_b; 
      elsif(i2c_enable_bitmask_i(05) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(05); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(05); 
        frameTX_i.err    <= frameTX_i2c(05).err; 
        frameTX_i.dout_a <= frameTX_i2c(05).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(05).dout_b; 
      elsif(i2c_enable_bitmask_i(06) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(06); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(06); 
        frameTX_i.err    <= frameTX_i2c(06).err; 
        frameTX_i.dout_a <= frameTX_i2c(06).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(06).dout_b; 
      elsif(i2c_enable_bitmask_i(07) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(07); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(07); 
        frameTX_i.err    <= frameTX_i2c(07).err; 
        frameTX_i.dout_a <= frameTX_i2c(07).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(07).dout_b; 
      elsif(i2c_enable_bitmask_i(08) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(08); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(08); 
        frameTX_i.err    <= frameTX_i2c(08).err; 
        frameTX_i.dout_a <= frameTX_i2c(08).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(08).dout_b; 
      elsif(i2c_enable_bitmask_i(09) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(09); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(09); 
        frameTX_i.err    <= frameTX_i2c(09).err; 
        frameTX_i.dout_a <= frameTX_i2c(09).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(09).dout_b; 
      elsif(i2c_enable_bitmask_i(10) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(10); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(10); 
        frameTX_i.err    <= frameTX_i2c(10).err; 
        frameTX_i.dout_a <= frameTX_i2c(10).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(10).dout_b; 
      elsif(i2c_enable_bitmask_i(11) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(11); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(11); 
        frameTX_i.err    <= frameTX_i2c(11).err; 
        frameTX_i.dout_a <= frameTX_i2c(11).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(11).dout_b; 
      elsif(i2c_enable_bitmask_i(12) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(12); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(12); 
        frameTX_i.err    <= frameTX_i2c(12).err; 
        frameTX_i.dout_a <= frameTX_i2c(12).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(12).dout_b; 
      elsif(i2c_enable_bitmask_i(13) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(13); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(13); 
        frameTX_i.err    <= frameTX_i2c(13).err; 
        frameTX_i.dout_a <= frameTX_i2c(13).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(13).dout_b; 
      elsif(i2c_enable_bitmask_i(14) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(14); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(14); 
        frameTX_i.err    <= frameTX_i2c(14).err; 
        frameTX_i.dout_a <= frameTX_i2c(14).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(14).dout_b; 
      elsif(i2c_enable_bitmask_i(15) = '1')then
        frame_newTX_i    <= frame_newTX_i2c(15); 
        frame_sizeTX_i   <= frame_sizeTX_i2c(15); 
        frameTX_i.err    <= frameTX_i2c(15).err; 
        frameTX_i.dout_a <= frameTX_i2c(15).dout_a; 
        frameTX_i.dout_b <= frameTX_i2c(15).dout_b; 
      else
        frameTX_i.err  <= error_generic; 
        frame_sizeTX_i <= "01"; 
        frame_newTX_i  <= '1'; 
      end if; 
    elsif(i2c_error = '1')then
      frameTX_i.err  <= error_chNotENa; 
      frame_sizeTX_i <= "01"; 
      frame_newTX_i  <= '1'; 
    else
      frameTX_i.err  <= (others => '0'); 
      frame_sizeTX_i <= (others => '0'); 
      frame_newTX_i  <= '0'; 
    end if; 
  end if; 
end process; 
  
-- pipeline the TX
pipeTX_proc : process(clk)
begin
  if(rising_edge(clk))then
    frameTX      <= frameTX_i; 
    frame_newTX  <= frame_newTX_i; 
    frame_sizeTX <= frame_sizeTX_i; 
  end if; 
end process; 
  
-- frame fan-out
fanOut_inst : for I in 0 to 15 generate
  frameRX_i2c(I)      <= frameRX_i; 
  frame_sizeRX_i2c(I) <= frame_sizeRX_i; 
end generate fanOut_inst; 
  
  frame_newRX_i2c <= i2c_enable_bitmask when i2c_enable = '1' else (others => '0'); 
  
end RTL; 
