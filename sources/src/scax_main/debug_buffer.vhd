----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 25.09.2018 17:25:06
-- Design Name: Debug Buffer Wrapper
-- Module Name: debug_buffer - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Buffer all data for debugging purposes
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 

entity debug_buffer is
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    rst           : in  std_logic; 
    rd_en_fifo    : in  std_logic; 
    fifo_empty    : out std_logic; 
    dout_rx       : out std_logic_vector(9 downto 0); 
    dout_tx       : out std_logic_vector(9 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_fifo_dout  : in  std_logic_vector(9 downto 0); -- rx fifo 8b10b decoded data
    state_dfrm    : in  std_logic_vector(3 downto 0); -- rx fifo rd_ena
    -- tx
    tx_fifo_wr    : in  std_logic;                     -- tx fifo wr_ena
    tx_fifo_din   : in  std_logic_vector(17 downto 0)  -- tx fifo din
  ); 
end debug_buffer; 

architecture RTL of debug_buffer is
  
component upstreamFIFO
  port (
    rst         : in  std_logic; 
    wr_clk      : in  std_logic; 
    rd_clk      : in  std_logic; 
    din         : in  std_logic_vector(9 downto 0); 
    wr_en       : in  std_logic; 
    rd_en       : in  std_logic; 
    dout        : out std_logic_vector(9 downto 0); 
    full        : out std_logic; 
    empty       : out std_logic; 
    wr_rst_busy : out std_logic; 
    rd_rst_busy : out std_logic
  ); 
end component; 

  signal wr_en_fifoRX  : std_logic := '0'; 
  signal wr_en_fifoTX  : std_logic := '0'; 
  signal rx_empty      : std_logic := '0'; 
  signal tx_empty      : std_logic := '0'; 
  signal rx_full       : std_logic := '0'; 
  signal tx_full       : std_logic := '0'; 
  signal state         : std_logic_vector(01 downto 0) := (others => '0'); 
  signal tx_buff_i     : std_logic_vector(17 downto 0) := (others => '0'); 
  signal tx_fifo_din_i : std_logic_vector(09 downto 0) := (others => '0'); 
  
begin
  
-- write RX proc
write_rx_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(state_dfrm = "0010")then
      wr_en_fifoRX <= '1'; 
    else
      wr_en_fifoRX <= '0'; 
    end if; 
  end if; 
end process; 
  
-- write TX proc
write_tx_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(state = "10")then
      wr_en_fifoTX  <= '1'; 
      tx_fifo_din_i <= tx_buff_i(17 downto 16) & tx_buff_i(7 downto 0); 
      state         <= "00"; 
    elsif(state = "01")then
      wr_en_fifoTX  <= '1'; 
      tx_fifo_din_i <= tx_buff_i(17 downto 16) & tx_buff_i(15 downto 8); 
      state         <= "10"; 
    elsif(tx_fifo_wr = '1')then
      wr_en_fifoTX <= '0'; 
      tx_buff_i    <= tx_fifo_din; 
      state        <= "01"; 
    else
      wr_en_fifoTX <= '0'; 
      tx_buff_i    <= (others => '0'); 
      state        <= "00"; 
    end if; 
  end if; 
end process; 
  
debugFIFO_rx_inst : upstreamFIFO
  port map(
    rst         => rst,
    wr_clk      => clk,
    rd_clk      => clk,
    din         => rx_fifo_dout,
    wr_en       => wr_en_fifoRX,
    rd_en       => rd_en_fifo,
    dout        => dout_rx,
    full        => rx_full,
    empty       => rx_empty,
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 

debugFIFO_tx_inst : upstreamFIFO
  port map(
    rst         => rst,
    wr_clk      => clk,
    rd_clk      => clk,
    din         => tx_fifo_din_i,
    wr_en       => wr_en_fifoTX,
    rd_en       => rd_en_fifo,
    dout        => dout_tx,
    full        => tx_full,
    empty       => tx_empty,
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 
  
  fifo_empty <= rx_empty and tx_empty; 
  
end RTL; 
