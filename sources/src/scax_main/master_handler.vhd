----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 22.09.2018 19:30:22
-- Design Name: Master Handler
-- Module Name: master_handler - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Master Handler of the SCAX. Routes frame contents to channel manager
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity master_handler is
  port(
    ------------------------------
    ------ General Interface -----
    clk               : in  std_logic; 
    rst               : in  std_logic; 
    rst_req           : out std_logic; 
    ctrl_err_pulse    : out std_logic; 
    state_o_mh        : out std_logic_vector(3 downto 0); 
    ------------------------------
    ----- Elink RX Interface -----
    deframer_busy     : in  std_logic; -- busy
    frame_new         : in  std_logic; -- new frame available
    frame_fieldsRX    : in  frame_rx;  -- frame contents
    frame_error       : in  std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_sizeRX      : in  std_logic_vector(1 downto 0); -- only 4 size types
    handler_busy      : out std_logic; -- handler not in idle
    ------------------------------
    ----- Elink TX Interface -----
    framer_busy       : in  std_logic; -- busy
    frame_fieldsTX    : out frame_tx;  -- frame contents
    tx_request        : out std_logic; -- tx new frame 
    frame_sizeTX      : out std_logic_vector(1 downto 0); -- only 4 size types
    ------------------------------
    --- I2C Masters Interface ----
    -- rx
    frameRX_i2c       : out frame_rx; 
    frame_newRX_i2c   : out std_logic; 
    frame_sizeRX_i2c  : out std_logic_vector(1 downto 0); 
    -- tx
    frameTX_i2c       : in  frame_tx; 
    frame_newTX_i2c   : in  std_logic; 
    frame_sizeTX_i2c  : in  std_logic_vector(1 downto 0); 
    ------------------------------
    ---- Controller Interface ----
    -- rx
    frameRX_ctrl      : out frame_rx; 
    frame_newRX_ctrl  : out std_logic; 
    frameRX_ctrlDummy : out std_logic; 
    frameRX_ctrlJtag  : out std_logic; 
    frame_sizeRX_ctrl : out std_logic_vector(1 downto 0); 
    -- tx
    frameTX_ctrl      : in frame_tx; 
    frame_newTX_ctrl  : in std_logic; 
    frame_sizeTX_ctrl : in std_logic_vector(1 downto 0)
  ); 
end master_handler; 

architecture RTL of master_handler is
  
component s_reply_manager
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    rst           : in  std_logic; 
    rst_req       : out std_logic; 
    ------------------------------
    ------ Handler Interface -----
    frame_newRX   : in  std_logic; -- busy
    frame_type    : in  std_logic_vector(6 downto 0); 
    frameRX       : in  frame_rx; -- frame contents
    ------------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; -- tx new frame  
    frame_sizeTX  : out std_logic_vector(1 downto 0)
  ); 
end component; 

  -- local registers
  signal frame_sizeRX_i   : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_error_i    : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_fieldsRX_i : frame_rx; 
  
  signal frame_sabm       : std_logic := '0'; 
  signal frame_rst        : std_logic := '0'; 
  signal frame_test       : std_logic := '0'; 
  signal frame_i2c        : std_logic := '0'; 
  signal frame_ctrl       : std_logic := '0'; 
  signal frame_ctrlDummy  : std_logic := '0'; 
  signal frame_ctrlJtag   : std_logic := '0'; 
  signal frame_unkn_s     : std_logic := '0'; 
  signal frame_s_reply    : std_logic := '0'; 
  signal frame_inv_chan   : std_logic := '0'; 
  signal wait_cnt         : unsigned(1 downto 0) := (others => '0'); 
  
  signal sel_s_reply      : std_logic := '0'; 
  signal sel_i2c          : std_logic := '0'; 
  signal sel_ctrl         : std_logic := '0'; 
  signal enable_tx_req    : std_logic := '0'; 
  signal rst_req_i        : std_logic := '0'; 
  signal chk_ctrl_i       : std_logic := '0'; 
  signal first_nonSfrm    : std_logic := '0'; 
  signal clr_nonSfrm      : std_logic := '0'; 
  signal control_prv      : std_logic_vector(7 downto 0) := (others => '0'); 
  signal state_o          : std_logic_vector(3 downto 0) := (others => '0'); 
  
  -- s-reply signals
  signal frame_newRX_s    : std_logic := '0'; 
  signal frame_newTX_s    : std_logic := '0'; 
  signal frame_sizeTX_s   : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_type_s     : std_logic_vector(6 downto 0) := (others => '0'); 
  signal frameRX_s        : frame_rx; 
  signal frameTX_s        : frame_tx; 
  
  type stateType is (ST_IDLE, ST_REG_FRM, ST_CHK_FRM, ST_CHK_CTRL, ST_WAIT_FRAMER, ST_DONE); 
  signal state : stateType := ST_IDLE; 
  
begin
  
-- master handler FSM
masterHandler_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      state_o                  <= "1111"; 
      handler_busy             <= '0'; 
      sel_s_reply              <= '0'; 
      sel_i2c                  <= '0'; 
      sel_ctrl                 <= '0'; 
      enable_tx_req            <= '0'; 
      rst_req                  <= '0'; 
      chk_ctrl_i               <= '0'; 
      first_nonSfrm            <= '0'; 
      clr_nonSfrm              <= '0'; 
      wait_cnt                 <= (others => '0'); 
      frame_error_i            <= (others => '0'); 
      frame_sizeRX_i           <= (others => '0'); 
      control_prv              <= (others => '0'); 
      frame_fieldsRX_i.address <= (others => '0'); 
      frame_fieldsRX_i.control <= (others => '0'); 
      frame_fieldsRX_i.tr_id   <= (others => '0'); 
      frame_fieldsRX_i.length  <= (others => '0'); 
      frame_fieldsRX_i.command <= (others => '0'); 
      frame_fieldsRX_i.din_a   <= (others => '0'); 
      frame_fieldsRX_i.din_b   <= (others => '0'); 
      state                    <= ST_IDLE; 
    else
      case state is
          
        -- wait for request
        when ST_IDLE => 
          state_o      <= "0000"; 
          handler_busy <= '0'; 
          rst_req      <= '0'; 
          sel_s_reply  <= '0'; 
          sel_i2c      <= '0'; 
          sel_ctrl     <= '0'; 
          
          if(frame_new = '1')then
            wait_cnt <= wait_cnt + 1; 
            if(wait_cnt = "11")then
              state <= ST_REG_FRM; 
            else
              state <= ST_IDLE; 
            end if; 
          else
            wait_cnt <= (others => '0'); 
            state    <= ST_IDLE; 
          end if; 
          
        -- register the frame
        when ST_REG_FRM => 
          state_o          <= "0001"; 
          frame_sizeRX_i   <= frame_sizeRX; 
          frame_error_i    <= frame_error; 
          frame_fieldsRX_i <= frame_fieldsRX; 
          wait_cnt         <= wait_cnt + 1; 
          
          if(wait_cnt = "11")then
            state <= ST_CHK_FRM; 
          else
            state <= ST_REG_FRM; 
          end if; 
          
        -- what's in it?
        when ST_CHK_FRM => 
          state_o       <= "0010"; 
          handler_busy  <= '1'; 
          enable_tx_req <= '1'; 
          state         <= ST_CHK_CTRL; 
          
          if(frame_s_reply = '1')then
            chk_ctrl_i  <= '0'; 
            sel_s_reply <= '1'; 
          elsif(frame_i2c = '1')then
            chk_ctrl_i <= '1'; 
            sel_i2c    <= '1'; 
          elsif(frame_ctrl = '1')then
            chk_ctrl_i <= '1'; 
            sel_ctrl   <= '1'; 
          else
            chk_ctrl_i  <= '0'; 
            sel_s_reply <= '1'; 
          end if; 
          
          -- inhibitor flag for ctrl checker
          if((frame_i2c = '1' or frame_ctrl = '1') and clr_nonSfrm = '0')then
            first_nonSfrm <= '1'; 
          else
            first_nonSfrm <= '0'; 
          end if; 
          
        -- check the control field
        when ST_CHK_CTRL => 
          state_o    <= "0011"; 
          chk_ctrl_i <= '0'; 
          state      <= ST_WAIT_FRAMER; 
          
          if(clr_nonSfrm = '0' and first_nonSfrm = '1')then
            clr_nonSfrm <= '1'; 
          elsif(clr_nonSfrm = '1' and first_nonSfrm = '1')then
            clr_nonSfrm <= '1'; -- keep it high, not first anymore
          elsif(clr_nonSfrm = '1' and first_nonSfrm = '0')then
            clr_nonSfrm <= '1'; -- keep it high, not first anymore
          else
            clr_nonSfrm <= '0'; 
          end if; 
          
          if(frame_s_reply = '0')then
            control_prv <= frame_fieldsRX_i.control; 
          else
            control_prv <= control_prv; 
          end if; 
          
        -- wait for framer to get the reply
        when ST_WAIT_FRAMER => 
          state_o <= "0100"; 
          
          if(framer_busy = '1')then
            state <= ST_DONE; 
          else
            state <= ST_WAIT_FRAMER; 
          end if; 
          
        -- final state, disable
        when ST_DONE => 
          state_o       <= "0101"; 
          enable_tx_req <= '0'; 
          
          if(framer_busy = '0' and rst_req_i = '0')then
            rst_req <= '0'; 
            state   <= ST_IDLE; 
          elsif(framer_busy = '0' and rst_req_i = '1')then
            rst_req <= '1'; 
            state   <= ST_IDLE; 
          else
            rst_req <= '0'; 
            state   <= ST_DONE; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- frame control-field checking process
frameCTRL_check_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(frame_error_i /= "00")then
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_sizeRX_i = "00" and frame_fieldsRX_i.control = x"2F")then -- SABM
      frame_sabm      <= '1'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_sizeRX_i = "00" and frame_fieldsRX_i.control = x"8F")then -- RESET
      frame_sabm      <= '0'; 
      frame_rst       <= '1'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_sizeRX_i = "00" and frame_fieldsRX_i.control = x"E3")then -- TEST
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '1'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_sizeRX_i = "00")then
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '1'; 
    elsif(frame_fieldsRX_i.channel = x"03" or frame_fieldsRX_i.channel = x"04" or -- I2C
        frame_fieldsRX_i.channel = x"05" or frame_fieldsRX_i.channel = x"06" or
        frame_fieldsRX_i.channel = x"07" or frame_fieldsRX_i.channel = x"08" or
        frame_fieldsRX_i.channel = x"09" or frame_fieldsRX_i.channel = x"0a" or
        frame_fieldsRX_i.channel = x"0b" or frame_fieldsRX_i.channel = x"0c" or
        frame_fieldsRX_i.channel = x"0d" or frame_fieldsRX_i.channel = x"0e" or
        frame_fieldsRX_i.channel = x"0f" or frame_fieldsRX_i.channel = x"10" or
        frame_fieldsRX_i.channel = x"11" or frame_fieldsRX_i.channel = x"12")then
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '1'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_fieldsRX_i.channel = x"00")then -- pure controller command
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '1'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_fieldsRX_i.channel = x"01" or frame_fieldsRX_i.channel = x"14" or -- disabled channels also address the controller
        frame_fieldsRX_i.channel = x"15" or frame_fieldsRX_i.channel = x"02")then
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '1'; 
      frame_ctrlDummy <= '1'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    elsif(frame_fieldsRX_i.channel = x"13")then -- dummy jtag
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '1'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '1'; 
      frame_inv_chan  <= '0'; 
      frame_unkn_s    <= '0'; 
    else
      frame_sabm      <= '0'; 
      frame_rst       <= '0'; 
      frame_test      <= '0'; 
      frame_i2c       <= '0'; 
      frame_ctrl      <= '0'; 
      frame_ctrlDummy <= '0'; 
      frame_ctrlJtag  <= '0'; 
      frame_inv_chan  <= '1'; 
      frame_unkn_s    <= '0'; 
    end if; 
  end if; 
end process; 
  
-- mux for the reply
reply_mux_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(sel_s_reply = '1')then
      frame_fieldsTX <= frameTX_s; 
      tx_request     <= frame_newTX_s and enable_tx_req; 
      frame_sizeTX   <= frame_sizeTX_s; 
    elsif(sel_ctrl = '1')then
      frame_fieldsTX    <= frameTX_ctrl; 
      tx_request        <= frame_newTX_ctrl and enable_tx_req; 
      frame_sizeTX      <= frame_sizeTX_ctrl; 
      frameRX_ctrlDummy <= frame_ctrlDummy; 
      frameRX_ctrlJtag  <= frame_ctrlJtag; 
    else
      frame_fieldsTX <= frameTX_i2c; 
      tx_request     <= frame_newTX_i2c and enable_tx_req; 
      frame_sizeTX   <= frame_sizeTX_i2c; 
    end if; 
  end if; 
end process; 

-- extra register for the channels
pipe_chan_proc : process(clk)
begin
  if(rising_edge(clk))then
    frame_newRX_s     <= sel_s_reply; 
    
    frame_type_s(6)   <= frame_sabm; 
    frame_type_s(5)   <= frame_rst; 
    frame_type_s(4)   <= frame_test; 
    frame_type_s(3)   <= frame_unkn_s; 
    frame_type_s(2)   <= frame_inv_chan; 
    frame_type_s(1)   <= frame_error_i(1); 
    frame_type_s(0)   <= frame_error_i(0); 
    
    frameRX_s         <= frame_fieldsRX_i; 
    
    frameRX_ctrl      <= frame_fieldsRX_i; 
    frame_newRX_ctrl  <= sel_ctrl; 
    frame_sizeRX_ctrl <= frame_sizeRX; 
    
    frameRX_i2c       <= frame_fieldsRX_i; 
    frame_newRX_i2c   <= sel_i2c; 
    frame_sizeRX_i2c  <= frame_sizeRX; 
  end if; 
end process; 
  
-- pipeline for the handler
handlerPipe_proc : process(clk)
begin
  if(rising_edge(clk))then
    frame_s_reply <= frame_sabm or frame_rst or frame_test or frame_inv_chan or frame_unkn_s or frame_error_i(0) or frame_error_i(1); -- frame_i2c should be removed
  end if; 
end process; 

-- control field checker
chk_ctrl_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(chk_ctrl_i = '1' and first_nonSfrm = '0')then
      if(unsigned(frame_fieldsRX_i.control(7 downto 4)) = unsigned(control_prv(7 downto 4)) + x"2")then
        ctrl_err_pulse <= '0'; 
      else
        ctrl_err_pulse <= '1'; 
      end if; 
    else
      ctrl_err_pulse <= '0'; 
    end if; 
  end if; 
end process; 
  
s_reply_manager_inst : s_reply_manager
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk,
    rst           => rst,
    rst_req       => rst_req_i,
    ------------------------------
    ------ Handler Interface -----
    frame_newRX   => frame_newRX_s,
    frame_type    => frame_type_s,
    frameRX       => frameRX_s,
    ------------------------------
    ----- Elink TX Interface -----
    frameTX       => frameTX_s,
    frame_newTX   => frame_newTX_s,
    frame_sizeTX  => frame_sizeTX_s
  ); 

  state_o_mh <= state_o; 
  
end RTL;