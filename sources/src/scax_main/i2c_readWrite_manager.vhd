----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.13.2018 11:37:06
-- Design Name: I2C Read/Write Manager
-- Module Name: i2c_readWrite_manager - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: I2C Read/Write Manager. Performs transactions with the register
-- file. Also contains the CDC manager
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

entity i2c_readWrite_manager is
  generic(is_cdc : std_logic := '0'); 
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    clk_regFile : in  std_logic; 
    ena_i2c     : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack     : in  std_logic; 
    reg_rd_data : in  std_logic_vector(31 downto 0); 
    reg_wr      : out std_logic; 
    reg_rd      : out std_logic; 
    reg_addr    : out std_logic_vector(9 downto 0); 
    reg_wr_data : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       : in  std_logic; 
    start_read  : in  std_logic; 
    mode        : in  std_logic_vector(2 downto 0);  -- how many?
    data0       : in  std_logic_vector(31 downto 0); -- for multi-byte
    data1       : in  std_logic_vector(31 downto 0); -- for multi-byte
    addrData    : in  std_logic_vector(17 downto 0); -- for single-byte
    -- to master
    mgr_data0   : out std_logic_vector(31 downto 0); 
    mgr_data1   : out std_logic_vector(31 downto 0); 
    mgr_ack     : out std_logic; 
    mgr_done_wr : out std_logic; 
    mgr_done_rd : out std_logic
  ); 
end i2c_readWrite_manager; 

architecture RTL of i2c_readWrite_manager is
  
component i2c_cdc_manager
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    clk_regFile : in  std_logic; 
    rst         : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack     : in  std_logic; 
    reg_rd_data : in  std_logic_vector(31 downto 0); 
    reg_wr      : out std_logic; 
    reg_rd      : out std_logic; 
    reg_addr    : out std_logic_vector(9 downto 0); 
    reg_wr_data : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       : in  std_logic; 
    start_read  : in  std_logic; 
    mode        : in  std_logic_vector(2 downto 0);  -- how many?
    data0       : in  std_logic_vector(31 downto 0); -- for multi-byte
    data1       : in  std_logic_vector(31 downto 0); -- for multi-byte
    addrData    : in  std_logic_vector(17 downto 0); -- for single-byte
                                                   -- to master
    mgr_data0   : out std_logic_vector(31 downto 0); 
    mgr_data1   : out std_logic_vector(31 downto 0); 
    mgr_ack     : out std_logic; 
    mgr_done_wr : out std_logic; 
    mgr_done_rd : out std_logic
  ); 
end component; 
   
  signal rst_i          : std_logic := '0'; 
  signal mgr_done       : std_logic := '0'; 
  signal sel_data       : std_logic_vector(1 downto 0) := (others => '0'); 
  signal wait_cnt       : unsigned(5 downto 0) := (others => '0'); 
  
  signal start_i        : std_logic := '0'; 
  signal start_read_i   : std_logic := '0'; 
  signal read_file      : std_logic := '0'; 
  signal sec_read       : std_logic := '0'; 
  signal mode_i         : std_logic_vector(2 downto 0)  := (others => '0'); 
  signal data0_i        : std_logic_vector(31 downto 0) := (others => '0'); 
  signal data1_i        : std_logic_vector(31 downto 0) := (others => '0'); 
  signal addrData_i     : std_logic_vector(17 downto 0) := (others => '0'); 
  
  signal clk_pipe       : std_logic := '0'; 
  signal reg_wr_i       : std_logic := '0'; 
  signal reg_rd_i       : std_logic := '0'; 
  signal reg_ack_i      : std_logic := '0'; 
  signal reg_wr_data_i  : std_logic_vector(31 downto 0) := (others => '0'); 
  signal reg_addr_i     : std_logic_vector(09 downto 0) := (others => '0'); 
  
  type stateType_s is (ST_IDLE, ST_WAIT, ST_CHK_RD, ST_SWITCH_ADDR, ST_CHK_MODE, ST_DONE); 
  signal state_s : stateType_s := ST_IDLE; 
  
begin
  
-- pipe in
pipe_in_proc : process(clk)
begin
  if(rising_edge(clk))then
    mode_i       <= mode; 
    start_i      <= start; 
    start_read_i <= start_read; 
    data0_i      <= data0; 
    data1_i      <= data1; 
    addrData_i   <= addrData; 
  end if; 
end process; 
  
  ---------------------------------------------------
  -- Simple Case ------------------------------------
  ---------------------------------------------------
  
noCDC_generate : if (is_cdc = '0') generate
  
-- simple FSM 
simpleFSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst_i = '1')then
      mgr_done    <= '0'; 
      mgr_ack     <= '0'; 
      reg_wr_i    <= '0'; 
      reg_rd_i    <= '0'; 
      read_file   <= '0'; 
      sec_read    <= '0';
      wait_cnt    <= (others => '0');  
      reg_addr_i  <= (others => '0'); 
      state_s     <= ST_IDLE; 
    else
      case state_s is
          
        -- start the transaction
        when ST_IDLE => 
          mgr_done  <= '0';
          sec_read  <= '0';
          mgr_ack   <= '0';
          wait_cnt  <= (others => '0');
          
          if(start_i = '1')then
            read_file   <= start_read_i; 
            reg_addr_i  <= addrData_i(17 downto 8); 
            state_s     <= ST_WAIT; 
          else
            read_file <= start_read_i; 
            reg_addr_i <= reg_addr_i;  
            state_s   <= ST_IDLE; 
          end if; 
          
        -- wait before writing into the register file's mux
        when ST_WAIT => 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "111111" and read_file = '0')then
            reg_wr_i <= '1'; 
            reg_rd_i <= '0'; 
            state_s  <= ST_CHK_RD; 
          elsif(wait_cnt = "111111" and read_file = '1')then
            reg_wr_i <= '0'; 
            reg_rd_i <= '1'; 
            state_s  <= ST_CHK_RD; 
          else
            reg_wr_i <= '0'; 
            reg_rd_i <= '0'; 
            state_s  <= ST_WAIT; 
          end if; 
          
        -- read again if necessary (for memory elements)
        when ST_CHK_RD => 
          reg_wr_i  <= '0'; 
          reg_rd_i  <= '0';
           
          if(reg_rd_i = '1' and sec_read = '0')then
            sec_read  <= '1';
            state_s   <= ST_WAIT;
          else 
            sec_read  <= sec_read;
            state_s   <= ST_DONE; 
          end if; 
          
        -- all good?
        when ST_DONE => 
          wait_cnt <= wait_cnt + 1; -- wait for one cycle for the ack because of pipeline at the bottom
          mgr_ack  <= reg_ack; 

          if(read_file = '1')then
            mgr_data0   <= reg_rd_data;
          end if;

          if(wait_cnt(0) = '1')then          
            mgr_done    <= '1';
            state_s     <= ST_IDLE; 
          else
            mgr_done    <= '0';
            state_s     <= ST_DONE; 
          end if;
          
        when others => state_s <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
 
  reg_wr_data_i(31 downto 00) <= data1_i;  
  mgr_done_wr <= mgr_done and (not read_file); 
  mgr_done_rd <= mgr_done and (read_file); 
  clk_pipe    <= clk; 
  
end generate noCDC_generate; 
    
---------------------------------------------------
-- Clock-Domain Crossing Case ---------------------
---------------------------------------------------
    
CDC_generate : if (is_cdc = '1') generate
  
i2c_cdc_manager_inst : i2c_cdc_manager
  port map(
    ------------------------------
    ------ General Interface -----
    clk         => clk,
    clk_regFile => clk_regFile,
    rst         => rst_i,
    ------------------------------
    -- Register File Interface ---
    reg_ack     => reg_ack,
    reg_rd_data => reg_rd_data,
    reg_wr      => reg_wr_i,
    reg_rd      => reg_rd_i,
    reg_addr    => reg_addr_i,
    reg_wr_data => reg_wr_data_i,
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       => start,
    start_read  => start_read,
    mode        => mode,
    data0       => data0,
    data1       => data1,
    addrData    => addrData,
    -- to master
    mgr_data0   => mgr_data0,
    mgr_data1   => mgr_data1,
    mgr_ack     => mgr_ack,
    mgr_done_wr => mgr_done_wr,
    mgr_done_rd => mgr_done_rd
  ); 

  clk_pipe <= clk_regFile; 
      
end generate CDC_generate; 
      
  rst_i <= not ena_i2c; 
      
-- last stage
last_pipe_proc : process(clk_pipe)
begin
  if(rising_edge(clk_pipe))then
    reg_wr      <= reg_wr_i; 
    reg_rd      <= reg_rd_i; 
    reg_wr_data <= reg_wr_data_i; 
    reg_addr    <= reg_addr_i; 
  end if; 
end process; 
      
end RTL; 
