----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 22.09.2018 18:24:37
-- Design Name: SCA Extension (SCAX)
-- Module Name: sca_extension - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Top module of SCAX.
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 
use work.registerFile_package.all; 

entity sca_extension is
  generic(
    debug_mode    : std_logic := '0'; 
    TX_dataRate   : integer   := 80;   -- RX is only 80, TX can be 80 or 320
    elinkEncoding : std_logic_vector(1 downto 0)  := "01"; -- only "01" (8b10b)
    cdc_bitmask   : std_logic_vector(15 downto 0) := x"0000"; 
    ser_input     : boolean   := false
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk_scax        : in  std_logic; -- scax main clock
    clk_regFile     : in  std_logic_vector(15 downto 0); -- all possible destination clocks
    clk_40          : in  std_logic; -- 40Mhz e-link clock
    clk_80          : in  std_logic; -- 80Mhz e-link clock
    clk_160         : in  std_logic; -- 160Mhz e-link clock
    clk_320         : in  std_logic; -- 320Mhz e-link clock
    rst             : in  std_logic; -- reset the modules
    -- dbg
    ena_flx_test    : in  std_logic; 
    dbg_fifo_rd     : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL; -- used when writing
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        : in  std_logic;-- 1-bit elink rx
    rx_elink2bit    : in  std_logic_vector(1 downto 0); -- 2-bit elink rx
    rx_swap         : in  std_logic; -- swap input bits
    -- tx
    tx_swap         : in  std_logic; -- swap output bits
    tx_elink        : out std_logic; -- 1-bit elink tx
    tx_elink8bit    : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit    : out std_logic_vector(1 downto 0) -- 2-bit elink tx
  ); 
end sca_extension; 

architecture RTL of sca_extension is
  
component interface_wrapper
  generic(
    debug_mode    : std_logic                    := '1'; 
    TX_dataRate   : integer                      := 80;   -- RX is only 80, TX can be 80 or 320
    elinkEncoding : std_logic_vector(1 downto 0) := "01"; -- only "01" (8b10b)
    ser_input     : boolean                      := false -- 2-bit wide bus
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock
    clk_40          : in  std_logic; -- 40Mhz e-link clock
    clk_80          : in  std_logic; -- 80Mhz e-link clock
    clk_160         : in  std_logic; -- 160Mhz e-link clock
    clk_320         : in  std_logic; -- 320Mhz e-link clock
    rst             : in  std_logic; -- reset the modules
    ena_flx_test    : in  std_logic; 
    reverse_rx      : in  std_logic; 
    reverse_tx      : in  std_logic; 
    dbg_fifo_rd     : in  std_logic; 
    dbg_fifo_empty  : out std_logic; 
    dbg_fifo_rx     : out std_logic_vector(9 downto 0); 
    dbg_fifo_tx     : out std_logic_vector(9 downto 0); 
    state_o_frm     : out std_logic_vector(3 downto 0); 
    state_o_drv     : out std_logic_vector(3 downto 0); 
    state_o_dfrm    : out std_logic_vector(3 downto 0); 
    drdy_raw        : out std_logic; 
    din_raw         : out std_logic_vector(9 downto 0); 
    wrElink_raw     : out std_logic; 
    dout_raw        : out std_logic_vector(17 downto 0); 
    ------------------------------
    -- Master Handler Interface --
    -- rx
    handler_busy    : in  std_logic; -- handler not in idle
    deframer_busy   : out std_logic; -- busy
    frame_new       : out std_logic; -- new frame available
    frame_fieldsRX  : out frame_rx;  -- frame contents
    frame_error     : out std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_sizeRX    : out std_logic_vector(1 downto 0); -- only 4 size types
    -- tx
    frame_fieldsTX  : in  frame_tx;  -- frame contents
    tx_request      : in  std_logic; -- tx new frame 
    frame_sizeTX    : in  std_logic_vector(1 downto 0); -- only 4 size types
    framer_busy     : out std_logic; -- busy
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        : in  std_logic; -- 1-bit elink rx
    rx_elink2bit    : in  std_logic_vector(1 downto 0);    -- 2-bit elink rx
    rx_swap         : in  std_logic; -- swap input bits
    -- tx
    tx_swap         : in  std_logic; -- swap output bits
    tx_elink        : out std_logic; -- 1-bit elink tx
    tx_elink8bit    : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit    : out std_logic_vector(1 downto 0)    -- 2-bit elink tx
  ); 
end component; 
  
component master_handler
  port(
  ------------------------------
  ------ General Interface -----
  clk               : in  std_logic; 
  rst               : in  std_logic; 
  rst_req           : out std_logic; 
  ctrl_err_pulse    : out std_logic; 
  state_o_mh        : out std_logic_vector(3 downto 0); 
  ------------------------------
  ----- Elink RX Interface -----
  deframer_busy     : in  std_logic; -- busy
  frame_new         : in  std_logic; -- new frame available
  frame_fieldsRX    : in  frame_rx;  -- frame contents
  frame_error       : in  std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
  frame_sizeRX      : in  std_logic_vector(1 downto 0); -- only 4 size types
  handler_busy      : out std_logic; -- handler not in idle
  ------------------------------
  ----- Elink TX Interface -----
  framer_busy       : in  std_logic; -- busy
  frame_fieldsTX    : out frame_tx;  -- frame contents
  tx_request        : out std_logic; -- tx new frame 
  frame_sizeTX      : out std_logic_vector(1 downto 0); -- only 4 size types
  ------------------------------
  --- I2C Masters Interface ----
  -- rx
  frameRX_i2c       : out frame_rx; 
  frame_newRX_i2c   : out std_logic; 
  frame_sizeRX_i2c  : out std_logic_vector(1 downto 0); 
  -- tx
  frameTX_i2c       : in  frame_tx; 
  frame_newTX_i2c   : in  std_logic; 
  frame_sizeTX_i2c  : in  std_logic_vector(1 downto 0); 
  ------------------------------
  ---- Controller Interface ----
  -- rx
  frameRX_ctrl      : out frame_rx; 
  frame_newRX_ctrl  : out std_logic; 
  frameRX_ctrlDummy : out std_logic; 
  frameRX_ctrlJtag  : out std_logic; 
  frame_sizeRX_ctrl : out std_logic_vector(1 downto 0); 
  -- tx
  frameTX_ctrl      : in frame_tx; 
  frame_newTX_ctrl  : in std_logic; 
  frame_sizeTX_ctrl : in std_logic_vector(1 downto 0)
    ); 
end component; 
  
component controller
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    clk_40        : in  std_logic; -- to get fpga dna 
    rst           : in  std_logic; 
    ena_i2c       : out std_logic_vector(15 downto 0); 
    state_o_ctrl  : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ Handler Interface -----
    frameRX       : in  frame_rx; 
    frame_newRX   : in  std_logic; 
    frameRX_dummy : in  std_logic; 
    frameRX_jtag  : in  std_logic; 
    frame_sizeRX  : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; 
    frame_sizeTX  : out std_logic_vector(1 downto 0)
    ); 
end component; 
  
component i2c_wrapper
  generic(cdc_bitmask : std_logic_vector(15 downto 0) := x"0000"); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock (SCAX)
    rst             : in  std_logic; 
    clk_regFile     : in  std_logic_vector(15 downto 0); -- all possible destination clocks
    ena_i2c         : in  std_logic_vector(15 downto 0); 
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL; -- used when writing
    ------------------------------
    ------ Handler Interface -----
    frameRX         : in  frame_rx; 
    frame_newRX     : in  std_logic; 
    frame_sizeRX    : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX         : out frame_tx; 
    frame_newTX     : out std_logic; 
    frame_sizeTX    : out std_logic_vector(1 downto 0)
  ); 
end component; 
  
component scax_watchdog
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    rst_req_int : in  std_logic; 
    rst_req_ext : in  std_logic; 
    rst_out     : out std_logic
  ); 
end component; 

component ila_overview
  port(
    clk    : in std_logic; 
    probe0 : in std_logic_vector(255 downto 0)
  ); 
end component; 
  
  signal clk_scax_i          : std_logic := '0'; 

  signal rst_f              : std_logic := '0'; 
  signal rst_req            : std_logic := '0'; 
  signal handler_busy       : std_logic := '0'; 
  signal deframer_busy      : std_logic := '0'; 
  signal framer_busy        : std_logic := '0'; 
  signal frame_new          : std_logic := '0'; 
  signal tx_request         : std_logic := '0'; 
  signal frame_error        : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal frame_sizeRX       : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal frame_sizeTX       : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal ena_i2c            : std_logic_vector(15 downto 0) := (others => '0'); 
  
  signal frame_fieldsRX     : frame_rx; 
  signal frameRX_i2c        : frame_rx; 
  signal frameRX_ctrl       : frame_rx; 
  signal frame_fieldsTX     : frame_tx; 
  signal frameTX_i2c        : frame_tx; 
  signal frameTX_ctrl       : frame_tx; 
  
  signal frame_newTX_i2c    : std_logic := '0'; 
  signal frame_newTX_ctrl   : std_logic := '0'; 
  signal frameRX_ctrlDummy  : std_logic := '0'; 
  signal frameRX_ctrlJtag   : std_logic := '0'; 
  signal frame_newRX_i2c    : std_logic := '0'; 
  signal frame_newRX_ctrl   : std_logic := '0'; 
  signal frame_sizeRX_i2c   : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_sizeTX_i2c   : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_sizeRX_ctrl  : std_logic_vector(1 downto 0) := (others => '0'); 
  signal frame_sizeTX_ctrl  : std_logic_vector(1 downto 0) := (others => '0'); 
  
  -- debug
  signal dbg_fifo_empty     : std_logic := '0'; 
  signal drdy_raw           : std_logic := '0'; 
  signal wrElink_raw        : std_logic := '0'; 
  signal rx_elink_i         : std_logic := '0'; 
  signal tx_elink_i         : std_logic := '0'; 
  signal ctrl_err_i         : std_logic := '0'; 
  signal ctrl_err_cnt       : unsigned(7 downto 0)          := (others => '0'); 
  signal tx_elink2bit_i     : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal state_o_frm        : std_logic_vector(3 downto 0)  := (others => '0'); 
  signal state_o_drv        : std_logic_vector(3 downto 0)  := (others => '0'); 
  signal state_o_dfrm       : std_logic_vector(3 downto 0)  := (others => '0'); 
  signal state_o_mh         : std_logic_vector(3 downto 0)  := (others => '0'); 
  signal state_o_ctrl       : std_logic_vector(3 downto 0)  := (others => '0'); 
  signal din_raw            : std_logic_vector(9 downto 0)  := (others => '0'); 
  signal dout_raw           : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dbg_fifo_rx        : std_logic_vector(9 downto 0)  := (others => '0'); 
  signal dbg_fifo_tx        : std_logic_vector(9 downto 0)  := (others => '0'); 
  
  --    attribute mark_debug                        : string;
  --    attribute mark_debug of handler_busy        : signal is "TRUE";
  --    attribute mark_debug of deframer_busy       : signal is "TRUE";
  --    attribute mark_debug of frame_new           : signal is "TRUE";
  --    attribute mark_debug of frame_sizeRX        : signal is "TRUE";
  --    attribute mark_debug of frame_sizeTX        : signal is "TRUE";
  --    attribute mark_debug of frame_error         : signal is "TRUE";
  --    attribute mark_debug of tx_request          : signal is "TRUE";
  --    attribute mark_debug of framer_busy         : signal is "TRUE";
  --    attribute mark_debug of rx_elink            : signal is "TRUE";
  
  --    attribute mark_debug of frame_fieldsRX      : signal is "TRUE";
  --    attribute mark_debug of frame_fieldsTX      : signal is "TRUE";
  
  --    attribute mark_debug of state_o_frm         : signal is "TRUE";
  --    attribute mark_debug of state_o_drv         : signal is "TRUE";
  --    attribute mark_debug of state_o_dfrm        : signal is "TRUE";
  --    attribute mark_debug of state_o_mh          : signal is "TRUE";
  --    attribute mark_debug of state_o_ctrl        : signal is "TRUE";
  --    attribute mark_debug of din_raw             : signal is "TRUE";
  --    attribute mark_debug of drdy_raw            : signal is "TRUE";
  --    attribute mark_debug of wrElink_raw         : signal is "TRUE";
  --    attribute mark_debug of dout_raw            : signal is "TRUE";
  --    attribute mark_debug of ctrl_err_i          : signal is "TRUE";
  --    attribute mark_debug of ctrl_err_cnt        : signal is "TRUE";
  
  --    attribute mark_debug of dbg_fifo_rd         : signal is "TRUE";
  --    attribute mark_debug of dbg_fifo_empty      : signal is "TRUE";
  --    attribute mark_debug of dbg_fifo_rx         : signal is "TRUE";
  --    attribute mark_debug of dbg_fifo_tx         : signal is "TRUE";
  --    attribute mark_debug of tx_elink_i          : signal is "TRUE";
  --    attribute mark_debug of tx_elink2bit_i      : signal is "TRUE";
  --    attribute mark_debug of rx_elink2bit        : signal is "TRUE";
  
begin
  
-- select the clock
selClk_dbg : process(clk_80, clk_scax)
begin
  case debug_mode is
    when '0' => clk_scax_i <= clk_scax; 
    when '1' => clk_scax_i <= clk_80; 
    when others => clk_scax_i <= clk_scax; 
  end case; 
end process; 
  
interface_wrapper_inst : interface_wrapper
  generic map(
    debug_mode      => debug_mode,
    TX_dataRate     => TX_dataRate,
    elinkEncoding   => elinkEncoding,
    ser_input       => ser_input)
  port map(
    ------------------------------
    ------ General Interface -----
    clk             => clk_scax_i,
    clk_40          => clk_40,
    clk_80          => clk_80,
    clk_160         => clk_160,
    clk_320         => clk_320,
    rst             => rst_f,
    ena_flx_test    => ena_flx_test,
    reverse_rx      => '1',
    reverse_tx      => '1',
    dbg_fifo_rd     => dbg_fifo_rd,
    dbg_fifo_empty  => dbg_fifo_empty,
    dbg_fifo_rx     => dbg_fifo_rx,
    dbg_fifo_tx     => dbg_fifo_tx,
    state_o_frm     => state_o_frm,
    state_o_drv     => state_o_drv,
    state_o_dfrm    => state_o_dfrm,
    drdy_raw        => drdy_raw,
    din_raw         => din_raw,
    wrElink_raw     => wrElink_raw,
    dout_raw        => dout_raw,
    -------         --------------
    -- Master Handler Interface --
    -- rx
    handler_busy    => handler_busy,
    deframer_busy   => deframer_busy,
    frame_new       => frame_new,
    frame_fieldsRX  => frame_fieldsRX,
    frame_error     => frame_error,
    frame_sizeRX    => frame_sizeRX,
    -- tx
    frame_fieldsTX  => frame_fieldsTX,
    tx_request      => tx_request,
    frame_sizeTX    => frame_sizeTX,
    framer_busy     => framer_busy,
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        => rx_elink,
    rx_elink2bit    => rx_elink2bit,
    rx_swap         => rx_swap,
    -- tx
    tx_swap         => tx_swap,
    tx_elink        => tx_elink_i,
    tx_elink8bit    => tx_elink8bit,
    tx_elink2bit    => tx_elink2bit_i
  ); 

master_handler_inst : master_handler
  port map(
    ------------------------------
    ------ General Interface -----
    clk               => clk_scax_i,
    rst               => rst_f,
    rst_req           => rst_req,
    ctrl_err_pulse    => ctrl_err_i,
    state_o_mh        => state_o_mh,
    ------------------------------
    ----- Elink RX Interface -----
    deframer_busy     => deframer_busy,
    frame_new         => frame_new,
    frame_fieldsRX    => frame_fieldsRX,
    frame_error       => frame_error,
    frame_sizeRX      => frame_sizeRX,
    handler_busy      => handler_busy,
    ------------------------------
    ----- Elink TX Interface -----
    framer_busy       => framer_busy,
    frame_fieldsTX    => frame_fieldsTX,
    tx_request        => tx_request,
    frame_sizeTX      => frame_sizeTX,
    ------------------------------
    --- I2C Masters Interface ----
    -- rx
    frameRX_i2c       => frameRX_i2c,
    frame_newRX_i2c   => frame_newRX_i2c,
    frame_sizeRX_i2c  => frame_sizeRX_i2c,
    -- tx
    frameTX_i2c       => frameTX_i2c,
    frame_newTX_i2c   => frame_newTX_i2c,
    frame_sizeTX_i2c  => frame_sizeTX_i2c,
    ------------------------------
    ---- Controller Interface ----
    -- rx
    frameRX_ctrl      => frameRX_ctrl,
    frame_newRX_ctrl  => frame_newRX_ctrl,
    frameRX_ctrlDummy => frameRX_ctrlDummy,
    frameRX_ctrlJtag  => frameRX_ctrlJtag,
    frame_sizeRX_ctrl => frame_sizeRX_ctrl,
    -- tx
    frameTX_ctrl      => frameTX_ctrl,
    frame_newTX_ctrl  => frame_newTX_ctrl,
    frame_sizeTX_ctrl => frame_sizeTX_ctrl
  ); 
  
controller_inst : controller
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk_scax_i,
    clk_40        => clk_40,
    rst           => rst_f,
    ena_i2c       => ena_i2c,
    state_o_ctrl  => state_o_ctrl,
    ------------------------------
    ------ Handler Interface -----
    frameRX       => frameRX_ctrl,
    frame_newRX   => frame_newRX_ctrl,
    frameRX_dummy => frameRX_ctrlDummy,
    frameRX_jtag  => frameRX_ctrlJtag,
    frame_sizeRX  => frame_sizeRX_ctrl,
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       => frameTX_ctrl,
    frame_newTX   => frame_newTX_ctrl,
    frame_sizeTX  => frame_sizeTX_ctrl
  ); 
  
i2c_wrapper_inst : i2c_wrapper
  generic map(cdc_bitmask => cdc_bitmask)
  port map(
    ------------------------------
    ------ General Interface -----
    clk             => clk_scax_i,
    rst             => rst_f,
    clk_regFile     => clk_regFile,
    ena_i2c         => ena_i2c,
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  => regs_UFLtoSCAX,
    regs_SCAXtoUFL  => regs_SCAXtoUFL,
    ------------------------------
    ------ Handler Interface -----
    frameRX         => frameRX_i2c,
    frame_newRX     => frame_newRX_i2c,
    frame_sizeRX    => frame_sizeRX_i2c,
    -----------------------------
    ----- Elink TX Interface -----
    frameTX         => frameTX_i2c,
    frame_newTX     => frame_newTX_i2c,
    frame_sizeTX    => frame_sizeTX_i2c
  ); 

scax_watchdog_inst : scax_watchdog
  port map(
    ------------------------------
    ------ General Interface -----
    clk         => clk_scax_i,
    rst_req_int => rst_req,
    rst_req_ext => rst, 
    rst_out     => rst_f
  ); 
  
ila_generate : if (debug_mode = '1') generate
  ila_sca_inst : ila_overview
    port map(
      clk                   => clk_80,
      probe0(0)             => handler_busy,
      probe0(1)             => deframer_busy,
      probe0(2)             => frame_new,
      probe0(4 downto 3)    => frame_sizeRX,
      probe0(6 downto 5)    => frame_sizeTX,
      probe0(8 downto 7)    => frame_error,
      probe0(9)             => tx_request,
      probe0(10)            => framer_busy,
      probe0(11)            => rx_elink,
      
      probe0(19 downto 12)  => frame_fieldsRX.address,
      probe0(27 downto 20)  => frame_fieldsRX.control,
      probe0(35 downto 28)  => frame_fieldsRX.tr_id,
      probe0(43 downto 36)  => frame_fieldsRX.channel,
      probe0(51 downto 44)  => frame_fieldsRX.length,
      probe0(59 downto 52)  => frame_fieldsRX.command,
      probe0(75 downto 60)  => frame_fieldsRX.din_a,
      probe0(91 downto 76)  => frame_fieldsRX.din_b,
      
      probe0(99 downto 92)   => frame_fieldsTX.address,
      probe0(107 downto 100) => frame_fieldsTX.control,
      probe0(115 downto 108) => frame_fieldsTX.tr_id,
      probe0(123 downto 116) => frame_fieldsTX.channel,
      probe0(131 downto 124) => frame_fieldsTX.err,
      probe0(139 downto 132) => frame_fieldsTX.length,
      probe0(155 downto 140) => frame_fieldsTX.dout_a,
      probe0(171 downto 156) => frame_fieldsTX.dout_b,
      
      probe0(175 downto 172) => state_o_frm,
      probe0(179 downto 176) => state_o_drv,
      probe0(183 downto 180) => state_o_dfrm,
      probe0(187 downto 184) => state_o_mh,
      probe0(191 downto 188) => state_o_ctrl,
      probe0(201 downto 192) => din_raw,
      probe0(202)            => drdy_raw,
      probe0(203)            => dbg_fifo_empty,
      probe0(213 downto 204) => dbg_fifo_tx,
      probe0(223 downto 214) => dbg_fifo_rx,
      probe0(224)            => wrElink_raw,
      probe0(242 downto 225) => dout_raw,
      probe0(244 downto 243) => rx_elink2bit,
      probe0(246 downto 245) => tx_elink2bit_i,
      probe0(247)            => ctrl_err_i,
      probe0(255 downto 248) => std_logic_vector(ctrl_err_cnt)
    ); 

  -- count the errors
  cnt_err_proc : process(clk_80)
  begin
    if(rising_edge(clk_80))then
      if(rst = '1')then
        ctrl_err_cnt <= (others => '0'); 
      else
        if(ctrl_err_i = '1')then
          ctrl_err_cnt <= ctrl_err_cnt + 1; 
        else
          ctrl_err_cnt <= ctrl_err_cnt; 
        end if; 
      end if; 
    end if; 
  end process; 
    
end generate ila_generate; 
    
  tx_elink2bit <= tx_elink2bit_i; 
    
  -- pipeline me
proc : process(clk_80)
begin
  if(rising_edge(clk_80))then
    tx_elink <= tx_elink_i; 
  end if; 
end process; 

end RTL; 
