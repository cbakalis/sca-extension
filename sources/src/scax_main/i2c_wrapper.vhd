----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.10.2018 18:08:12
-- Design Name: I2C Wrapper
-- Module Name: i2c_wrapper - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Wrapper for the I2C modules and the register files.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 
use work.registerFile_package.all; 

entity i2c_wrapper is
  generic(cdc_bitmask : std_logic_vector(15 downto 0) := x"0000"); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock (SCAX)
    rst             : in  std_logic; 
    clk_regFile     : in  std_logic_vector(15 downto 0); -- all possible destination clocks
    ena_i2c         : in  std_logic_vector(15 downto 0); 
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL; -- used when writing
    ------------------------------
    ------ Handler Interface -----
    frameRX         : in  frame_rx; 
    frame_newRX     : in  std_logic; 
    frame_sizeRX    : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX         : out frame_tx; 
    frame_newTX     : out std_logic; 
    frame_sizeTX    : out std_logic_vector(1 downto 0)
  ); 
end i2c_wrapper; 

architecture RTL of i2c_wrapper is
  
component i2c_router
  port(
    ------------------------------
    ------ General Interface -----
    clk               : in  std_logic; -- user clock (SCAX)
    rst               : in  std_logic; 
    impl_i2c          : in  std_logic_vector(15 downto 0); 
    ena_i2c           : in  std_logic_vector(15 downto 0); 
    ------------------------------
    ------ Handler Interface -----
    frameRX           : in  frame_rx; 
    frame_newRX       : in  std_logic; 
    frame_sizeRX      : in  std_logic_vector(1 downto 0); 
    ------------------------------
    ------ I2C Interface ---------
    -- from i2c
    frameTX_i2c       : in  i2c_array_tx; 
    frame_newTX_i2c   : in  std_logic_vector(15 downto 0); 
    frame_sizeTX_i2c  : in  i2c_array_size; 
    -- to i2c
    frameRX_i2c       : out i2c_array_rx; 
    frame_newRX_i2c   : out std_logic_vector(15 downto 0); 
    frame_sizeRX_i2c  : out i2c_array_size; 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX           : out frame_tx; 
    frame_newTX       : out std_logic; 
    frame_sizeTX      : out std_logic_vector(1 downto 0)
  ); 
end component; 

component i2c_master
  generic(
      i2c_master_id : integer := 0;
      is_cdc        : std_logic := '0'); 
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    clk_regFile   : in  std_logic; 
    ena_i2c       : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack       : in  std_logic; 
    reg_rd_data   : in  std_logic_vector(31 downto 0); 
    reg_wr        : out std_logic; 
    reg_rd        : out std_logic; 
    reg_addr      : out std_logic_vector(9 downto 0); 
    reg_wr_data   : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Router Interface -----
    frameRX       : in  frame_rx; 
    frame_newRX   : in  std_logic; 
    frame_sizeRX  : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; 
    frame_sizeTX  : out std_logic_vector(1 downto 0)
  ); 
end component; 
  
component registerFile
  generic(i2c_master_id : integer := 0); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock (SCAX) or registerFile clock
    ------------------------------
    -- I2C Master Interface ------
    reg_wr          : in  std_logic; 
    reg_rd          : in  std_logic; 
    reg_addr        : in  std_logic_vector(9 downto 0); 
    reg_wr_data     : in  std_logic_vector(31 downto 0); 
    reg_ack         : out std_logic; 
    reg_rd_data     : out std_logic_vector(31 downto 0); 
    ------------------------------
    -- FPGA Register Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL  -- used when writing
  ); 
end component;  

component pipeline
  generic(
    NUMBER_OF_BITS    : natural := 8;   -- number of signals to be pipelined
    NUMBER_OF_STAGES  : natural := 4);  -- number of pipeline stages
  port(
    clk               : in  std_logic; -- logic clock                                    
    data_in           : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);
    data_out_p        : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0) 
  ); 
end component; 
  
  signal ena_s0             : std_logic_vector(15 downto 0) := (others => '0'); 
  signal ena_s1             : std_logic_vector(15 downto 0) := (others => '0'); 
  signal ena_s2             : std_logic_vector(15 downto 0) := (others => '0'); 
  constant impl_i2c         : std_logic_vector(15 downto 0) := x"FFFF";
  signal clk_dst            : std_logic_vector(15 downto 0) := (others => '0');
  
  signal frameTX_i2c_i      : i2c_array_tx; 
  signal frame_newTX_i2c_i  : std_logic_vector(15 downto 0) := (others => '0'); 
  signal frame_newTX_i2c_p  : std_logic_vector(15 downto 0) := (others => '0'); 
  signal frame_sizeTX_i2c_i : i2c_array_size; 
  
  signal frameRX_i2c_i      : i2c_array_rx; 
  signal frame_newRX_i2c_i  : std_logic_vector(15 downto 0) := (others => '0'); 
  signal frame_newRX_i2c_p  : std_logic_vector(15 downto 0) := (others => '0'); 
  signal frame_sizeRX_i2c_i : i2c_array_size; 
  
  signal reg_rd_data        : data_array; 
  signal reg_addr           : regAddr_array; 
  signal reg_wr_data        : data_array; 
  signal reg_ack            : std_logic_vector(15 downto 0) := (others => '0'); 
  signal reg_wr             : std_logic_vector(15 downto 0) := (others => '0'); 
  signal reg_rd             : std_logic_vector(15 downto 0) := (others => '0'); 
  
begin
  
-- reg the ena
ena_proc : process(clk)
begin
  if(rising_edge(clk))then
    ena_s0 <= ena_i2c; 
    ena_s1 <= ena_s0; 
    ena_s2 <= ena_s1; 
  end if; 
end process; 
  
i2c_router_inst : i2c_router
  port map(
    ------------------------------
    ------ General Interface -----
    clk               => clk,
    rst               => rst,
    impl_i2c          => impl_i2c,
    ena_i2c           => ena_s0,
    ------------------------------
    ------ Handler Interface -----
    frameRX           => frameRX,
    frame_newRX       => frame_newRX,
    frame_sizeRX      => frame_sizeRX,
    ------------------------------
    ------ I2C Interface ---------
    -- from i2c
    frameTX_i2c       => frameTX_i2c_i,
    frame_newTX_i2c   => frame_newTX_i2c_p,
    frame_sizeTX_i2c  => frame_sizeTX_i2c_i,
    -- to i2c
    frameRX_i2c       => frameRX_i2c_i,
    frame_newRX_i2c   => frame_newRX_i2c_i,
    frame_sizeRX_i2c  => frame_sizeRX_i2c_i,
    -----------------------------
    ----- Elink TX Interface -----
    frameTX           => frameTX,
    frame_newTX       => frame_newTX,
    frame_sizeTX      => frame_sizeTX
  ); 


conf_instances: for I in 0 to 15 generate 

i2c_master_inst : i2c_master
  generic map(i2c_master_id => I, is_cdc => cdc_bitmask(I))
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk,
    clk_regFile   => clk_dst(I),
    ena_i2c       => ena_s2(I),
    ------------------------------
    -- Register File Interface ---
    reg_ack       => reg_ack(I),
    reg_rd_data   => reg_rd_data(I),
    reg_wr        => reg_wr(I),
    reg_rd        => reg_rd(I),
    reg_addr      => reg_addr(I),
    reg_wr_data   => reg_wr_data(I),
    ------------------------------
    --- I2C Router Interface -----
    frameRX       => frameRX_i2c_i(I),
    frame_newRX   => frame_newRX_i2c_p(I),
    frame_sizeRX  => frame_sizeRX_i2c_i(I),
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       => frameTX_i2c_i(I),
    frame_newTX   => frame_newTX_i2c_i(I),
    frame_sizeTX  => frame_sizeTX_i2c_i(I)
  ); 
  
registerFile_inst : registerFile
  generic map(i2c_master_id => I)
  port map(
    ------------------------------
    ------ General Interface -----
    clk             => clk_dst(I),
    ------------------------------
    -- I2C Master Interface ------
    reg_wr          => reg_wr(I),
    reg_rd          => reg_rd(I),
    reg_addr        => reg_addr(I),
    reg_wr_data     => reg_wr_data(I),
    reg_ack         => reg_ack(I),
    reg_rd_data     => reg_rd_data(I),
    ------------------------------
    -- FPGA Register Interface ---
    regs_UFLtoSCAX  => regs_UFLtoSCAX,
    regs_SCAXtoUFL  => regs_SCAXtoUFL 
  ); 
  
noCDC_generate : if (cdc_bitmask(I) = '0') generate
  clk_dst(I) <= clk; 
end generate noCDC_generate; 
    
CDC_generate : if (cdc_bitmask(I) = '1') generate
  clk_dst(I) <= clk_regFile(I); 
end generate CDC_generate;

end generate conf_instances;

pipeline_masterToRouter: pipeline
  generic map(
    NUMBER_OF_BITS    => 16,
    NUMBER_OF_STAGES  => 8)
  port map(
    clk               => clk,
    data_in           => frame_newTX_i2c_i,
    data_out_p        => frame_newTX_i2c_p
  ); 

pipeline_routerToMaster: pipeline
  generic map(
    NUMBER_OF_BITS    => 16,
    NUMBER_OF_STAGES  => 8)
  port map(
    clk               => clk,
    data_in           => frame_newRX_i2c_i,
    data_out_p        => frame_newRX_i2c_p
  ); 
  
end RTL; 
