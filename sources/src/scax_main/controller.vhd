----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 23.09.2018 19:09:58
-- Design Name: SCAX Controller
-- Module Name: controller - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: SCAX Controller. Designed based on the SCA v8.2 documentation.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity controller is
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic;
    clk_40        : in  std_logic; -- to get fpga dna 
    rst           : in  std_logic; 
    ena_i2c       : out std_logic_vector(15 downto 0); 
    state_o_ctrl  : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ Handler Interface -----
    frameRX       : in  frame_rx; 
    frame_newRX   : in  std_logic; 
    frameRX_dummy : in  std_logic; 
    frameRX_jtag  : in  std_logic; 
    frame_sizeRX  : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; 
    frame_sizeTX  : out std_logic_vector(1 downto 0)
  ); 
end controller; 

architecture RTL of controller is

component dna_reader
  port(
    clk     : in  std_logic;
    clk_40  : in  std_logic;
    rst     : in  std_logic;
    dna     : out std_logic_vector(56 downto 0)
  );
end component;
  
  signal crb_i          : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal crc_i          : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal crd_i          : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal insel_i        : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal jtag_freq      : std_logic_vector(15 downto 0) := (others => '0'); 
  signal jtag_ctrl      : std_logic_vector(15 downto 0) := (others => '0'); 
  signal gpioDir_i      : std_logic_vector(31 downto 0) := (others => '0'); 
  signal gpioDout_i     : std_logic_vector(31 downto 0) := (others => '0');
  signal fpga_dna       : std_logic_vector(56 downto 0) := (others => '0');
  signal scax_id        : std_logic_vector(23 downto 0) := (others => '0');
  constant gpioDin_i    : std_logic_vector(31 downto 0) := x"c0cac01a"; 
  
  signal frameTX_i      : frame_tx; 
  signal frame_newTX_i  : std_logic                    := '0'; 
  signal frame_sizeTX_i : std_logic_vector(1 downto 0) := (others => '0'); 
  
  signal writeNrml_reg  : std_logic := '0'; 
  signal readNrml_reg   : std_logic := '0'; 
  signal writeDmmy_reg  : std_logic := '0'; 
  signal readDmmy_reg   : std_logic := '0'; 
  signal writeJtag_reg  : std_logic := '0'; 
  signal readJtag_reg   : std_logic := '0'; 
  
  signal rstn_spi       : std_logic := '0'; 
  signal rstn_gpio      : std_logic := '0'; 
  signal rstn_jtag      : std_logic := '0'; 
  signal rstn_adc       : std_logic := '0'; 
  signal state_o        : std_logic_vector(3 downto 0) := (others => '0'); 
  
  type stateType is (ST_IDLE, ST_CHK_CMD_NRML, ST_CHK_CMD_DMMY, ST_CHK_CMD_JTAG, ST_CALC_CTRL, ST_SEND_REPLY); 
  signal state : stateType := ST_IDLE; 
  
begin
  
-- controller main FSM
ctrl_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      state_o        <= "1111"; 
      frame_newTX_i  <= '0'; 
      writeNrml_reg  <= '0'; 
      readNrml_reg   <= '0'; 
      writeDmmy_reg  <= '0'; 
      readDmmy_reg   <= '0'; 
      writeJtag_reg  <= '0'; 
      readJtag_reg   <= '0'; 
      frame_sizeTX_i <= "00"; 
      state          <= ST_IDLE; 
    else
      case state is
          
        -- wait for request
        when ST_IDLE => 
          state_o       <= "0000"; 
          frame_newTX_i <= '0'; 
          
          if(frame_newRX = '1' and frameRX_dummy = '0' and frameRX_jtag = '0')then -- normal command
            state <= ST_CHK_CMD_NRML; 
          elsif(frame_newRX = '1' and frameRX_dummy = '1' and frameRX_jtag = '0')then -- thats for a dummy channel
            state <= ST_CHK_CMD_DMMY; 
          elsif(frame_newRX = '1' and frameRX_dummy = '0' and frameRX_jtag = '1')then -- thats for the dummy jtag channel
            state <= ST_CHK_CMD_JTAG; 
          else
            state <= ST_IDLE; 
          end if; 
          
        -- what is the command field?
        when ST_CHK_CMD_NRML => 
          state_o <= "0001"; 
          state   <= ST_CALC_CTRL; 
          
          case frameRX.command is
            when CTRL_W_CRB | CTRL_W_CRC | CTRL_W_CRD => 
              writeNrml_reg  <= '1'; 
              readNrml_reg   <= '0'; 
              frame_sizeTX_i <= "01"; -- ok ack
            when CTRL_R_CRB | CTRL_R_CRC | CTRL_R_CRD => 
              writeNrml_reg  <= '0'; 
              readNrml_reg   <= '1'; 
              frame_sizeTX_i <= "10"; -- the value
            when CTRL_R_SEU => 
              writeNrml_reg  <= '0'; 
              readNrml_reg   <= '1'; 
              frame_sizeTX_i <= "11"; -- the value
            when CTRL_C_SEU => 
              writeNrml_reg  <= '0'; 
              readNrml_reg   <= '0'; 
              frame_sizeTX_i <= "01"; -- ok ack
            when others => 
              writeNrml_reg <= '0'; 
              readNrml_reg  <= '0'; 
          end case; 
          
        -- dummy command to the controller
        when ST_CHK_CMD_DMMY => 
          state_o <= "0010"; 
          state   <= ST_CALC_CTRL; 
          
          case frameRX.command is
            when GPIO_W_DIR | ADC_W_MUX | GPIO_W_DOUT => 
              writeDmmy_reg  <= '1'; 
              readDmmy_reg   <= '0'; 
              frame_sizeTX_i <= "01"; -- ok ack
            when ADC_R_MUX => 
              writeDmmy_reg  <= '0'; 
              readDmmy_reg   <= '1'; 
              frame_sizeTX_i <= "10"; -- the value
            when CTRL_R_ID | GPIO_R_DIR | GPIO_R_DOUT | GPIO_R_DIN | ADC_GO => 
              writeDmmy_reg  <= '0'; 
              readDmmy_reg   <= '1'; 
              frame_sizeTX_i <= "11"; -- the value
            when others => 
              writeDmmy_reg <= '0'; 
              readDmmy_reg  <= '0'; 
          end case; 
          
        -- jtag command to the controller
        when ST_CHK_CMD_JTAG => 
          state_o <= "0010"; 
          state   <= ST_CALC_CTRL; 
          
          case frameRX.command is
            when JTAG_W_CTRL | JTAG_W_FREQ | JTAG_W_TDO0 |
              JTAG_W_TDO1 | JTAG_W_TDO2 | JTAG_W_TDO3 |
              JTAG_W_TMS0 | JTAG_W_TMS1 | JTAG_W_TMS2 |
              JTAG_W_TMS3 | JTAG_GO => 
              writeJtag_reg  <= '1'; 
              readJtag_reg   <= '0'; 
              frame_sizeTX_i <= "01"; -- ok ack
            when JTAG_R_CTRL | JTAG_R_FREQ => 
              writeJtag_reg  <= '0'; 
              readJtag_reg   <= '1'; 
              frame_sizeTX_i <= "10"; -- the value
            when JTAG_R_TDI0 | JTAG_R_TDI1 | JTAG_R_TDI2 |
              JTAG_R_TDI3 | JTAG_R_TMS0 | JTAG_R_TMS1 | 
              JTAG_R_TMS2 | JTAG_R_TMS3 => 
              writeJtag_reg  <= '0'; 
              readJtag_reg   <= '1'; 
              frame_sizeTX_i <= "11"; -- the value
            when others => 
              writeJtag_reg <= '0'; 
              readJtag_reg  <= '0'; 
          end case; 
          
        -- calculate the new control field
        when ST_CALC_CTRL => 
          state_o       <= "0011"; 
          writeNrml_reg <= '0'; 
          readNrml_reg  <= '0'; 
          writeDmmy_reg <= '0'; 
          readDmmy_reg  <= '0'; 
          frameTX_i.control(7 downto 4) <= std_logic_vector(unsigned(frameRX.control(7 downto 4)) + 2); 
          frameTX_i.control(3 downto 0) <= frameRX.control(3 downto 0); 
          state                         <= ST_SEND_REPLY; 
          
        -- send the reply
        when ST_SEND_REPLY => 
          state_o       <= "0011"; 
          frame_newTX_i <= '1'; 
          
          if(frame_newRX = '0')then
            state <= ST_IDLE; 
          else
            state <= ST_SEND_REPLY; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- controller command demux
cmd_demux_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      crb_i      <= (others => '0'); 
      crc_i      <= (others => '0'); 
      crd_i      <= (others => '0'); 
      insel_i    <= (others => '0'); 
      gpioDir_i  <= (others => '0'); 
      gpioDout_i <= (others => '0'); 
      jtag_freq  <= (others => '0'); 
      jtag_ctrl  <= (others => '0'); 
    else
      if(writeNrml_reg = '1')then
        
        insel_i   <= insel_i; gpioDir_i <= gpioDir_i; gpioDout_i <= gpioDout_i; -- stays the same by definition
        jtag_freq <= jtag_freq; jtag_ctrl <= jtag_ctrl; 
        
        case frameRX.command is
          when CTRL_W_CRB => crb_i <= frameRX.din_a(7 downto 0); crc_i <= crc_i; crd_i <= crd_i; 
          when CTRL_W_CRC => crc_i <= frameRX.din_a(7 downto 0); crd_i <= crd_i; crb_i <= crb_i; 
          when CTRL_W_CRD => crd_i <= frameRX.din_a(7 downto 0); crb_i <= crb_i; crc_i <= crc_i; 
          when others => crb_i <= crb_i; crc_i <= crc_i; crd_i <= crd_i; 
        end case; 
        
      elsif(writeDmmy_reg = '1')then
        
        crb_i     <= crb_i; crc_i <= crc_i; crd_i <= crd_i; -- stays the same by definition
        jtag_freq <= jtag_freq; jtag_ctrl <= jtag_ctrl; 
        
        case frameRX.command is
          when GPIO_W_DIR => gpioDir_i(15 downto 0) <= frameRX.din_a; gpioDir_i(31 downto 16) <= frameRX.din_b; gpioDout_i <= gpioDout_i; insel_i <= insel_i; 
          when GPIO_W_DOUT => gpioDout_i(15 downto 0) <= frameRX.din_a; gpioDout_i(31 downto 16) <= frameRX.din_b; insel_i <= insel_i; gpioDir_i <= gpioDir_i; 
          when ADC_W_MUX => insel_i <= frameRX.din_b(7 downto 0); gpioDir_i <= gpioDir_i; gpioDout_i <= gpioDout_i; 
          when others => insel_i <= insel_i; gpioDir_i <= gpioDir_i; gpioDout_i <= gpioDout_i; 
        end case; 
        
      elsif(writeJtag_reg = '1')then
        
        crb_i   <= crb_i; crc_i <= crc_i; crd_i <= crd_i; -- stays the same by definition
        insel_i <= insel_i; gpioDir_i <= gpioDir_i; gpioDout_i <= gpioDout_i; 
        
        case frameRX.command is
          when JTAG_W_CTRL => jtag_ctrl <= frameRX.din_a; jtag_freq <= jtag_freq; 
          when JTAG_W_FREQ => jtag_freq <= frameRX.din_a; jtag_ctrl <= jtag_ctrl; 
          when others => jtag_freq <= jtag_freq; jtag_ctrl <= jtag_ctrl; 
        end case; 
        
      else
        crb_i <= crb_i; crc_i <= crc_i; crd_i <= crd_i; insel_i <= insel_i; gpioDir_i <= gpioDir_i; gpioDout_i <= gpioDout_i; jtag_freq <= jtag_freq; jtag_ctrl <= jtag_ctrl; 
      end if; 
      
      if(readNrml_reg = '1')then
        case frameRX.command is
          when CTRL_R_CRB => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= crb_i; 
          when CTRL_R_CRC => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= crc_i; 
          when CTRL_R_CRD => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= crd_i; 
          when CTRL_R_SEU => frameTX_i.dout_a <= x"0000"; frameTX_i.dout_b <= x"0000"; 
          when others => frameTX_i.dout_a <= frameTX_i.dout_a; frameTX_i.dout_b <= frameTX_i.dout_b; 
        end case; 
      elsif(readDmmy_reg = '1')then
        case frameRX.command is
          when GPIO_R_DOUT => frameTX_i.dout_a <= gpioDout_i(15 downto 0); frameTX_i.dout_b <= gpioDout_i(31 downto 16); 
          when GPIO_R_DIN => frameTX_i.dout_a <= gpioDin_i(15 downto 0); frameTX_i.dout_b <= gpioDin_i(31 downto 16); 
          when GPIO_R_DIR => frameTX_i.dout_a <= gpioDir_i(15 downto 0); frameTX_i.dout_b <= gpioDir_i(31 downto 16); 
          when CTRL_R_ID => frameTX_i.dout_a(07 downto 0) <= (others => '0'); 
            frameTX_i.dout_a(15 downto 8) <= scax_id(23 downto 16); --0x12
            frameTX_i.dout_b(15 downto 8) <= scax_id(7 downto 0);   --0x34
            frameTX_i.dout_b(07 downto 0) <= scax_id(15 downto 8);  --0xcb
          when ADC_R_MUX => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= insel_i; 
          when ADC_GO => frameTX_i.dout_a <= x"AAAA"; frameTX_i.dout_b <= x"AAAA"; 
          when others => frameTX_i.dout_a <= frameTX_i.dout_a; frameTX_i.dout_b <= frameTX_i.dout_b; 
        end case; 
      elsif(readJtag_reg = '1')then
        case frameRX.command is
          when JTAG_R_CTRL => frameTX_i.dout_a <= jtag_ctrl; 
          when JTAG_R_FREQ => frameTX_i.dout_a <= jtag_freq; 
          when others => frameTX_i.dout_a <= x"C0CA"; frameTX_i.dout_b <= x"C01A"; 
        end case; 
      else
        frameTX_i.dout_a <= frameTX_i.dout_a; frameTX_i.dout_b <= frameTX_i.dout_b; 
      end if; 
    end if; 
  end if; 
end process; 
  
-- pipeline the TX data
pipeTX_proc : process(clk)
begin
  if(rising_edge(clk))then
    frameTX_i.address <= x"FF"; 
    -- control calculated from other process
    frameTX_i.tr_id   <= frameRX.tr_id; 
    frameTX_i.channel <= frameRX.channel; 
    -- error calculated from other process
    frameTX_i.err    <= error_none; 
    frameTX_i.length <= x"03"; -- unused
                               -- data fields calculated from other process
    
    frameTX      <= frameTX_i; 
    frame_sizeTX <= frame_sizeTX_i; 
    frame_newTX  <= frame_newTX_i; 
  end if; 
end process; 

dna_reader_inst: dna_reader
  port map(
    clk     => clk,
    clk_40  => clk_40,
    rst     => rst,
    dna     => fpga_dna
   );
  
  -- crb
  rstn_spi    <= crb_i(1); 
  rstn_gpio   <= crb_i(2); 
  ena_i2c(00) <= crb_i(3); 
  ena_i2c(01) <= crb_i(4); 
  ena_i2c(02) <= crb_i(5); 
  ena_i2c(03) <= crb_i(6); 
  ena_i2c(04) <= crb_i(7); 
  -- crc
  ena_i2c(05) <= crc_i(0); 
  ena_i2c(06) <= crc_i(1); 
  ena_i2c(07) <= crc_i(2); 
  ena_i2c(08) <= crc_i(3); 
  ena_i2c(09) <= crc_i(4); 
  ena_i2c(10) <= crc_i(5); 
  ena_i2c(11) <= crc_i(6); 
  ena_i2c(12) <= crc_i(7); 
  -- crd
  ena_i2c(13) <= crd_i(0); 
  ena_i2c(14) <= crd_i(1); 
  ena_i2c(15) <= crd_i(2); 
  rstn_jtag   <= crd_i(3); 
  rstn_adc    <= crd_i(4); 
  
  state_o_ctrl          <= state_o; 
  
  scax_id(3 downto 0)   <= fpga_dna(56 downto 53);  -- random
  scax_id(7 downto 4)   <= fpga_dna(5 downto 2);    -- random
  scax_id(11 downto 8)  <= fpga_dna(42 downto 39);  -- random
  scax_id(15 downto 12) <= fpga_dna(30 downto 27);  -- random
  scax_id(19 downto 16) <= fpga_dna(48 downto 45);  -- random
  scax_id(23 downto 20) <= fpga_dna(24 downto 21);  -- random
  
  
end RTL; 
