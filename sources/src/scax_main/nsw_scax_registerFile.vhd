-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
--
-- Source auto-generated via jinja2 templates for use with wuppercodegen
-- ... by Lawrence Lee (lawrence.lee.jr@cern.ch)
--
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--
-- Create Date: 10.10.2018 18:08:12
-- Design Name: Register File
-- Module Name: registerFile - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Actual register file for all I2C masters.
--
-- Dependencies:
--
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use UNISIM.VComponents.all;
use work.registerFile_package.all;

entity registerFile is
  generic(i2c_master_id : integer := 0);
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; -- user clock (SCAX) or registerFile clock
    ------------------------------
    -- I2C Master Interface ------
    reg_wr        : in  std_logic;
    reg_rd        : in  std_logic;
    reg_addr      : in  std_logic_vector(9 downto 0);
    reg_wr_data   : in  std_logic_vector(31 downto 0);
    reg_ack       : out std_logic;
    reg_rd_data   : out std_logic_vector(31 downto 0);
    ------------------------------
    -- FPGA Register Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL  -- used when writing

  );
end registerFile;

architecture RTL of registerFile is

  signal regs_UFLtoSCAX_i : UFLtoSCAX;

begin


int_reg_proc: process(clk)
begin
  if(rising_edge(clk))then
    regs_UFLtoSCAX_i <= regs_UFLtoSCAX;
  end if;
end process;



generate0 : if (i2c_master_id = 0) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- NSW_Firmware_version_time_stamp_0000
        -- Xilinx USR_ACCESS  timestamp of firmware implementation  0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.NSW_Firmware_version_time_stamp_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_EndcapSector_ID_0001
        --  0001
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_EndcapSector_ID_0001 <= reg_wr_data(4 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(4 downto 0) <= regs_UFLtoSCAX_i.NSW_EndcapSector_ID_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_XL1ID_0002
        -- high 8 bits of L1ID 0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_XL1ID_0002 <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.NSW_XL1ID_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_Run_number_0003
        -- Run number, set at begin run 0003
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_Run_number_0003 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.NSW_Run_number_0003; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_orbit_count_0004
        -- reset by TTC 0004
        when "0000000100" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.NSW_orbit_count_0004; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_delta_theta_cut_0005
        --  0005
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_delta_theta_cut_0005 <= reg_wr_data(4 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(4 downto 0) <= regs_UFLtoSCAX_i.NSW_delta_theta_cut_0005; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_mezz_PLL_reset_0006
        --  0006
        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_mezz_PLL_reset_0006 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.NSW_mezz_PLL_reset_0006 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.NSW_mezz_PLL_reset_0006 <= '0'; reg_ack <= '0';
          end if;

        -- NSW_XVC_IP_low_byte_0007
        -- to be OR'ed with Sector ID 0007
        when "0000000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_XVC_IP_low_byte_0007 <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.NSW_XVC_IP_low_byte_0007; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_BC_offset_0008
        -- BCID loaded when BCR received 0008
        when "0000001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_BC_offset_0008 <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.NSW_BC_offset_0008; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_BC_L1A_window_0009
        -- BCID window for L1A output 0009
        when "0000001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_BC_L1A_window_0009 <= reg_wr_data(2 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(2 downto 0) <= regs_UFLtoSCAX_i.NSW_BC_L1A_window_0009; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_BC_monitoring_window_000a
        -- BCID window for monitoring output 000a
        when "0000001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_BC_monitoring_window_000a <= reg_wr_data(2 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(2 downto 0) <= regs_UFLtoSCAX_i.NSW_BC_monitoring_window_000a; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_send_RODBUSY_on_000b
        -- reset by FPGA after sending 000b
        when "0000001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_send_RODBUSY_on_000b <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_ack <= '0';
          else reg_ack <= '0';
          end if;

        -- NSW_send_RODBUSY_off_000c
        -- reset by FPGA after sending 000c
        when "0000001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_send_RODBUSY_off_000c <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_ack <= '0';
          else reg_ack <= '0';
          end if;

        -- NSW_enable_LIA_data_output_000d
        --  000d
        when "0000001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.NSW_enable_LIA_data_output_000d <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_enable_LIA_data_output_000d; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_FELIX_RX_link_OK_000e
        --  000e
        when "0000001110" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_FELIX_RX_link_OK_000e; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_Jitter_cleaner_status_000f(0)
        -- Please edit me, bit-width was '?' Vector [4] 000f(0)
        when "0000001111" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_Jitter_cleaner_status_000f(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_Jitter_cleaner_status_000f(1)
        -- Please edit me, bit-width was '?' Vector [4] 000f(1)
        when "0000010000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_Jitter_cleaner_status_000f(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_Jitter_cleaner_status_000f(2)
        -- Please edit me, bit-width was '?' Vector [4] 000f(2)
        when "0000010001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_Jitter_cleaner_status_000f(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- NSW_Jitter_cleaner_status_000f(3)
        -- Please edit me, bit-width was '?' Vector [4] 000f(3)
        when "0000010010" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.NSW_Jitter_cleaner_status_000f(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate0;

generate1 : if (i2c_master_id = 1) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- MM_lnk_gbt_link_alligned_0000
        -- input serializer OK 0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_lnk_gbt_link_alligned_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_lnk_gbt_pll_locked_0001
        -- GBT PLL link status, 1 per quad 0001
        when "0000000001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(8 downto 0) <= regs_UFLtoSCAX_i.MM_lnk_gbt_pll_locked_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_lnk_gbt_pll_reset_0002
        -- GBT PLL reset, 1 per quad 0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_lnk_gbt_pll_reset_0002 <= reg_wr_data(8 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_lnk_gbt_pll_reset_0002 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.MM_lnk_gbt_pll_reset_0002 <= (others => '0'); reg_ack <= '0';
          end if;

        -- MM_lnk_gbt_hw_reset_0003
        -- Reset all GBT links 0003
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_lnk_gbt_hw_reset_0003 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_lnk_gbt_hw_reset_0003 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_lnk_gbt_hw_reset_0003 <= '0'; reg_ack <= '0';
          end if;

        -- MM_lnk_gbt_link_error_0004
        -- Detect misaligned or unlocked links 0004
        when "0000000100" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_lnk_gbt_link_error_0004; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_lnk_gbt_channel_mask_0005
        -- Mask unconnected links 0005
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_lnk_gbt_channel_mask_0005 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_lnk_gbt_channel_mask_0005; reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate1;

generate2 : if (i2c_master_id = 2) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- MM_addc_addc_packet_parity_error_0000
        -- GBT link parity errors 0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_parity_error_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_bc_error_0001
        -- Detect inconsistent BC offset between packet and local BC 0001
        when "0000000001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_bc_error_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_format_error_0002
        -- Detect packet data  formatting errors 0002
        when "0000000010" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_format_error_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(0)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(0)
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(0) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(1)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(1)
        when "0000000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(1) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(2)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(2)
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(2) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(3)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(3)
        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(3) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(4)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(4)
        when "0000000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(4) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(5)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(5)
        when "0000001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(5) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(6)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(6)
        when "0000001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(6) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(7)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(7)
        when "0000001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(7) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(8)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(8)
        when "0000001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(8) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(9)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(9)
        when "0000001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(9) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(10)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(10)
        when "0000001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(10) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(11)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(11)
        when "0000001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(11) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(12)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(12)
        when "0000001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(12) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(13)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(13)
        when "0000010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(13) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(14)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(14)
        when "0000010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(14) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(15)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(15)
        when "0000010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(15) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(16)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(16)
        when "0000010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(16) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(17)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(17)
        when "0000010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(17) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(18)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(18)
        when "0000010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(18) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(19)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(19)
        when "0000010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(19) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(20)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(20)
        when "0000010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(20) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(21)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(21)
        when "0000011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(21) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(22)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(22)
        when "0000011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(22) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(23)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(23)
        when "0000011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(23) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(24)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(24)
        when "0000011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(24) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(24); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(25)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(25)
        when "0000011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(25) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(25); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(26)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(26)
        when "0000011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(26) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(26); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(27)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(27)
        when "0000011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(27) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(27); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(28)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(28)
        when "0000011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(28) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(28); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(29)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(29)
        when "0000100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(29) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(29); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(30)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(30)
        when "0000100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(30) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(30); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_addc_addc_packet_BC_offset_0003(31)
        -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(31)
        when "0000100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(31) <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.MM_addc_addc_packet_BC_offset_0003(31); reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate2;

generate3 : if (i2c_master_id = 3) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- MM_tp_status_0000
        -- Current state of the TP including a global error bit 0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_tp_status_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_tp_local_bc_disable_0001
        -- Pause the local BC counter 0001
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_tp_local_bc_disable_0001 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.MM_tp_local_bc_disable_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_tp_local_bc_reset_0002
        -- Reset the local BC counter 0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_tp_local_bc_reset_0002 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_tp_local_bc_reset_0002 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_tp_local_bc_reset_0002 <= '0'; reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate3;

generate4 : if (i2c_master_id = 4) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- MM_alg_algor_strip0_offset_0000(0)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(0)
        when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(1)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(1)
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(2)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(2)
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(3)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(3)
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(4)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(4)
        when "0000000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(4) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(5)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(5)
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(5) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(6)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(6)
        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(6) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(7)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(7)
        when "0000000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(7) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(8)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(8)
        when "0000001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(8) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(9)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(9)
        when "0000001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(9) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(10)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(10)
        when "0000001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(10) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(11)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(11)
        when "0000001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(11) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(12)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(12)
        when "0000001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(12) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(13)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(13)
        when "0000001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(13) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(14)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(14)
        when "0000001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(14) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(15)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(15)
        when "0000001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(15) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(16)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(16)
        when "0000010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(16) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(17)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(17)
        when "0000010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(17) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(18)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(18)
        when "0000010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(18) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(19)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(19)
        when "0000010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(19) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(20)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(20)
        when "0000010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(20) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(21)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(21)
        when "0000010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(21) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(22)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(22)
        when "0000010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(22) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(23)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(23)
        when "0000010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(23) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(24)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(24)
        when "0000011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(24) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(24); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(25)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(25)
        when "0000011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(25) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(25); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(26)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(26)
        when "0000011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(26) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(26); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(27)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(27)
        when "0000011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(27) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(27); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(28)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(28)
        when "0000011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(28) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(28); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(29)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(29)
        when "0000011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(29) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(29); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(30)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(30)
        when "0000011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(30) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(30); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(31)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(31)
        when "0000011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(31) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(31); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(32)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(32)
        when "0000100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(32) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(32); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(33)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(33)
        when "0000100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(33) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(33); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(34)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(34)
        when "0000100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(34) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(34); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(35)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(35)
        when "0000100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(35) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(35); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(36)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(36)
        when "0000100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(36) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(36); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(37)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(37)
        when "0000100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(37) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(37); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(38)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(38)
        when "0000100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(38) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(38); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(39)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(39)
        when "0000100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(39) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(39); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(40)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(40)
        when "0000101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(40) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(40); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(41)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(41)
        when "0000101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(41) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(41); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(42)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(42)
        when "0000101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(42) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(42); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(43)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(43)
        when "0000101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(43) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(43); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(44)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(44)
        when "0000101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(44) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(44); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(45)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(45)
        when "0000101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(45) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(45); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(46)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(46)
        when "0000101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(46) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(46); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(47)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(47)
        when "0000101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(47) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(47); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(48)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(48)
        when "0000110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(48) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(48); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(49)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(49)
        when "0000110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(49) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(49); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(50)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(50)
        when "0000110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(50) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(50); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(51)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(51)
        when "0000110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(51) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(51); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(52)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(52)
        when "0000110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(52) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(52); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(53)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(53)
        when "0000110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(53) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(53); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(54)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(54)
        when "0000110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(54) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(54); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(55)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(55)
        when "0000110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(55) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(55); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(56)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(56)
        when "0000111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(56) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(56); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(57)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(57)
        when "0000111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(57) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(57); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(58)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(58)
        when "0000111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(58) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(58); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(59)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(59)
        when "0000111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(59) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(59); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(60)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(60)
        when "0000111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(60) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(60); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(61)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(61)
        when "0000111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(61) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(61); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(62)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(62)
        when "0000111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(62) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(62); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(63)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(63)
        when "0000111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(63) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(63); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(64)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(64)
        when "0001000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(64) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(64); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(65)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(65)
        when "0001000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(65) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(65); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(66)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(66)
        when "0001000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(66) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(66); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(67)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(67)
        when "0001000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(67) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(67); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(68)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(68)
        when "0001000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(68) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(68); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(69)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(69)
        when "0001000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(69) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(69); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(70)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(70)
        when "0001000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(70) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(70); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(71)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(71)
        when "0001000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(71) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(71); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(72)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(72)
        when "0001001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(72) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(72); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(73)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(73)
        when "0001001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(73) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(73); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(74)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(74)
        when "0001001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(74) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(74); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(75)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(75)
        when "0001001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(75) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(75); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(76)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(76)
        when "0001001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(76) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(76); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(77)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(77)
        when "0001001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(77) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(77); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(78)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(78)
        when "0001001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(78) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(78); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(79)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(79)
        when "0001001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(79) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(79); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(80)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(80)
        when "0001010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(80) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(80); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(81)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(81)
        when "0001010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(81) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(81); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(82)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(82)
        when "0001010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(82) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(82); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(83)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(83)
        when "0001010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(83) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(83); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(84)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(84)
        when "0001010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(84) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(84); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(85)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(85)
        when "0001010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(85) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(85); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(86)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(86)
        when "0001010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(86) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(86); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(87)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(87)
        when "0001010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(87) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(87); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(88)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(88)
        when "0001011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(88) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(88); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(89)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(89)
        when "0001011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(89) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(89); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(90)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(90)
        when "0001011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(90) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(90); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(91)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(91)
        when "0001011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(91) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(91); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(92)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(92)
        when "0001011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(92) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(92); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(93)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(93)
        when "0001011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(93) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(93); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(94)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(94)
        when "0001011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(94) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(94); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(95)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(95)
        when "0001011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(95) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(95); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(96)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(96)
        when "0001100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(96) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(96); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(97)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(97)
        when "0001100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(97) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(97); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(98)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(98)
        when "0001100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(98) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(98); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(99)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(99)
        when "0001100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(99) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(99); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(100)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(100)
        when "0001100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(100) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(100); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(101)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(101)
        when "0001100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(101) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(101); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(102)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(102)
        when "0001100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(102) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(102); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(103)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(103)
        when "0001100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(103) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(103); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(104)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(104)
        when "0001101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(104) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(104); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(105)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(105)
        when "0001101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(105) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(105); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(106)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(106)
        when "0001101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(106) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(106); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(107)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(107)
        when "0001101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(107) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(107); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(108)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(108)
        when "0001101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(108) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(108); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(109)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(109)
        when "0001101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(109) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(109); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(110)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(110)
        when "0001101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(110) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(110); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(111)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(111)
        when "0001101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(111) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(111); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(112)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(112)
        when "0001110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(112) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(112); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(113)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(113)
        when "0001110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(113) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(113); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(114)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(114)
        when "0001110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(114) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(114); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(115)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(115)
        when "0001110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(115) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(115); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(116)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(116)
        when "0001110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(116) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(116); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(117)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(117)
        when "0001110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(117) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(117); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(118)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(118)
        when "0001110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(118) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(118); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(119)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(119)
        when "0001110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(119) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(119); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(120)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(120)
        when "0001111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(120) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(120); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(121)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(121)
        when "0001111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(121) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(121); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(122)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(122)
        when "0001111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(122) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(122); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(123)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(123)
        when "0001111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(123) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(123); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(124)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(124)
        when "0001111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(124) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(124); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(125)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(125)
        when "0001111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(125) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(125); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(126)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(126)
        when "0001111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(126) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(126); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_strip0_offset_0000(127)
        -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(127)
        when "0001111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_strip0_offset_0000(127) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_strip0_offset_0000(127); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(0)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(0)
        when "0010000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(1)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(1)
        when "0010000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(2)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(2)
        when "0010000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(3)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(3)
        when "0010000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(4)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(4)
        when "0010000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(4) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(5)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(5)
        when "0010000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(5) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(6)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(6)
        when "0010000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(6) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(7)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(7)
        when "0010000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(7) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(8)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(8)
        when "0010001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(8) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(9)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(9)
        when "0010001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(9) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(10)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(10)
        when "0010001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(10) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(11)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(11)
        when "0010001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(11) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(12)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(12)
        when "0010001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(12) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(13)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(13)
        when "0010001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(13) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(14)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(14)
        when "0010001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(14) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(15)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(15)
        when "0010001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(15) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(16)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(16)
        when "0010010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(16) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(17)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(17)
        when "0010010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(17) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(18)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(18)
        when "0010010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(18) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(19)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(19)
        when "0010010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(19) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(20)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(20)
        when "0010010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(20) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(21)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(21)
        when "0010010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(21) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(22)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(22)
        when "0010010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(22) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(23)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(23)
        when "0010010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(23) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(24)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(24)
        when "0010011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(24) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(24); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(25)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(25)
        when "0010011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(25) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(25); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(26)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(26)
        when "0010011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(26) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(26); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(27)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(27)
        when "0010011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(27) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(27); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(28)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(28)
        when "0010011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(28) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(28); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(29)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(29)
        when "0010011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(29) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(29); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(30)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(30)
        when "0010011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(30) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(30); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(31)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(31)
        when "0010011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(31) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(31); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(32)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(32)
        when "0010100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(32) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(32); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(33)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(33)
        when "0010100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(33) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(33); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(34)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(34)
        when "0010100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(34) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(34); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(35)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(35)
        when "0010100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(35) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(35); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(36)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(36)
        when "0010100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(36) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(36); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(37)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(37)
        when "0010100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(37) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(37); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(38)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(38)
        when "0010100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(38) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(38); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(39)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(39)
        when "0010100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(39) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(39); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(40)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(40)
        when "0010101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(40) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(40); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(41)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(41)
        when "0010101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(41) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(41); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(42)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(42)
        when "0010101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(42) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(42); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(43)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(43)
        when "0010101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(43) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(43); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(44)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(44)
        when "0010101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(44) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(44); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(45)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(45)
        when "0010101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(45) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(45); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(46)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(46)
        when "0010101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(46) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(46); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(47)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(47)
        when "0010101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(47) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(47); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(48)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(48)
        when "0010110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(48) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(48); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(49)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(49)
        when "0010110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(49) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(49); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(50)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(50)
        when "0010110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(50) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(50); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(51)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(51)
        when "0010110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(51) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(51); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(52)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(52)
        when "0010110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(52) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(52); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(53)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(53)
        when "0010110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(53) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(53); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(54)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(54)
        when "0010110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(54) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(54); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(55)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(55)
        when "0010110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(55) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(55); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(56)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(56)
        when "0010111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(56) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(56); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(57)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(57)
        when "0010111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(57) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(57); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(58)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(58)
        when "0010111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(58) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(58); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(59)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(59)
        when "0010111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(59) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(59); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(60)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(60)
        when "0010111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(60) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(60); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(61)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(61)
        when "0010111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(61) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(61); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(62)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(62)
        when "0010111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(62) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(62); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(63)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(63)
        when "0010111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(63) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(63); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(64)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(64)
        when "0011000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(64) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(64); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(65)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(65)
        when "0011000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(65) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(65); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(66)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(66)
        when "0011000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(66) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(66); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(67)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(67)
        when "0011000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(67) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(67); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(68)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(68)
        when "0011000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(68) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(68); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(69)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(69)
        when "0011000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(69) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(69); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(70)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(70)
        when "0011000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(70) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(70); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(71)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(71)
        when "0011000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(71) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(71); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(72)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(72)
        when "0011001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(72) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(72); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(73)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(73)
        when "0011001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(73) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(73); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(74)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(74)
        when "0011001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(74) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(74); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(75)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(75)
        when "0011001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(75) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(75); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(76)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(76)
        when "0011001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(76) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(76); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(77)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(77)
        when "0011001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(77) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(77); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(78)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(78)
        when "0011001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(78) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(78); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(79)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(79)
        when "0011001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(79) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(79); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(80)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(80)
        when "0011010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(80) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(80); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(81)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(81)
        when "0011010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(81) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(81); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(82)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(82)
        when "0011010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(82) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(82); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(83)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(83)
        when "0011010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(83) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(83); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(84)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(84)
        when "0011010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(84) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(84); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(85)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(85)
        when "0011010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(85) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(85); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(86)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(86)
        when "0011010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(86) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(86); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(87)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(87)
        when "0011010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(87) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(87); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(88)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(88)
        when "0011011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(88) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(88); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(89)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(89)
        when "0011011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(89) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(89); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(90)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(90)
        when "0011011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(90) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(90); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(91)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(91)
        when "0011011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(91) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(91); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(92)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(92)
        when "0011011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(92) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(92); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(93)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(93)
        when "0011011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(93) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(93); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(94)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(94)
        when "0011011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(94) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(94); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(95)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(95)
        when "0011011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(95) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(95); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(96)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(96)
        when "0011100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(96) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(96); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(97)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(97)
        when "0011100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(97) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(97); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(98)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(98)
        when "0011100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(98) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(98); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(99)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(99)
        when "0011100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(99) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(99); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(100)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(100)
        when "0011100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(100) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(100); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(101)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(101)
        when "0011100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(101) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(101); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(102)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(102)
        when "0011100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(102) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(102); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(103)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(103)
        when "0011100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(103) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(103); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(104)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(104)
        when "0011101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(104) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(104); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(105)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(105)
        when "0011101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(105) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(105); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(106)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(106)
        when "0011101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(106) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(106); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(107)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(107)
        when "0011101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(107) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(107); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(108)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(108)
        when "0011101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(108) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(108); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(109)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(109)
        when "0011101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(109) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(109); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(110)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(110)
        when "0011101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(110) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(110); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(111)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(111)
        when "0011101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(111) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(111); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(112)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(112)
        when "0011110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(112) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(112); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(113)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(113)
        when "0011110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(113) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(113); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(114)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(114)
        when "0011110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(114) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(114); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(115)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(115)
        when "0011110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(115) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(115); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(116)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(116)
        when "0011110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(116) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(116); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(117)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(117)
        when "0011110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(117) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(117); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(118)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(118)
        when "0011110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(118) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(118); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(119)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(119)
        when "0011110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(119) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(119); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(120)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(120)
        when "0011111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(120) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(120); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(121)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(121)
        when "0011111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(121) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(121); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(122)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(122)
        when "0011111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(122) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(122); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(123)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(123)
        when "0011111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(123) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(123); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(124)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(124)
        when "0011111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(124) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(124); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(125)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(125)
        when "0011111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(125) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(125); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(126)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(126)
        when "0011111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(126) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(126); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_0080(127)
        -- Distance from IP to plane, one per plane region Vector [128] 0080(127)
        when "0011111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_0080(127) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_0080(127); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(0)
        -- reciprocal of algor_plane_offset Vector [128] 0100(0)
        when "0100000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(1)
        -- reciprocal of algor_plane_offset Vector [128] 0100(1)
        when "0100000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(2)
        -- reciprocal of algor_plane_offset Vector [128] 0100(2)
        when "0100000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(3)
        -- reciprocal of algor_plane_offset Vector [128] 0100(3)
        when "0100000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(4)
        -- reciprocal of algor_plane_offset Vector [128] 0100(4)
        when "0100000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(4) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(5)
        -- reciprocal of algor_plane_offset Vector [128] 0100(5)
        when "0100000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(5) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(6)
        -- reciprocal of algor_plane_offset Vector [128] 0100(6)
        when "0100000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(6) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(7)
        -- reciprocal of algor_plane_offset Vector [128] 0100(7)
        when "0100000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(7) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(8)
        -- reciprocal of algor_plane_offset Vector [128] 0100(8)
        when "0100001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(8) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(9)
        -- reciprocal of algor_plane_offset Vector [128] 0100(9)
        when "0100001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(9) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(10)
        -- reciprocal of algor_plane_offset Vector [128] 0100(10)
        when "0100001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(10) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(11)
        -- reciprocal of algor_plane_offset Vector [128] 0100(11)
        when "0100001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(11) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(12)
        -- reciprocal of algor_plane_offset Vector [128] 0100(12)
        when "0100001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(12) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(13)
        -- reciprocal of algor_plane_offset Vector [128] 0100(13)
        when "0100001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(13) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(14)
        -- reciprocal of algor_plane_offset Vector [128] 0100(14)
        when "0100001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(14) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(15)
        -- reciprocal of algor_plane_offset Vector [128] 0100(15)
        when "0100001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(15) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(16)
        -- reciprocal of algor_plane_offset Vector [128] 0100(16)
        when "0100010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(16) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(17)
        -- reciprocal of algor_plane_offset Vector [128] 0100(17)
        when "0100010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(17) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(18)
        -- reciprocal of algor_plane_offset Vector [128] 0100(18)
        when "0100010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(18) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(19)
        -- reciprocal of algor_plane_offset Vector [128] 0100(19)
        when "0100010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(19) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(20)
        -- reciprocal of algor_plane_offset Vector [128] 0100(20)
        when "0100010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(20) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(21)
        -- reciprocal of algor_plane_offset Vector [128] 0100(21)
        when "0100010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(21) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(22)
        -- reciprocal of algor_plane_offset Vector [128] 0100(22)
        when "0100010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(22) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(23)
        -- reciprocal of algor_plane_offset Vector [128] 0100(23)
        when "0100010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(23) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(24)
        -- reciprocal of algor_plane_offset Vector [128] 0100(24)
        when "0100011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(24) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(24); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(25)
        -- reciprocal of algor_plane_offset Vector [128] 0100(25)
        when "0100011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(25) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(25); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(26)
        -- reciprocal of algor_plane_offset Vector [128] 0100(26)
        when "0100011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(26) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(26); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(27)
        -- reciprocal of algor_plane_offset Vector [128] 0100(27)
        when "0100011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(27) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(27); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(28)
        -- reciprocal of algor_plane_offset Vector [128] 0100(28)
        when "0100011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(28) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(28); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(29)
        -- reciprocal of algor_plane_offset Vector [128] 0100(29)
        when "0100011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(29) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(29); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(30)
        -- reciprocal of algor_plane_offset Vector [128] 0100(30)
        when "0100011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(30) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(30); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(31)
        -- reciprocal of algor_plane_offset Vector [128] 0100(31)
        when "0100011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(31) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(31); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(32)
        -- reciprocal of algor_plane_offset Vector [128] 0100(32)
        when "0100100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(32) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(32); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(33)
        -- reciprocal of algor_plane_offset Vector [128] 0100(33)
        when "0100100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(33) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(33); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(34)
        -- reciprocal of algor_plane_offset Vector [128] 0100(34)
        when "0100100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(34) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(34); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(35)
        -- reciprocal of algor_plane_offset Vector [128] 0100(35)
        when "0100100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(35) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(35); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(36)
        -- reciprocal of algor_plane_offset Vector [128] 0100(36)
        when "0100100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(36) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(36); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(37)
        -- reciprocal of algor_plane_offset Vector [128] 0100(37)
        when "0100100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(37) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(37); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(38)
        -- reciprocal of algor_plane_offset Vector [128] 0100(38)
        when "0100100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(38) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(38); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(39)
        -- reciprocal of algor_plane_offset Vector [128] 0100(39)
        when "0100100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(39) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(39); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(40)
        -- reciprocal of algor_plane_offset Vector [128] 0100(40)
        when "0100101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(40) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(40); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(41)
        -- reciprocal of algor_plane_offset Vector [128] 0100(41)
        when "0100101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(41) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(41); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(42)
        -- reciprocal of algor_plane_offset Vector [128] 0100(42)
        when "0100101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(42) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(42); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(43)
        -- reciprocal of algor_plane_offset Vector [128] 0100(43)
        when "0100101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(43) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(43); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(44)
        -- reciprocal of algor_plane_offset Vector [128] 0100(44)
        when "0100101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(44) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(44); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(45)
        -- reciprocal of algor_plane_offset Vector [128] 0100(45)
        when "0100101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(45) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(45); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(46)
        -- reciprocal of algor_plane_offset Vector [128] 0100(46)
        when "0100101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(46) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(46); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(47)
        -- reciprocal of algor_plane_offset Vector [128] 0100(47)
        when "0100101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(47) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(47); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(48)
        -- reciprocal of algor_plane_offset Vector [128] 0100(48)
        when "0100110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(48) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(48); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(49)
        -- reciprocal of algor_plane_offset Vector [128] 0100(49)
        when "0100110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(49) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(49); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(50)
        -- reciprocal of algor_plane_offset Vector [128] 0100(50)
        when "0100110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(50) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(50); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(51)
        -- reciprocal of algor_plane_offset Vector [128] 0100(51)
        when "0100110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(51) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(51); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(52)
        -- reciprocal of algor_plane_offset Vector [128] 0100(52)
        when "0100110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(52) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(52); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(53)
        -- reciprocal of algor_plane_offset Vector [128] 0100(53)
        when "0100110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(53) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(53); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(54)
        -- reciprocal of algor_plane_offset Vector [128] 0100(54)
        when "0100110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(54) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(54); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(55)
        -- reciprocal of algor_plane_offset Vector [128] 0100(55)
        when "0100110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(55) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(55); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(56)
        -- reciprocal of algor_plane_offset Vector [128] 0100(56)
        when "0100111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(56) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(56); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(57)
        -- reciprocal of algor_plane_offset Vector [128] 0100(57)
        when "0100111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(57) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(57); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(58)
        -- reciprocal of algor_plane_offset Vector [128] 0100(58)
        when "0100111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(58) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(58); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(59)
        -- reciprocal of algor_plane_offset Vector [128] 0100(59)
        when "0100111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(59) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(59); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(60)
        -- reciprocal of algor_plane_offset Vector [128] 0100(60)
        when "0100111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(60) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(60); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(61)
        -- reciprocal of algor_plane_offset Vector [128] 0100(61)
        when "0100111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(61) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(61); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(62)
        -- reciprocal of algor_plane_offset Vector [128] 0100(62)
        when "0100111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(62) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(62); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(63)
        -- reciprocal of algor_plane_offset Vector [128] 0100(63)
        when "0100111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(63) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(63); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(64)
        -- reciprocal of algor_plane_offset Vector [128] 0100(64)
        when "0101000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(64) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(64); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(65)
        -- reciprocal of algor_plane_offset Vector [128] 0100(65)
        when "0101000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(65) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(65); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(66)
        -- reciprocal of algor_plane_offset Vector [128] 0100(66)
        when "0101000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(66) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(66); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(67)
        -- reciprocal of algor_plane_offset Vector [128] 0100(67)
        when "0101000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(67) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(67); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(68)
        -- reciprocal of algor_plane_offset Vector [128] 0100(68)
        when "0101000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(68) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(68); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(69)
        -- reciprocal of algor_plane_offset Vector [128] 0100(69)
        when "0101000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(69) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(69); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(70)
        -- reciprocal of algor_plane_offset Vector [128] 0100(70)
        when "0101000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(70) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(70); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(71)
        -- reciprocal of algor_plane_offset Vector [128] 0100(71)
        when "0101000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(71) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(71); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(72)
        -- reciprocal of algor_plane_offset Vector [128] 0100(72)
        when "0101001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(72) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(72); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(73)
        -- reciprocal of algor_plane_offset Vector [128] 0100(73)
        when "0101001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(73) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(73); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(74)
        -- reciprocal of algor_plane_offset Vector [128] 0100(74)
        when "0101001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(74) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(74); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(75)
        -- reciprocal of algor_plane_offset Vector [128] 0100(75)
        when "0101001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(75) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(75); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(76)
        -- reciprocal of algor_plane_offset Vector [128] 0100(76)
        when "0101001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(76) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(76); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(77)
        -- reciprocal of algor_plane_offset Vector [128] 0100(77)
        when "0101001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(77) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(77); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(78)
        -- reciprocal of algor_plane_offset Vector [128] 0100(78)
        when "0101001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(78) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(78); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(79)
        -- reciprocal of algor_plane_offset Vector [128] 0100(79)
        when "0101001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(79) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(79); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(80)
        -- reciprocal of algor_plane_offset Vector [128] 0100(80)
        when "0101010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(80) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(80); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(81)
        -- reciprocal of algor_plane_offset Vector [128] 0100(81)
        when "0101010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(81) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(81); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(82)
        -- reciprocal of algor_plane_offset Vector [128] 0100(82)
        when "0101010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(82) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(82); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(83)
        -- reciprocal of algor_plane_offset Vector [128] 0100(83)
        when "0101010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(83) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(83); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(84)
        -- reciprocal of algor_plane_offset Vector [128] 0100(84)
        when "0101010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(84) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(84); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(85)
        -- reciprocal of algor_plane_offset Vector [128] 0100(85)
        when "0101010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(85) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(85); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(86)
        -- reciprocal of algor_plane_offset Vector [128] 0100(86)
        when "0101010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(86) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(86); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(87)
        -- reciprocal of algor_plane_offset Vector [128] 0100(87)
        when "0101010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(87) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(87); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(88)
        -- reciprocal of algor_plane_offset Vector [128] 0100(88)
        when "0101011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(88) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(88); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(89)
        -- reciprocal of algor_plane_offset Vector [128] 0100(89)
        when "0101011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(89) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(89); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(90)
        -- reciprocal of algor_plane_offset Vector [128] 0100(90)
        when "0101011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(90) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(90); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(91)
        -- reciprocal of algor_plane_offset Vector [128] 0100(91)
        when "0101011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(91) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(91); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(92)
        -- reciprocal of algor_plane_offset Vector [128] 0100(92)
        when "0101011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(92) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(92); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(93)
        -- reciprocal of algor_plane_offset Vector [128] 0100(93)
        when "0101011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(93) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(93); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(94)
        -- reciprocal of algor_plane_offset Vector [128] 0100(94)
        when "0101011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(94) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(94); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(95)
        -- reciprocal of algor_plane_offset Vector [128] 0100(95)
        when "0101011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(95) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(95); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(96)
        -- reciprocal of algor_plane_offset Vector [128] 0100(96)
        when "0101100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(96) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(96); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(97)
        -- reciprocal of algor_plane_offset Vector [128] 0100(97)
        when "0101100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(97) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(97); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(98)
        -- reciprocal of algor_plane_offset Vector [128] 0100(98)
        when "0101100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(98) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(98); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(99)
        -- reciprocal of algor_plane_offset Vector [128] 0100(99)
        when "0101100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(99) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(99); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(100)
        -- reciprocal of algor_plane_offset Vector [128] 0100(100)
        when "0101100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(100) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(100); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(101)
        -- reciprocal of algor_plane_offset Vector [128] 0100(101)
        when "0101100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(101) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(101); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(102)
        -- reciprocal of algor_plane_offset Vector [128] 0100(102)
        when "0101100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(102) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(102); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(103)
        -- reciprocal of algor_plane_offset Vector [128] 0100(103)
        when "0101100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(103) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(103); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(104)
        -- reciprocal of algor_plane_offset Vector [128] 0100(104)
        when "0101101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(104) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(104); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(105)
        -- reciprocal of algor_plane_offset Vector [128] 0100(105)
        when "0101101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(105) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(105); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(106)
        -- reciprocal of algor_plane_offset Vector [128] 0100(106)
        when "0101101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(106) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(106); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(107)
        -- reciprocal of algor_plane_offset Vector [128] 0100(107)
        when "0101101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(107) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(107); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(108)
        -- reciprocal of algor_plane_offset Vector [128] 0100(108)
        when "0101101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(108) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(108); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(109)
        -- reciprocal of algor_plane_offset Vector [128] 0100(109)
        when "0101101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(109) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(109); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(110)
        -- reciprocal of algor_plane_offset Vector [128] 0100(110)
        when "0101101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(110) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(110); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(111)
        -- reciprocal of algor_plane_offset Vector [128] 0100(111)
        when "0101101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(111) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(111); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(112)
        -- reciprocal of algor_plane_offset Vector [128] 0100(112)
        when "0101110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(112) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(112); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(113)
        -- reciprocal of algor_plane_offset Vector [128] 0100(113)
        when "0101110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(113) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(113); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(114)
        -- reciprocal of algor_plane_offset Vector [128] 0100(114)
        when "0101110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(114) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(114); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(115)
        -- reciprocal of algor_plane_offset Vector [128] 0100(115)
        when "0101110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(115) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(115); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(116)
        -- reciprocal of algor_plane_offset Vector [128] 0100(116)
        when "0101110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(116) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(116); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(117)
        -- reciprocal of algor_plane_offset Vector [128] 0100(117)
        when "0101110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(117) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(117); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(118)
        -- reciprocal of algor_plane_offset Vector [128] 0100(118)
        when "0101110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(118) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(118); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(119)
        -- reciprocal of algor_plane_offset Vector [128] 0100(119)
        when "0101110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(119) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(119); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(120)
        -- reciprocal of algor_plane_offset Vector [128] 0100(120)
        when "0101111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(120) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(120); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(121)
        -- reciprocal of algor_plane_offset Vector [128] 0100(121)
        when "0101111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(121) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(121); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(122)
        -- reciprocal of algor_plane_offset Vector [128] 0100(122)
        when "0101111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(122) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(122); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(123)
        -- reciprocal of algor_plane_offset Vector [128] 0100(123)
        when "0101111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(123) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(123); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(124)
        -- reciprocal of algor_plane_offset Vector [128] 0100(124)
        when "0101111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(124) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(124); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(125)
        -- reciprocal of algor_plane_offset Vector [128] 0100(125)
        when "0101111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(125) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(125); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(126)
        -- reciprocal of algor_plane_offset Vector [128] 0100(126)
        when "0101111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(126) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(126); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_plane_offset_recip_0100(127)
        -- reciprocal of algor_plane_offset Vector [128] 0100(127)
        when "0101111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_plane_offset_recip_0100(127) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_plane_offset_recip_0100(127); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_mmfe8_invert_0180
        -- selct which MMFE8s are in an inverted location 0180
        when "0110000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_mmfe8_invert_0180 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_mmfe8_invert_0180; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_roi_map_wrAddr_0181
        -- ROI map (address is now inferred) RAM Memory wrAddr 0181
        when "0110000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_roi_map_wrAddr_0181 <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_roi_map_wrAddr_0181; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_roi_map_wrData_0182
        -- ROI map (address is now inferred) RAM Memory wrData 0182
        when "0110000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_roi_map_wrData_0182 <= reg_wr_data(15 downto 0); regs_SCAXtoUFL.MM_alg_algor_roi_map_wrEn_0182 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_alg_algor_roi_map_wrEn_0182 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_alg_algor_roi_map_wrEn_0182 <= '0'; reg_ack <= '0';
          end if;

        -- MM_alg_algor_mxlocal_parameters_wrAddr_0183
        -- MX local algorithm parameters (address is now inferred) RAM Memory wrAddr 0183
        when "0110000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_mxlocal_parameters_wrAddr_0183 <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_mxlocal_parameters_wrAddr_0183; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_mxlocal_parameters_wrData_0184
        -- MX local algorithm parameters (address is now inferred) RAM Memory wrData 0184
        when "0110000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_mxlocal_parameters_wrData_0184 <= reg_wr_data(15 downto 0); regs_SCAXtoUFL.MM_alg_algor_mxlocal_parameters_wrEn_0184 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_alg_algor_mxlocal_parameters_wrEn_0184 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_alg_algor_mxlocal_parameters_wrEn_0184 <= '0'; reg_ack <= '0';
          end if;

        -- MM_alg_algor_dtheta_parameters_wrAddr_0185
        -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrAddr 0185
        when "0110000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_dtheta_parameters_wrAddr_0185 <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_dtheta_parameters_wrAddr_0185; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_alg_algor_dtheta_parameters_wrData_0186
        -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrData 0186
        when "0110000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_dtheta_parameters_wrData_0186 <= reg_wr_data(15 downto 0); regs_SCAXtoUFL.MM_alg_algor_dtheta_parameters_wrEn_0186 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_alg_algor_dtheta_parameters_wrEn_0186 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_alg_algor_dtheta_parameters_wrEn_0186 <= '0'; reg_ack <= '0';
          end if;

        -- MM_alg_algor_region_mask_0187
        -- Mask algorithm regions 0187
        when "0110000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_alg_algor_region_mask_0187 <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.MM_alg_algor_region_mask_0187; reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate4;

generate5 : if (i2c_master_id = 5) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- MM_diag_diag_fifo_ef_0000
        -- Diagnostic FIFO empty flag 0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_diag_diag_fifo_ef_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_diag_diag_fifo_of_0001
        -- Diagnostic FIFO overflow flag 0001
        when "0000000001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_diag_diag_fifo_of_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_diag_diag_gbt_loopback_data_wrAddr_0002
        -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrAddr 0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_diag_diag_gbt_loopback_data_wrAddr_0002 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.MM_diag_diag_gbt_loopback_data_wrAddr_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- MM_diag_diag_gbt_loopback_data_wrData_0003
        -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrData 0003
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.MM_diag_diag_gbt_loopback_data_wrData_0003 <= reg_wr_data(31 downto 0); regs_SCAXtoUFL.MM_diag_diag_gbt_loopback_data_wrEn_0003 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.MM_diag_diag_gbt_loopback_data_wrEn_0003 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.MM_diag_diag_gbt_loopback_data_wrEn_0003 <= '0'; reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate5;

generate6 : if (i2c_master_id = 6) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- sTGC_lnk_GTH_MMC_RX_lock_0000
        --  0000
        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(2 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_MMC_RX_lock_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_TX_FSM_RESET_DONE_32_0001
        --  0001
        when "0000000001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_TX_FSM_RESET_DONE_32_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_RX_FSM_RESET_DONE_32_0002
        --  0002
        when "0000000010" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_RX_FSM_RESET_DONE_32_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cpllfbclklost_32_0003
        --  0003
        when "0000000011" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_cpllfbclklost_32_0003; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cplllock_32_0004
        --  0004
        when "0000000100" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_cplllock_32_0004; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_rxresetdone_32_0005
        --  0005
        when "0000000101" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_rxresetdone_32_0005; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_txresetdone_32_0006
        --  0006
        when "0000000110" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_txresetdone_32_0006; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_TX_FSM_RESET_DONE_4_0007
        --  0007
        when "0000000111" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_TX_FSM_RESET_DONE_4_0007; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_RX_FSM_RESET_DONE_4_0008
        --  0008
        when "0000001000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_RX_FSM_RESET_DONE_4_0008; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cpllfbclklost_4_0009
        --  0009
        when "0000001001" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_cpllfbclklost_4_0009; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cplllock_4_000a
        --  000a
        when "0000001010" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_cplllock_4_000a; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_rxresetdone_4_000b
        --  000b
        when "0000001011" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_rxresetdone_4_000b; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_txresetdone_4_000c
        --  000c
        when "0000001100" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_txresetdone_4_000c; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_QPLLLOCK_000d
        --  000d
        when "0000001101" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_QPLLLOCK_000d; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_QPLLREFCLKLOST_000e
        --  000e
        when "0000001110" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_QPLLREFCLKLOST_000e; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_QPLLRESET_000f
        --  000f
        when "0000001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_QPLLRESET_000f <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_QPLLRESET_000f <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_QPLLRESET_000f <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GlobalReset_0010
        --  0010
        when "0000010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GlobalReset_0010 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GlobalReset_0010 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GlobalReset_0010 <= '0'; reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_rxuserrdy_32_0011
        --  0011
        when "0000010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_rxuserrdy_32_0011 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_rxuserrdy_32_0011; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_rxuserrdy_4_0012
        --  0012
        when "0000010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_rxuserrdy_4_0012 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_rxuserrdy_4_0012; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_txuserrdy_32_0013
        --  0013
        when "0000010011" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_txuserrdy_32_0013; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_txuserrdy_4_0014
        --  0014
        when "0000010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_txuserrdy_4_0014 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_lnk_GTH_txuserrdy_4_0014; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cpllreset_32_0015
        --  0015
        when "0000010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_32_0015 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_32_0015 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_32_0015 <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_cpllreset_4_0016
        --  0016
        when "0000010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_4_0016 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_4_0016 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_cpllreset_4_0016 <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_gtrxreset_32_0017
        --  0017
        when "0000010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_32_0017 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_32_0017 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_32_0017 <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_gtrxreset_4_0018
        --  0018
        when "0000011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_4_0018 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_4_0018 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_gtrxreset_4_0018 <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_gttxreset_32_0019
        --  0019
        when "0000011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_32_0019 <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_32_0019 <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_32_0019 <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_GTH_gttxreset_4_001a
        --  001a
        when "0000011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_4_001a <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_4_001a <= (others => '0'); reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_lnk_GTH_gttxreset_4_001a <= (others => '0'); reg_ack <= '0';
          end if;

        -- sTGC_lnk_V7_ANLINK_RX_WR_EN_001b
        --  001b
        when "0000011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_V7_ANLINK_RX_WR_EN_001b <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_lnk_V7_ANLINK_RX_WR_EN_001b; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_V7_ANLINK_RX_RD_EN_001c
        --  001c
        when "0000011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_V7_ANLINK_RX_RD_EN_001c <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_lnk_V7_ANLINK_RX_RD_EN_001c; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_V7_ANLINK_TX_WR_EN_001d
        --  001d
        when "0000011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_V7_ANLINK_TX_WR_EN_001d <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_lnk_V7_ANLINK_TX_WR_EN_001d; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_lnk_V7_ANLINK_TX_RD_EN_001e
        --  001e
        when "0000011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_lnk_V7_ANLINK_TX_RD_EN_001e <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_lnk_V7_ANLINK_TX_RD_EN_001e; reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate6;

generate7 : if (i2c_master_id = 7) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- sTGC_V6_V6_global_RST_0000
        --  0000
        when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_global_RST_0000 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.sTGC_V6_V6_global_RST_0000 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.sTGC_V6_V6_global_RST_0000 <= '0'; reg_ack <= '0';
          end if;

        -- sTGC_V6_V6_UDP_GTX_RST_0001
        --  0001
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_UDP_GTX_RST_0001 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_V6_V6_UDP_GTX_RST_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_V6_V6_ANLINK_RX_WR_EN_0002
        --  0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_ANLINK_RX_WR_EN_0002 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_V6_V6_ANLINK_RX_WR_EN_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_V6_V6_ANLINK_RX_RD_EN_0003
        --  0003
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_ANLINK_RX_RD_EN_0003 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_V6_V6_ANLINK_RX_RD_EN_0003; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_V6_V6_ANLINK_TX_WR_EN_0004
        --  0004
        when "0000000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_ANLINK_TX_WR_EN_0004 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_V6_V6_ANLINK_TX_WR_EN_0004; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_V6_V6_ANLINK_TX_RD_EN_0005
        --  0005
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_V6_V6_ANLINK_TX_RD_EN_0005 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_V6_V6_ANLINK_TX_RD_EN_0005; reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate7;

generate8 : if (i2c_master_id = 8) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- sTGC_alg_radial_offset_0000(0)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(0)
        when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(0) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(1)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(1)
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(1) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(2)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(2)
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(2) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(3)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(3)
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(3) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(4)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(4)
        when "0000000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(4) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(5)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(5)
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(5) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(6)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(6)
        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(6) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(7)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(7)
        when "0000000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(7) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(8)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(8)
        when "0000001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(8) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(9)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(9)
        when "0000001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(9) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(10)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(10)
        when "0000001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(10) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(11)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(11)
        when "0000001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(11) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(12)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(12)
        when "0000001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(12) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(13)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(13)
        when "0000001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(13) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(14)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(14)
        when "0000001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(14) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(15)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(15)
        when "0000001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(15) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(16)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(16)
        when "0000010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(16) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(17)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(17)
        when "0000010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(17) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(18)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(18)
        when "0000010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(18) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(19)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(19)
        when "0000010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(19) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(20)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(20)
        when "0000010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(20) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(21)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(21)
        when "0000010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(21) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(22)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(22)
        when "0000010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(22) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_radial_offset_0000(23)
        -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(23)
        when "0000010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_radial_offset_0000(23) <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_radial_offset_0000(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(0)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(0)
        when "0000011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(1)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(1)
        when "0000011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(2)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(2)
        when "0000011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(3)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(3)
        when "0000011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(4)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(4)
        when "0000011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(4) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(5)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(5)
        when "0000011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(5) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(6)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(6)
        when "0000011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(6) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(7)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(7)
        when "0000011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(7) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(8)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(8)
        when "0000100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(8) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(9)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(9)
        when "0000100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(9) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(10)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(10)
        when "0000100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(10) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(11)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(11)
        when "0000100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(11) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(12)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(12)
        when "0000100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(12) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(13)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(13)
        when "0000100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(13) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(14)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(14)
        when "0000100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(14) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(15)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(15)
        when "0000100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(15) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(16)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(16)
        when "0000101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(16) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(17)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(17)
        when "0000101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(17) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(18)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(18)
        when "0000101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(18) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(19)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(19)
        when "0000101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(19) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(20)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(20)
        when "0000101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(20) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(21)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(21)
        when "0000101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(21) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(22)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(22)
        when "0000101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(22) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_plane_offset_0018(23)
        -- Distance from IP to plane, one per quadruplet Vector [24] 0018(23)
        when "0000101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_plane_offset_0018(23) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_plane_offset_0018(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_max_cluster_size_0030
        --  0030
        when "0000110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_max_cluster_size_0030 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_max_cluster_size_0030; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_min_cluster_size_0031
        --  0031
        when "0000110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_min_cluster_size_0031 <= reg_wr_data(3 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(3 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_min_cluster_size_0031; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(0)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(0)
        when "0000110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(0) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(1)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(1)
        when "0000110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(1) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(2)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(2)
        when "0000110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(2) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(3)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(3)
        when "0000110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(3) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(4)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(4)
        when "0000110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(4) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(5)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(5)
        when "0000110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(5) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(6)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(6)
        when "0000111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(6) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(7)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(7)
        when "0000111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(7) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(8)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(8)
        when "0000111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(8) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(9)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(9)
        when "0000111011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(9) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(10)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(10)
        when "0000111100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(10) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(11)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(11)
        when "0000111101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(11) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(12)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(12)
        when "0000111110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(12) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(13)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(13)
        when "0000111111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(13) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(14)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(14)
        when "0001000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(14) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(15)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(15)
        when "0001000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(15) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(16)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(16)
        when "0001000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(16) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(17)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(17)
        when "0001000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(17) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(18)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(18)
        when "0001000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(18) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(19)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(19)
        when "0001000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(19) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(20)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(20)
        when "0001000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(20) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(21)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(21)
        when "0001000111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(21) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(22)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(22)
        when "0001001000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(22) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_cluster_0032(23)
        -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(23)
        when "0001001001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_cluster_0032(23) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_cluster_0032(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(0)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(0)
        when "0001001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(0) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(1)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(1)
        when "0001001011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(1) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(2)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(2)
        when "0001001100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(2) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(3)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(3)
        when "0001001101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(3) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(4)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(4)
        when "0001001110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(4) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(5)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(5)
        when "0001001111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(5) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(6)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(6)
        when "0001010000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(6) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(7)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(7)
        when "0001010001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(7) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(8)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(8)
        when "0001010010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(8) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(9)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(9)
        when "0001010011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(9) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(10)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(10)
        when "0001010100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(10) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(11)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(11)
        when "0001010101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(11) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(12)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(12)
        when "0001010110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(12) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(13)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(13)
        when "0001010111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(13) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(14)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(14)
        when "0001011000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(14) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(15)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(15)
        when "0001011001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(15) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(16)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(16)
        when "0001011010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(16) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(17)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(17)
        when "0001011011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(17) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(18)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(18)
        when "0001011100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(18) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(19)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(19)
        when "0001011101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(19) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(20)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(20)
        when "0001011110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(20) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(21)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(21)
        when "0001011111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(21) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(22)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(22)
        when "0001100000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(22) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_charge_thresh_centroid_004a(23)
        -- charge threshold for determining centroid, per gas gap Vector [24] 004a(23)
        when "0001100001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_charge_thresh_centroid_004a(23) <= reg_wr_data(5 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(5 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_charge_thresh_centroid_004a(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_strip_dir_0062
        -- strip direction (+ / -) list 0062
        when "0001100010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_strip_dir_0062 <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_strip_dir_0062; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(0)
        -- radial offset (per gas gap) Vector [24] 0063(0)
        when "0001100011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(1)
        -- radial offset (per gas gap) Vector [24] 0063(1)
        when "0001100100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(2)
        -- radial offset (per gas gap) Vector [24] 0063(2)
        when "0001100101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(3)
        -- radial offset (per gas gap) Vector [24] 0063(3)
        when "0001100110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(4)
        -- radial offset (per gas gap) Vector [24] 0063(4)
        when "0001100111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(4) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(4); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(5)
        -- radial offset (per gas gap) Vector [24] 0063(5)
        when "0001101000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(5) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(5); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(6)
        -- radial offset (per gas gap) Vector [24] 0063(6)
        when "0001101001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(6) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(6); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(7)
        -- radial offset (per gas gap) Vector [24] 0063(7)
        when "0001101010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(7) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(7); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(8)
        -- radial offset (per gas gap) Vector [24] 0063(8)
        when "0001101011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(8) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(8); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(9)
        -- radial offset (per gas gap) Vector [24] 0063(9)
        when "0001101100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(9) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(9); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(10)
        -- radial offset (per gas gap) Vector [24] 0063(10)
        when "0001101101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(10) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(10); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(11)
        -- radial offset (per gas gap) Vector [24] 0063(11)
        when "0001101110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(11) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(11); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(12)
        -- radial offset (per gas gap) Vector [24] 0063(12)
        when "0001101111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(12) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(12); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(13)
        -- radial offset (per gas gap) Vector [24] 0063(13)
        when "0001110000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(13) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(13); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(14)
        -- radial offset (per gas gap) Vector [24] 0063(14)
        when "0001110001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(14) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(14); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(15)
        -- radial offset (per gas gap) Vector [24] 0063(15)
        when "0001110010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(15) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(15); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(16)
        -- radial offset (per gas gap) Vector [24] 0063(16)
        when "0001110011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(16) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(16); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(17)
        -- radial offset (per gas gap) Vector [24] 0063(17)
        when "0001110100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(17) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(17); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(18)
        -- radial offset (per gas gap) Vector [24] 0063(18)
        when "0001110101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(18) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(18); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(19)
        -- radial offset (per gas gap) Vector [24] 0063(19)
        when "0001110110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(19) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(19); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(20)
        -- radial offset (per gas gap) Vector [24] 0063(20)
        when "0001110111" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(20) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(20); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(21)
        -- radial offset (per gas gap) Vector [24] 0063(21)
        when "0001111000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(21) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(21); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(22)
        -- radial offset (per gas gap) Vector [24] 0063(22)
        when "0001111001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(22) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(22); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_alg_R_offset_0063(23)
        -- radial offset (per gas gap) Vector [24] 0063(23)
        when "0001111010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_alg_R_offset_0063(23) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_alg_R_offset_0063(23); reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate8;

generate9 : if (i2c_master_id = 9) generate

  fileMux: process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        -- sTGC_mrg_enable_MM_segments_0000
        --  0000
        when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_enable_MM_segments_0000 <= reg_wr_data(7 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(7 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_enable_MM_segments_0000; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_enable_SL_output_0001
        --  0001
        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_enable_SL_output_0001 <= reg_wr_data(0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(0) <= regs_UFLtoSCAX_i.sTGC_mrg_enable_SL_output_0001; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_SL_out_offset_0002
        -- number of 320MHz clocks to wait before output to SL 0002
        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_SL_out_offset_0002 <= reg_wr_data(2 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(2 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_SL_out_offset_0002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_fiber_ctrl_param_0003(0)
        -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(0)
        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_fiber_ctrl_param_0003(0) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_fiber_ctrl_param_0003(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_fiber_ctrl_param_0003(1)
        -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(1)
        when "0000000100" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_fiber_ctrl_param_0003(1) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_fiber_ctrl_param_0003(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_fiber_ctrl_param_0003(2)
        -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(2)
        when "0000000101" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_fiber_ctrl_param_0003(2) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_fiber_ctrl_param_0003(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        -- sTGC_mrg_fiber_ctrl_param_0003(3)
        -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(3)
        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.sTGC_mrg_fiber_ctrl_param_0003(3) <= reg_wr_data(15 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(15 downto 0) <= regs_UFLtoSCAX_i.sTGC_mrg_fiber_ctrl_param_0003(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;


        when others =>
          reg_ack <= '0';

      end case;
    end if;
  end process;

end generate generate9;


end RTL;
