----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.09.2018 13:45:47
-- Design Name: SCAX Watchdog
-- Module Name: scax_watchdog - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: SCAX Watchdog. Only a reset manager for now
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity scax_watchdog is
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    rst_req_int : in  std_logic; 
    rst_req_ext : in  std_logic; 
    rst_out     : out std_logic
  ); 
end scax_watchdog; 

architecture RTL of scax_watchdog is
  
  signal rst_step0 : std_logic := '0'; 
  signal rst_step1 : std_logic := '0'; 
  signal rst_out_i : std_logic := '0'; 
  signal rst_cnt   : unsigned(7 downto 0) := (others => '0'); 
  
begin
  
-- reset managing process
rst_manager_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst_step1 = '1')then
      rst_cnt   <= rst_cnt + 1; 
      rst_step0 <= '0'; 
      
      if(rst_cnt = "11111111")then
        rst_step1 <= '0'; 
      else
        rst_step1 <= '1'; 
      end if; 
      
      if(rst_cnt > "11100000" and rst_cnt < "11111111")then
        rst_out_i <= '1'; 
      else
        rst_out_i <= '0'; 
      end if; 
      
    elsif(rst_step0 = '1')then
      rst_cnt   <= rst_cnt + 1; 
      rst_out_i <= '0'; 
      
      if(rst_cnt = "11111111")then
        rst_step0 <= '0'; 
        rst_step1 <= '1'; 
      else
        rst_step0 <= '1'; 
        rst_step1 <= '0'; 
      end if; 
      
    elsif(rst_req_int = '1')then
      rst_cnt   <= (others => '0'); 
      rst_out_i <= '0'; 
      rst_step0 <= '1'; 
      rst_step1 <= '0'; 
    else
      rst_cnt   <= (others => '0'); 
      rst_out_i <= '0'; 
      rst_step0 <= '0'; 
      rst_step1 <= '0'; 
    end if; 
  end if; 
end process; 
  
-- reset fan-out (this register has to be a false_path)
rstFanOut_proc : process(clk)
begin
  if(rising_edge(clk))then
    rst_out <= rst_out_i or rst_req_ext; 
  --rst_out <= rst_req_ext;   
  end if; 
end process; 

end RTL; 
