----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.10.2018 18:08:12
-- Design Name: I2C Master
-- Module Name: i2c_master - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: I2C Master. Can read/write from/to a register file.
-- 
-- Dependencies:
-- 
-- Changelog:
-- 14.02.2018 Changes according to OPC-client/server handling of commands.
-- Now supports only 32-bit read/write commands. For a 32-bit write SCAX receives
-- a nByte = 8. For a 32-bit read SCAX receives a nByte = 4, and consecutive write
-- and read commands. The nByte = 4 write is ignored. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.scax_package.all; 

entity i2c_master is
  generic(
      i2c_master_id : integer := 0;
      is_cdc        : std_logic := '0');
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    clk_regFile   : in  std_logic; 
    ena_i2c       : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack       : in  std_logic; 
    reg_rd_data   : in  std_logic_vector(31 downto 0); 
    reg_wr        : out std_logic; 
    reg_rd        : out std_logic; 
    reg_addr      : out std_logic_vector(9 downto 0); 
    reg_wr_data   : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Router Interface -----
    frameRX       : in  frame_rx; 
    frame_newRX   : in  std_logic; 
    frame_sizeRX  : in  std_logic_vector(1 downto 0); 
    -----------------------------
    ----- Elink TX Interface -----
    frameTX       : out frame_tx; 
    frame_newTX   : out std_logic; 
    frame_sizeTX  : out std_logic_vector(1 downto 0)
  ); 
end i2c_master; 

architecture RTL of i2c_master is
  
component i2c_readWrite_manager
  generic(is_cdc : std_logic := '0'); 
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    clk_regFile : in  std_logic; 
    ena_i2c     : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack     : in  std_logic; 
    reg_rd_data : in  std_logic_vector(31 downto 0); 
    reg_wr      : out std_logic; 
    reg_rd      : out std_logic; 
    reg_addr    : out std_logic_vector(9 downto 0); 
    reg_wr_data : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       : in  std_logic; 
    start_read  : in  std_logic; 
    mode        : in  std_logic_vector(2 downto 0);  -- how many?
    data0       : in  std_logic_vector(31 downto 0); -- for multi-byte
    data1       : in  std_logic_vector(31 downto 0); -- for multi-byte
    addrData    : in  std_logic_vector(17 downto 0); -- for single-byte
    -- to master
    mgr_data0   : out std_logic_vector(31 downto 0); 
    mgr_data1   : out std_logic_vector(31 downto 0); 
    mgr_ack     : out std_logic; 
    mgr_done_wr : out std_logic; 
    mgr_done_rd : out std_logic
  ); 
end component; 
  
  signal wait_cnt         : unsigned(3 downto 0) := (others => '0'); 
  signal frameRX_i        : frame_rx; 
  
  signal frameTX_i        : frame_tx; 
  signal frame_newTX_i    : std_logic := '0';
  signal frame_sizeTX_i   : std_logic_vector(1 downto 0) := (others => '0'); 
  
  signal write_reg        : std_logic := '0'; 
  signal read_reg         : std_logic := '0'; 
  signal start            : std_logic := '0'; 
  signal start_write      : std_logic := '0'; 
  signal start_read       : std_logic := '0'; 
  signal start_read_i     : std_logic := '0'; 
  signal send_status      : std_logic := '0'; 
  signal send_stData      : std_logic := '0'; 
  signal fake_ack         : std_logic := '0';
  signal mgr_ack          : std_logic := '0'; 
  
  signal addrData         : std_logic_vector(17 downto 0) := (others => '0'); 
  signal data0            : std_logic_vector(31 downto 0) := (others => '0'); 
  signal data1            : std_logic_vector(31 downto 0) := (others => '0'); 
  signal data3            : std_logic_vector(31 downto 0) := (others => '0'); 
  signal mgr_data0        : std_logic_vector(31 downto 0) := (others => '0'); 
  signal mgr_data1        : std_logic_vector(31 downto 0) := (others => '0'); 
  signal status           : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal status_wr        : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal status_rd        : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal control          : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal nByte            : std_logic_vector(4 downto 0)  := (others => '0'); 
  
  signal write_done       : std_logic := '0'; 
  signal read_done        : std_logic := '0'; 
  signal write_done_i     : std_logic := '0'; 
  signal read_done_i      : std_logic := '0'; 
  signal mgr_ack_i        : std_logic := '0'; 
  signal write_from_file  : std_logic := '0'; 
  signal byteNumber_01    : std_logic := '0'; 
  signal byteNumber_04    : std_logic := '0'; 
  signal byteNumber_08    : std_logic := '0'; 
  signal mode             : std_logic_vector(2 downto 0) := (others => '0'); 
  
  type stateType is (ST_IDLE, ST_REG_FRM, ST_CHK_CMD, ST_DEF_REPLY, ST_WAIT_SLAVE, ST_SEND_REPLY, ST_DONE); 
  signal state : stateType := ST_IDLE; 

begin

-- main I2C FSM
mainFSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(ena_i2c = '0')then
      wait_cnt      <= (others => '0'); 
      write_reg     <= '0'; 
      read_reg      <= '0'; 
      start_write   <= '0'; 
      start_read    <= '0'; 
      frameTX_i.err <= error_none; 
      status        <= (others => '0'); 
      frame_newTX_i <= '0';
      send_status   <= '0'; 
      send_stData   <= '0'; 
      fake_ack      <= '0'; 
      state         <= ST_IDLE; 
    else
      case state is
          
        -- wait for a new frame to come
        when ST_IDLE => 
          frame_newTX_i <= '0';
          fake_ack      <= '0'; 
          
          if(frame_newRX = '1')then
            wait_cnt <= wait_cnt + 1; 
            if(wait_cnt = "0011")then
              state <= ST_REG_FRM; 
            else
              state <= ST_IDLE; 
            end if; 
          else
            wait_cnt <= (others => '0'); 
            state    <= ST_IDLE; 
          end if; 
          
        -- reg the frame
        when ST_REG_FRM =>
          wait_cnt  <= (others => '0');
          frameRX_i <= frameRX;
          state     <= ST_CHK_CMD;  
          
        -- check the command field
        when ST_CHK_CMD => 
          
          state <= ST_DEF_REPLY; 
          
          case frameRX_i.command is
            when I2C_W_CTRL | I2C_W_DATA0 | I2C_W_DATA1 => 
              write_reg      <= '1'; 
              read_reg       <= '0'; 
              start_write    <= '0'; 
              start_read     <= '0'; 
              frame_sizeTX_i <= "01"; 
              frameTX_i.err  <= error_none; -- ok ack, send no bytes
            when I2C_R_CTRL | I2C_R_STR => 
              write_reg      <= '0'; 
              read_reg       <= '1'; 
              start_write    <= '0'; 
              start_read     <= '0'; 
              frame_sizeTX_i <= "10"; -- ok ack, send 8 bytes
              frameTX_i.err  <= error_none; 
            when I2C_R_DATA0 | I2C_R_DATA1 | I2C_R_DATA3 => 
              write_reg      <= '0'; 
              read_reg       <= '1'; 
              start_write    <= '0'; 
              start_read     <= '0'; 
              frame_sizeTX_i <= "11"; -- ok ack, send 32 bytes
              frameTX_i.err  <= error_none; 
            when I2C_M_10B_W => -- status to be defined later
              write_reg      <= '0'; 
              read_reg       <= '0';
              start_write    <= '1'; 
              start_read     <= '0'; 
              frame_sizeTX_i <= "10"; 
              frameTX_i.err  <= error_none; -- ok ack, send no bytes
              
              if(mode = "100")then -- if 8-byte transfer, it is actually a 4-byte write, proceed
                start_write <= '1'; 
                fake_ack    <= '0'; 
              else -- if 4-byte transfer, it is actually a 4-byte read, ignore the write
                start_write <= '0'; 
                fake_ack    <= '1'; 
              end if; 
              
            when I2C_M_10B_R => 
              write_reg      <= '0'; 
              read_reg       <= '0'; 
              start_write    <= '0'; 
              start_read     <= '1'; 
              frame_sizeTX_i <= "10"; 
              frameTX_i.err  <= error_none; -- ok ack, send no bytes, status register to be defined later
            when others => 
              write_reg      <= '0'; 
              read_reg       <= '0'; 
              start_write    <= '0'; 
              start_read     <= '0'; 
              frame_sizeTX_i <= "01"; -- error
              frameTX_i.err  <= error_invChCmd; 
          end case; 
          
        -- what to send back?
        when ST_DEF_REPLY => 
          write_reg   <= '0'; 
          read_reg    <= '0'; 
          start_write <= '0'; 
          start_read  <= '0'; 
          
          if(fake_ack = '1')then
            state <= ST_WAIT_SLAVE; 
          elsif(write_reg = '1')then
            state <= ST_SEND_REPLY; 
          elsif(read_reg = '1')then
            state <= ST_SEND_REPLY; 
          elsif(start_write = '1' or start_read = '1')then
            state <= ST_WAIT_SLAVE; 
          else
            state <= ST_SEND_REPLY; -- that was an invalid command...
          end if; 
          
        -- wait for the slave to finish
        when ST_WAIT_SLAVE => 
          if(fake_ack = '1')then
            status      <= status_wr; 
            send_status <= '1'; -- just send a good status, fake write
            send_stData <= '0'; 
            state       <= ST_SEND_REPLY; 
          elsif(write_done = '1')then
            status      <= status_wr; 
            send_status <= '1'; -- just send the status
            send_stData <= '0'; 
            state       <= ST_SEND_REPLY; 
          elsif(read_done = '1' and (mode = "100" or mode = "010"))then
            status      <= status_rd; 
            send_status <= '1'; -- just send the status, multi-byte read
            send_stData <= '0'; 
            state       <= ST_SEND_REPLY; 
          else
            status      <= status; 
            send_status <= '0'; 
            send_stData <= '0'; 
            state       <= ST_WAIT_SLAVE; 
          end if; 
          
        -- wait before sending the reply
        when ST_SEND_REPLY => 
          send_status <= '0'; 
          send_stData <= '0'; 
          wait_cnt    <= wait_cnt + 1; 
          
          if(wait_cnt = "1111")then
            state <= ST_DONE; 
          else
            state <= ST_SEND_REPLY; 
          end if; 
          
        -- done, send reply
        when ST_DONE => 
          frame_newTX_i <= '1';
          
          if(frame_newRX = '0')then
            state <= ST_IDLE; 
          else
            state <= ST_DONE; 
          end if; 
          
      end case; 
    end if; 
  end if; 
end process; 
  
-- command demux
cmd_demux_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(ena_i2c = '0')then
      control          <= (others => '0'); 
      data0            <= (others => '0'); 
      data1            <= (others => '0'); 
      data3            <= (others => '0'); 
      frameTX_i.dout_a <= (others => '0'); 
      frameTX_i.dout_b <= (others => '0'); 
    else
      if(write_reg = '1')then
        case frameRX_i.command is
          when I2C_W_CTRL   => control <= frameRX_i.din_a(7 downto 0); data0 <= data0; data1 <= data1; 
          when I2C_W_DATA0  => control <= control; data0(15 downto 0) <= frameRX_i.din_a; data0(31 downto 16) <= frameRX_i.din_b; data1 <= data1; 
          when I2C_W_DATA1  => control <= control; data0 <= data0; data1(15 downto 0) <= frameRX_i.din_a; data1(31 downto 16) <= frameRX_i.din_b; 
          when others       => control <= control; data0 <= data0; data1 <= data1; 
        end case; 
        
        data3 <= data3; 
        
      elsif(write_from_file = '1')then
        data3 <= mgr_data0; -- the only possibility
      else
        control <= control; data0 <= data0; data1 <= data1; data3 <= data3; 
      end if; 
      
      if(read_reg = '1')then
        case frameRX_i.command is
          when I2C_R_CTRL   => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= control; frameTX_i.dout_b <= (others => '0'); 
          when I2C_R_STR    => frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= status; frameTX_i.dout_b <= (others => '0'); 
          when I2C_R_DATA0  => frameTX_i.dout_a <= data0(15 downto 0); frameTX_i.dout_b <= data0(31 downto 16); -- !!!!!! maybe the other way around? 
          when I2C_R_DATA1  => frameTX_i.dout_a <= data1(15 downto 0); frameTX_i.dout_b <= data1(31 downto 16); -- !!!!!! maybe the other way around? 
          when I2C_R_DATA3  => frameTX_i.dout_a <= data3(15 downto 0); frameTX_i.dout_b <= data3(31 downto 16); -- !!!!!! maybe the other way around?
          when others       => frameTX_i.dout_a <= frameTX_i.dout_a; frameTX_i.dout_b <= frameTX_i.dout_b; 
        end case; 
      elsif(send_status = '1')then
        frameTX_i.dout_a(15 downto 8) <= (others => '0'); frameTX_i.dout_a(7 downto 0) <= status; frameTX_i.dout_b <= (others => '0'); 
      elsif(send_stData = '1')then
        frameTX_i.dout_a(15 downto 8) <= mgr_data0(7 downto 0); frameTX_i.dout_a(7 downto 0) <= status; frameTX_i.dout_b <= (others => '0'); 
      else
        frameTX_i.dout_a <= frameTX_i.dout_a; frameTX_i.dout_b <= frameTX_i.dout_b; 
      end if; 
    end if; 
  end if; 
end process; 
  
-- address grabber, also pipeline
addr_grabber_pipe_proc : process(clk)
begin
  if(rising_edge(clk))then
    
    -- pipeline
    start        <= start_read or start_write; 
    start_read_i <= start_read; 
    
    if(start_write = '1' or start_read = '1')then
      addrData(17 downto 8) <= data0(9 downto 0); addrData(7 downto 0) <= data0(15 downto 8); 
    else
      addrData <= addrData; 
    end if; 
    
  end if; 
end process; 
  
-- pipeline the TX data
pipeTX_proc : process(clk)
begin
  if(rising_edge(clk))then
    frameTX      <= frameTX_i; 
    frame_sizeTX <= frame_sizeTX_i; 
    frame_newTX  <= frame_newTX_i;
  end if; 
end process; 

-- what kind of transaction?
multiSingle_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(unsigned(nByte) = "00001")then -- one byte
      byteNumber_01 <= '1'; 
      byteNumber_04 <= '0'; 
      byteNumber_08 <= '0'; 
    elsif(unsigned(nByte) = "00100")then -- four bytes
      byteNumber_01 <= '0'; 
      byteNumber_04 <= '1'; 
      byteNumber_08 <= '0'; 
    elsif(unsigned(nByte) = "01000")then -- eight bytes
      byteNumber_01 <= '0'; 
      byteNumber_04 <= '0'; 
      byteNumber_08 <= '1'; 
    else -- ... what was that? (multibyte but no valid nByte value)
      byteNumber_01 <= '0'; 
      byteNumber_04 <= '1'; -- now only supports 32
      byteNumber_08 <= '0'; 
    end if; 
  end if; 
end process; 
  
-- grab the status and the data fields
statusData_grabber_proc : process(clk)
begin
  if(rising_edge(clk))then
    
    write_done    <= write_done_i;
    mgr_ack       <= mgr_ack_i;

    if(fake_ack = '1')then
      status_wr <= status_good; -- always good
    elsif(write_done_i = '1' and (mgr_ack_i = '1' or mgr_ack = '1'))then -- stretch the ack
      status_wr <= status_good; 
    elsif(write_done_i = '1')then
      status_wr <= status_noAck; 
    else
      status_wr <= status_wr; 
    end if; 
    
    read_done <= read_done_i; 
    
    if(read_done_i = '1' and (mgr_ack_i = '1' or mgr_ack = '1'))then  -- stretch the ack
      status_rd       <= status_good; 
      write_from_file <= '1'; 
    elsif(read_done_i = '1')then
      status_rd       <= status_noAck; 
      write_from_file <= '0'; 
    else
      status_rd       <= status_wr; 
      write_from_file <= '0'; 
    end if; 
    
  end if; 
end process; 
  
i2c_readWrite_manager_inst : i2c_readWrite_manager
  generic map(is_cdc => is_cdc)
  port map(
    ------------------------------
    ------ General Interface -----
    clk         => clk,
    clk_regFile => clk_regFile,
    ena_i2c     => ena_i2c,
    ------------------------------
    -- Register File Interface ---
    reg_ack     => reg_ack,
    reg_rd_data => reg_rd_data,
    reg_wr      => reg_wr,
    reg_rd      => reg_rd,
    reg_addr    => reg_addr,
    reg_wr_data => reg_wr_data,
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       => start,
    start_read  => start_read_i,
    mode        => "010", -- fixed to 4-byte transfer
    data0       => data0,
    data1       => data1,
    addrData    => addrData,
    -- to master
    mgr_data0   => mgr_data0,
    mgr_data1   => mgr_data1,
    mgr_ack     => mgr_ack_i,
    mgr_done_wr => write_done_i,
    mgr_done_rd => read_done_i
  );
  
  nByte   <= control(6 downto 2); 

  mode(2) <= byteNumber_08; 
  mode(1) <= byteNumber_04; 
  mode(0) <= byteNumber_01; 
  
end RTL; 

