-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.10.2018 18:08:12
-- Design Name: Register File
-- Module Name: registerFile - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Dummy register file for an I2C master.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 
use work.registerFile_package.all;

entity registerFile is
  generic(i2c_master_id : integer := 0); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock (SCAX) or registerFile clock
    ------------------------------
    -- I2C Master Interface ------
    reg_wr          : in  std_logic; 
    reg_rd          : in  std_logic; 
    reg_addr        : in  std_logic_vector(9 downto 0); 
    reg_wr_data     : in  std_logic_vector(31 downto 0); 
    reg_ack         : out std_logic; 
    reg_rd_data     : out std_logic_vector(31 downto 0); 
    ------------------------------
    -- FPGA Register Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL  -- used when writing
  ); 
end registerFile; 

architecture RTL of registerFile is

  signal regs_UFLtoSCAX_i : UFLtoSCAX;

begin

int_reg_proc: process(clk)
begin
  if(rising_edge(clk))then
    regs_UFLtoSCAX_i <= regs_UFLtoSCAX;  
  end if;
end process;
  
generate0 : if (i2c_master_id = 0) generate
  fileMux : process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        when "0000000000" =>
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy32_000; reg_ack <= '1';
          else reg_ack <= '0';
          end if; 

        when "0000000011" => 
          if(reg_wr = '1')then regs_SCAXtoUFL.master00_dummy4of32_003(0) <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy4of32_003(0); reg_ack <= '1';
          else reg_ack <= '0';
          end if; 

        when "0000000100" =>  
          if(reg_wr = '1')then regs_SCAXtoUFL.master00_dummy4of32_003(1) <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy4of32_003(1); reg_ack <= '1';
          else reg_ack <= '0';
          end if; 

        when "0000000101" =>  
          if(reg_wr = '1')then regs_SCAXtoUFL.master00_dummy4of32_003(2) <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy4of32_003(2); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        when "0000000110" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master00_dummy4of32_003(3) <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy4of32_003(3); reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        when "0000001010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master00_dummy32_00a <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master00_dummy32_00a; reg_ack <= '1';
          else reg_ack <= '0';
          end if;
          
        when others => reg_ack <= '0';
      end case; 
    end if; 
  end process; 

end generate generate0; 
    
generate1 : if (i2c_master_id = 1) generate

  fileMux : process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

        when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master01_rdAddr12_000 <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.master01_rdAddr12_000; reg_ack <= '1';
          else reg_ack <= '0';
          end if; 

        when "0000000001" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master01_rdEn_001 <= '0'; reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master01_rdData32_001; regs_SCAXtoUFL.master01_rdEn_001 <= '1'; reg_ack <= '1';
          else regs_SCAXtoUFL.master01_rdEn_001 <= '0'; reg_ack <= '0';
          end if; 

        when "0000000010" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master01_wrAddr12_002 <= reg_wr_data(11 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(11 downto 0) <= regs_UFLtoSCAX_i.master01_wrAddr12_002; reg_ack <= '1';
          else reg_ack <= '0';
          end if;

        when "0000000011" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master01_wrData32_003 <= reg_wr_data(31 downto 0); regs_SCAXtoUFL.master01_wrEn_003 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.master01_wrEn_003 <= '0'; reg_ack <= '0';
          else regs_SCAXtoUFL.master01_wrEn_003 <= '0'; reg_ack <= '0';
          end if; 

        when "1110101011" =>  
          if(reg_wr = '1')then reg_ack <= '0';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master01_dummy32_3ab; reg_ack <= '1';
          else reg_ack <= '0';
          end if; 

        when "1111111111" => 
          if(reg_wr = '1')then regs_SCAXtoUFL.master01_dummy32_3ff <= reg_wr_data(31 downto 0); reg_ack <= '1';
          elsif(reg_rd = '1')then reg_rd_data(31 downto 0) <= regs_UFLtoSCAX_i.master01_dummy32_3ff; reg_ack <= '1';
          else reg_ack <= '0';
          end if;
          
        when others => reg_ack <= '0';
      end case; 
    end if; 
  end process; 

end generate generate1; 
      
generate2 : if (i2c_master_id = 2) generate

  fileMux : process(clk)
  begin
    if(rising_edge(clk))then
      case reg_addr is

      when "0000000000" =>
          if(reg_wr = '1')then regs_SCAXtoUFL.master02_rst_000 <= '1'; reg_ack <= '1';
          elsif(reg_rd = '1')then regs_SCAXtoUFL.master02_rst_000 <= '0';  reg_ack <= '0'; 
          else regs_SCAXtoUFL.master02_rst_000 <= '0'; reg_ack <= '0';
          end if; 

      when others => reg_ack <= '0';
      
      end case;
    end if;
  end process;
end generate generate2; 

generate3 : if (i2c_master_id = 3) generate
end generate generate3; 
          
generate4 : if (i2c_master_id = 4) generate
end generate generate4; 
            
generate5 : if (i2c_master_id = 5) generate
end generate generate5; 
              
generate6 : if (i2c_master_id = 6) generate
end generate generate6; 
                  
generate7 : if (i2c_master_id = 7) generate
end generate generate7; 
                  
generate8 : if (i2c_master_id = 8) generate
end generate generate8; 
                    
generate9 : if (i2c_master_id = 9) generate
end generate generate9; 
                      
generate10 : if (i2c_master_id = 10) generate
end generate generate10; 

generate11 : if (i2c_master_id = 11) generate
end generate generate11; 

generate12 : if (i2c_master_id = 12) generate
end generate generate12; 

generate13 : if (i2c_master_id = 13) generate
end generate generate13; 

generate14 : if (i2c_master_id = 14) generate
end generate generate14; 

generate15 : if (i2c_master_id = 15) generate
end generate generate15; 

end RTL; 

