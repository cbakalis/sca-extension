----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.13.2018 11:37:06
-- Design Name: I2C CDC Manager
-- Module Name: i2c_cdc_manager - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: I2C CDC Read/Write Manager. Writes into the register file, through
-- a CDC FIFO.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

entity i2c_cdc_manager is
  port(
    ------------------------------
    ------ General Interface -----
    clk         : in  std_logic; 
    clk_regFile : in  std_logic; 
    rst         : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    reg_ack     : in  std_logic; 
    reg_rd_data : in  std_logic_vector(31 downto 0); 
    reg_wr      : out std_logic; 
    reg_rd      : out std_logic; 
    reg_addr    : out std_logic_vector(9 downto 0); 
    reg_wr_data : out std_logic_vector(31 downto 0); 
    ------------------------------
    --- I2C Master Interface -----
    -- from master
    start       : in  std_logic; 
    start_read  : in  std_logic; 
    mode        : in  std_logic_vector(2 downto 0);  -- how many?
    data0       : in  std_logic_vector(31 downto 0); -- for multi-byte
    data1       : in  std_logic_vector(31 downto 0); -- for multi-byte
    addrData    : in  std_logic_vector(17 downto 0); -- for single-byte
    -- to master
    mgr_data0   : out std_logic_vector(31 downto 0); 
    mgr_data1   : out std_logic_vector(31 downto 0); 
    mgr_ack     : out std_logic; 
    mgr_done_wr : out std_logic; 
    mgr_done_rd : out std_logic
  ); 
end i2c_cdc_manager; 

architecture RTL of i2c_cdc_manager is
  
component i2c_cdc_buffer
  port(
    rst         : in  std_logic; 
    wr_clk      : in  std_logic; 
    rd_clk      : in  std_logic; 
    din         : in  std_logic_vector(31 downto 0); 
    wr_en       : in  std_logic; 
    rd_en       : in  std_logic; 
    dout        : out std_logic_vector(31 downto 0); 
    full        : out std_logic; 
    empty       : out std_logic; 
    wr_rst_busy : out std_logic; 
    rd_rst_busy : out std_logic
  ); 
end component; 

component CDCC
  generic(
    NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
  port(
    clk_src    : in  std_logic;                                     -- input clk (source clock)
    clk_dst    : in  std_logic;                                     -- input clk (dest clock)
    data_in    : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0); -- data to be synced
    data_out_s : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)  -- synced data to clk_dst
  ); 
end component; 
  
  signal address_i        : std_logic_vector(9 downto 0) := (others => '0'); 
  signal write_reg_file   : std_logic := '0'; 
  signal read_reg_file    : std_logic := '0'; 
  signal sel_data         : std_logic_vector(1 downto 0) := (others => '0'); 
  signal wait_cnt         : unsigned(5 downto 0)         := (others => '0'); 
  signal mgr_done         : std_logic := '0'; 
  
  signal start_i          : std_logic := '0'; 
  signal start_read_i     : std_logic := '0'; 
  signal read_file        : std_logic := '0'; 
  signal sec_read         : std_logic := '0'; 
  signal mode_i           : std_logic_vector(2 downto 0)  := (others => '0'); 
  signal data0_i          : std_logic_vector(31 downto 0) := (others => '0'); 
  signal data1_i          : std_logic_vector(31 downto 0) := (others => '0'); 
  signal file_data0_i     : std_logic_vector(31 downto 0) := (others => '0'); 
  signal file_data1_i     : std_logic_vector(31 downto 0) := (others => '0'); 
  signal addrData_i       : std_logic_vector(17 downto 0) := (others => '0'); 
  
  -- cdc signals  
  signal read_file_s      : std_logic := '0'; 
  signal mode_s           : std_logic_vector(2 downto 0)  := (others => '0'); 
  signal data0_s          : std_logic_vector(31 downto 0) := (others => '0'); 
  signal data1_s          : std_logic_vector(31 downto 0) := (others => '0'); 
  signal addrData_s       : std_logic_vector(17 downto 0) := (others => '0'); 
  
  signal wr_en_file       : std_logic := '0'; 
  signal wr_en_scax       : std_logic := '0'; 
  signal rd_en_scax       : std_logic := '0'; 
  signal rd_en_file       : std_logic := '0'; 
  signal wr_en_scax2file  : std_logic := '0'; 
  signal rd_en_scax2file  : std_logic := '0'; 
  signal wr_en_file2scax  : std_logic := '0'; 
  signal rd_en_file2scax  : std_logic := '0'; 
  signal sel_scax2file    : unsigned(1 downto 0)          := (others => '0'); 
  signal sel_file2scax    : unsigned(1 downto 0)          := (others => '0'); 
  signal cnt_rd_file      : unsigned(2 downto 0)          := (others => '0'); 
  signal cnt_rd_scax      : unsigned(2 downto 0)          := (others => '0'); 
  signal din_scax2file    : std_logic_vector(31 downto 0) := (others => '0'); 
  signal din_file2scax    : std_logic_vector(31 downto 0) := (others => '0'); 
  signal dout_scax2file   : std_logic_vector(31 downto 0) := (others => '0'); 
  signal dout_file2scax   : std_logic_vector(31 downto 0) := (others => '0'); 
  signal scax2file_full   : std_logic := '0'; 
  signal scax2file_empty  : std_logic := '0'; 
  signal file2scax_full   : std_logic := '0'; 
  signal file2scax_empty  : std_logic := '0'; 
  signal start_cdc        : std_logic := '0'; 
  signal start_cdc_s      : std_logic := '0'; 
  signal cdc_done         : std_logic := '0'; 
  signal cdc_done_s       : std_logic := '0'; 
  signal cdc_ack          : std_logic := '0'; 
  signal cdc_ack_s        : std_logic := '0';
  signal cdc_ack_i        : std_logic := '0';
  
  type stateType_scax is (ST_IDLE, ST_WRITE, ST_WAIT, ST_RD_FIFO, ST_DONE); 
  signal state_scax : stateType_scax := ST_IDLE; 
  
  type stateType_file is (ST_IDLE, ST_READ, ST_CHK_MOD, ST_WAIT, ST_CHK_RD, ST_WR_FIFO_0, ST_WR_FIFO_1, ST_DONE); 
  signal state_file : stateType_file := ST_IDLE; 
  
begin
  
-- pipe in
pipe_in_proc : process(clk)
begin
  if(rising_edge(clk))then
    mode_i       <= mode; 
    start_i      <= start; 
    start_read_i <= start_read; 
    data0_i      <= data1; -- swapped
    data1_i      <= data0; -- swapped
    addrData_i   <= addrData; 
  end if; 
end process; 
  
-- CDC FSM, SCAX domain 
cdcFSMwr_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      sel_scax2file <= (others => '0'); 
      mgr_data0     <= (others => '0'); 
      mgr_data1     <= (others => '0'); 
      wr_en_scax    <= '0'; 
      mgr_done      <= '0'; 
      start_cdc     <= '0'; 
      read_file     <= '0'; 
      rd_en_scax    <= '0'; 
      mgr_ack       <= '0'; 
      state_scax    <= ST_IDLE; 
    else
      case state_scax is
          
        -- start the transaction, write to the scax2file fifo
        when ST_IDLE => 
          mgr_done <= '0'; 

          if(start_i = '1')then -- single-byte
            read_file <= start_read_i; 
            wr_en_scax <= '1'; 
            state_scax <= ST_WRITE; 
          else
            read_file <= read_file; 
            wr_en_scax <= '0'; 
            state_scax <= ST_IDLE; 
          end if; 
          
        -- keep writing
        when ST_WRITE => 
          sel_scax2file <= sel_scax2file + 1; 

          if(sel_scax2file = "10")then
            wr_en_scax <= '0'; 
            state_scax <= ST_WAIT; 
          else
            wr_en_scax <= '1'; 
            state_scax <= ST_WRITE; 
          end if; 
          
        -- wait for the other clock-domain FSM to finish
        when ST_WAIT => 
          sel_scax2file <= (others => '0'); 
          start_cdc     <= '1'; 

          if(cdc_done_s = '1' and file2scax_empty = '0')then
            rd_en_scax <= '1'; 
            state_scax <= ST_RD_FIFO; 
          else
            rd_en_scax <= '0'; 
            state_scax <= ST_WAIT; 
          end if; 
          
        -- read the file2scax fifo
        when ST_RD_FIFO => 
          start_cdc   <= '0'; 
          cnt_rd_scax <= cnt_rd_scax + 1; 
          
          case cnt_rd_scax is
            when "000" => rd_en_scax <= '1'; state_scax <= ST_RD_FIFO; 
            when "001" => rd_en_scax <= '1'; state_scax <= ST_RD_FIFO; 
            when "010" => rd_en_scax <= '0'; state_scax <= ST_RD_FIFO; 
            when "011" => rd_en_scax <= '0'; mgr_data0  <= dout_file2scax; state_scax <= ST_RD_FIFO; 
            when "100" => rd_en_scax <= '0'; mgr_ack <= dout_file2scax(31); state_scax <= ST_DONE; 
            when others => state_scax <= ST_DONE; 
          end case; 
          
        -- done, signal master
        when ST_DONE => 
          mgr_done   <= '1'; 
          cnt_rd_scax <= (others => '0'); 
          state_scax  <= ST_IDLE; 
          
        when others => state_scax <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- FIFO input process
scax2file_fifoIn_proc : process(clk)
begin
  if(rising_edge(clk))then
    wr_en_scax2file <= wr_en_scax; 
    case sel_scax2file is
      when "00" => din_scax2file(31 downto 29) <= mode_i; 
        din_scax2file(28)           <= read_file; 
        din_scax2file(17 downto 0)  <= addrData_i; 
        din_scax2file(27 downto 18) <= (others => '0'); 
      when "01" => din_scax2file <= data0_i; 
      when "10" => din_scax2file <= data1_i; 
      when others => din_scax2file(31 downto 29) <= mode_i; 
        din_scax2file(28)           <= read_file; 
        din_scax2file(17 downto 0)  <= addrData_i; 
        din_scax2file(27 downto 18) <= (others => '0'); 
    end case; 
  end if; 
end process; 

-- CDC FSM, file domain 
cdcFSMrd_proc : process(clk_regFile)
begin
  if(rising_edge(clk_regFile))then
    if(rst = '1')then
      rd_en_file     <= '0'; 
      cdc_done       <= '0'; 
      write_reg_file <= '0'; 
      cdc_ack        <= '0';
      sec_read       <= '0';
      cdc_ack_i      <= '0';
      sel_data       <= (others => '0'); 
      wait_cnt       <= (others => '0'); 
      state_file     <= ST_IDLE; 
    else
      case state_file is
          
        when ST_IDLE => 
          cdc_done    <= '0';
          sec_read    <= '0';
          cdc_ack_i   <= '0';
          cnt_rd_file <= (others => '0'); 
          
          if(start_cdc_s = '1' and scax2file_empty = '0')then
            rd_en_file <= '1'; 
            state_file <= ST_READ; 
          else
            rd_en_file <= '0'; 
            state_file <= ST_IDLE; 
          end if; 
          
        -- read the FIFO
        when ST_READ => 
          cnt_rd_file <= cnt_rd_file + 1; 
          
          case cnt_rd_file is
            when "000"  => rd_en_file <= '1'; state_file <= ST_READ; 
            when "001"  => rd_en_file <= '1'; state_file <= ST_READ; 
            when "010"  => rd_en_file <= '0'; read_file_s <= dout_scax2file(28); addrData_s <= dout_scax2file(17 downto 0); state_file <= ST_READ; 
            when "011"  => rd_en_file <= '0'; data0_s <= dout_scax2file; state_file <= ST_READ; 
            when "100"  => rd_en_file <= '0'; address_i <= addrData_s(17 downto 8); data1_s <= dout_scax2file; state_file <= ST_WAIT; 
            when others => state_file <= ST_DONE; 
          end case; 
          
        -- wait before writing into the register file's mux
        when ST_WAIT => 
          wait_cnt    <= wait_cnt + 1; 
          
          if(wait_cnt = "111111" and read_file_s = '0')then
            write_reg_file <= '1'; 
            read_reg_file  <= '0'; 
            state_file     <= ST_CHK_RD; 
          elsif(wait_cnt = "111111" and read_file_s = '1')then
            write_reg_file <= '0'; 
            read_reg_file  <= '1'; 
            state_file     <= ST_CHK_RD; 
          else
            write_reg_file <= '0'; 
            read_reg_file  <= '0'; 
            state_file     <= ST_WAIT; 
          end if; 
          
        -- read again if necessary (for memory elements)
        when ST_CHK_RD => 
          write_reg_file <= '0'; 
          read_reg_file  <= '0'; 
          
          if(read_reg_file = '1' and sec_read = '0')then
            sec_read    <= '1';
            state_file  <= ST_WAIT;
          else 
            sec_read    <= sec_read;
            state_file  <= ST_WR_FIFO_0; 
          end if; 
          
        -- write into the file2scax fifo
        when ST_WR_FIFO_0 =>  
          wr_en_file    <= '1';

          if(read_file_s = '1')then
            file_data0_i  <= reg_rd_data; 
          else
            file_data0_i  <= (others => '0');
          end if;
          
          state_file    <= ST_WR_FIFO_1; 
          
        -- keep writing
        when ST_WR_FIFO_1 => 
          sel_file2scax <= sel_file2scax + 1;

          if(sel_file2scax = "00")then
            cdc_ack_i  <= reg_ack; -- get the ack 
          elsif(sel_file2scax = "01")then
            cdc_ack    <= reg_ack or cdc_ack_i; -- get the ack
          elsif(sel_file2scax = "10")then
            wr_en_file <= '0'; 
            state_file <= ST_DONE; 
          else
            wr_en_file <= '1'; 
            state_file <= ST_WR_FIFO_1; 
          end if; 
          
        -- wait for the other clock-domain FSM to get the message
        when ST_DONE => 
          cdc_done      <= '1'; 
          sel_file2scax <= (others => '0'); 
          if(start_cdc_s = '0')then
            state_file <= ST_IDLE; 
          else
            state_file <= ST_DONE; 
          end if; 
          
        when others => state_file <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- pipeline out CDC
pipe_out_proc : process(clk_regFile)
begin
  if(rising_edge(clk_regFile))then
    reg_addr                  <= address_i; 
    reg_wr                    <= write_reg_file; 
    reg_rd                    <= read_reg_file;
    reg_wr_data(31 downto 00) <= data0_s; 
  end if;
end process; 
  
-- FIFO input process
reg2scax_fifoIn_proc : process(clk_regFile)
begin
  if(rising_edge(clk_regFile))then
    wr_en_file2scax <= wr_en_file; 
    case sel_file2scax is
      when "00"   => din_file2scax      <= (others => '0');
      when "01"   => din_file2scax      <= file_data0_i; 
      when "10"   => din_file2scax(31)  <= cdc_ack; din_file2scax(30 downto 0) <= (others => '0');
      when others => din_file2scax(31)  <= cdc_ack; din_file2scax(30 downto 0) <= (others => '0'); 
    end case; 
  end if; 
end process; 
  
scax2file_inst : i2c_cdc_buffer
  port map(
    rst         => rst,
    wr_clk      => clk,
    rd_clk      => clk_regFile,
    din         => din_scax2file,
    wr_en       => wr_en_scax2file,
    rd_en       => rd_en_scax2file,
    dout        => dout_scax2file,
    full        => scax2file_full,
    empty       => scax2file_empty,
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 
  
file2scax_inst : i2c_cdc_buffer
  port map(
    rst         => rst,
    wr_clk      => clk_regFile,
    rd_clk      => clk,
    din         => din_file2scax,
    wr_en       => wr_en_file2scax,
    rd_en       => rd_en_file2scax,
    dout        => dout_file2scax,
    full        => file2scax_full,
    empty       => file2scax_empty,
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 
  
CDCC_wrToRd_inst : CDCC
  generic map(NUMBER_OF_BITS => 1)
  port map(
    clk_src       => clk,
    clk_dst       => clk_regFile,
    data_in(0)    => start_cdc,
    data_out_s(0) => start_cdc_s
  ); 

CDCC_rdToWr_inst : CDCC
  generic map(NUMBER_OF_BITS => 1)
  port map(
    clk_src       => clk_regFile,
    clk_dst       => clk,
    data_in(0)    => cdc_done,
    data_out_s(0) => cdc_done_s
  ); 
  
  rd_en_scax2file <= rd_en_file; 
  rd_en_file2scax <= rd_en_scax; 
  mgr_done_wr    <= mgr_done and (not read_file); 
  mgr_done_rd    <= mgr_done and (read_file); 
  
end RTL; 
