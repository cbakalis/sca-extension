----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission
--    Copyright 2018 Christos Bakalis (christos.bakalis@cern.ch)

--    This file is part of the GBT-FPGA_VC709 project.

--    GBT-FPGA_VC709 is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.

--    GBT-FPGA_VC709 is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.

--    You should have received a copy of the GNU General Public License
--    along with GBT-FPGA_VC709.  If not, see <http://www.gnu.org/licenses/>.
--    
--    Special thanks to Dimitrios Matakias (dimitrios.matakias@cern.ch)
--    and to Panagiotis Gkountoumis (panagiotis.gkountoumis@cern.ch) for their help.
--    This design has been ported from the VC707 example design from CERN's GBT-FPGA
--    group (https://gitlab.cern.ch/gbt-fpga).

--  
-- Create Date: 18.12.2018 17:54:56
-- Design Name: GBT-FPGA_VC709 Wrapper
-- Module Name: gbt_fpga_wrapper - RTL
-- Project Name: GBT-FPGA_VC709
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: GBT-FPGA wrapper (VC709 part)
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

entity gbt_fpga_wrapper is
    port(
        -----------------------------------
        -------- General Interface --------
        cpu_reset           : in  std_logic; -- CPU_RST of VC709 (assert if clock loss)
        user_clock_p        : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        user_clock_n        : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        sma_mgt_refclk_p    : in  std_logic; -- clean 120.237 clock from clock board
        sma_mgt_refclk_n    : in  std_logic; -- clean 120.237 clock from clock board
        rst_rx_pll          : in  std_logic; -- reset the recovered clock PLL
        mgt_ready           : out std_logic; -- mgt_ready signal
        gbt_rx_ready        : out std_logic; -- RX-side ready
        tx_pll_lock         : out std_logic; -- e-link PLL is locked
        rx_pll_lock         : out std_logic; -- recovered clock PLL is locked
        -----------------------------------
        ---- GBT-FPGA Data Interface ------
        gbt_fpga_tx         : in  std_logic_vector(83 downto 0);
        gbt_fpga_rx         : out std_logic_vector(83 downto 0);
        -----------------------------------
        --- Clock-Forwarding Interface ----
        rx_userclk40        : out std_logic; -- RX recovered clock 40.079 (clean it)
        userclk             : out std_logic; -- raw 156.250 clock (after global buffer)
        clk_40              : out std_logic; -- e-link clock
        clk_80              : out std_logic; -- e-link clock
        clk_160             : out std_logic; -- e-link clock
        clk_320             : out std_logic; -- e-link clock
        clk_640             : out std_logic; -- e-link clock
        -----------------------------------
        ---------- SFP Interface ----------
        sfp_rx_p            : in  std_logic;    
        sfp_rx_n            : in  std_logic;
        sfp_tx_p            : out std_logic;
        sfp_tx_n            : out std_logic
    );
end gbt_fpga_wrapper;

architecture RTL of gbt_fpga_wrapper is

COMPONENT xlx_k7v7_tx_pll 
    PORT(
        clk_in1     :  in std_logic;
        RESET       :  in std_logic;
        CLK_OUT1    : out std_logic;
        CLK_OUT2    : out std_logic;
        CLK_OUT3    : out std_logic;
        CLK_OUT4    : out std_logic;
        CLK_OUT5    : out std_logic;
        LOCKED      : out std_logic
    );
END COMPONENT;

COMPONENT xlx_k7v7_rx_pll 
    PORT(
        clk_in1     : in std_logic;
        RESET       : in std_logic;
        CLK_OUT1    : out std_logic;
        LOCKED      : out std_logic
    );
END COMPONENT;

   --================================ Signal Declarations ================================--          
   
   --===============--     
   -- General reset --     
   --===============--     

   signal reset_from_genRst                      : std_logic := '0';    
   
   --===============--
   -- Clocks scheme -- 
   --===============--   
   
   -- Fabric clock:
   ----------------
   
   signal fabricClk_from_userClockIbufgds        : std_logic := '0';     

   -- MGT(GTX) reference clock:     
   ----------------------------     
  
   signal mgtRefClk_from_smaMgtRefClkIbufdsGtxe2 : std_logic := '0';   
   
   signal rxWordClk_from_gbtExmplDsgn : std_logic := '0';
   signal elink_pllLock               : std_logic := '0';

    -- Frame clocks:
    ---------------
    signal txFrameClk_from_txPll    : std_logic := '0';
    signal clk80_int                : std_logic := '0';
    signal clk160_int               : std_logic := '0';
    signal clk320_int               : std_logic := '0';
    signal clk640_int               : std_logic := '0';

    --=========================
   -- GBT Bank example design --
   --=========================--
   
   -- Control:
   -----------
   signal generalReset_from_user                     : std_logic := '0';
   signal manualResetTx_from_user                    : std_logic := '0';
   signal manualResetRx_from_user                    : std_logic := '0';
   signal clkMuxSel_from_user                        : std_logic := '0';
   signal testPatterSel_from_user                    : std_logic_vector(1 downto 0) := (others => '0');
   signal loopBack_from_user                         : std_logic_vector(2 downto 0) := (others => '0');
   signal resetDataErrorSeenFlag_from_user           : std_logic_vector(0 downto 0) := (others => '0');
   signal resetGbtRxReadyLostFlag_from_user          : std_logic_vector(0 downto 0) := (others => '0');
   signal txIsDataSel_from_user                      : std_logic_vector(0 downto 0) := (others => '1');
   --------------------------------------------------      
   signal latOptGbtBankTx_from_gbtExmplDsgn          : std_logic := '0';
   signal latOptGbtBankRx_from_gbtExmplDsgn          : std_logic := '0';
   signal txFrameClkPllLocked_from_gbtExmplDsgn      : std_logic := '0';
   signal rxFrameClkPllLocked_from_gbtExmplDsgn      : std_logic := '0';
   signal mgtReady_from_gbtExmplDsgn                 : std_logic_vector(0 downto 0) := (others => '0'); 
   signal rxBitSlipNbr_from_gbtExmplDsgn1            : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0) := (others => '0');
   signal rxBitSlipNbr_from_gbtExmplDsgn2            : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0) := (others => '0');
   signal rxBitSlipNbr_from_gbtExmplDsgn3            : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0) := (others => '0');
   signal rxWordClkReady_from_gbtExmplDsgn           : std_logic := '0';
   signal rxWordClk_from_gbtExmplDsgn_bufg           : std_logic := '0';
   signal rxFrameClkReady_from_gbtExmplDsgn          : std_logic := '0';               
   signal gbtRxReady_from_gbtExmplDsgn               : std_logic_vector(0 downto 0) := (others => '0');    
   signal rxIsData_from_gbtExmplDsgn                 : std_logic_vector(0 downto 0) := (others => '0');        
   signal gbtRxReadyLostFlag_from_gbtExmplDsgn       : std_logic_vector(0 downto 0) := (others => '0'); 
   signal rxDataErrorSeen_from_gbtExmplDsgn          : std_logic_vector(0 downto 0) := (others => '0'); 
   signal rxExtrDataWidebusErSeen_from_gbtExmplDsgn  : std_logic_vector(0 downto 0) := (others => '0'); 
   signal gtx_polar_sel                              : std_logic_vector(0 downto 0) := (others => '0');
   
   -- Data:
   --------
   signal gbtbank_tx_data                            : gbtframe_A(1 to 1); 
   signal txData_from_gbtExmplDsgn                   : gbtframe_A(1 to 1);
   signal rxData_from_gbtExmplDsgn                   : gbtframe_A(1 to 1);
   --------------------------------------------------      
   signal txExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0) := (others => '0');
   signal rxExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0) := (others => '0');
   
   ----===========--
   ---- Extra -----
   ----===========--
   
   signal rst1_b_i                                   : std_logic := '0';
   signal rst2_b_i                                   : std_logic := '0';

begin
    
    rst1_b_i <= not cpu_reset;
    rst2_b_i <= not generalReset_from_user;

   -- reset gen
   genRst: entity work.xlx_k7v7_reset
      generic map (
         CLK_FREQ                                    => 156e6)
      port map (     
         CLK_I                                       => fabricClk_from_userClockIbufgds,
         RESET1_B_I                                  => rst1_b_i,
         RESET2_B_I                                  => rst2_b_i,
         RESET_O                                     => reset_from_genRst 
      );
      
   -- Comment: USER_CLOCK frequency: 156.250MHz 
      
      userClockIbufgds: ibufgds
         generic map (
            IBUF_LOW_PWR                                => FALSE,      
            IOSTANDARD                                  => "LVDS_25")
         port map (     
            O                                           => fabricClk_from_userClockIbufgds,   
            I                                           => user_clock_p,  
            IB                                          => user_clock_n 
         );
    
   -- global buffer 
   smaMgtRefClkIbufdsGtxe2: ibufds_gte2
      port map (
         O                                           => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
         ODIV2                                       => open,
         CEB                                         => '0',
         I                                           => sma_mgt_refclk_p,
         IB                                          => sma_mgt_refclk_n
      );
      
  -- global buffer for rx clock     
   BUFG_inst : BUFG
    port map (
        O => rxWordClk_from_gbtExmplDsgn_bufg,
        I => rxWordClk_from_gbtExmplDsgn -- 120.237 MHz
     );

   -- recovered clock
   rxPll: xlx_k7v7_rx_pll
      port map(
        clk_in1                                  => rxWordClk_from_gbtExmplDsgn_bufg, -- 120.237 MHz
        CLK_OUT1                                 => rx_userclk40, -- 40.079 MHz
        -----------------------------------------  
        RESET                                    => rst_rx_pll,
        LOCKED                                   => rxFrameClkPllLocked_from_gbtExmplDsgn
      );

   -- Frame clock
   txPll: xlx_k7v7_tx_pll
      port map(
        clk_in1                                  => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
        CLK_OUT1                                 => txFrameClk_from_txPll,
        CLK_OUT2                                 => clk80_int,
        CLK_OUT3                                 => clk160_int,
        CLK_OUT4                                 => clk320_int,
        CLK_OUT5                                 => clk640_int,
        -----------------------------------------  
        RESET                                    => '0',
        LOCKED                                   => txFrameClkPllLocked_from_gbtExmplDsgn
      );

 --=========================--
   -- GBT Bank example design --
   --=========================--    
  gbtExmplDsgn_inst: entity work.xlx_k7v7_gbt_example_design
      generic map(
          GBT_BANK_ID                                            => 1,
          NUM_LINKS                                              => GBT_BANKS_USER_SETUP(1).NUM_LINKS,
          TX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION,
          RX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION,
          TX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).TX_ENCODING,
          RX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).RX_ENCODING
      )
      port map (
    
          --==============--
          -- Clocks       --
          --==============--
          FRAMECLK_40MHZ                                         => txFrameClk_from_txPll,
          XCVRCLK                                                => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
          
          TX_FRAMECLK_O                                          => open, --txFrameClk_from_gbtExmplDsgn,        
          TX_WORDCLK_O                                           => open, --txWordClk_from_gbtExmplDsgn,          
          RX_FRAMECLK_O                                          => open, --rxFrameClk_from_gbtExmplDsgn,         
          RX_WORDCLK_O(1)                                        => rxWordClk_from_gbtExmplDsgn, --rxWordClk_from_gbtExmplDsgn,      
          
          --==============--
          -- Reset        --
          --==============--
          GBTBANK_GENERAL_RESET_I                                => reset_from_genRst,
          GBTBANK_MANUAL_RESET_TX_I                              => manualResetTx_from_user,
          GBTBANK_MANUAL_RESET_RX_I                              => manualResetRx_from_user,
          
          --==============--
          -- Serial lanes --
          --==============--
          GBTBANK_MGT_RX_P(1)                                       => sfp_rx_p,
          GBTBANK_MGT_RX_N(1)                                       => sfp_rx_n,
          GBTBANK_MGT_TX_P(1)                                       => sfp_tx_p,
          GBTBANK_MGT_TX_N(1)                                       => sfp_tx_n,
          
          --==============--
          -- Data             --
          --==============--        
          GBTBANK_GBT_DATA_I                                     => gbtbank_tx_data,--(others => "0"),
          GBTBANK_WB_DATA_I                                      => (others => "0"),
          
          TX_DATA_O                                              => txData_from_gbtExmplDsgn,            
          WB_DATA_O                                              => open, --txExtraDataWidebus_from_gbtExmplDsgn,
          
          GBTBANK_GBT_DATA_O                                     => rxData_from_gbtExmplDsgn,
          GBTBANK_WB_DATA_O                                      => open, --rxExtraDataWidebus_from_gbtExmplDsgn,
          
          --==============--
          -- Reconf.         --
          --==============--
          GBTBANK_MGT_DRP_RST                                    => '0',
          GBTBANK_MGT_DRP_CLK                                    => fabricClk_from_userClockIbufgds, --
          
          --==============--
          -- TX ctrl        --
          --==============--
          GBTBANK_TX_ISDATA_SEL_I                                => txIsDataSel_from_user, --
          GBTBANK_TEST_PATTERN_SEL_I                             => testPatterSel_from_user, --
          
          --==============--
          -- RX ctrl      --
          --==============--
          GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I                   => resetGbtRxReadyLostFlag_from_user, --
          GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I                    => resetDataErrorSeenFlag_from_user, --
          
          --==============--
          -- TX Status    --
          --==============--
          GBTBANK_LINK_READY_O                                   => mgtReady_from_gbtExmplDsgn, --
          GBTBANK_TX_MATCHFLAG_O                                 => open, --txMatchFlag_from_gbtExmplDsgn,--
          
          --==============--
          -- RX Status    --
          --==============--
          GBTBANK_GBTRX_READY_O                                  => gbtRxReady_from_gbtExmplDsgn, --
          GBTBANK_LINK1_BITSLIP_O                                => rxBitSlipNbr_from_gbtExmplDsgn1, --
          GBTBANK_GBTRXREADY_LOST_FLAG_O                         => gbtRxReadyLostFlag_from_gbtExmplDsgn, --
          GBTBANK_RXDATA_ERRORSEEN_FLAG_O                        => rxDataErrorSeen_from_gbtExmplDsgn, --
          GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O           => rxExtrDataWidebusErSeen_from_gbtExmplDsgn, --
          GBTBANK_RX_MATCHFLAG_O                                 => open, --rxMatchFlag_from_gbtExmplDsgn, --
          GBTBANK_RX_ISDATA_SEL_O                                => rxIsData_from_gbtExmplDsgn, --
--          RXDATA_ERROR_CNT                                          => open,
--          RXDATA_WORD_CNT                                           => open,
          GBTBANK_RX_ERRORDETECTED_O                             => open,
          
          --==============--
          -- XCVR ctrl    --
          --==============--
          GBTBANK_LOOPBACK_I                                     => loopBack_from_user, --
          
          GBTBANK_TX_POL                                         => gtx_polar_sel, --"111", --'1', --
          GBTBANK_RX_POL                                         => "0" --'0' --
     );
     
        mgt_ready           <= mgtReady_from_gbtExmplDsgn(0);
        gbt_rx_ready        <= gbtRxReady_from_gbtExmplDsgn(0);
        tx_pll_lock         <= txFrameClkPllLocked_from_gbtExmplDsgn;
        rx_pll_lock         <= rxFrameClkPllLocked_from_gbtExmplDsgn;

        userclk             <= fabricClk_from_userClockIbufgds;
        clk_40              <= txFrameClk_from_txPll;
        clk_80              <= clk80_int;
        clk_160             <= clk160_int;
        clk_320             <= clk320_int;
        clk_640             <= clk640_int;
        gbtbank_tx_data(1)  <= gbt_fpga_tx;
        --txData_from_gbtExmplDsgn(1) <= gbt_fpga_tx;
        txData_from_gbtExmplDsgn(1) <= (others => '0');
        gbt_fpga_rx         <= rxData_from_gbtExmplDsgn(1);
        
end RTL;
