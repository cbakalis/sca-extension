----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 06.03.2019 19:24:45
-- Design Name: GTH Phase Detector
-- Module Name: phase_detector - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Phase detector for the local oscillator and the recovered clock.
-- implements a phase-frequency detector, similar to a PLL's.
-- Output of detector is phase_diff_i. phase_diff_i resembles a 25ns clock
-- for a few periods if the clocks are aligned. FSM checks phase_diff_i with 
-- 1.5625 ns accuracy. phase_calib allows for assertion of output 'lockl' signal
-- within different phases of the local/recovered clock.
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity phase_detector is
  port(
    ---------------------------
    ----- Clock Interface -----
    clk_in_lcl  : in  std_logic; -- local clock (40.079)
    clk_in_rcv  : in  std_logic; -- recovered clock (40.079)
    clk_in_640  : in  std_logic; -- 640, related to MGTrefClk
    ----------------------------
    --- FSM Control Interface --
    cnt_per_lim : in  std_logic_vector(2 downto 0); -- suggested: "111"
    phase_calib : in  std_logic_vector(3 downto 0); -- for calibration 
    rst_det     : in  std_logic; -- reset the FSM
    rx_lock     : in  std_logic; -- rxPll locked
    -- dbg
    state_high  : out std_logic;
    state_low   : out std_logic;
    phase_diff  : out std_logic;
    ----------------------------
    ------ Clocks Aligned ------
    clks_align  : out std_logic -- clocks aligned
  );
end phase_detector;

architecture RTL of phase_detector is

component CDCC
  generic(
    NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
  port(
    clk_src    : in  std_logic;                                     -- input clk (source clock)
    clk_dst    : in  std_logic;                                     -- input clk (dest clock)
    data_in    : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0); -- data to be synced
    data_out_s : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)  -- synced data to clk_dst
  ); 
end component; 

  signal lcl_i            : std_logic := '0';
  signal rcv_i            : std_logic := '0';
  signal phase_diff_i     : std_logic := '0';
  signal phase_diff_s     : std_logic := '0';
  signal rx_lock_s        : std_logic := '0';
  signal rst_det_s        : std_logic := '0';
  signal clks_align_fsm   : std_logic := '0';
  signal clks_align_i     : std_logic := '0';
  signal clk_align_sreg   : std_logic_vector(15 downto 0) := (others => '0');
  signal cnt_lvl          : unsigned(4 downto 0) := (others => '0');
  signal cnt_per          : unsigned(2 downto 0) := (others => '0');
  
  type stateType is (ST_SCAN_HIGH, ST_SCAN_LOW, ST_LOCKED); 
  signal state : stateType := ST_SCAN_HIGH; 

begin

--------------------------------------
--- phase-frequency detector begin ---
--------------------------------------

-- local clock DFF
dff_local_proc: process(clk_in_lcl)
begin
  if(rising_edge(clk_in_lcl))then
    if(phase_diff_i = '1')then
        lcl_i <= '0';
    else
        lcl_i <= '1';
    end if;
  end if;
end process;

-- recovered clock DFF
dff_recovered_proc: process(clk_in_rcv)
begin
  if(rising_edge(clk_in_rcv))then
    if(phase_diff_i = '1')then
        rcv_i <= '0';
    else
        rcv_i <= '1';
    end if;
  end if;
end process;

  phase_diff_i <= lcl_i and rcv_i;

--------------------------------------
--- phase-frequency detector end -----
--------------------------------------

--------------------------------------
--- sync module begin ----------------
--------------------------------------

CDCC_inst: CDCC
  generic map(NUMBER_OF_BITS => 3)
  port map(
    clk_src       => clk_in_640,
    clk_dst       => clk_in_640,
    data_in(0)    => phase_diff_i,
    data_in(1)    => rx_lock,
    data_in(2)    => rst_det,
    data_out_s(0) => phase_diff_s,
    data_out_s(1) => rx_lock_s,
    data_out_s(2) => rst_det_s
  ); 

--------------------------------------
--- sync module end ------------------
--------------------------------------


--------------------------------------
--- FSM begin ------------------------
--------------------------------------

-- align FSM
alignFSM_proc: process(clk_in_640)
begin
  if(rising_edge(clk_in_640))then
    if(rst_det_s = '1' or rx_lock_s = '0')then
      clks_align_fsm  <= '0';
      state_high      <= '0';
      state_low       <= '0'; 
      cnt_lvl         <= (others => '0');
      cnt_per         <= (others => '0');
      state           <= ST_SCAN_HIGH;
    else
      case state is

        -- start counting the high-level of phase_diff
        when ST_SCAN_HIGH =>
          state_high      <= '1';
          state_low       <= '0';
          clks_align_fsm  <= '0';
        
          if(cnt_per = unsigned(cnt_per_lim))then
            cnt_lvl <= (others => '0');
            cnt_per <= (others => '0');
            state   <= ST_LOCKED;
          elsif(cnt_lvl(4) = '1')then
            cnt_lvl <= "00001";
            cnt_per <= cnt_per + 1;
            state   <= ST_SCAN_LOW;
          elsif(phase_diff_s = '1' and cnt_lvl(4) = '0')then
            cnt_lvl <= cnt_lvl + 1;
            cnt_per <= cnt_per;
            state   <= ST_SCAN_HIGH;
          else
            cnt_lvl <= (others => '0');
            cnt_per <= (others => '0');
            state   <= ST_SCAN_HIGH;
          end if;

        -- start counting the low-level of phase_diff
        when ST_SCAN_LOW =>
          state_high      <= '0';
          state_low       <= '1';
          clks_align_fsm  <= '0';
        
          if(cnt_per = unsigned(cnt_per_lim))then
            state   <= ST_LOCKED;
          elsif(cnt_lvl(4) = '1')then
            cnt_lvl <= "00001";
            cnt_per <= cnt_per + 1;
            state   <= ST_SCAN_HIGH;
          elsif(phase_diff_s = '0' and cnt_lvl(4) = '0')then
            cnt_lvl <= cnt_lvl + 1;
            cnt_per <= cnt_per;
            state   <= ST_SCAN_LOW;
          else
            cnt_lvl <= (others => '0');
            cnt_per <= (others => '0');
            state   <= ST_SCAN_HIGH;
          end if;

        -- locked state. clocks are aligned
        when ST_LOCKED =>
          state_high      <= '0';
          state_low       <= '0';   
          clks_align_fsm  <= '1';
          state           <= ST_LOCKED;

        when others =>
          clks_align_fsm  <= '0';
          state_high      <= '0';
          state_low       <= '0'; 
          cnt_lvl         <= (others => '0');
          cnt_per         <= (others => '0');
          state           <= ST_SCAN_HIGH;

      end case;
    end if;
  end if;
end process;

--------------------------------------
--- FSM end --------------------------
--------------------------------------

--------------------------------------
--- phase selector begin -------------
--------------------------------------

-- get in the shift-reg
sreg_proc: process(clk_in_640)
begin
  if(rising_edge(clk_in_640))then
    clk_align_sreg <= clks_align_fsm & clk_align_sreg(15 downto 1);
  end if;
end process;

-- select the in-25ns-period clock-phase the lock signal will be asserted
calib_proc: process(clk_in_640)
begin
  if(rising_edge(clk_in_640))then
    case phase_calib is
      when "0000" => clks_align_i <= clk_align_sreg(15);
      when "0001" => clks_align_i <= clk_align_sreg(14);
      when "0010" => clks_align_i <= clk_align_sreg(13);
      when "0011" => clks_align_i <= clk_align_sreg(12);
      when "0100" => clks_align_i <= clk_align_sreg(11);
      when "0101" => clks_align_i <= clk_align_sreg(10);
      when "0110" => clks_align_i <= clk_align_sreg(09);
      when "0111" => clks_align_i <= clk_align_sreg(08);
      when "1000" => clks_align_i <= clk_align_sreg(07);
      when "1001" => clks_align_i <= clk_align_sreg(06);
      when "1010" => clks_align_i <= clk_align_sreg(05);
      when "1011" => clks_align_i <= clk_align_sreg(04);
      when "1100" => clks_align_i <= clk_align_sreg(03);
      when "1101" => clks_align_i <= clk_align_sreg(02);
      when "1110" => clks_align_i <= clk_align_sreg(01);
      when "1111" => clks_align_i <= clk_align_sreg(00);
      when others => clks_align_i <= clk_align_sreg(15);
    end case;
  end if;
end process;

-- last register (set_max/min delay to this register and the mux's sel)
lastReg_proc: process(clk_in_640)
begin
  if(rising_edge(clk_in_640))then
    clks_align <= clks_align_i;
  end if;
end process;

--------------------------------------
--- phase selector end ---------------
--------------------------------------

  phase_diff <= phase_diff_s;

end RTL;
