----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    18/03/2015
--! Module Name:    EPROC_OUT8
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee,work;
use ieee.std_logic_1164.all;
use work.all;

--! E-link processor, 8bit output
entity EPROC_OUT8 is
generic (
            egroupID                        : integer := 0;
            EnableFrHo_Egroup0Eproc8_8b10b  : boolean := true;
            EnableFrHo_Egroup1Eproc8_8b10b  : boolean := true;
            EnableFrHo_Egroup2Eproc8_8b10b  : boolean := true;
            EnableFrHo_Egroup3Eproc8_8b10b  : boolean := true;
            EnableFrHo_Egroup4Eproc8_8b10b  : boolean := true;
            do_generate                     : boolean := true;
            includeNoEncodingCase           : boolean := true
        );
port ( 
    bitCLK      : in  std_logic;
    bitCLKx2    : in  std_logic;
    bitCLKx4    : in  std_logic;
    rst         : in  std_logic;
    ENA         : in  std_logic;
	swap_outbits: in std_logic;
    getDataTrig : out std_logic; -- @ bitCLKx4
    ENCODING    : in  std_logic_vector (3 downto 0);
    EDATA_OUT   : out std_logic_vector (7 downto 0);
    TTCin       : in  std_logic_vector (9 downto 0);
    fhCR_REVERSE_10B : in std_logic;
    DATA_IN     : in  std_logic_vector (9 downto 0);
    DATA_RDY    : in  std_logic
    );
end EPROC_OUT8;

architecture Behavioral of EPROC_OUT8 is

constant zeros8bit  : std_logic_vector (7 downto 0) := (others=>'0');
signal EdataOUT_ENC8b10b_case, EdataOUT_direct_case, EdataOUT_HDLC_case, EdataOUT_TTC3_case, EdataOUT_TTC4_case : std_logic_vector (7 downto 0);
signal rst_s, rst_case000, rst_case001, rst_case010, rst_case011 : std_logic;
signal getDataTrig_ENC8b10b_case, getDataTrig_direct_case, getDataTrig_HDLC_case, getDataTrig_TTC_cases : std_logic;
signal edata_out_s : std_logic_vector (7 downto 0);

signal TTCin_r  : std_logic_vector(9 downto 0); -- use to sync the TTCin vector with the extended BCR pulse.
signal TTCin_r2 : std_logic; -- BCR signal. Use to extend the BCR two a pulse of 2 clocks for OCR.
signal TTCin_r9 : std_logic; -- broadcast bit#7 signal. When set, extend the BCR to 2 clocks pulse (OCR indication). 

begin

--IG gen_enabled: if do_generate = true generate
Module_enable: if (
                        ((EnableFrHo_Egroup0Eproc8_8b10b = true) and (egroupID = 0)) or
                        ((EnableFrHo_Egroup1Eproc8_8b10b = true) and (egroupID = 1)) or
                        ((EnableFrHo_Egroup2Eproc8_8b10b = true) and (egroupID = 2)) or
                        ((EnableFrHo_Egroup3Eproc8_8b10b = true) and (egroupID = 3)) or
                        ((EnableFrHo_Egroup4Eproc8_8b10b = true) and (egroupID = 4)) or
                        ((includeNoEncodingCase          = true)                   )
                    ) generate

rst_s <= rst or (not ENA);

-------------------------------------------------------------------------------------------
-- case 0: direct data, no delimeter...
-------------------------------------------------------------------------------------------
direct_data_enabled: if includeNoEncodingCase = true generate
rst_case000 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "000")) else '1';
getDataTrig_direct_case <= '1' when (ENCODING(2 downto 0) = "000") else '0';
EdataOUT_direct_case    <= DATA_IN(7 downto 0); 
end generate direct_data_enabled;
--
direct_data_disabled: if includeNoEncodingCase = false generate
EdataOUT_direct_case <= (others=>'0');
end generate direct_data_disabled;
--

-------------------------------------------------------------------------------------------
-- case 1: DEC8b10b
-------------------------------------------------------------------------------------------
Enc_8b10b_generated: if (
                            ((EnableFrHo_Egroup0Eproc8_8b10b = true) and (egroupID = 0)) or
                            ((EnableFrHo_Egroup1Eproc8_8b10b = true) and (egroupID = 1)) or
                            ((EnableFrHo_Egroup2Eproc8_8b10b = true) and (egroupID = 2)) or
                            ((EnableFrHo_Egroup3Eproc8_8b10b = true) and (egroupID = 3)) or
                            ((EnableFrHo_Egroup4Eproc8_8b10b = true) and (egroupID = 4))
                        ) generate

rst_case001 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "001")) else '1';
--
ENC8b10b_case: entity work.EPROC_OUT8_ENC8b10b
port map(
    bitCLK      => bitCLK,
    bitCLKx2    => bitCLKx2,
    bitCLKx4    => bitCLKx4,
    rst         => rst_case001, 
    getDataTrig => getDataTrig_ENC8b10b_case,  
    edataIN     => DATA_IN,
    edataINrdy  => DATA_RDY,
    fhCR_REVERSE_10B => fhCR_REVERSE_10B,
    EdataOUT    => EdataOUT_ENC8b10b_case
    );
--

end generate Enc_8b10b_generated;

--IG: check if 8b10b decoding require
Enc_8b10b_disable: if   (
                            ((EnableFrHo_Egroup0Eproc8_8b10b = false) and (egroupID = 0)) or
                            ((EnableFrHo_Egroup1Eproc8_8b10b = false) and (egroupID = 1)) or
                            ((EnableFrHo_Egroup2Eproc8_8b10b = false) and (egroupID = 2)) or
                            ((EnableFrHo_Egroup3Eproc8_8b10b = false) and (egroupID = 3)) or
                            ((EnableFrHo_Egroup4Eproc8_8b10b = false) and (egroupID = 4))
                        ) generate

    EdataOUT_ENC8b10b_case      <= (others => '0');
    getDataTrig_ENC8b10b_case   <= '0';
    
end generate Enc_8b10b_disable;

-------------------------------------------------------------------------------------------
-- case 2: HDLC
-------------------------------------------------------------------------------------------
rst_case010 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "010")) else '1';
--
getDataTrig_HDLC_case   <= '0'; --'1' when (ENCODING(2 downto 0) = "010") else '0';
EdataOUT_HDLC_case      <= (others=>'0'); --<---TBD
--

-------------------------------------------------------------------------------------------
-- case 3&4: TTC-3 & TTC-4  
-------------------------------------------------------------------------------------------
rst_case011 <= '0' when ((rst_s = '0') and ((ENCODING(2 downto 0) = "011") or (ENCODING(2 downto 0) = "100"))) else '1';
--
getDataTrig_TTC_cases <= '0'; --'1' when ((ENCODING(2 downto 0) = "011") or (ENCODING(2 downto 0) = "100")) else '0';
--

--IG: from - <repo>/firmware/sources/ttc/ttc_decoder/ttc_fmc_wrapper_xilinx.vhd [lines: 493-502] *switch the names from "TTC_out_unsync" to "TTCin" 
--IG: TTCin(0)    <= l1a;
--IG: TTCin(1)    <= channelB; 
--IG: TTCin(2)    <= brc_b; *BCR*      0  0
--IG: TTCin(3)    <= brc_e; *ECR*      1  6
--IG: TTCin(4)    <= brc_d4(0);        2  5
--IG: TTCin(5)    <= brc_d4(1);        3  4
--IG: TTCin(6)    <= brc_d4(2);        4  3
--IG: TTCin(7)    <= brc_d4(3);        5  2
--IG: TTCin(8)    <= brc_t2(0);        6  1
--IG: TTCin(9)    <= brc_t2(1);        7  7

ttc_r: process(bitCLK)
begin
    if bitCLK'event and bitCLK = '1' then
        if (rst_case011 = '1') then
            EdataOUT_TTC3_case <= zeros8bit;
            EdataOUT_TTC4_case <= zeros8bit;
        else
            EdataOUT_TTC3_case <= TTCin(1) & TTCin(7 downto 2) & TTCin(0);
--IG            EdataOUT_TTC4_case <= TTCin(8 downto 2) & TTCin(0);
            --IG: Read Out Controller implementation:
            --IG: TTCin(2) - data overwrite by TTCin_r2 and extend to 2 clock cycles once an "OCR" (pulse from brc_t2(1)) set
            --IG:                |  EC0R     |  SCA       |  L0A       |  BCR     |  ECR       | Test Pulse | Soft Reset |  L1A      | 
--IG        EdataOUT_TTC4_case <= TTCin_r(6) & TTCin_r(7) & TTCin_r(8) & TTCin_r2 & TTCin_r(3) & TTCin_r(4) & TTCin_r(5) & TTCin_r(0);
--IG        EdataOUT_TTC4_case <= TTCin_r(5) & TTCin_r(4) & TTCin_r(3) & TTCin_r2 & TTCin_r(8) & TTCin_r(7) & TTCin_r(6) & TTCin_r(0); --IG: following the new bit order of the broadcast
            --IG:                |  EC0R     |  SCA       |  L0A-->L1A |  BCR     |  ECR       | Test Pulse | Soft Reset |  L1A      | 
--IG        EdataOUT_TTC4_case <= TTCin_r(5) & TTCin_r(4) & TTCin_r(3) & TTCin_r2 & TTCin_r(0) & TTCin_r(7) & TTCin_r(6) & TTCin_r(0); --IG: duplicate L1A over L0A

            --IG:                |  L1A      | Soft Reset | Test Pulse |  ECR     |  BCR       |  L0A       |  SCA       |  EC0R     | 
            EdataOUT_TTC4_case <= TTCin_r(0) & TTCin_r(5) & TTCin_r(4) & TTCin_r(3) & TTCin_r2 & TTCin_r(0) & TTCin_r(7) & TTCin_r(6);
        end if;	   
	end if;
end process;
--
-- sample the TTCin vector to support OCR modification
process(bitCLK)
begin
    if rising_edge(bitCLK) then
        -- synchronous reset
        if (rst_case011 = '1') then
            TTCin_r     <= (others => '0');
        else
            TTCin_r     <= TTCin;
        end if;
    end if;
end process;

-- set the OCR indication
process(bitCLK)
begin
    if rising_edge(bitCLK) then
        -- synchronous reset
        if (rst_case011 = '1') then
            TTCin_r9      <= '0';
        -- OCR indication set
        elsif (TTCin(9) = '1') then
            TTCin_r9      <= '1';
        -- clear the OCR indication when OCR indication set and BCR arrive
        elsif ((TTCin_r9 = '1') and (TTCin_r2 = '1')) then
            TTCin_r9      <= '0';
        -- keep the OCR indication value
        else
            TTCin_r9      <= TTCin_r9;
        end if;
    end if;
end process;

-- determine the BCR value
process(bitCLK)
begin
    if rising_edge(bitCLK) then
        -- synchronous reset
        if (rst_case011 = '1') then
            TTCin_r2      <= '0';
        -- BCR arrive
        elsif (TTCin(2) = '1') then
            TTCin_r2      <= '1';
        -- OCR indication set and BCR arrive
        elsif ((TTCin_r9 = '1') and (TTCin_r2 = '1')) then
            TTCin_r2      <= '1';
        -- default value
        else
            TTCin_r2      <= '0';
        end if;
    end if;
end process;



-------------------------------------------------------------------------------------------
-- output data and busy according to the encoding settings
-------------------------------------------------------------------------------------------
dataOUTmux: entity work.MUX8_Nbit 
generic map (N=>8)
port map( 
	data0    => EdataOUT_direct_case,
	data1    => EdataOUT_ENC8b10b_case,
	data2    => EdataOUT_HDLC_case,
	data3    => EdataOUT_TTC3_case,
	data4    => EdataOUT_TTC4_case,
	data5    => zeros8bit,
	data6    => zeros8bit,
	data7    => zeros8bit,
	sel      => ENCODING(2 downto 0),
	data_out => edata_out_s
	);
--
getDataTrig  <= ENA and (getDataTrig_TTC_cases or getDataTrig_HDLC_case or getDataTrig_ENC8b10b_case or getDataTrig_direct_case);
--

out_sel: process(swap_outbits,edata_out_s)
begin   
    if swap_outbits = '1' then
        EDATA_OUT <= edata_out_s(0) & edata_out_s(1) & edata_out_s(2) & edata_out_s(3) & edata_out_s(4) & edata_out_s(5) & edata_out_s(6) & edata_out_s(7);
    else
        EDATA_OUT <= edata_out_s;
    end if;	   
end process;


--IG end generate gen_enabled;
end generate Module_enable;
--
--IG gen_disabled: if do_generate = false generate

-- can't add the includeNoEncodingCase generic into this check since most of the time its value is flase state
Module_almost_disable: if   (
                                ((EnableFrHo_Egroup0Eproc8_8b10b = false) and (egroupID = 0)) or
                                ((EnableFrHo_Egroup1Eproc8_8b10b = false) and (egroupID = 1)) or
                                ((EnableFrHo_Egroup2Eproc8_8b10b = false) and (egroupID = 2)) or
                                ((EnableFrHo_Egroup3Eproc8_8b10b = false) and (egroupID = 3)) or
                                ((EnableFrHo_Egroup4Eproc8_8b10b = false) and (egroupID = 4))
                            ) generate

-- the module can't be disable if the generic is set
Module_disable: if  (
                        (includeNoEncodingCase = false)
                    ) generate

    EDATA_OUT   <= (others=>'0');
    getDataTrig <= '0';

--IG end generate gen_disabled;
end generate Module_disable;
end generate Module_almost_disable;


end Behavioral;

