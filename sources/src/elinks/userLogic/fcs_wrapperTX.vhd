----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 04.09.2018 16:43:27
-- Design Name: FCS Wrapper TX
-- Module Name: fcs_wrapperTX - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series
-- Tool Versions: Vivado 2018.2
-- Description: Wrapper for the FCS/CRC modules - TX version
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 

entity fcs_wrapperTX is
  generic(rev_fields : std_logic := '0'); 
  port(
    clk     : in  std_logic; 
    crc_en  : in  std_logic; 
    len     : in  std_logic_vector(1 downto 0); 
    din     : in  std_logic_vector(79 downto 0); 
    fcs_out : out std_logic_vector(15 downto 0); 
    fcs_rdy : out std_logic
  ); 
end fcs_wrapperTX; 

architecture RTL of fcs_wrapperTX is
  
component crc_1byte
  port(
    clk     : in  std_logic; 
    rst     : in  std_logic; 
    crc_en  : in  std_logic; 
    data_in : in  std_logic_vector(7 downto 0); 
    crc_out : out std_logic_vector(15 downto 0)
  ); 
end component; 
  
-- bit-reversal function
function bit_rev(s1 : std_logic_vector; high : natural; bytes : natural) return std_logic_vector is 
  variable rr : std_logic_vector((7*bytes + bytes-1) downto 0); 
begin 
  for ii in (7*bytes + bytes-1) downto 0 loop 
    rr(ii) := s1(high-ii); 
  end loop; 
  return rr; 
end bit_rev; 
  
  signal rst_i      : std_logic := '1'; 
  signal crc_en_i   : std_logic := '0'; 
  signal crc_en_cnt : unsigned(3 downto 0)          := (others => '0'); 
  signal crc_en_thr : unsigned(3 downto 0)          := (others => '0'); 
  signal crc_din    : std_logic_vector(7 downto 0)  := (others => '0'); 
  signal dout_crc_i : std_logic_vector(15 downto 0) := (others => '0'); 
  signal din_i      : std_logic_vector(79 downto 0) := (others => '0'); 
  -- if set to 1, will calculate the crc for the reversed field
  -- note 19.09.2018: in reality, the MSB of the rev_fields should be this constant
  -- and the reversal of the tx fields should not be happening at all (maybe)
  constant rev_i : std_logic := '1'; 
  
  type stateType is (ST_IDLE, ST_EN_FCS, ST_FCS_RDY); 
  signal state : stateType := ST_IDLE; 
  
begin
  
-- register the length
regLen_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(crc_en = '1')then
      case len is
        when "00"   => crc_en_thr <= "0010"; -- high for 2 cycles (shift-in 2 bytes)
        when "01"   => crc_en_thr <= "0110"; -- high for 6 cycles (shift-in 6 bytes)
        when "10"   => crc_en_thr <= "1000"; -- high for 8 cycles (shift-in 8 bytes)
        when "11"   => crc_en_thr <= "1010"; -- high for 10 cycles (shift-in 10 bytes)
        when others => crc_en_thr <= (others => '0'); 
      end case; 
    else
      crc_en_thr <= crc_en_thr; 
    end if; 
  end if; 
end process; 
  
-- FSM for the FCS
fcs_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    case state is
        
      -- wait for the enable
      when ST_IDLE => 
        fcs_rdy  <= '0'; 
        crc_en_i <= '0'; 
        if(crc_en = '1')then
          rst_i <= '0'; 
          state <= ST_EN_FCS; 
        else
          rst_i <= '1'; 
          state <= ST_IDLE; 
        end if; 
        
      -- enable the FCS for N cycles
      when ST_EN_FCS => 
        if(crc_en_cnt = crc_en_thr)then
          crc_en_i   <= '0'; 
          crc_en_cnt <= (others => '0'); 
          state      <= ST_FCS_RDY; 
        else
          crc_en_i   <= '1'; 
          crc_en_cnt <= crc_en_cnt + 1; 
          state      <= ST_EN_FCS; 
          
          case crc_en_cnt is
            when "0000" => crc_din <= din_i(79 downto 72); -- addr
            when "0001" => crc_din <= din_i(71 downto 64); -- control
            when "0010" => crc_din <= din_i(63 downto 56); -- tr_id
            when "0011" => crc_din <= din_i(55 downto 48); -- channel
            when "0100" => crc_din <= din_i(47 downto 40); -- error
            when "0101" => crc_din <= din_i(39 downto 32); -- length
            when "0110" => crc_din <= din_i(31 downto 24); -- dout_a 1/2
            when "0111" => crc_din <= din_i(23 downto 16); -- dout_a 2/2
            when "1000" => crc_din <= din_i(15 downto 08); -- dout_b 1/2
            when "1001" => crc_din <= din_i(07 downto 00); -- dout_b 2/2
            when others => crc_din <= (others => '0'); 
          end case; 
        end if; 
        
      -- assert the FCS rdy
      when ST_FCS_RDY => 
        fcs_rdy <= '1'; 
        state   <= ST_IDLE; 
        
        if(rev_fields = '1')then
          fcs_out <= bit_rev(dout_crc_i, 15, 1) & bit_rev(dout_crc_i, 7, 1); 
        else
          fcs_out <= dout_crc_i; 
        end if; 
        
      when others => state <= ST_IDLE; 
    end case; 
  end if; 
end process; 
  
normal_generate : if (rev_i = '0') generate
  din_i <= din; 
end generate normal_generate; 
    
reverse_generate : if (rev_i = '1') generate
  din_i(79 downto 72) <= bit_rev(din, 79, 1); 
  din_i(71 downto 64) <= bit_rev(din, 71, 1); 
  din_i(63 downto 56) <= bit_rev(din, 63, 1); 
  din_i(55 downto 48) <= bit_rev(din, 55, 1); 
  din_i(47 downto 40) <= bit_rev(din, 47, 1); 
  din_i(39 downto 32) <= bit_rev(din, 39, 1); 
  din_i(31 downto 24) <= bit_rev(din, 31, 1); 
  din_i(23 downto 16) <= bit_rev(din, 23, 1); 
  din_i(15 downto 08) <= bit_rev(din, 15, 1); 
  din_i(07 downto 00) <= bit_rev(din, 07, 1); 
end generate reverse_generate; 
      
crc_1byte_inst : crc_1byte
  port map(
    clk     => clk,
    rst     => rst_i,
    crc_en  => crc_en_i,
    data_in => crc_din,
    crc_out => dout_crc_i
  ); 
      
end RTL; 
      