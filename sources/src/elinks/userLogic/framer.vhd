----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 09.04.2018 13:40:18
-- Design Name: Framer
-- Module Name: framer - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Module that assembles a frame 
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use work.scax_package.all; 
use work.centralRouter_package.all; 

entity framer is
  generic(rev_fields : std_logic_vector(1 downto 0) := (others => '0')); --00:revNone,01:revData,10:revFCS,11:revBoth
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; -- user clock
    rst           : in  std_logic; -- reset
    framer_busy   : out std_logic; -- busy
    state_o_frm   : out std_logic_vector(3 downto 0); 
    state_o_drv   : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull : in  std_logic; -- tx fifo prog full
    tx_fifo_wr    : out std_logic; -- tx fifo wr_ena
    tx_fifo_din   : out std_logic_vector(17 downto 0); -- tx fifo din
    ------------------------------
    -- Master Handler Interface --
    frame_fields  : in  frame_tx;  -- frame contents
    tx_request    : in  std_logic; -- tx new frame 
    frame_size    : in  std_logic_vector(1 downto 0) -- only 4 size types
  ); 
end framer; 

architecture RTL of framer is
  
component fcs_wrapperTX
  generic(rev_fields : std_logic := '0'); 
  port(
    clk     : in  std_logic; 
    crc_en  : in  std_logic; 
    len     : in  std_logic_vector(1 downto 0); 
    din     : in  std_logic_vector(79 downto 0); 
    fcs_out : out std_logic_vector(15 downto 0); 
    fcs_rdy : out std_logic
  ); 
end component; 
  
component elink_driver
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    state_o       : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull : in  std_logic; 
    tx_fifo_wr    : out std_logic; 
    tx_fifo_din   : out std_logic_vector(17 downto 0); 
    ------------------------------
    ------ Framer Interface ------
    enable        : in  std_logic; 
    frame_size    : in  std_logic_vector(1 downto 0); 
    din           : in  std_logic_vector(79 downto 0); 
    fcs           : in  std_logic_vector(15 downto 0); 
    done          : out std_logic
  ); 
end component; 
  
  signal crc_en     : std_logic := '0'; 
  signal fcs_rdy    : std_logic := '0'; 
  signal busy       : std_logic := '0'; 
  
  signal reg_frm    : std_logic := '0'; 
  signal drv_ena    : std_logic := '0'; 
  
  signal wait_cnt    : unsigned(1 downto 0)           := (others => '0'); 
  signal state_o     : std_logic_vector(3 downto 0)   := "1111"; 
  signal frameSize_i : std_logic_vector(1 downto 0)   := (others => '0'); 
  signal din_i       : std_logic_vector(79 downto 0)  := (others => '0'); 
  signal fcs_i       : std_logic_vector(15 downto 0)  := (others => '0'); 
  signal fcs_o       : std_logic_vector(15 downto 0)  := (others => '0'); 
  
  signal drv_done     : std_logic := '0'; 
  
  signal cnt_strobe   : std_logic := '0'; 
  signal cnt          : unsigned(15 downto 0) := (others => '0'); 
  
  signal dout_a_1of2 : std_logic_vector(7 downto 0) := (others => '0'); 
  signal dout_a_2of2 : std_logic_vector(7 downto 0) := (others => '0'); 
  signal dout_b_1of2 : std_logic_vector(7 downto 0) := (others => '0'); 
  signal dout_b_2of2 : std_logic_vector(7 downto 0) := (others => '0'); 
  
  type stateType is (ST_IDLE, ST_REG_FRM, ST_GEN_FCS, ST_WAIT_FCS, ST_ENA_DRV, ST_START_CNT); 
  signal state : stateType := ST_IDLE; 
  
-- bit-reversal function
function bit_rev(s1 : std_logic_vector; high : natural; bytes : natural) return std_logic_vector is 
  variable rr : std_logic_vector((7*bytes + bytes-1) downto 0); 
begin 
  for ii in (7*bytes + bytes-1) downto 0 loop 
    rr(ii) := s1(high-ii); 
  end loop; 
  return rr; 
end bit_rev; 
  
begin
  
-- framer FSM
framer_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      busy        <= '0'; 
      reg_frm     <= '0'; 
      crc_en      <= '0'; 
      drv_ena     <= '0'; 
      fcs_i       <= (others => '0'); 
      wait_cnt    <= (others => '0'); 
      frameSize_i <= (others => '0'); 
      cnt         <= (others => '0'); 
      cnt_strobe  <= '0'; 
      state_o     <= "1111"; 
      state       <= ST_IDLE; 
    else
      case state is
          
        -- wait for requests
        when ST_IDLE => 
          state_o    <= "0000"; 
          cnt        <= (others => '0'); 
          cnt_strobe <= '0'; 
          busy       <= '0'; 
          
          if(tx_request = '1')then
            reg_frm     <= '1'; 
            frameSize_i <= frame_size; 
            state       <= ST_REG_FRM; 
          else
            reg_frm     <= '0'; 
            frameSize_i <= (others => '0'); 
            state       <= ST_IDLE; 
          end if; 
          
        -- register the frame contents and the size
        when ST_REG_FRM => 
          state_o  <= "0001"; 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "11")then
            reg_frm     <= '0'; 
            frameSize_i <= frameSize_i; 
            busy        <= '1'; 
            state       <= ST_GEN_FCS; 
          else
            reg_frm     <= '1'; 
            frameSize_i <= frame_size; 
            busy        <= '0'; 
            state       <= ST_REG_FRM; 
          end if; 
          
        -- generate the FCS 
        when ST_GEN_FCS => 
          state_o  <= "0010"; 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "11")then
            crc_en <= '1'; 
            state  <= ST_WAIT_FCS; 
          else
            crc_en <= '0'; 
            state  <= ST_GEN_FCS; 
          end if; 
          
        -- wait for the FCS generator
        when ST_WAIT_FCS => 
          state_o <= "0011"; 
          crc_en  <= '0'; 
          
          if(fcs_rdy = '1')then
            fcs_i <= fcs_o; 
            state <= ST_ENA_DRV; 
          else
            fcs_i <= fcs_i; 
            state <= ST_WAIT_FCS; 
          end if; 
          
        -- enable the driver
        when ST_ENA_DRV => 
          state_o <= "0100"; 
          
          if(drv_done = '1')then
            drv_ena <= '0'; 
            state   <= ST_IDLE; -- ST_START_CNT
          else
            drv_ena <= '1'; 
            state   <= ST_ENA_DRV; 
          end if; 
          
        -- wait a bit
        when ST_START_CNT => 
          state_o    <= "0101"; 
          cnt_strobe <= not cnt_strobe; 
          
          if(cnt_strobe = '1')then
            cnt <= cnt + 1; 
          else
            cnt <= cnt; 
          end if; 
          
          if(cnt = x"FFFF")then
            state <= ST_IDLE; 
          else
            state <= ST_START_CNT; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
normal_generate : if (rev_fields(0) = '0') generate
  -- reg frame contents
  reg_frame_proc : process(clk)
  begin
    if(rising_edge(clk))then
      if(rst = '1')then
        din_i <= (others => '0'); 
      else
        if(reg_frm = '1')then
          din_i(79 downto 72) <= frame_fields.address; 
          din_i(71 downto 64) <= frame_fields.control; 
          din_i(63 downto 56) <= frame_fields.tr_id; 
          din_i(55 downto 48) <= frame_fields.channel; 
          din_i(47 downto 40) <= frame_fields.err; 
          din_i(39 downto 32) <= frame_fields.length; 
          din_i(31 downto 24) <= dout_a_1of2; 
          din_i(23 downto 16) <= dout_a_2of2; 
          din_i(15 downto 08) <= dout_b_1of2; 
          din_i(07 downto 00) <= dout_b_2of2; 
        else
          din_i <= din_i; 
        end if; 
      end if; 
    end if; 
  end process; 
end generate normal_generate; 
    
reverse_generate : if (rev_fields(0) = '1') generate
  -- reg frame contents
  reg_frame_proc : process(clk)
  begin
    if(rising_edge(clk))then
      if(rst = '1')then
        din_i <= (others => '0'); 
      else
        if(reg_frm = '1')then
          din_i(79 downto 72) <= bit_rev(frame_fields.address, 7, 1); 
          din_i(71 downto 64) <= bit_rev(frame_fields.control, 7, 1); 
          din_i(63 downto 56) <= bit_rev(frame_fields.tr_id, 7, 1); 
          din_i(55 downto 48) <= bit_rev(frame_fields.channel, 7, 1); 
          din_i(47 downto 40) <= bit_rev(frame_fields.err, 7, 1); 
          din_i(39 downto 32) <= bit_rev(frame_fields.length, 7, 1); 
          din_i(31 downto 24) <= bit_rev(dout_a_1of2, 7, 1); 
          din_i(23 downto 16) <= bit_rev(dout_a_2of2, 7, 1); 
          din_i(15 downto 08) <= bit_rev(dout_b_1of2, 7, 1); 
          din_i(07 downto 00) <= bit_rev(dout_b_2of2, 7, 1); 
        else
          din_i <= din_i; 
        end if; 
      end if; 
    end if; 
  end process; 
end generate reverse_generate; 
      
fcs_wrapperTX_inst : fcs_wrapperTX
  generic map(rev_fields => rev_fields(1))
  port map(
    clk     => clk,
    crc_en  => crc_en,
    len     => frameSize_i,
    din     => din_i,
    fcs_out => fcs_o,
    fcs_rdy => fcs_rdy
  ); 
      
elink_driver_inst : elink_driver
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk,
    state_o       => state_o_drv,
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull => tx_fifo_pfull,
    tx_fifo_wr    => tx_fifo_wr,
    tx_fifo_din   => tx_fifo_din,
    ------------------------------
    ------ Framer Interface ------
    enable        => drv_ena,
    frame_size    => frameSize_i,
    din           => din_i,
    fcs           => fcs_i,
    done          => drv_done
  ); 
      
  framer_busy <= busy; 
  state_o_frm <= state_o; 
  dout_a_1of2 <= frame_fields.dout_a(15 downto 8); 
  dout_a_2of2 <= frame_fields.dout_a(07 downto 0); 
  dout_b_1of2 <= frame_fields.dout_b(15 downto 8); 
  dout_b_2of2 <= frame_fields.dout_b(07 downto 0); 
      
end RTL; 
