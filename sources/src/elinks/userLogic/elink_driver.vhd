----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 09.04.2018 19:38:47
-- Design Name: E-Link Driver
-- Module Name: elink_driver - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Module that forwards the frame contents to the e-link interface.
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use work.scax_package.all; 
use work.centralRouter_package.all; 

entity elink_driver is
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    state_o       : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull : in  std_logic; 
    tx_fifo_wr    : out std_logic; 
    tx_fifo_din   : out std_logic_vector(17 downto 0); 
    ------------------------------
    ------ Framer Interface ------
    enable        : in  std_logic; 
    frame_size    : in  std_logic_vector(1 downto 0); 
    din           : in  std_logic_vector(79 downto 0); 
    fcs           : in  std_logic_vector(15 downto 0); 
    done          : out std_logic
  ); 
end elink_driver; 

architecture RTL of elink_driver is
  
  signal wr_en    : std_logic                     := '0'; 
  signal wait_cnt : unsigned(2 downto 0)          := (others => '0'); 
  signal sel_cnt  : unsigned(2 downto 0)          := (others => '0'); 
  signal dout_i0  : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dout_i1  : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dout_i2  : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dout_i3  : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dout_i   : std_logic_vector(17 downto 0) := (others => '0'); 
  signal max_cnt  : std_logic_vector(2 downto 0)  := (others => '0'); 
  
  
  type stateType is (ST_IDLE, ST_WR, ST_WAIT_0, ST_WAIT_1, ST_SWITCH, ST_DONE); 
  signal state : stateType := ST_IDLE; 
  
begin

drv_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(enable = '0')then
      done     <= '0'; 
      wr_en    <= '0'; 
      wait_cnt <= (others => '0'); 
      sel_cnt  <= (others => '0'); 
      max_cnt  <= (others => '0'); 
      state_o  <= "1111"; 
      state    <= ST_IDLE; 
    else
      case state is
          
        -- how many 16-bit words to write?
        when ST_IDLE => 
          state_o <= "0000"; 
          
          case frame_size is
            when "00"   => max_cnt <= "011"; 
            when "01"   => max_cnt <= "101"; 
            when "10"   => max_cnt <= "110"; 
            when "11"   => max_cnt <= "111"; 
            when others => max_cnt <= "011"; 
          end case; 
          
          state <= ST_WR; 
          
        -- write to the e-link FIFO
        when ST_WR => 
          state_o  <= "0001"; 
          wait_cnt <= (others => '0'); 
          
          if(tx_fifo_pfull = '0')then
            wr_en <= '1'; 
            state <= ST_WAIT_0; 
          else
            wr_en <= '0'; 
            state <= ST_WR; 
          end if; 
          
        -- first wait state
        when ST_WAIT_0 => 
          state_o  <= "0010"; 
          wr_en    <= '0'; 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "011")then
            state <= ST_SWITCH; 
          else
            state <= ST_WAIT_0; 
          end if; 
          
        -- increment the counter
        when ST_SWITCH => 
          state_o  <= "0011"; 
          wait_cnt <= (others => '0'); 
          
          if(sel_cnt = unsigned(max_cnt))then
            sel_cnt <= sel_cnt; 
            state   <= ST_DONE; 
          else
            sel_cnt <= sel_cnt + 1; 
            state   <= ST_WAIT_1; 
          end if; 
          
        -- second wait state
        when ST_WAIT_1 => 
          state_o  <= "0100"; 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "011")then
            state <= ST_WR; 
          else
            state <= ST_WAIT_1; 
          end if; 
          
        -- done!
        when ST_DONE => 
          state_o <= "0101"; 
          done    <= '1'; 
          
        when others => state <= ST_IDLE; 
          
      end case; 
    end if; 
  end if; 
end process; 
  
-- note: 13.09.2018: should these muxes be async?
-- sel din process
sel_din0_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(frame_size = "00")then
      case sel_cnt is
        when "000" => dout_i0   <= "10" & x"00" & x"00"; 
        when "001" => dout_i0   <= "00" & din(79 downto 64); -- addr & ctrl
        when "010" => dout_i0   <= "00" & fcs; 
        when "011" => dout_i0   <= "01" & x"00" & x"00"; 
        when "100" => dout_i0   <= (others => '0'); 
        when "101" => dout_i0   <= (others => '0'); 
        when "110" => dout_i0   <= (others => '0'); 
        when "111" => dout_i0   <= (others => '0'); 
        when others => dout_i0  <= (others => '0'); 
      end case; 
    else
      dout_i0 <= (others => '0'); 
    end if; 
  end if; 
end process; 
  
-- sel din process
sel_din1_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(frame_size = "01")then
      case sel_cnt is
        when "000" => dout_i1   <= "10" & x"00" & x"00"; 
        when "001" => dout_i1   <= "00" & din(79 downto 64); -- addr & ctrl
        when "010" => dout_i1   <= "00" & din(63 downto 48); -- tr_id & chan
        when "011" => dout_i1   <= "00" & din(47 downto 32); -- err & len
        when "100" => dout_i1   <= "00" & fcs; 
        when "101" => dout_i1   <= "01" & x"00" & x"00"; 
        when "110" => dout_i1   <= (others => '0'); 
        when "111" => dout_i1   <= (others => '0'); 
        when others => dout_i1  <= (others => '0'); 
      end case; 
    else
      dout_i1 <= (others => '0'); 
    end if; 
  end if; 
end process; 

-- sel din process
sel_din2_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(frame_size = "10")then
      case sel_cnt is
        when "000" => dout_i2   <= "10" & x"00" & x"00"; 
        when "001" => dout_i2   <= "00" & din(79 downto 64); -- addr & ctrl
        when "010" => dout_i2   <= "00" & din(63 downto 48); -- tr_id & chan
        when "011" => dout_i2   <= "00" & din(47 downto 32); -- err & len
        when "100" => dout_i2   <= "00" & din(31 downto 16); -- dout_a
        when "101" => dout_i2   <= "00" & fcs; 
        when "110" => dout_i2   <= "01" & x"00" & x"00"; 
        when "111" => dout_i2   <= (others => '0'); 
        when others => dout_i2  <= (others => '0'); 
      end case; 
    else
      dout_i2 <= (others => '0'); 
    end if; 
  end if; 
end process; 
  
-- sel din process
sel_din3_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(frame_size = "11")then
      case sel_cnt is
        when "000" => dout_i3   <= "10" & x"00" & x"00"; 
        when "001" => dout_i3   <= "00" & din(79 downto 64); -- addr & ctrl
        when "010" => dout_i3   <= "00" & din(63 downto 48); -- tr_id & chan
        when "011" => dout_i3   <= "00" & din(47 downto 32); -- err & len
        when "100" => dout_i3   <= "00" & din(31 downto 16); -- dout_a
        when "101" => dout_i3   <= "00" & din(15 downto 0);  -- dout_b
        when "110" => dout_i3   <= "00" & fcs; 
        when "111" => dout_i3   <= "01" & x"00" & x"00"; 
        when others => dout_i3  <= (others => '0'); 
      end case; 
    else
      dout_i3 <= (others => '0'); 
    end if; 
  end if; 
end process; 
  
dout_i <= dout_i0 or dout_i1 or dout_i2 or dout_i3; 

pipe_proc : process(clk)
begin
  if(rising_edge(clk))then
    tx_fifo_wr  <= wr_en; 
    tx_fifo_din <= dout_i; 
  end if; 
end process; 
  
end RTL; 
