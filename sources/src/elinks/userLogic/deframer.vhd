----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 28.08.2018 13:30:10
-- Design Name: Deframer
-- Module Name: deframer - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Module that dissects received frame contents and performs the FCS
-- check
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use work.scax_package.all; 
use work.centralRouter_package.all; 

entity deframer is
  generic(
    elinkEncoding : std_logic_vector(1 downto 0) := "01"; -- 8b10b (01) hdlc (10)
    rev_fields    : std_logic_vector(1 downto 0) := (others => '0')); --00:revNone,01:revData,10:revFCS,11:revBoth
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; -- user clock
    rst           : in  std_logic; -- reset
    deframer_busy : out std_logic; -- busy
    state_o_dfrm  : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    rx_fifo_empty : in  std_logic; -- rx fifo empty
    rx_fifo_full  : in  std_logic; -- rx fifo full
    rx_fifo_dout  : in  std_logic_vector(9 downto 0); -- rx fifo 8b10b decoded data
    rx_fifo_rd    : out std_logic; -- rx fifo rd_ena
    ------------------------------
    -- Master Handler Interface --
    handler_busy  : in  std_logic; -- handler not in idle
    frame_new     : out std_logic; -- new frame available
    frame_fields  : out frame_rx;  -- frame contents
    frame_error   : out std_logic_vector(1 downto 0);  -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_size    : out std_logic_vector(1 downto 0)   -- only 4 size types
  ); 
end deframer; 

architecture RTL of deframer is
  
component fcs_wrapperRX
  generic(rev_fields : std_logic := '0'); -- bit-reverse frame contents
  port(
    clk     : in  std_logic; 
    crc_en  : in  std_logic; 
    len     : in  std_logic_vector(3 downto 0); 
    din     : in  std_logic_vector(95 downto 0); 
    fcs_rdy : out std_logic; 
    fcs_ok  : out std_logic
  ); 
end component; 
  
  signal rd_en        : std_logic := '0'; 
  signal fcs_en       : std_logic := '0'; 
  signal frame_len    : unsigned(3 downto 0)          := (others => '0'); 
  signal wait_cnt     : unsigned(2 downto 0)          := (others => '0'); 
  signal dinFlag      : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal din_i        : std_logic_vector(9 downto 0)  := (others => '0'); 
  signal din_a_i      : std_logic_vector(15 downto 0) := (others => '0'); 
  signal din_b_i      : std_logic_vector(15 downto 0) := (others => '0'); 
  signal state_o      : std_logic_vector(3 downto 0)  := (others => '0'); 
  
  signal crc_en       : std_logic := '0'; 
  signal fcs_rdy      : std_logic := '0'; 
  signal fcs_ok       : std_logic := '0'; 
  signal busy         : std_logic := '0'; 
  signal reg_frm      : std_logic := '0'; 
  signal frame_i      : frame_rx; 
  signal frame_size_i : std_logic_vector(1 downto 0)  := (others => '0'); 
  signal din_fcs      : std_logic_vector(95 downto 0) := (others => '0'); 
  
  signal delim_end : std_logic_vector(7 downto 0) := (others => '0'); 
  signal delim_bgn : std_logic_vector(7 downto 0) := (others => '0'); 
  
  
  type arrayType is array (12 downto 0) of std_logic_vector(7 downto 0); -- one more index for error
  signal frame_array : arrayType; 
  
  type stateType is (ST_IDLE, ST_RD, ST_REG, ST_CHK_LEN, ST_WAIT_FCS, ST_CHK_MH, ST_DONE); 
  signal state : stateType := ST_IDLE; 
  
-- bit-reversal function
function bit_rev(s1 : std_logic_vector; high : natural; bytes : natural) return std_logic_vector is 
  variable rr : std_logic_vector((7*bytes + bytes-1) downto 0); 
begin 
  for ii in (7*bytes + bytes-1) downto 0 loop 
    rr(ii) := s1(high-ii); 
  end loop; 
  return rr; 
end bit_rev; 

begin
  
-- deframer FSM
def_FSM_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(rst = '1')then
      rd_en       <= '0'; 
      fcs_en      <= '0'; 
      busy        <= '0'; 
      reg_frm     <= '0'; 
      frame_len   <= (others => '0'); 
      wait_cnt    <= (others => '0'); 
      frame_error <= (others => '0'); 
      state_o     <= "1111"; 
      state       <= ST_IDLE; 
    else
      case state is
          
        -- monitor the empty signal
        when ST_IDLE => 
          state_o     <= "0000"; 
          frame_new   <= '0'; 
          reg_frm     <= '0'; 
          rd_en       <= '0'; 
          wait_cnt    <= (others => '0'); 
          frame_error <= (others => '0'); 
          
          if(rx_fifo_empty = '0')then
            state <= ST_RD; 
          else
            state <= ST_IDLE; 
          end if; 
          
        -- read the fifo
        when ST_RD => 
          state_o  <= "0001"; 
          busy     <= '1'; 
          wait_cnt <= wait_cnt + 1; 
          
          -- rd_en control
          if(wait_cnt = "000")then
            rd_en <= '1'; 
          else
            rd_en <= '0'; 
          end if; 
          -- state switching control
          if(wait_cnt = "111")then
            state <= ST_REG; 
          else
            state <= ST_RD; 
          end if; 
          
        -- inspect the byte and register
        when ST_REG => 
          state_o <= "0010"; 
          
          if(frame_len > "1100")then -- bad length
            frame_len   <= frame_len; 
            frame_error <= "10"; 
            state       <= ST_CHK_MH; -- error
          elsif(dinFlag = "10")then   -- SOP found
            frame_len   <= frame_len; 
            frame_error <= "00"; 
            state       <= ST_IDLE; 
          elsif(dinFlag = "00")then -- data
            frame_len                          <= frame_len + 1; 
            frame_error                        <= "00"; 
            frame_array(to_integer(frame_len)) <= din_i(7 downto 0); 
            state                              <= ST_IDLE; 
          elsif(dinFlag = "01")then -- EOP found
            frame_len   <= frame_len; 
            frame_error <= "00"; 
            state       <= ST_CHK_LEN; 
          else
            frame_len   <= frame_len; -- what was that???
            frame_error <= "01"; 
            state       <= ST_CHK_MH; -- error
          end if; 
          
        -- what is the length?
        when ST_CHK_LEN => 
          state_o <= "0011"; 
          
          case frame_len is
            when "0100" | "1000" | "1010" | "1100" => -- good length
              fcs_en      <= '1'; 
              frame_error <= "00"; 
              state       <= ST_WAIT_FCS; 
            when others => -- bad length
              fcs_en      <= '0'; 
              frame_error <= "10"; 
              state       <= ST_CHK_MH; -- error
          end case; 
          
        -- wait for fcs checking
        when ST_WAIT_FCS => 
          state_o <= "0100"; 
          fcs_en  <= '0'; 
          
          if(fcs_rdy = '1' and fcs_ok = '1')then -- good fcs
            reg_frm     <= '1'; 
            frame_error <= "00"; 
            state       <= ST_CHK_MH; 
          elsif(fcs_rdy = '1' and fcs_ok = '0')then -- bad fcs -> SREJ
            reg_frm     <= '0'; 
            frame_error <= "11"; 
            state       <= ST_CHK_MH; -- error
          else
            reg_frm     <= '0'; 
            frame_error <= "00"; 
            state       <= ST_WAIT_FCS; 
          end if; 
          
        -- check MH
        when ST_CHK_MH => 
          state_o <= "0101"; 
          
          if(handler_busy = '0')then
            frame_new <= '1'; 
            state     <= ST_DONE; 
          else
            frame_new <= '0'; 
            state     <= ST_CHK_MH; 
          end if; 
          
        -- back to idle when MH goes busy
        when ST_DONE => 
          state_o   <= "0110"; 
          frame_len <= (others => '0'); 
          busy      <= '0'; 
          
          if(handler_busy = '1')then
            state <= ST_IDLE; 
          else
            state <= ST_DONE; 
          end if; 
          
        when others => state <= ST_IDLE; 
          
      end case; 
    end if; 
  end if; 
end process; 
  
-- din_flag asserted
dinFlag_proc : process(clk)
begin
  if(rising_edge(clk))then
    if(din_i = "10" & delim_bgn)then
      dinFlag <= "10"; -- SOP detected
    elsif(din_i = "01" & delim_end)then
      dinFlag <= "01"; -- EOP detected
    elsif(din_i(9 downto 8) = "00")then
      dinFlag <= "00"; -- MOP detected
    else
      dinFlag <= "11"; -- Unknown
    end if; 
  end if; 
end process; 
  
-- fcs demux proc
fcs_demux_proc : process(clk)
begin
  if(rising_edge(clk))then
    crc_en <= fcs_en; 
    case frame_len is
      when "0100" =>                             -- 4
        din_fcs(95 downto 88) <= frame_array(0); -- addr
        din_fcs(87 downto 80) <= frame_array(1); -- ctrl
        din_fcs(79 downto 72) <= frame_array(2); -- fcs 1/2
        din_fcs(71 downto 64) <= frame_array(3); -- fcs 2/2
        din_fcs(63 downto 56) <= (others => '0'); 
        din_fcs(55 downto 48) <= (others => '0'); 
        din_fcs(47 downto 40) <= (others => '0'); 
        din_fcs(39 downto 32) <= (others => '0'); 
        din_fcs(31 downto 24) <= (others => '0'); 
        din_fcs(23 downto 16) <= (others => '0'); 
        din_fcs(15 downto 08) <= (others => '0'); 
        din_fcs(07 downto 00) <= (others => '0'); 
      when "1000" =>                             -- 8
        din_fcs(95 downto 88) <= frame_array(0); -- addr
        din_fcs(87 downto 80) <= frame_array(1); -- ctrl
        din_fcs(79 downto 72) <= frame_array(2); -- tr.id
        din_fcs(71 downto 64) <= frame_array(3); -- channel
        din_fcs(63 downto 56) <= frame_array(4); -- len
        din_fcs(55 downto 48) <= frame_array(5); -- cmd
        din_fcs(47 downto 40) <= frame_array(6); -- fcs 1/2
        din_fcs(39 downto 32) <= frame_array(7); -- fcs 2/2
        din_fcs(31 downto 24) <= (others => '0'); 
        din_fcs(23 downto 16) <= (others => '0'); 
        din_fcs(15 downto 08) <= (others => '0'); 
        din_fcs(07 downto 00) <= (others => '0'); 
      when "1010" =>                             -- 10
        din_fcs(95 downto 88) <= frame_array(0); -- addr
        din_fcs(87 downto 80) <= frame_array(1); -- ctrl
        din_fcs(79 downto 72) <= frame_array(2); -- tr.id
        din_fcs(71 downto 64) <= frame_array(3); -- channel
        din_fcs(63 downto 56) <= frame_array(4); -- len
        din_fcs(55 downto 48) <= frame_array(5); -- cmd
        din_fcs(47 downto 40) <= frame_array(6); -- data 1/2
        din_fcs(39 downto 32) <= frame_array(7); -- data 2/2
        din_fcs(31 downto 24) <= frame_array(8); -- fcs 1/2
        din_fcs(23 downto 16) <= frame_array(9); -- fcs 2/2
        din_fcs(15 downto 08) <= (others => '0'); 
        din_fcs(07 downto 00) <= (others => '0'); 
      when "1100" =>                              -- 12
        din_fcs(95 downto 88) <= frame_array(0);  -- addr
        din_fcs(87 downto 80) <= frame_array(1);  -- ctrl
        din_fcs(79 downto 72) <= frame_array(2);  -- tr.id
        din_fcs(71 downto 64) <= frame_array(3);  -- channel
        din_fcs(63 downto 56) <= frame_array(4);  -- len
        din_fcs(55 downto 48) <= frame_array(5);  -- cmd
        din_fcs(47 downto 40) <= frame_array(6);  -- data 1/4
        din_fcs(39 downto 32) <= frame_array(7);  -- data 2/4
        din_fcs(31 downto 24) <= frame_array(8);  -- data 3/4
        din_fcs(23 downto 16) <= frame_array(9);  -- data 4/4
        din_fcs(15 downto 08) <= frame_array(10); -- fcs 1/2
        din_fcs(07 downto 00) <= frame_array(11); -- fcs 2/2
      when others => 
        din_fcs <= (others => '0'); 
    end case; 
  end if; 
end process; 

normal_generate : if (rev_fields(0) = '0') generate
  -- reg the frame
  reg_frame_proc : process(clk)
  begin
    if(rising_edge(clk))then
      if(rst = '1')then
        frame_size_i    <= (others => '0'); 
        frame_i.address <= (others => '0'); 
        frame_i.control <= (others => '0'); 
        frame_i.tr_id   <= (others => '0'); 
        frame_i.channel <= (others => '0'); 
        frame_i.length  <= (others => '0'); 
        frame_i.command <= (others => '0'); 
        frame_i.din_a   <= (others => '0'); 
        frame_i.din_b   <= (others => '0'); 
      else
        if(reg_frm = '1')then
          case frame_len is
            when "0100" => -- 4
              frame_size_i    <= "00"; 
              frame_i.address <= frame_array(0); 
              frame_i.control <= frame_array(1); 
            when "1000" => -- 8
              frame_size_i    <= "01"; 
              frame_i.address <= frame_array(0); 
              frame_i.control <= frame_array(1); 
              frame_i.tr_id   <= frame_array(2); 
              frame_i.channel <= frame_array(3); 
              frame_i.length  <= frame_array(4); 
              frame_i.command <= frame_array(5); 
            when "1010" => -- 10
              frame_size_i    <= "10"; 
              frame_i.address <= frame_array(0); 
              frame_i.control <= frame_array(1); 
              frame_i.tr_id   <= frame_array(2); 
              frame_i.channel <= frame_array(3); 
              frame_i.length  <= frame_array(4); 
              frame_i.command <= frame_array(5); 
              frame_i.din_a   <= din_a_i; 
            when "1100" => -- 12
              frame_size_i    <= "11"; 
              frame_i.address <= frame_array(0); 
              frame_i.control <= frame_array(1); 
              frame_i.tr_id   <= frame_array(2); 
              frame_i.channel <= frame_array(3); 
              frame_i.length  <= frame_array(4); 
              frame_i.command <= frame_array(5); 
              frame_i.din_a   <= din_a_i; 
              frame_i.din_b   <= din_b_i; 
            when others => 
              frame_size_i <= frame_size_i; 
              frame_i      <= frame_i; 
          end case; 
        else
          frame_size_i <= frame_size_i; 
          frame_i      <= frame_i; 
        end if; 
      end if; 
    end if; 
  end process; 
end generate normal_generate; 
    
reverse_generate : if (rev_fields(0) = '1') generate
  -- reg the frame
  reg_frame_proc : process(clk)
  begin
    if(rising_edge(clk))then
      if(rst = '1')then
        frame_size_i    <= (others => '0'); 
        frame_i.address <= (others => '0'); 
        frame_i.control <= (others => '0'); 
        frame_i.tr_id   <= (others => '0'); 
        frame_i.channel <= (others => '0'); 
        frame_i.length  <= (others => '0'); 
        frame_i.command <= (others => '0'); 
        frame_i.din_a   <= (others => '0'); 
        frame_i.din_b   <= (others => '0'); 
      else
        if(reg_frm = '1')then
          case frame_len is
            when "0100" => -- 4
              frame_size_i    <= "00"; 
              frame_i.address <= bit_rev(frame_array(0), 7, 1); 
              frame_i.control <= bit_rev(frame_array(1), 7, 1); 
            when "1000" => -- 8
              frame_size_i    <= "01"; 
              frame_i.address <= bit_rev(frame_array(0), 7, 1); 
              frame_i.control <= bit_rev(frame_array(1), 7, 1); 
              frame_i.tr_id   <= bit_rev(frame_array(2), 7, 1); 
              frame_i.channel <= bit_rev(frame_array(3), 7, 1); 
              frame_i.length  <= bit_rev(frame_array(4), 7, 1); 
              frame_i.command <= bit_rev(frame_array(5), 7, 1); 
            when "1010" => -- 10
              frame_size_i    <= "10"; 
              frame_i.address <= bit_rev(frame_array(0), 7, 1); 
              frame_i.control <= bit_rev(frame_array(1), 7, 1); 
              frame_i.tr_id   <= bit_rev(frame_array(2), 7, 1); 
              frame_i.channel <= bit_rev(frame_array(3), 7, 1); 
              frame_i.length  <= bit_rev(frame_array(4), 7, 1); 
              frame_i.command <= bit_rev(frame_array(5), 7, 1); 
              frame_i.din_a   <= bit_rev(din_a_i, 15, 2); 
            when "1100" => -- 12
              frame_size_i    <= "11"; 
              frame_i.address <= bit_rev(frame_array(0), 7, 1); 
              frame_i.control <= bit_rev(frame_array(1), 7, 1); 
              frame_i.tr_id   <= bit_rev(frame_array(2), 7, 1); 
              frame_i.channel <= bit_rev(frame_array(3), 7, 1); 
              frame_i.length  <= bit_rev(frame_array(4), 7, 1); 
              frame_i.command <= bit_rev(frame_array(5), 7, 1); 
              frame_i.din_a   <= bit_rev(din_a_i, 15, 2); 
              frame_i.din_b   <= bit_rev(din_b_i, 15, 2); 
            when others => 
              frame_size_i <= frame_size_i; 
              frame_i      <= frame_i; 
          end case; 
        else
          frame_size_i <= frame_size_i; 
          frame_i      <= frame_i; 
        end if; 
      end if; 
    end if; 
  end process; 
  end generate reverse_generate; 
      
fcs_wrapperRX_inst : fcs_wrapperRX
  generic map(rev_fields => rev_fields(1))
  port map(
    clk     => clk,
    crc_en  => crc_en,
    len     => std_logic_vector(frame_len),
    din     => din_fcs,
    fcs_rdy => fcs_rdy,
    fcs_ok  => fcs_ok
  ); 
      
  state_o_dfrm  <= state_o; 
  rx_fifo_rd    <= rd_en; 
  deframer_busy <= busy; 
  frame_size    <= frame_size_i; 
  frame_fields  <= frame_i; 
  din_a_i       <= frame_array(6) & frame_array(7); 
  din_b_i       <= frame_array(8) & frame_array(9); 
  delim_bgn     <= HDLC_flag when elinkEncoding = "10" else Kchar_sop; 
  delim_end     <= HDLC_flag when elinkEncoding = "10" else Kchar_eop; 
      
din_pipe_proc : process(clk)
begin
  if(rising_edge(clk))then
    din_i <= rx_fifo_dout; 
  end if; 
end process; 
      
end RTL; 
