----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 21.11.2017 16:08:28
-- Design Name: Felix connection tester
-- Module Name: flx_tester - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Tests the TX to FELIX
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 

entity flx_tester is
  port(
    ------------------------------
    ------- General Interface ----
    clk_in        : in  std_logic; 
    ena_flx_test  : in  std_logic; 
    ------------------------------
    ------ SCA Config Interface --
    dout_tester   : out std_logic_vector(17 downto 0); 
    dvalid_tester : out std_logic
  ); 
end flx_tester; 

architecture RTL of flx_tester is
  
  signal dout_tester_i    : std_logic_vector(17 downto 0) := (others => '0'); 
  signal dvalid_tester_i  : std_logic := '0'; 
  signal wr_elink         : std_logic := '0'; 
  signal cnt_data         : unsigned(2 downto 0)  := (others => '0'); 
  signal wait_cnt         : unsigned(1 downto 0)  := (others => '0'); 
  
  constant omit_eop       : std_logic := '0'; 
  
  type stateType is (ST_IDLE, ST_WRITE, ST_CNT, ST_WAIT, ST_DONE); 
  signal state     : stateType := ST_IDLE; 
  signal state_prv : stateType := ST_IDLE; 
  
  attribute FSM_ENCODING          : string; 
  attribute FSM_ENCODING of state : signal is "ONE_HOT"; 
  
begin
  
-- FSM that drives the data to the HDLC TX core
FSM_flx_tester_proc : process(clk_in)
begin
  if(rising_edge(clk_in))then
    if(ena_flx_test = '0')then
      wr_elink <= '0'; 
      cnt_data <= (others => '0'); 
      wait_cnt <= (others => '0'); 
      state    <= ST_IDLE; 
    else
      case state is
        when ST_IDLE => 
          state_prv <= ST_IDLE; 
          state     <= ST_WAIT; -- to ST_WRITE
          
        -- write the data to the HDLC core
        when ST_WRITE => 
          wr_elink  <= '1'; 
          state_prv <= ST_WRITE; 
          state     <= ST_WAIT; -- to ST_CNT
          
        -- drive the data to the MUX
        when ST_CNT => 
          cnt_data  <= cnt_data + 1; 
          state_prv <= ST_CNT; 

          if(cnt_data = "011")then
            state <= ST_DONE; 
          else
            state <= ST_WAIT; -- to ST_WRITE;
          end if; 
          
        -- stay here until reset by VIO
        when ST_DONE => 
          state <= ST_DONE; 
          
        -- generic state that waits
        when ST_WAIT => 
          wr_elink <= '0'; 
          wait_cnt <= wait_cnt + 1; 
          
          if(wait_cnt = "11")then
            case state_prv is
              when ST_IDLE => state <= ST_WRITE; 
              when ST_WRITE => state <= ST_CNT; 
              when ST_CNT => state <= ST_WRITE; 
              when others => state <= ST_WAIT; 
            end case; 
          else
            state <= ST_WAIT; 
          end if; 
          
        when others => state <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 
  
-- data to be sent
dout_tester_sel_proc : process(cnt_data)
begin
  case cnt_data is
    when "000"  => dout_tester_i <= "10" & x"00" & x"00"; -- SOP == SOF
    when "001"  => dout_tester_i <= "00" & x"FF" & x"63"; -- ADDR & CTRL
    when "010"  => dout_tester_i <= "00" & x"E5" & x"5E"; -- FCS 1/2 FCS 2/2
    when "011"  => dout_tester_i <= "01" & x"00" & x"00"; -- LEN & CMD
    when others => dout_tester_i <= (others => '0'); 
  end case; 
end process; 

pipe_proc : process(clk_in)
begin
  if(rising_edge(clk_in))then
    
    dout_tester <= dout_tester_i; 
    
    if(dout_tester_i(17 downto 16) = "01" and omit_eop = '1')then
      dvalid_tester <= '0'; 
    else
      dvalid_tester <= wr_elink; 
    end if; 
  end if; 
end process; 

end RTL; 
