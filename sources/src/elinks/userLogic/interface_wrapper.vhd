----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 19.09.2018 15:32:59
-- Design Name: Interface Wrapper
-- Module Name: interface_wrapper - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Wrapper for the E-Link modules and the framer/deframer.
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use work.scax_package.all; 
use work.centralRouter_package.all; 

entity interface_wrapper is
  generic(
    debug_mode    : std_logic := '1'; 
    TX_dataRate : integer   := 80;   -- only 80
    elinkEncoding : std_logic_vector(1 downto 0) := "01"; -- only "01" (8b10b)
    ser_input     : boolean   := false -- 2-bit wide bus
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk             : in  std_logic; -- user clock
    clk_40          : in  std_logic; -- 40Mhz e-link clock
    clk_80          : in  std_logic; -- 80Mhz e-link clock
    clk_160         : in  std_logic; -- 160Mhz e-link clock
    clk_320         : in  std_logic; -- 320Mhz e-link clock
    rst             : in  std_logic; -- reset the modules
    ena_flx_test    : in  std_logic; 
    reverse_rx      : in  std_logic; 
    reverse_tx      : in  std_logic; 
    dbg_fifo_rd     : in  std_logic; 
    dbg_fifo_rx     : out std_logic_vector(9 downto 0); 
    dbg_fifo_tx     : out std_logic_vector(9 downto 0); 
    state_o_frm     : out std_logic_vector(3 downto 0); 
    state_o_drv     : out std_logic_vector(3 downto 0); 
    state_o_dfrm    : out std_logic_vector(3 downto 0); 
    dbg_fifo_empty  : out std_logic; 
    drdy_raw        : out std_logic; 
    din_raw         : out std_logic_vector(9 downto 0); 
    wrElink_raw     : out std_logic; 
    dout_raw        : out std_logic_vector(17 downto 0); 
    -------         --------------
    -- Master Handler Interface --
    -- rx
    handler_busy    : in  std_logic; -- handler not in idle
    deframer_busy   : out std_logic; -- busy
    frame_new       : out std_logic; -- new frame available
    frame_fieldsRX  : out frame_rx;  -- frame contents
    frame_error     : out std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_sizeRX    : out std_logic_vector(1 downto 0); -- only 4 size types
    -- tx
    frame_fieldsTX  : in  frame_tx;  -- frame contents
    tx_request      : in  std_logic; -- tx new frame 
    frame_sizeTX    : in  std_logic_vector(1 downto 0); -- only 4 size types
    framer_busy     : out std_logic; -- busy
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        : in std_logic; -- 1-bit elink rx
    rx_elink2bit    : in std_logic_vector(1 downto 0);    -- 2-bit elink rx
    rx_swap         : in std_logic; -- swap input bits
    -- tx
    tx_swap         : in  std_logic; -- swap output bits
    tx_elink        : out std_logic; -- 1-bit elink tx
    tx_elink8bit    : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit    : out std_logic_vector(1 downto 0)    -- 2-bit elink tx
  ); 
end interface_wrapper; 

architecture RTL of interface_wrapper is
  
component framer
  generic(rev_fields : std_logic_vector(1 downto 0) := (others => '0')); --00:revNone,01:revData,10:revFCS,11:revBoth
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; -- user clock
    rst           : in  std_logic; -- reset
    framer_busy   : out std_logic; -- busy
    state_o_frm   : out std_logic_vector(3 downto 0); 
    state_o_drv   : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull : in  std_logic; -- tx fifo prog full
    tx_fifo_wr    : out std_logic; -- tx fifo wr_ena
    tx_fifo_din   : out std_logic_vector(17 downto 0); -- tx fifo din
     ------------------------------
     -- Master Handler Interface --
    frame_fields  : in frame_tx;  -- frame contents
    tx_request    : in std_logic; -- tx new frame 
    frame_size    : in std_logic_vector(1 downto 0) -- only 4 size types
  ); 
end component; 
  
component deframer
  generic(
    elinkEncoding : std_logic_vector(1 downto 0) := "01"; -- 8b10b (01) or hdlc (10)
    rev_fields    : std_logic_vector(1 downto 0) := (others => '0')); --00:revNone,01:revData,10:revFCS,11:revBoth
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; -- user clock
    rst           : in  std_logic; -- reset
    deframer_busy : out std_logic; -- busy
    state_o_dfrm  : out std_logic_vector(3 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    rx_fifo_empty : in  std_logic; -- rx fifo empty
    rx_fifo_full  : in  std_logic; -- rx fifo full
    rx_fifo_dout  : in  std_logic_vector(9 downto 0); -- rx fifo 8b10b decoded data
    rx_fifo_rd    : out std_logic; -- rx fifo rd_ena
    ------------------------------
    -- Master Handler Interface --
    handler_busy  : in  std_logic; -- handler not in idle
    frame_new     : out std_logic; -- new frame available
    frame_fields  : out frame_rx;  -- frame contents
    frame_error   : out std_logic_vector(1 downto 0); -- error while dissecting (3 codes: "01"=generic, "10"=badLen, "11"=srej)
    frame_size    : out std_logic_vector(1 downto 0)  -- only 4 size types
  ); 
end component; 
  
component elink_wrapper
  generic(
    TX_dataRate      : integer                       := 80;   -- RX is only 80, TX can be 80 or 320
    elinkEncoding    : std_logic_vector (1 downto 0) := "01"; -- only "01" (8b10b)
    serialized_input : boolean                       := false -- 2-bit wide bus
  ); 
  port(
    -----------------------------
    ------ General Interface ----
    clk_usr       : in  std_logic; -- user clock (rd/wr fifo domain)
    clk_40        : in  std_logic; -- elink clock
    clk_80        : in  std_logic; -- elink clock
    clk_160       : in  std_logic; -- elink clock
    clk_320       : in  std_logic; -- elink clock
    rst           : in  std_logic; -- reset the core. must be high at init
    reverse_rx    : in  std_logic; 
    reverse_tx    : in  std_logic; 
    drdy_raw      : out std_logic; 
    din_raw       : out std_logic_vector(9 downto 0); 
    -----------------------------
    ------ RX Interface ---------
    rx_elink      : in  std_logic; -- 1-bit elink rx
    rx_elink2bit  : in  std_logic_vector(1 downto 0);  -- 2-bit elink rx
    rx_swap       : in  std_logic; -- swap input bits
    rx_fifo_rd    : in  std_logic; -- rx fifo rd_ena
    rx_fifo_empty : out std_logic; -- rx fifo empty
    rx_fifo_full  : out std_logic; -- rx fifo full
    rx_fifo_dout  : out std_logic_vector(9 downto 0);  -- rx fifo 8b10b decoded data
    -----------------------------
    ------ TX Interface ---------
    tx_swap       : in  std_logic; -- swap output bits
    tx_fifo_wr    : in  std_logic; -- tx fifo wr_ena
    tx_fifo_din   : in  std_logic_vector(17 downto 0); -- tx fifo din
    tx_fifo_pfull : out std_logic; -- tx fifo prog full
    tx_elink      : out std_logic; -- 1-bit elink tx
    tx_elink8bit  : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit  : out std_logic_vector(1 downto 0)   -- 2-bit elink tx
  ); 
end component; 

component debug_buffer
  port(
    ------------------------------
    ------ General Interface -----
    clk           : in  std_logic; 
    rst           : in  std_logic; 
    rd_en_fifo    : in  std_logic; 
    fifo_empty    : out std_logic; 
    dout_rx       : out std_logic_vector(9 downto 0); 
    dout_tx       : out std_logic_vector(9 downto 0); 
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_fifo_dout  : in std_logic_vector(9 downto 0); -- rx fifo 8b10b decoded data
    state_dfrm    : in std_logic_vector(3 downto 0); -- rx fifo rd_ena
    -- tx
    tx_fifo_wr    : in std_logic; -- tx fifo wr_ena
    tx_fifo_din   : in std_logic_vector(17 downto 0)  -- tx fifo din
  ); 
end component; 
  
component flx_tester
  port(
    ----------------------------
    --- General Interface ------
    clk_in       : in std_logic; 
    ena_flx_test : in std_logic; 
    -----------------------------
    --- SCA Config Interface ----
    dout_tester   : out std_logic_vector(17 downto 0); 
    dvalid_tester : out std_logic
  ); 
end component; 
  
  signal rx_fifo_rd       : std_logic := '0'; 
  signal rx_fifo_empty    : std_logic := '0'; 
  signal rx_fifo_full     : std_logic := '0'; 
  signal tx_fifo_pfull    : std_logic := '0'; 
  signal tx_fifo_wr       : std_logic := '0'; 
  signal tx_fifo_wr_frm   : std_logic := '0'; 
  signal tx_fifo_wr_tst   : std_logic := '0'; 
  signal state_o_dfrm_i   : std_logic_vector(03 downto 0) := (others => '0'); 
  signal rx_fifo_dout     : std_logic_vector(09 downto 0) := (others => '0'); 
  signal tx_fifo_din      : std_logic_vector(17 downto 0) := (others => '0'); 
  signal tx_fifo_din_frm  : std_logic_vector(17 downto 0) := (others => '0'); 
  signal tx_fifo_din_tst  : std_logic_vector(17 downto 0) := (others => '0'); 
  
  constant rev_fieldsRX   : std_logic_vector(1 downto 0) := "10"; --00:revNone,01:revData,10:revFCS,11:revBoth
  constant rev_fieldsTX   : std_logic_vector(1 downto 0) := "10"; 
  
begin
  
framer_inst : framer
  generic map(rev_fields => rev_fieldsTX)
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk,
    rst           => rst,
    framer_busy   => framer_busy,
    state_o_frm   => state_o_frm,
    state_o_drv   => state_o_drv,
    ------------------------------
    ------ E-Link Interface ------
    tx_fifo_pfull => tx_fifo_pfull,
    tx_fifo_wr    => tx_fifo_wr_frm,
    tx_fifo_din   => tx_fifo_din_frm,
    ------------------------------
    -- Master Handler Interface --
    frame_fields  => frame_fieldsTX,
    tx_request    => tx_request,
    frame_size    => frame_sizeTX
  ); 
  
deframer_inst : deframer
  generic map(
    elinkEncoding => elinkEncoding,
    rev_fields    => rev_fieldsRX)
  port map(
    ------------------------------
    ------ General Interface -----
    clk           => clk,
    rst           => rst,
    deframer_busy => deframer_busy,
    state_o_dfrm  => state_o_dfrm_i,
    ------------------------------
    ------ E-Link Interface ------
    rx_fifo_empty => rx_fifo_empty,
    rx_fifo_full  => rx_fifo_full,
    rx_fifo_dout  => rx_fifo_dout,
    rx_fifo_rd    => rx_fifo_rd,
    ------------------------------
    -- Master Handler Interface --
    handler_busy  => handler_busy,
    frame_new     => frame_new,
    frame_fields  => frame_fieldsRX,
    frame_error   => frame_error,
    frame_size    => frame_sizeRX
  ); 
  
elink_wrapper_inst : elink_wrapper
  generic map(
    TX_dataRate      => TX_dataRate,
    elinkEncoding    => elinkEncoding,
    serialized_input => ser_input) 
  port map(
    -----------------------------
    ------ General Interface ----
    clk_usr       => clk,
    clk_40        => clk_40,
    clk_80        => clk_80,
    clk_160       => clk_160,
    clk_320       => clk_320,
    rst           => rst,
    reverse_rx    => reverse_rx,
    reverse_tx    => reverse_tx,
    drdy_raw      => drdy_raw,
    din_raw       => din_raw,
    -----------------------------
    ------ RX Interface ---------
    rx_elink      => rx_elink,
    rx_elink2bit  => rx_elink2bit,
    rx_swap       => rx_swap,
    rx_fifo_rd    => rx_fifo_rd,
    rx_fifo_empty => rx_fifo_empty,
    rx_fifo_full  => rx_fifo_full,
    rx_fifo_dout  => rx_fifo_dout,
    -----------------------------
    ------ TX Interface ---------
    tx_swap       => tx_swap,
    tx_fifo_wr    => tx_fifo_wr,
    tx_fifo_din   => tx_fifo_din,
    tx_fifo_pfull => tx_fifo_pfull,
    tx_elink      => tx_elink,
    tx_elink8bit  => tx_elink8bit,
    tx_elink2bit  => tx_elink2bit
  ); 
  
flx_tester_inst : flx_tester
  port map(
    ------------------------------------
    ------- General Interface ----------
    clk_in       => clk,
    ena_flx_test => ena_flx_test,
    ------------------------------------
    ------ SCA Config Interface --------
    dout_tester   => tx_fifo_din_tst,
    dvalid_tester => tx_fifo_wr_tst
  ); 

debug_buffer_generate : if (debug_mode = '1') generate 
  debug_buffer_inst : debug_buffer
    port map(
      ------------------------------
      ------ General Interface -----
      clk           => clk,
      rst           => rst,
      rd_en_fifo    => dbg_fifo_rd,
      fifo_empty    => dbg_fifo_empty,
      dout_rx       => dbg_fifo_rx,
      dout_tx       => dbg_fifo_tx,
      ------------------------------
      ------ E-Link Interface ------
      -- rx
      rx_fifo_dout  => rx_fifo_dout,
      state_dfrm    => state_o_dfrm_i,
      -- tx
      tx_fifo_wr    => tx_fifo_wr,
      tx_fifo_din   => tx_fifo_din
    ); 
end generate debug_buffer_generate; 
    
-- tx din mux
txDin_mux_proc : process(clk)
begin
  if(rising_edge(clk))then
    case ena_flx_test is
      when '0' => tx_fifo_din <= tx_fifo_din_frm; tx_fifo_wr <= tx_fifo_wr_frm; 
      when '1' => tx_fifo_din <= tx_fifo_din_tst; tx_fifo_wr <= tx_fifo_wr_tst; 
      when others => tx_fifo_din <= tx_fifo_din_frm; tx_fifo_wr <= tx_fifo_wr_frm; 
    end case; 
  end if; 
end process; 
    
  state_o_dfrm <= state_o_dfrm_i; 
  wrElink_raw  <= tx_fifo_wr; 
  dout_raw     <= tx_fifo_din; 
    
end RTL; 
