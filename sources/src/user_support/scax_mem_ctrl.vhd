----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 19.01.2019 21:22:31
-- Design Name: SCAX RAM Controller
-- Module Name: scax_mem_ctrl - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Read/Write Manager for RAM primitives via the SCAX. Associated
-- with one RAM primitive port. The user has to set the correct width of the
-- RAM address and the RAM din.
--
-- Dependencies:
-- 
-- Changelog:
-- 02.08.2019: Added read version. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

entity scax_mem_ctrl is
  generic(
    Nbits_addr_W  : natural := 12;  -- address width (write operation)
    Nbits_data_W  : natural := 32;  -- data width (write operation)
    Nbits_addr_R  : natural := 12;  -- address width (read operation) 
    Nbits_data_R  : natural := 32); -- data width (read operation)
  port(
    -------------------------------
    ------ General Interface ------
    wr_clk            : in std_logic; -- regFile wr_clk + mem element wr_clk
    rd_clk            : in std_logic; -- regFile wr_clk + mem element rd_clk
    rst               : in std_logic; -- reset
    -------------------------------
    -- Register File Interface ----
    -- write
    wrEn_fromSCAX     : in  std_logic;                                 -- write-pulse the memDin into the mem ctrl
    wr_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_W-1 downto 0); -- addr to write into
    din_fromSCAX      : in  std_logic_vector(Nbits_data_W-1 downto 0); -- data to write
    wr_addr_toSCAX    : out std_logic_vector(Nbits_addr_W-1 downto 0); -- current addr (back to scax)
    -- read
    rdEn_fromSCAX     : in  std_logic;                                 -- read-pulse the memDout into the mem ctrl
    rd_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_R-1 downto 0); -- addr to read from
    dout_toSCAX       : out std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read (back to scax)
    rd_addr_toSCAX    : out std_logic_vector(Nbits_addr_R-1 downto 0); -- current addr (back to scax)
    -------------------------------
    -- Memory Element Interface ---
    -- write
    wrEn_toMem        : out std_logic;                                 -- to ram/fifo wr_en
    wr_addr_toMem     : out std_logic_vector(Nbits_addr_W-1 downto 0); -- to ram/fifo addr
    din_toMem         : out std_logic_vector(Nbits_data_W-1 downto 0); -- to ram/fifo din
    --read
    dout_fromMem      : in  std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read
    rdEn_toMem        : out std_logic;
    rd_addr_toMem     : out std_logic_vector(Nbits_addr_R-1 downto 0) -- to ram/fifo addr
  ); 
end scax_mem_ctrl; 

architecture RTL of scax_mem_ctrl is
  
  -- write FSM
  signal first_write            : std_logic := '1'; 
  signal wrEn_toMem_i           : std_logic := '0'; 
  signal wr_addr_fromSCAX_i     : std_logic_vector(Nbits_addr_W-1 downto 0) := (others => '0'); 
  signal wr_addr_fromSCAX_init  : std_logic_vector(Nbits_addr_W-1 downto 0) := (others => '0'); 
  signal wr_addr_toMem_i        : std_logic_vector(Nbits_addr_W-1 downto 0) := (others => '0'); 
  signal din_toMem_i            : std_logic_vector(Nbits_data_W-1 downto 0) := (others => '0'); 
  signal wr_ramAddr_cnt         : unsigned(Nbits_addr_W-1 downto 0)         := (others => '0'); 
  
  type stateType_wr is (ST_IDLE, ST_CHK_ADDR, ST_LD_ADDR, ST_WR_EN, ST_DONE); 
  signal state_wr : stateType_wr := ST_IDLE; 

  -- read FSM
  signal first_read             : std_logic := '1'; 
  signal rdEn_toMem_i           : std_logic := '0'; 
  signal rd_addr_fromSCAX_i     : std_logic_vector(Nbits_addr_R-1 downto 0) := (others => '0'); 
  signal rd_addr_fromSCAX_init  : std_logic_vector(Nbits_addr_R-1 downto 0) := (others => '0'); 
  signal rd_addr_toMem_i        : std_logic_vector(Nbits_addr_R-1 downto 0) := (others => '0'); 
  signal dout_fromMem_i         : std_logic_vector(Nbits_data_R-1 downto 0) := (others => '0');
  signal rd_w8_cnt              : unsigned(1 downto 0)                      := (others => '0');
  signal rd_ramAddr_cnt         : unsigned(Nbits_addr_R-1 downto 0)         := (others => '0'); 

  type stateType_rd is (ST_IDLE, ST_CHK_ADDR, ST_LD_ADDR, ST_WAIT, ST_REG_DOUT, ST_WAIT_2ND, ST_DONE); 
  signal state_rd : stateType_rd := ST_IDLE; 
  
begin
  
  ---------------------------------------------------
  -- regFile I/F FSM (WR) ---------------------------
  ---------------------------------------------------
  
-- gets the write info from the regFile/SCAX
wrFSM_proc : process(wr_clk)
begin
  if(rising_edge(wr_clk))then
    if(rst = '1')then
      first_write           <= '1'; 
      wrEn_toMem_i          <= '0'; 
      wr_addr_fromSCAX_i    <= (others => '0'); 
      wr_addr_fromSCAX_init <= (others => '0'); 
      din_toMem_i           <= (others => '0'); 
      wr_ramAddr_cnt        <= (others => '0'); 
      wr_addr_toMem_i       <= (others => '0');
      state_wr              <= ST_IDLE; 
    else
      case state_wr is
          
        -- wait for memWrEn from SCAX
        when ST_IDLE => 
          if(wrEn_fromSCAX = '1')then
            wr_addr_fromSCAX_i  <= wr_addr_fromSCAX; 
            din_toMem_i         <= din_fromSCAX; 
            state_wr            <= ST_CHK_ADDR; 
          else
            wr_addr_fromSCAX_i  <= wr_addr_fromSCAX_i; 
            din_toMem_i         <= din_toMem_i; 
            state_wr            <= ST_IDLE; 
          end if; 
          
        -- check the registered address against the first received
        when ST_CHK_ADDR => 
          if(first_write = '1' or wr_addr_fromSCAX_i /= wr_addr_fromSCAX_init)then
            wr_ramAddr_cnt <= unsigned(wr_addr_fromSCAX_i); 
          else
            wr_ramAddr_cnt <= wr_ramAddr_cnt + 1; 
          end if; 
          
          state_wr <= ST_LD_ADDR; 
          
        -- we have the new address
        when ST_LD_ADDR => 
          wr_addr_toMem_i <= std_logic_vector(wr_ramAddr_cnt); 
          state_wr        <= ST_WR_EN; 

        -- write into memory
        when ST_WR_EN =>
          wrEn_toMem_i  <= '1';
          state_wr      <= ST_DONE;
          
        -- done, grab the address for future use
        when ST_DONE => 
          wrEn_toMem_i          <= '0'; 
          first_write           <= '0'; 
          wr_addr_fromSCAX_init <= wr_addr_fromSCAX; 
          
          if(wrEn_fromSCAX = '0')then
            state_wr <= ST_IDLE; 
          else
            state_wr <= ST_DONE; 
          end if; 
          
        when others => state_wr <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 

-- register level to regFile
wr_regFile_regLevel_proc : process(wr_clk)
begin
  if(rising_edge(wr_clk))then
    wr_addr_toSCAX <= wr_addr_toMem_i; 
  end if; 
end process; 
  
-- register level to RAM/FIFO
wr_MEM_regLevel_proc : process(wr_clk)
begin
  if(rising_edge(wr_clk))then
    din_toMem     <= din_toMem_i; 
    wr_addr_toMem <= wr_addr_toMem_i; 
    wrEn_toMem    <= wrEn_toMem_i; 
  end if; 
end process; 

-- read FSM
--  ---------------------------------------------------
--  -- regFile I/F FSM (RD) ---------------------------
--  ---------------------------------------------------        
  
-- gets the read info from the regFile/SCAX
rdFSM_proc : process(rd_clk)
begin
  if(rising_edge(rd_clk))then
    if(rst = '1')then
      first_read            <= '1'; 
      rdEn_toMem_i          <= '0'; 
      rd_addr_fromSCAX_i    <= (others => '0'); 
      rd_addr_fromSCAX_init <= (others => '0'); 
      rd_ramAddr_cnt        <= (others => '0');
      dout_fromMem_i        <= (others => '0');
      rd_addr_toMem_i       <= (others => '0'); 
      rd_w8_cnt             <= (others => '0');
      state_rd              <= ST_IDLE; 
    else
      case state_rd is
          
        -- wait for memWrEn from SCAX
        when ST_IDLE => 
          if(rdEn_fromSCAX = '1')then
            rd_addr_fromSCAX_i  <= rd_addr_fromSCAX; 
            state_rd            <= ST_CHK_ADDR; 
          else
            rd_addr_fromSCAX_i  <= rd_addr_fromSCAX_i; 
            state_rd            <= ST_IDLE; 
          end if; 
          
        -- check the registered address against the first received
        when ST_CHK_ADDR => 
          if(first_read = '1' or rd_addr_fromSCAX_i /= rd_addr_fromSCAX_init)then
            rd_ramAddr_cnt <= unsigned(rd_addr_fromSCAX_i); 
          else
            rd_ramAddr_cnt <= rd_ramAddr_cnt + 1; 
          end if; 
          
          state_rd <= ST_LD_ADDR; 
          
        -- we have the new address
        when ST_LD_ADDR => 
          rd_addr_toMem_i <= std_logic_vector(rd_ramAddr_cnt);  
          state_rd        <= ST_WAIT;

        -- wait for 3 cycles to account for any output registers of the memory
        when ST_WAIT => 
          rd_w8_cnt     <= rd_w8_cnt + 1;

          if(rd_w8_cnt = "00")then
            rdEn_toMem_i  <= '1';
          elsif(rd_w8_cnt = "01")then
            rdEn_toMem_i  <= '0';
          elsif(rd_w8_cnt = "11")then
            state_rd      <= ST_REG_DOUT; 
          else
            state_rd      <= ST_WAIT;
          end if;

        -- register the dout
        when ST_REG_DOUT =>
          dout_fromMem_i  <= dout_fromMem;
          state_rd        <= ST_WAIT_2ND;

        -- wait for the second rdEn pulse
        when ST_WAIT_2ND =>
          if(rdEn_fromSCAX = '1')then
            state_rd <= ST_DONE;
          else
            state_rd <= ST_WAIT_2ND;
          end if;
          
        -- done, grab the address for future use
        when ST_DONE => 
          first_read            <= '0'; 
          rd_addr_fromSCAX_init <= rd_addr_fromSCAX; 
          
          if(rdEn_fromSCAX = '0')then
            state_rd <= ST_IDLE; 
          else
            state_rd <= ST_DONE; 
          end if; 
          
        when others => state_rd <= ST_IDLE; 
      end case; 
    end if; 
  end if; 
end process; 

-- register level to regFile
rd_regFile_regLevel_proc : process(rd_clk)
begin
  if(rising_edge(rd_clk))then
    rd_addr_toSCAX  <= rd_addr_toMem_i;
    dout_toSCAX     <= dout_fromMem_i;
  end if; 
end process; 
  
-- register level to RAM/FIFO
rd_MEM_regLevel_proc : process(rd_clk)
begin
  if(rising_edge(rd_clk))then
    rd_addr_toMem <= rd_addr_toMem_i; 
    rdEn_toMem    <= rdEn_toMem_i; 
  end if; 
end process; 
  
end RTL; 
