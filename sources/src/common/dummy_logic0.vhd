-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.04.2019 12:18:52
-- Design Name: Dummy Logic 0
-- Module Name: dummy_logic0 - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Dummy loopback dummy logic
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
library UNISIM; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

use work.registerFile_package.all;

entity dummy_logic0 is
  port(
    clk             : in  std_logic; -- logic clock 
    regs_SCAXtoUFL  : in  SCAXtoUFL; -- used when writing
    regs_UFLtoSCAX  : out UFLtoSCAX -- used when reading 
  ); 
end dummy_logic0; 

architecture RTL of dummy_logic0 is
  
begin

dummy_proc: process(clk)
begin
  if(rising_edge(clk))then
    -- auto-gen regFile
    --regs_UFLtoSCAX.NSW_Firmware_version_time_stamp_0000 <= x"cb0123cb";
    --regs_UFLtoSCAX.NSW_EndcapSector_ID_0001         <= regs_SCAXtoUFL.NSW_EndcapSector_ID_0001;
    --regs_UFLtoSCAX.NSW_XL1ID_0002                   <= regs_SCAXtoUFL.NSW_XL1ID_0002;
    --regs_UFLtoSCAX.NSW_Run_number_0003              <= regs_SCAXtoUFL.NSW_Run_number_0003;
    --regs_UFLtoSCAX.NSW_delta_theta_cut_0005         <= regs_SCAXtoUFL.NSW_delta_theta_cut_0005;
    --regs_UFLtoSCAX.NSW_XVC_IP_low_byte_0007         <= regs_SCAXtoUFL.NSW_XVC_IP_low_byte_0007;
    --regs_UFLtoSCAX.NSW_BC_offset_0008               <= regs_SCAXtoUFL.NSW_BC_offset_0008;
    --regs_UFLtoSCAX.NSW_BC_L1A_window_0009           <= regs_SCAXtoUFL.NSW_BC_L1A_window_0009;
    --regs_UFLtoSCAX.NSW_BC_monitoring_window_000a    <= regs_SCAXtoUFL.NSW_BC_monitoring_window_000a;
    --regs_UFLtoSCAX.NSW_enable_LIA_data_output_000d  <= regs_SCAXtoUFL.NSW_enable_LIA_data_output_000d;

    regs_UFLtoSCAX.master00_dummy32_00a       <= regs_SCAXtoUFL.master00_dummy32_00a;
    regs_UFLtoSCAX.master00_dummy4of32_003(0) <= regs_SCAXtoUFL.master00_dummy4of32_003(0);
    regs_UFLtoSCAX.master00_dummy4of32_003(1) <= regs_SCAXtoUFL.master00_dummy4of32_003(1);
    regs_UFLtoSCAX.master00_dummy4of32_003(2) <= regs_SCAXtoUFL.master00_dummy4of32_003(2);
    regs_UFLtoSCAX.master00_dummy4of32_003(3) <= regs_SCAXtoUFL.master00_dummy4of32_003(3);
    regs_UFLtoSCAX.master00_dummy32_000       <= x"deadbeef";

  end if;
end process;
  
end RTL;