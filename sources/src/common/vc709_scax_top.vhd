----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.09.2018 16:54:22
-- Design Name: SCA Extension VC709 (SCAX - VC709)
-- Module Name: vc709_scax_top - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: SCAX implementation on a VC709
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.registerFile_package.all;
use work.scax_package.all;

entity vc709_scax_top is
  port(
    ---------------------------------
    ------ General Interface --------
    GPIO_SW           : IN  STD_LOGIC_VECTOR(4 DOWNTO 0); -- VC709 switchbuttons
    GPIO_DIP          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0); -- VC709 DIP switches
    CPU_RST           : IN  STD_LOGIC; -- CPU RST (assert if MGT clock is lost)
    USER_CLOCK_P      : IN  STD_LOGIC; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
    USER_CLOCK_N      : IN  STD_LOGIC; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
    SMA_MGT_REFCLK_P  : IN  STD_LOGIC; -- clean 120.237 clock from clock board
    SMA_MGT_REFCLK_N  : IN  STD_LOGIC; -- clean 120.237 clock from clock board
    GPIO_LED          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- VC709 status LEDs
    ---------------------------------
    --------- SFP Interface ---------
    SFP_RX_P          : IN  STD_LOGIC; -- SFP cage P4
    SFP_RX_N          : IN  STD_LOGIC; -- SFP cage P4
    SFP_TX_P          : OUT STD_LOGIC; -- SFP cage P4
    SFP_TX_N          : OUT STD_LOGIC; -- SFP cage P4
    SFP_TX_DISABLE    : OUT STD_LOGIC  -- SFP cage P4
  );
end vc709_scax_top;

architecture RTL of vc709_scax_top is

component sca_extension
  generic(
    debug_mode    : std_logic := '0'; 
    TX_dataRate   : integer   := 80;   -- RX is only 80, TX can be 80 or 320
    elinkEncoding : std_logic_vector(1 downto 0)  := "01"; -- only "01" (8b10b)
    cdc_bitmask   : std_logic_vector(15 downto 0) := x"0000"; 
    ser_input     : boolean   := false
  ); 
  port(
    ------------------------------
    ------ General Interface -----
    clk_scax        : in  std_logic; -- scax main clock
    clk_regFile     : in  std_logic_vector(15 downto 0); -- all possible destination clocks
    clk_40          : in  std_logic; -- 40Mhz e-link clock
    clk_80          : in  std_logic; -- 80Mhz e-link clock
    clk_160         : in  std_logic; -- 160Mhz e-link clock
    clk_320         : in  std_logic; -- 320Mhz e-link clock
    rst             : in  std_logic; -- reset the modules
    -- dbg
    ena_flx_test    : in  std_logic; 
    dbg_fifo_rd     : in  std_logic; 
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  : in  UFLtoSCAX; -- used when reading
    regs_SCAXtoUFL  : out SCAXtoUFL; -- used when writing
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        : in  std_logic;-- 1-bit elink rx
    rx_elink2bit    : in  std_logic_vector(1 downto 0); -- 2-bit elink rx
    rx_swap         : in  std_logic; -- swap input bits
    -- tx
    tx_swap         : in  std_logic; -- swap output bits
    tx_elink        : out std_logic; -- 1-bit elink tx
    tx_elink8bit    : out std_logic_vector(7 downto 0); -- 8-bit elink tx
    tx_elink2bit    : out std_logic_vector(1 downto 0) -- 2-bit elink tx
  ); 
end component;

component gbt_fpga_wrapper
  port(
    -----------------------------------
    -------- General Interface --------
    cpu_reset         : in  std_logic; -- CPU_RST of VC709 (assert if clock loss)
    user_clock_p      : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
    user_clock_n      : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
    sma_mgt_refclk_p  : in  std_logic; -- clean 120.237 clock from clock board
    sma_mgt_refclk_n  : in  std_logic; -- clean 120.237 clock from clock board
    rst_rx_pll        : in  std_logic; -- reset the recovered clock PLL
    mgt_ready         : out std_logic; -- mgt_ready signal
    gbt_rx_ready      : out std_logic; -- RX-side ready
    tx_pll_lock       : out std_logic; -- e-link PLL is locked
    rx_pll_lock       : out std_logic; -- recovered clock PLL is locked
    -----------------------------------
    ---- GBT-FPGA Data Interface ------
    gbt_fpga_tx       : in  std_logic_vector(83 downto 0);
    gbt_fpga_rx       : out std_logic_vector(83 downto 0);
    -----------------------------------
    --- Clock-Forwarding Interface ----
    rx_userclk40      : out std_logic; -- RX recovered clock 40.079 (clean it)
    userclk           : out std_logic; -- raw 156.250 clock (after global buffer)
    clk_40            : out std_logic; -- e-link clock
    clk_80            : out std_logic; -- e-link clock
    clk_160           : out std_logic; -- e-link clock
    clk_320           : out std_logic; -- e-link clock
    clk_640           : out std_logic; -- e-link clock
    -----------------------------------
    ---------- SFP Interface ----------
    sfp_rx_p          : in  std_logic;    
    sfp_rx_n          : in  std_logic;
    sfp_tx_p          : out std_logic;
    sfp_tx_n          : out std_logic
  );
end component;

component dummy_logic0
  port(
    clk             : in  std_logic; -- logic clock 
    regs_SCAXtoUFL  : in  SCAXtoUFL; -- used when writing
    regs_UFLtoSCAX  : out UFLtoSCAX -- used when reading 
  ); 
end component;

component dummy_logic1
  port(
    clk             : in  std_logic; -- logic clock 
    rst             : in  std_logic;
    regs_SCAXtoUFL  : in  SCAXtoUFL; -- used when writing
    regs_UFLtoSCAX  : out UFLtoSCAX -- used when reading 
  ); 
end component;

component vio_debug
  port(
    clk         : in  std_logic;
    probe_out0  : out std_logic_vector(0 downto 0);
    probe_out1  : out std_logic_vector(0 downto 0);
    probe_out2  : out std_logic_vector(0 downto 0);
    probe_out3  : out std_logic_vector(0 downto 0);
    probe_out4  : out std_logic_vector(0 downto 0);
    probe_out5  : out std_logic_vector(0 downto 0)
  );
end component;

  signal clk_40           : std_logic := '0';
  signal clk_80           : std_logic := '0';
  signal clk_160          : std_logic := '0';
  signal clk_320          : std_logic := '0';
  signal clk_640          : std_logic := '0';
  signal userclk          : std_logic := '0';
  signal rst_scax         : std_logic := '0';
  signal rxClk_pll_lock   : std_logic := '0';
  signal clk_regFile      : std_logic_vector(15 downto 0) := (others => '0');

  signal gpio_led_i       : std_logic_vector(7 downto 0) := (others => '0');
  signal gpio_dip_i       : std_logic_vector(7 downto 0) := (others => '0');
  signal gpio_sw_i        : std_logic_vector(4 downto 0) := (others => '0');

  signal mgt_ready        : std_logic := '0';
  signal gbt_rx_ready     : std_logic := '0';
  signal elink_pll_lock   : std_logic := '0';
  signal cpu_rst_i        : std_logic := '0';

  signal gbt_fpga_tx      : std_logic_vector(83 downto 0) := (others => '0');
  signal gbt_fpga_rx      : std_logic_vector(83 downto 0) := (others => '0');
  signal scax_elink_rx    : std_logic_vector(1 downto 0)  := (others => '0');
  signal scax_elink2_tx   : std_logic_vector(1 downto 0)  := (others => '0');
  signal scax_elink8_tx   : std_logic_vector(7 downto 0)  := (others => '0');

  signal regs_UFLtoSCAX   : UFLtoSCAX;
  signal regs_SCAXtoUFL   : SCAXtoUFL;

  -- vio
  signal dbg_fifo_rd      : std_logic := '0';
  signal rst_scax_i       : std_logic := '0';
  signal ena_flx_test     : std_logic := '0';
  signal swap_bits_rx     : std_logic := '0';
  signal swap_bits_tx     : std_logic := '0';
  signal rst_rx_pll       : std_logic := '0';

  attribute mark_debug                    : string;
  attribute mark_debug of dbg_fifo_rd     : signal is "TRUE";
  attribute mark_debug of rst_scax_i      : signal is "TRUE";
  attribute mark_debug of ena_flx_test    : signal is "TRUE";
  attribute mark_debug of swap_bits_rx    : signal is "TRUE";
  attribute mark_debug of swap_bits_tx    : signal is "TRUE";
  attribute mark_debug of rst_rx_pll      : signal is "TRUE";

begin

scax_inst: sca_extension
  generic map(
    debug_mode    => debug_mode,
    TX_dataRate   => TX_dataRate,
    elinkEncoding => elinkEncoding,
    cdc_bitmask   => cdc_bitmask,
    ser_input     => ser_input)
  port map(
    ------------------------------
    ------ General Interface -----
    clk_scax        => clk_320,
    clk_regFile     => clk_regFile,
    clk_40          => clk_40,
    clk_80          => clk_80,
    clk_160         => clk_160,
    clk_320         => clk_320,
    rst             => rst_scax,
    -- dbg
    ena_flx_test    => ena_flx_test,
    dbg_fifo_rd     => dbg_fifo_rd,
    ------------------------------
    -- Register File Interface ---
    regs_UFLtoSCAX  => regs_UFLtoSCAX,
    regs_SCAXtoUFL  => regs_SCAXtoUFL,
    ------------------------------
    ------ E-Link Interface ------
    -- rx
    rx_elink        => '0',
    rx_elink2bit    => scax_elink_rx,
    rx_swap         => swap_bits_rx,
    -- tx
    tx_swap         => swap_bits_tx,
    tx_elink        => open,
    tx_elink8bit    => scax_elink8_tx,
    tx_elink2bit    => scax_elink2_tx
   );

gbt_fpga_wrapper_inst: gbt_fpga_wrapper
  port map(
    -----------------------------------
    -------- General Interface --------
    cpu_reset         => cpu_rst_i,
    user_clock_p      => USER_CLOCK_P,
    user_clock_n      => USER_CLOCK_N,
    sma_mgt_refclk_p  => SMA_MGT_REFCLK_P,
    sma_mgt_refclk_n  => SMA_MGT_REFCLK_N,
    rst_rx_pll        => rst_rx_pll,
    mgt_ready         => mgt_ready,
    gbt_rx_ready      => gbt_rx_ready,
    tx_pll_lock       => elink_pll_lock,
    rx_pll_lock       => rxClk_pll_lock,
    -----------------------------------
    ---- GBT-FPGA Data Interface ------
    gbt_fpga_tx       => gbt_fpga_tx,
    gbt_fpga_rx       => gbt_fpga_rx,
    -----------------------------------
    --- Clock-Forwarding Interface ----
    rx_userclk40      => open,
    userclk           => userclk,
    clk_40            => clk_40,
    clk_80            => clk_80,
    clk_160           => clk_160,
    clk_320           => clk_320,
    clk_640           => clk_640,
    -----------------------------------
    ---------- SFP Interface ----------
    sfp_rx_p          => SFP_RX_P,
    sfp_rx_n          => SFP_RX_N,
    sfp_tx_p          => SFP_TX_P,
    sfp_tx_n          => SFP_TX_N
  );

dummy_logic0_inst: dummy_logic0
  port map(
    clk             => clk_320,
    regs_SCAXtoUFL  => regs_SCAXtoUFL,
    regs_UFLtoSCAX  => regs_UFLtoSCAX
  ); 

dummy_logic1_inst: dummy_logic1
  port map(
    clk             => clk_320,
    rst             => rst_scax,
    regs_SCAXtoUFL  => regs_SCAXtoUFL,
    regs_UFLtoSCAX  => regs_UFLtoSCAX
  ); 


vio_scax_inst: vio_debug
  port map(
    clk           => clk_40,
    probe_out0(0) => rst_scax_i,
    probe_out1(0) => dbg_fifo_rd,
    probe_out2(0) => ena_flx_test,
    probe_out3(0) => swap_bits_rx,
    probe_out4(0) => swap_bits_tx,
    probe_out5(0) => rst_rx_pll
  );

-- select output/input from dataRate
dataRate_sel_proc: process(scax_elink2_tx, scax_elink8_tx, gbt_fpga_rx)
begin
  case TX_dataRate is
  when 80     =>
              -- tx (e-link 0x88 80Mbps)
              gbt_fpga_tx(16)  <= scax_elink2_tx(1); 
              gbt_fpga_tx(17)  <= scax_elink2_tx(0);
              -- rx (e-link 0x88 80Mbps)
              scax_elink_rx(1) <= gbt_fpga_rx(16);
              scax_elink_rx(0) <= gbt_fpga_rx(17);

  when 320    => 
              -- tx (e-link 0x89 320Mbps)
              gbt_fpga_tx(16)  <= scax_elink8_tx(0); 
              gbt_fpga_tx(17)  <= scax_elink8_tx(1);
              gbt_fpga_tx(18)  <= scax_elink8_tx(2); 
              gbt_fpga_tx(19)  <= scax_elink8_tx(3);
              gbt_fpga_tx(20)  <= scax_elink8_tx(4); 
              gbt_fpga_tx(21)  <= scax_elink8_tx(5);
              gbt_fpga_tx(22)  <= scax_elink8_tx(6);
              gbt_fpga_tx(23)  <= scax_elink8_tx(7);
              -- rx (e-link 0x89 80Mbps)
              scax_elink_rx(1) <= gbt_fpga_rx(18);
              scax_elink_rx(0) <= gbt_fpga_rx(19);

  when others => 
              -- tx (e-link 0x88 80Mbps)
              gbt_fpga_tx(16)  <= scax_elink2_tx(1); 
              gbt_fpga_tx(17)  <= scax_elink2_tx(0);
              -- rx (e-link 0x88 80Mbps)
              scax_elink_rx(1) <= gbt_fpga_rx(16);
              scax_elink_rx(0) <= gbt_fpga_rx(17);
  end case;
end process;

-- i/o buffers
gen_buffers_led: for I in 0 to 7 generate
led_obuf: OBUF  port map (O => GPIO_LED(I), I => gpio_led_i(I));
end generate gen_buffers_led;

gen_buffers_sw: for I in 0 to 4 generate
sw_ibuf:  IBUF  port map (O => gpio_sw_i(I), I => GPIO_SW(I));
end generate gen_buffers_sw;

gen_buffers_dip: for I in 0 to 7 generate
dip_ibuf: IBUF  port map (O => gpio_dip_i(I), I => GPIO_DIP(I));
end generate gen_buffers_dip;

cpuRst_buf:    IBUF  port map (O => cpu_rst_i, I => CPU_RST);
txDisable_buf: OBUF  port map (O => SFP_TX_DISABLE, I => '0');

    rst_scax        <= rst_scax_i or (not elink_pll_lock);
    
    gpio_led_i(0)   <= elink_pll_lock;
    gpio_led_i(1)   <= mgt_ready;
    gpio_led_i(2)   <= gbt_rx_ready;  
      
    gpio_led_i(3)   <= rxClk_pll_lock;
    
    gpio_led_i(4)   <= '0';
    gpio_led_i(5)   <= '0';
    gpio_led_i(6)   <= '0';
    gpio_led_i(7)   <= '0';


    clk_regFile(00) <= '0';
    clk_regFile(01) <= '0';
    clk_regFile(02) <= '0';
    clk_regFile(03) <= '0';
    clk_regFile(04) <= '0';
    clk_regFile(05) <= '0';
    clk_regFile(06) <= '0';
    clk_regFile(07) <= '0';
    clk_regFile(08) <= '0';
    clk_regFile(09) <= '0';
    clk_regFile(10) <= '0';
    clk_regFile(11) <= '0';
    clk_regFile(12) <= '0';
    clk_regFile(13) <= '0';
    clk_regFile(14) <= '0';
    clk_regFile(15) <= '0';
    
end RTL;
