----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 10.10.2018 18:08:12
-- Design Name: Register File 0
-- Module Name: registerFile_package_0 - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Package for the dummy register file for I2C master 0.
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 

package registerFile_package is

  -- define vector of vectors
  type vecVec_4of32 is array (3 downto 0) of std_logic_vector(31 downto 0);

  type UFLtoSCAX is record -- when SCAX reads

    -- register file 0
    master00_dummy32_000    : std_logic_vector(31 downto 0); -- addr: 0x000
    master00_dummy32_00a    : std_logic_vector(31 downto 0); -- addr: 0x00a
    master00_dummy4of32_003 : vecVec_4of32; -- addr: 0x003

    -- register file 1
    master01_rdAddr12_000   : std_logic_vector(11 downto 0); -- addr: 0x001
    master01_rdData32_001   : std_logic_vector(31 downto 0); -- addr: 0x001

    master01_wrAddr12_002   : std_logic_vector(11 downto 0); -- addr: 0x002

    master01_dummy32_3ab    : std_logic_vector(31 downto 0); -- addr: 0x3ab
    master01_dummy32_3ff    : std_logic_vector(31 downto 0); -- addr: 0x3ff

  end record;

  type SCAXtoUFL is record -- when SCAX writes

    -- register file 0
    master00_dummy32_00a    : std_logic_vector(31 downto 0); -- addr: 0x00a
    master00_dummy4of32_003 : vecVec_4of32; -- addr: 0x003

    -- register file 1
    master01_rdAddr12_000   : std_logic_vector(11 downto 0); -- addr: 0x000
    master01_rdEn_001       : std_logic;                     -- addr: 0x001

    master01_wrAddr12_002   : std_logic_vector(11 downto 0); -- addr: 0x002
    master01_wrData32_003   : std_logic_vector(31 downto 0); -- addr: 0x003
    master01_wrEn_003       : std_logic;
    
    master01_dummy32_3ff    : std_logic_vector(31 downto 0); -- addr: 0x3ff

    -- register file 2
    master02_rst_000        : std_logic; -- addr: 000 (i am a pulse)
  end record;
  
  
end registerFile_package; 
