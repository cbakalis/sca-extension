----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.09.2018 16:54:22
-- Design Name: SCA Extension MMFE8 (SCAX - MMFE8)
-- Module Name: mmfe8_scax_top - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: SCAX implementation on an MMFE8
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;


entity mmfe8_scax_top is
    port(
        ---------------------------------
        ------ General Interface --------
        GPIO_LED    : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
        ---------------------------------
        ------ E-Link Interface ---------
        ELINK_CLK_P : IN  STD_LOGIC;
        ELINK_CLK_N : IN  STD_LOGIC;
        ELINK_RX_P  : IN  STD_LOGIC;
        ELINK_RX_N  : IN  STD_LOGIC;
        ELINK_TX_P  : OUT STD_LOGIC;
        ELINK_TX_N  : OUT STD_LOGIC
    );
end mmfe8_scax_top;

architecture RTL of mmfe8_scax_top is

    ------------------------------
    -- Global Variables/Generics 
    ------------------------------
    constant scax_id         : std_logic_vector(23 downto 0) := x"12" & x"cb" & x"34"; -- SCAX_ID
    constant elinkEncoding  : std_logic_vector(1 downto 0)  := "01"; -- 01: 8b10b, 10: hdlc
    constant ser_input      : boolean   := true; -- use 1-bit e-link
    constant debug_mode     : std_logic := '1'; -- disable/enable ILA and debug buffer. uncomment mark_debug in scax if set to '1'

component mmcm_elink
    port(
        -- Clock in ports
        clk_in1_p   : in  std_logic;
        clk_in1_n   : in  std_logic;
        -- Clock out ports
        clk_out_40  : out std_logic;
        clk_out_80  : out std_logic;
        clk_out_160 : out std_logic;
        clk_out_320 : out std_logic;
        -- Status and control signals
        reset       : in  std_logic;
        mmcm_locked : out std_logic
    );
end component;

component sca_extension
    generic(
        debug_mode      : std_logic := '1'; -- activate ILA
        noOf_masters    : natural := 16;  -- how many I2C masters?
        scax_id          : std_logic_vector(23 downto 0) := x"12" & x"cb" & x"34"; -- SCAX_ID
        InputDataRate   : integer := 80; -- only 80
        elinkEncoding   : std_logic_vector(1 downto 0) := "01"; -- only "01" (8b10b) or "10" (hdlc)
        ser_input       : boolean := false; -- 2-bit wide bus
        rev_fieldsRX    : std_logic_vector(1 downto 0) := "10"; --00:revNone,01:revData,10:revFCS,11:revBoth
        rev_fieldsTX    : std_logic_vector(1 downto 0) := "10" 
    );
    port(
        ------------------------------
        ------ General Interface -----
        clk             : in  std_logic; -- user clock
        clk_40          : in  std_logic; -- 40Mhz e-link clock
        clk_80          : in  std_logic; -- 80Mhz e-link clock
        clk_160         : in  std_logic; -- 160Mhz e-link clock
        clk_320         : in  std_logic; -- 320Mhz e-link clock
        rst             : in  std_logic; -- reset the modules
        -- dbg
        ena_flx_test    : in  std_logic;
        dbg_fifo_rd     : in  std_logic;
        reverse_rx      : in  std_logic;
        reverse_tx      : in  std_logic;
        ------------------------------
        ------ E-Link Interface ------
        -- rx
        rx_elink        : in  std_logic; -- 1-bit elink rx
        rx_elink2bit    : in  std_logic_vector(1 downto 0); -- 2-bit elink rx
        rx_swap         : in  std_logic; -- swap input bits
        -- tx
        tx_swap         : in  std_logic; -- swap output bits
        tx_elink        : out std_logic; -- 1-bit elink tx
        tx_elink2bit    : out std_logic_vector(1 downto 0) -- 2-bit elink tx
    );
end component;

component vio_debug
    port(
        clk         : in  std_logic;
        probe_out0  : out std_logic_vector(0 downto 0);
        probe_out1  : out std_logic_vector(0 downto 0);
        probe_out2  : out std_logic_vector(0 downto 0)
    );
end component;

    signal clk_40       : std_logic := '0';
    signal clk_80       : std_logic := '0';
    signal clk_160      : std_logic := '0';
    signal clk_320      : std_logic := '0';
    signal mmcm_locked  : std_logic := '0';
    signal rst_scax      : std_logic := '0';
    
    signal elink_tx_i   : std_logic := '0';
    signal elink_rx_i   : std_logic := '0';

    signal gpio_led_i   : std_logic_vector(7 downto 0) := (others => '0');
    
    -- vio
    signal dbg_fifo_rd  : std_logic := '0';
    signal rst_scax_i    : std_logic := '0';
    signal ena_flx_test : std_logic := '0';

    attribute mark_debug                    : string;
    attribute mark_debug of dbg_fifo_rd     : signal is "TRUE";
    attribute mark_debug of rst_scax_i       : signal is "TRUE";
    attribute mark_debug of ena_flx_test    : signal is "TRUE";
    
begin

mmcm_elink_inst: mmcm_elink
    port map ( 
        -- Clock out ports  
        clk_out_40  => clk_40,
        clk_out_80  => clk_80,
        clk_out_160 => clk_160,
        clk_out_320 => clk_320,
        -- Status and control signals                
        reset       => '0',
        mmcm_locked => mmcm_locked,
        -- Clock in ports
        clk_in1_p   => ELINK_CLK_P,
        clk_in1_n   => ELINK_CLK_N
    );

scax_inst: sca_extension
    generic map(
        debug_mode      => debug_mode,
        noOf_masters    => 16,
        scax_id          => scax_id,
        InputDataRate   => 80,
        elinkEncoding   => elinkEncoding,
        ser_input       => ser_input,
        rev_fieldsRX    => "10", -- good
        rev_fieldsTX    => "10") -- good
    port map(
        ------------------------------
        ------ General Interface -----
        clk             => clk_320,
        clk_40          => clk_40,
        clk_80          => clk_80,
        clk_160         => clk_160,
        clk_320         => clk_320,
        rst             => rst_scax,
        -- dbg
        ena_flx_test    => ena_flx_test,
        dbg_fifo_rd     => dbg_fifo_rd,
        reverse_rx      => '0',
        reverse_tx      => '1',
        ------------------------------
        ------ E-Link Interface ------
        -- rx
        rx_elink        => elink_rx_i,
        rx_elink2bit    => (others => '0'),
        rx_swap         => '0',
        -- tx
        tx_swap         => '0',
        tx_elink        => elink_tx_i,
        tx_elink2bit    => open
     );

vio_scax_inst: vio_debug
    port map(
         clk            => clk_80,
         probe_out0(0)  => rst_scax_i,
         probe_out1(0)  => dbg_fifo_rd,
         probe_out2(0)  => ena_flx_test
    );

-- i/o buffers
gen_buffers_led: for I in 0 to 5 generate
led_obuf:       OBUF  port map (O => GPIO_LED(I), I => gpio_led_i(I));
end generate gen_buffers_led;

elinkTX_obufds: OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => ELINK_TX_P, OB => ELINK_TX_N, I => elink_tx_i);
elinkRX_ibufds: IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => elink_rx_i, I => ELINK_RX_P, IB => ELINK_RX_N);

    rst_scax         <= rst_scax_i or (not mmcm_locked);
    gpio_led_i(0)   <= mmcm_locked;

end RTL;
