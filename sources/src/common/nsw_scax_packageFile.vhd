----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
--
--
-- Source auto-generated via jinja2 templates for use with wuppercodegen
-- ... by Lawrence Lee (lawrence.lee.jr@cern.ch)
--
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--
-- Create Date: 10.10.2018 18:08:12
-- Design Name: Register File 0
-- Module Name: registerFile_package_0 - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Package for the actual register file.
--
-- Dependencies:
--
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package registerFile_package is


    -- define vector of vectors
    type vecVec_4of1 is array (3 downto 0) of std_logic;
    type vecVec_32of8 is array (31 downto 0) of std_logic_vector(7 downto 0);
    type vecVec_128of16 is array (127 downto 0) of std_logic_vector(15 downto 0);
    type vecVec_24of12 is array (23 downto 0) of std_logic_vector(11 downto 0);
    type vecVec_24of16 is array (23 downto 0) of std_logic_vector(15 downto 0);
    type vecVec_24of6 is array (23 downto 0) of std_logic_vector(5 downto 0);
    type vecVec_4of16 is array (3 downto 0) of std_logic_vector(15 downto 0);

    ----------------------------------------

    type SCAXtoUFL is record -- when SCAX writes


        -- NSW --

            --  0001
            NSW_EndcapSector_ID_0001       : std_logic_vector(4 downto 0);
            -- high 8 bits of L1ID 0002
            NSW_XL1ID_0002                 : std_logic_vector(7 downto 0);
            -- Run number, set at begin run 0003
            NSW_Run_number_0003            : std_logic_vector(31 downto 0);
            --  0005
            NSW_delta_theta_cut_0005       : std_logic_vector(4 downto 0);
            --  0006
            NSW_mezz_PLL_reset_0006        : std_logic;
            -- to be OR'ed with Sector ID 0007
            NSW_XVC_IP_low_byte_0007       : std_logic_vector(7 downto 0);
            -- BCID loaded when BCR received 0008
            NSW_BC_offset_0008             : std_logic_vector(11 downto 0);
            -- BCID window for L1A output 0009
            NSW_BC_L1A_window_0009         : std_logic_vector(2 downto 0);
            -- BCID window for monitoring output 000a
            NSW_BC_monitoring_window_000a  : std_logic_vector(2 downto 0);
            -- reset by FPGA after sending 000b
            NSW_send_RODBUSY_on_000b       : std_logic;
            -- reset by FPGA after sending 000c
            NSW_send_RODBUSY_off_000c      : std_logic;
            --  000d
            NSW_enable_LIA_data_output_000d : std_logic;

        -- MM_lnk --

            -- GBT PLL reset, 1 per quad 0002
            MM_lnk_gbt_pll_reset_0002      : std_logic_vector(8 downto 0);
            -- Reset all GBT links 0003
            MM_lnk_gbt_hw_reset_0003       : std_logic;
            -- Mask unconnected links 0005
            MM_lnk_gbt_channel_mask_0005   : std_logic_vector(31 downto 0);

        -- MM_addc --

            -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(0)
            MM_addc_addc_packet_BC_offset_0003 : vecVec_32of8;

        -- MM --

            -- Pause the local BC counter 0001
            MM_tp_local_bc_disable_0001    : std_logic;
            -- Reset the local BC counter 0002
            MM_tp_local_bc_reset_0002      : std_logic;

        -- MM_alg --

            -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(0)
            MM_alg_algor_strip0_offset_0000 : vecVec_128of16;
            -- Distance from IP to plane, one per plane region Vector [128] 0080(0)
            MM_alg_algor_plane_offset_0080 : vecVec_128of16;
            -- reciprocal of algor_plane_offset Vector [128] 0100(0)
            MM_alg_algor_plane_offset_recip_0100 : vecVec_128of16;
            -- selct which MMFE8s are in an inverted location 0180
            MM_alg_algor_mmfe8_invert_0180 : std_logic_vector(31 downto 0);
            -- ROI map (address is now inferred) RAM Memory wrAddr 0181
            MM_alg_algor_roi_map_wrAddr_0181 : std_logic_vector(15 downto 0);
            -- ROI map (address is now inferred) RAM Memory wrData 0182
            MM_alg_algor_roi_map_wrData_0182 : std_logic_vector(15 downto 0);
            -- ROI map (address is now inferred) RAM Memory wrEn 0182
            MM_alg_algor_roi_map_wrEn_0182 : std_logic;
            -- MX local algorithm parameters (address is now inferred) RAM Memory wrAddr 0183
            MM_alg_algor_mxlocal_parameters_wrAddr_0183 : std_logic_vector(15 downto 0);
            -- MX local algorithm parameters (address is now inferred) RAM Memory wrData 0184
            MM_alg_algor_mxlocal_parameters_wrData_0184 : std_logic_vector(15 downto 0);
            -- MX local algorithm parameters (address is now inferred) RAM Memory wrEn 0184
            MM_alg_algor_mxlocal_parameters_wrEn_0184 : std_logic;
            -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrAddr 0185
            MM_alg_algor_dtheta_parameters_wrAddr_0185 : std_logic_vector(15 downto 0);
            -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrData 0186
            MM_alg_algor_dtheta_parameters_wrData_0186 : std_logic_vector(15 downto 0);
            -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrEn 0186
            MM_alg_algor_dtheta_parameters_wrEn_0186 : std_logic;
            -- Mask algorithm regions 0187
            MM_alg_algor_region_mask_0187  : std_logic_vector(15 downto 0);

        -- MM_diag --

            -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrAddr 0002
            MM_diag_diag_gbt_loopback_data_wrAddr_0002 : std_logic_vector(31 downto 0);
            -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrData 0003
            MM_diag_diag_gbt_loopback_data_wrData_0003 : std_logic_vector(31 downto 0);
            -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrEn 0003
            MM_diag_diag_gbt_loopback_data_wrEn_0003 : std_logic;

        -- sTGC_lnk --

            --  000f
            sTGC_lnk_GTH_QPLLRESET_000f    : std_logic_vector(11 downto 0);
            --  0010
            sTGC_lnk_GlobalReset_0010      : std_logic;
            --  0011
            sTGC_lnk_GTH_rxuserrdy_32_0011 : std_logic_vector(31 downto 0);
            --  0012
            sTGC_lnk_GTH_rxuserrdy_4_0012  : std_logic_vector(3 downto 0);
            --  0014
            sTGC_lnk_GTH_txuserrdy_4_0014  : std_logic_vector(3 downto 0);
            --  0015
            sTGC_lnk_GTH_cpllreset_32_0015 : std_logic_vector(31 downto 0);
            --  0016
            sTGC_lnk_GTH_cpllreset_4_0016  : std_logic_vector(3 downto 0);
            --  0017
            sTGC_lnk_GTH_gtrxreset_32_0017 : std_logic_vector(31 downto 0);
            --  0018
            sTGC_lnk_GTH_gtrxreset_4_0018  : std_logic_vector(3 downto 0);
            --  0019
            sTGC_lnk_GTH_gttxreset_32_0019 : std_logic_vector(31 downto 0);
            --  001a
            sTGC_lnk_GTH_gttxreset_4_001a  : std_logic_vector(3 downto 0);
            --  001b
            sTGC_lnk_V7_ANLINK_RX_WR_EN_001b : std_logic;
            --  001c
            sTGC_lnk_V7_ANLINK_RX_RD_EN_001c : std_logic;
            --  001d
            sTGC_lnk_V7_ANLINK_TX_WR_EN_001d : std_logic;
            --  001e
            sTGC_lnk_V7_ANLINK_TX_RD_EN_001e : std_logic;

        -- sTGC_V6 --

            --  0000
            sTGC_V6_V6_global_RST_0000     : std_logic;
            --  0001
            sTGC_V6_V6_UDP_GTX_RST_0001    : std_logic;
            --  0002
            sTGC_V6_V6_ANLINK_RX_WR_EN_0002 : std_logic;
            --  0003
            sTGC_V6_V6_ANLINK_RX_RD_EN_0003 : std_logic;
            --  0004
            sTGC_V6_V6_ANLINK_TX_WR_EN_0004 : std_logic;
            --  0005
            sTGC_V6_V6_ANLINK_TX_RD_EN_0005 : std_logic;

        -- sTGC_alg --

            -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(0)
            sTGC_alg_radial_offset_0000    : vecVec_24of12;
            -- Distance from IP to plane, one per quadruplet Vector [24] 0018(0)
            sTGC_alg_plane_offset_0018     : vecVec_24of16;
            --  0030
            sTGC_alg_max_cluster_size_0030 : std_logic_vector(3 downto 0);
            --  0031
            sTGC_alg_min_cluster_size_0031 : std_logic_vector(3 downto 0);
            -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(0)
            sTGC_alg_charge_thresh_cluster_0032 : vecVec_24of6;
            -- charge threshold for determining centroid, per gas gap Vector [24] 004a(0)
            sTGC_alg_charge_thresh_centroid_004a : vecVec_24of6;
            -- strip direction (+ / -) list 0062
            sTGC_alg_strip_dir_0062        : std_logic_vector(7 downto 0);
            -- radial offset (per gas gap) Vector [24] 0063(0)
            sTGC_alg_R_offset_0063         : vecVec_24of16;

        -- sTGC_mrg --

            --  0000
            sTGC_mrg_enable_MM_segments_0000 : std_logic_vector(7 downto 0);
            --  0001
            sTGC_mrg_enable_SL_output_0001 : std_logic;
            -- number of 320MHz clocks to wait before output to SL 0002
            sTGC_mrg_SL_out_offset_0002    : std_logic_vector(2 downto 0);
            -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(0)
            sTGC_mrg_fiber_ctrl_param_0003 : vecVec_4of16;

    end record;

    type UFLtoSCAX is record -- when SCAX reads

        -- NSW --

            -- Xilinx USR_ACCESS  timestamp of firmware implementation  0000
            NSW_Firmware_version_time_stamp_0000 : std_logic_vector(31 downto 0);
            --  0001
            NSW_EndcapSector_ID_0001       : std_logic_vector(4 downto 0);
            -- high 8 bits of L1ID 0002
            NSW_XL1ID_0002                 : std_logic_vector(7 downto 0);
            -- Run number, set at begin run 0003
            NSW_Run_number_0003            : std_logic_vector(31 downto 0);
            -- reset by TTC 0004
            NSW_orbit_count_0004           : std_logic_vector(31 downto 0);
            --  0005
            NSW_delta_theta_cut_0005       : std_logic_vector(4 downto 0);
            -- to be OR'ed with Sector ID 0007
            NSW_XVC_IP_low_byte_0007       : std_logic_vector(7 downto 0);
            -- BCID loaded when BCR received 0008
            NSW_BC_offset_0008             : std_logic_vector(11 downto 0);
            -- BCID window for L1A output 0009
            NSW_BC_L1A_window_0009         : std_logic_vector(2 downto 0);
            -- BCID window for monitoring output 000a
            NSW_BC_monitoring_window_000a  : std_logic_vector(2 downto 0);
            --  000d
            NSW_enable_LIA_data_output_000d : std_logic;
            --  000e
            NSW_FELIX_RX_link_OK_000e      : std_logic;
            -- Please edit me, bit-width was '?' Vector [4] 000f(0)
            NSW_Jitter_cleaner_status_000f : vecVec_4of1;

        -- MM_lnk --

            -- input serializer OK 0000
            MM_lnk_gbt_link_alligned_0000  : std_logic_vector(31 downto 0);
            -- GBT PLL link status, 1 per quad 0001
            MM_lnk_gbt_pll_locked_0001     : std_logic_vector(8 downto 0);
            -- Detect misaligned or unlocked links 0004
            MM_lnk_gbt_link_error_0004     : std_logic_vector(31 downto 0);
            -- Mask unconnected links 0005
            MM_lnk_gbt_channel_mask_0005   : std_logic_vector(31 downto 0);

        -- MM_addc --

            -- GBT link parity errors 0000
            MM_addc_addc_packet_parity_error_0000 : std_logic_vector(31 downto 0);
            -- Detect inconsistent BC offset between packet and local BC 0001
            MM_addc_addc_packet_bc_error_0001 : std_logic_vector(31 downto 0);
            -- Detect packet data  formatting errors 0002
            MM_addc_addc_packet_format_error_0002 : std_logic_vector(31 downto 0);
            -- Constant ADDC BC offset between packet and local BC Vector [32] 0003(0)
            MM_addc_addc_packet_BC_offset_0003 : vecVec_32of8;

        -- MM --

            -- Current state of the TP including a global error bit 0000
            MM_tp_status_0000              : std_logic_vector(31 downto 0);
            -- Pause the local BC counter 0001
            MM_tp_local_bc_disable_0001    : std_logic;

        -- MM_alg --

            -- Strip 0 offset from beam-line in units of strip pitch, one per plane region Vector [128] 0000(0)
            MM_alg_algor_strip0_offset_0000 : vecVec_128of16;
            -- Distance from IP to plane, one per plane region Vector [128] 0080(0)
            MM_alg_algor_plane_offset_0080 : vecVec_128of16;
            -- reciprocal of algor_plane_offset Vector [128] 0100(0)
            MM_alg_algor_plane_offset_recip_0100 : vecVec_128of16;
            -- selct which MMFE8s are in an inverted location 0180
            MM_alg_algor_mmfe8_invert_0180 : std_logic_vector(31 downto 0);
            -- ROI map (address is now inferred) RAM Memory wrAddr 0181
            MM_alg_algor_roi_map_wrAddr_0181 : std_logic_vector(15 downto 0);
            -- MX local algorithm parameters (address is now inferred) RAM Memory wrAddr 0183
            MM_alg_algor_mxlocal_parameters_wrAddr_0183 : std_logic_vector(15 downto 0);
            -- Delta theta algorithm parameters (address is now inferred) RAM Memory wrAddr 0185
            MM_alg_algor_dtheta_parameters_wrAddr_0185 : std_logic_vector(15 downto 0);
            -- Mask algorithm regions 0187
            MM_alg_algor_region_mask_0187  : std_logic_vector(15 downto 0);

        -- MM_diag --

            -- Diagnostic FIFO empty flag 0000
            MM_diag_diag_fifo_ef_0000      : std_logic_vector(31 downto 0);
            -- Diagnostic FIFO overflow flag 0001
            MM_diag_diag_fifo_of_0001      : std_logic_vector(31 downto 0);
            -- ADDC emulator GBT loopback ART hit data (address is now inferred) RAM Memory wrAddr 0002
            MM_diag_diag_gbt_loopback_data_wrAddr_0002 : std_logic_vector(31 downto 0);

        -- sTGC_lnk --

            --  0000
            sTGC_lnk_GTH_MMC_RX_lock_0000  : std_logic_vector(2 downto 0);
            --  0001
            sTGC_lnk_GTH_TX_FSM_RESET_DONE_32_0001 : std_logic_vector(31 downto 0);
            --  0002
            sTGC_lnk_GTH_RX_FSM_RESET_DONE_32_0002 : std_logic_vector(31 downto 0);
            --  0003
            sTGC_lnk_GTH_cpllfbclklost_32_0003 : std_logic_vector(31 downto 0);
            --  0004
            sTGC_lnk_GTH_cplllock_32_0004  : std_logic_vector(31 downto 0);
            --  0005
            sTGC_lnk_GTH_rxresetdone_32_0005 : std_logic_vector(31 downto 0);
            --  0006
            sTGC_lnk_GTH_txresetdone_32_0006 : std_logic_vector(31 downto 0);
            --  0007
            sTGC_lnk_GTH_TX_FSM_RESET_DONE_4_0007 : std_logic_vector(3 downto 0);
            --  0008
            sTGC_lnk_GTH_RX_FSM_RESET_DONE_4_0008 : std_logic_vector(3 downto 0);
            --  0009
            sTGC_lnk_GTH_cpllfbclklost_4_0009 : std_logic_vector(3 downto 0);
            --  000a
            sTGC_lnk_GTH_cplllock_4_000a   : std_logic_vector(3 downto 0);
            --  000b
            sTGC_lnk_GTH_rxresetdone_4_000b : std_logic_vector(3 downto 0);
            --  000c
            sTGC_lnk_GTH_txresetdone_4_000c : std_logic_vector(3 downto 0);
            --  000d
            sTGC_lnk_GTH_QPLLLOCK_000d     : std_logic_vector(11 downto 0);
            --  000e
            sTGC_lnk_GTH_QPLLREFCLKLOST_000e : std_logic_vector(11 downto 0);
            --  0011
            sTGC_lnk_GTH_rxuserrdy_32_0011 : std_logic_vector(31 downto 0);
            --  0012
            sTGC_lnk_GTH_rxuserrdy_4_0012  : std_logic_vector(3 downto 0);
            --  0013
            sTGC_lnk_GTH_txuserrdy_32_0013 : std_logic_vector(31 downto 0);
            --  0014
            sTGC_lnk_GTH_txuserrdy_4_0014  : std_logic_vector(3 downto 0);
            --  001b
            sTGC_lnk_V7_ANLINK_RX_WR_EN_001b : std_logic;
            --  001c
            sTGC_lnk_V7_ANLINK_RX_RD_EN_001c : std_logic;
            --  001d
            sTGC_lnk_V7_ANLINK_TX_WR_EN_001d : std_logic;
            --  001e
            sTGC_lnk_V7_ANLINK_TX_RD_EN_001e : std_logic;

        -- sTGC_V6 --

            --  0001
            sTGC_V6_V6_UDP_GTX_RST_0001    : std_logic;
            --  0002
            sTGC_V6_V6_ANLINK_RX_WR_EN_0002 : std_logic;
            --  0003
            sTGC_V6_V6_ANLINK_RX_RD_EN_0003 : std_logic;
            --  0004
            sTGC_V6_V6_ANLINK_TX_WR_EN_0004 : std_logic;
            --  0005
            sTGC_V6_V6_ANLINK_TX_RD_EN_0005 : std_logic;

        -- sTGC_alg --

            -- radial offset of each layer of each quadruplet in the sector Vector [24] 0000(0)
            sTGC_alg_radial_offset_0000    : vecVec_24of12;
            -- Distance from IP to plane, one per quadruplet Vector [24] 0018(0)
            sTGC_alg_plane_offset_0018     : vecVec_24of16;
            --  0030
            sTGC_alg_max_cluster_size_0030 : std_logic_vector(3 downto 0);
            --  0031
            sTGC_alg_min_cluster_size_0031 : std_logic_vector(3 downto 0);
            -- charge threshold for determining cluster size, per gas gap Vector [24] 0032(0)
            sTGC_alg_charge_thresh_cluster_0032 : vecVec_24of6;
            -- charge threshold for determining centroid, per gas gap Vector [24] 004a(0)
            sTGC_alg_charge_thresh_centroid_004a : vecVec_24of6;
            -- strip direction (+ / -) list 0062
            sTGC_alg_strip_dir_0062        : std_logic_vector(7 downto 0);
            -- radial offset (per gas gap) Vector [24] 0063(0)
            sTGC_alg_R_offset_0063         : vecVec_24of16;

        -- sTGC_mrg --

            --  0000
            sTGC_mrg_enable_MM_segments_0000 : std_logic_vector(7 downto 0);
            --  0001
            sTGC_mrg_enable_SL_output_0001 : std_logic;
            -- number of 320MHz clocks to wait before output to SL 0002
            sTGC_mrg_SL_out_offset_0002    : std_logic_vector(2 downto 0);
            -- LUT that contains control parameters for Band Builder Control module Vector [4] 0003(0)
            sTGC_mrg_fiber_ctrl_param_0003 : vecVec_4of16;

    end record;

    ----------------------------------------

end registerFile_package;
