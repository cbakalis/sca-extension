-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.04.2019 12:18:52
-- Design Name: Pipeline
-- Module Name: pipeline - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Generic pipeline design
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
library UNISIM; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

entity pipeline is
  generic(
    NUMBER_OF_BITS    : natural := 8;   -- number of signals to be pipelined
    NUMBER_OF_STAGES  : natural := 4);  -- number of pipeline stages
  port(
    clk               : in  std_logic; -- logic clock                                    
    data_in           : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);
    data_out_p        : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0) 
  ); 
end pipeline; 

architecture RTL of pipeline is

  type data_array is array (NUMBER_OF_STAGES - 1 downto 0) of std_logic_vector(NUMBER_OF_BITS - 1 downto 0); 
  signal data_in_reg       : data_array;

  attribute dont_touch                : string; 
  attribute keep                      : string; 
  attribute dont_touch of data_in_reg : signal is "TRUE"; 
  attribute keep of data_in_reg       : signal is "TRUE"; 
  
begin

normalStages_generate: if NUMBER_OF_STAGES >= 2 generate 
  
-------------------------------------------------------
-- First pipeline stage
-------------------------------------------------------
register_input_proc : process(clk)
begin
  if(rising_edge(clk))then
    data_in_reg(0) <= data_in; 
  end if; 
end process; 

-------------------------------------------------------
-- Intermediate pipeline stages
-------------------------------------------------------

pipeline_instances: for I in 1 to (NUMBER_OF_STAGES - 1) generate 

registers_internal_proc : process(clk)
begin
  if(rising_edge(clk))then
    data_in_reg(I) <= data_in_reg(I - 1); 
  end if; 
end process; 

end generate pipeline_instances;

-------------------------------------------------------
-- Final "stage"
-------------------------------------------------------
  data_out_p <= data_in_reg(NUMBER_OF_STAGES - 1); 

end generate normalStages_generate;

twoStages_generate: if NUMBER_OF_STAGES = 1 generate 
register_inputOutput_proc : process(clk)
begin
  if(rising_edge(clk))then
    data_in_reg(0)  <= data_in; 
  end if; 
end process;

  data_out_p <= data_in_reg(0);

end generate twoStages_generate;

noStages_generate: if NUMBER_OF_STAGES = 0 generate 
    data_out_p <= data_in;
end generate noStages_generate;
  
end RTL;