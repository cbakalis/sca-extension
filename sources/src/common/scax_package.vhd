----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 28.08.2018 00:12:10
-- Design Name: Frame Package
-- Module Name: scax_package - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Package for logical grouping of frame contents
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 

package scax_package is
  
  type frame_rx is record
    address : std_logic_vector(7 downto 0); 
    control : std_logic_vector(7 downto 0); 
    ---------------------------------------
    tr_id   : std_logic_vector(7 downto 0); 
    channel : std_logic_vector(7 downto 0); 
    length  : std_logic_vector(7 downto 0); 
    command : std_logic_vector(7 downto 0); 
    din_a   : std_logic_vector(15 downto 0); 
    din_b   : std_logic_vector(15 downto 0); 
    ---------------------------------------
    fcs : std_logic_vector(15 downto 0); 
  end record; 
  
  type frame_tx is record
    address : std_logic_vector(7 downto 0); 
    control : std_logic_vector(7 downto 0); 
    ---------------------------------------
    tr_id   : std_logic_vector(7 downto 0); 
    channel : std_logic_vector(7 downto 0); 
    err     : std_logic_vector(7 downto 0); 
    length  : std_logic_vector(7 downto 0); 
    dout_a  : std_logic_vector(15 downto 0); 
    dout_b  : std_logic_vector(15 downto 0); 
    ---------------------------------------
    fcs : std_logic_vector(15 downto 0); 
  end record; 
  
  --------------------------
  -- SCAX Variables/Generics 
  --------------------------
  constant elinkEncoding : std_logic_vector(1 downto 0)  := "01";  -- 01: 8b10b, 10: hdlc
  constant ser_input     : boolean                       := false; -- use 1-bit e-link
  constant TX_dataRate   : integer                       := 80;    -- RX is only 80, TX can be 80 or 320
  constant debug_mode    : std_logic                     := '0';   -- disable/enable ILA and debug buffer. comment-out mark_debug in sca_extension.vhd if set to '0'
  constant cdc_bitmask   : std_logic_vector(15 downto 0) := "0000000000000000"; -- which i2c_master crosses clock domains?
  
  type i2c_array_rx is array (15 downto 0) of frame_rx; 
  type i2c_array_tx is array (15 downto 0) of frame_tx; 
  type i2c_array_size is array (15 downto 0) of std_logic_vector(1 downto 0); 
  type regAddr_array is array (15 downto 0) of std_logic_vector(9 downto 0); 
  type data_array is array (15 downto 0) of std_logic_vector(31 downto 0); 
  
  -- s-frame control fields for replies
  constant uACK_reply         : std_logic_vector(7 downto 0) := x"63"; 
  constant frameReject_reply  : std_logic_vector(7 downto 0) := "10010111"; 
  
  -- error constants
  constant error_none         : std_logic_vector(7 downto 0) := "00000000"; 
  constant error_generic      : std_logic_vector(7 downto 0) := "00000001"; 
  constant error_invChReq     : std_logic_vector(7 downto 0) := "00000010"; 
  constant error_invChCmd     : std_logic_vector(7 downto 0) := "00000100"; 
  constant error_invTrId      : std_logic_vector(7 downto 0) := "00001000"; 
  constant error_invLen       : std_logic_vector(7 downto 0) := "00010000"; 
  constant error_chNotENa     : std_logic_vector(7 downto 0) := "00100000"; 
  
  -- controller commands
  constant CTRL_R_ID          : std_logic_vector(7 downto 0) := x"D1"; 
  constant CTRL_W_CRB         : std_logic_vector(7 downto 0) := x"02"; 
  constant CTRL_W_CRC         : std_logic_vector(7 downto 0) := x"04"; 
  constant CTRL_W_CRD         : std_logic_vector(7 downto 0) := x"06"; 
  constant CTRL_R_CRB         : std_logic_vector(7 downto 0) := x"03"; 
  constant CTRL_R_CRC         : std_logic_vector(7 downto 0) := x"05"; 
  constant CTRL_R_CRD         : std_logic_vector(7 downto 0) := x"07"; 
  constant CTRL_R_SEU         : std_logic_vector(7 downto 0) := x"F1"; 
  constant CTRL_C_SEU         : std_logic_vector(7 downto 0) := x"F0"; 
  
  -- dummy GPIO
  constant GPIO_W_DOUT        : std_logic_vector(7 downto 0) := x"10"; 
  constant GPIO_R_DOUT        : std_logic_vector(7 downto 0) := x"11"; 
  constant GPIO_R_DIN         : std_logic_vector(7 downto 0) := x"01"; 
  constant GPIO_W_DIR         : std_logic_vector(7 downto 0) := x"20"; 
  constant GPIO_R_DIR         : std_logic_vector(7 downto 0) := x"21"; 
  
  -- dummy ADC
  constant ADC_GO             : std_logic_vector(7 downto 0) := x"02"; 
  constant ADC_W_MUX          : std_logic_vector(7 downto 0) := x"50"; 
  constant ADC_R_MUX          : std_logic_vector(7 downto 0) := x"51"; 
  
  -- dummy JTAG
  constant JTAG_W_CTRL        : std_logic_vector(7 downto 0) := x"80"; 
  constant JTAG_R_CTRL        : std_logic_vector(7 downto 0) := x"81"; 
  constant JTAG_W_FREQ        : std_logic_vector(7 downto 0) := x"90"; 
  constant JTAG_R_FREQ        : std_logic_vector(7 downto 0) := x"91"; 
  constant JTAG_W_TDO0        : std_logic_vector(7 downto 0) := x"00"; 
  constant JTAG_R_TDI0        : std_logic_vector(7 downto 0) := x"01"; 
  constant JTAG_W_TDO1        : std_logic_vector(7 downto 0) := x"10"; 
  constant JTAG_R_TDI1        : std_logic_vector(7 downto 0) := x"11"; 
  constant JTAG_W_TDO2        : std_logic_vector(7 downto 0) := x"20"; 
  constant JTAG_R_TDI2        : std_logic_vector(7 downto 0) := x"21"; 
  constant JTAG_W_TDO3        : std_logic_vector(7 downto 0) := x"30"; 
  constant JTAG_R_TDI3        : std_logic_vector(7 downto 0) := x"31"; 
  constant JTAG_W_TMS0        : std_logic_vector(7 downto 0) := x"40"; 
  constant JTAG_R_TMS0        : std_logic_vector(7 downto 0) := x"41"; 
  constant JTAG_W_TMS1        : std_logic_vector(7 downto 0) := x"50"; 
  constant JTAG_R_TMS1        : std_logic_vector(7 downto 0) := x"51"; 
  constant JTAG_W_TMS2        : std_logic_vector(7 downto 0) := x"60"; 
  constant JTAG_R_TMS2        : std_logic_vector(7 downto 0) := x"61"; 
  constant JTAG_W_TMS3        : std_logic_vector(7 downto 0) := x"70"; 
  constant JTAG_R_TMS3        : std_logic_vector(7 downto 0) := x"71"; 
  constant JTAG_GO            : std_logic_vector(7 downto 0) := x"A2"; 
  
  
  -- i2c commands
  constant I2C_W_CTRL         : std_logic_vector(7 downto 0) := x"30"; 
  constant I2C_R_CTRL         : std_logic_vector(7 downto 0) := x"31"; 
  constant I2C_R_STR          : std_logic_vector(7 downto 0) := x"11"; 
  constant I2C_W_DATA0        : std_logic_vector(7 downto 0) := x"40"; -- address goes here
  constant I2C_R_DATA0        : std_logic_vector(7 downto 0) := x"41"; 
  constant I2C_W_DATA1        : std_logic_vector(7 downto 0) := x"50"; -- data go here
  constant I2C_R_DATA1        : std_logic_vector(7 downto 0) := x"51"; 
  constant I2C_R_DATA3        : std_logic_vector(7 downto 0) := x"71"; -- readback data go here
  constant I2C_M_10B_W        : std_logic_vector(7 downto 0) := x"E2"; 
  constant I2C_M_10B_R        : std_logic_vector(7 downto 0) := x"E6"; 
  
  -- i2c status register status
  constant status_good        : std_logic_vector(7 downto 0) := "00000100"; 
  constant status_invcom      : std_logic_vector(7 downto 0) := "00100000"; 
  constant status_noAck       : std_logic_vector(7 downto 0) := "01000000"; 
  
  
  
end scax_package; 
