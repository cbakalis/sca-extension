-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.04.2019 12:18:52
-- Design Name: Dummy Logic 1
-- Module Name: dummy_logic1 - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: Dummy loopback dummy logic
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
library UNISIM; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all; 

use work.registerFile_package.all;

entity dummy_logic1 is
  port(
    clk             : in  std_logic; -- logic clock 
    rst             : in  std_logic;
    regs_SCAXtoUFL  : in  SCAXtoUFL; -- used when writing
    regs_UFLtoSCAX  : out UFLtoSCAX -- used when reading 
  ); 
end dummy_logic1; 

architecture RTL of dummy_logic1 is

component scax_mem_ctrl
  generic(
    Nbits_addr_W      : natural := 12;  -- address width (write operation)
    Nbits_data_W      : natural := 32;  -- data width (write operation)
    Nbits_addr_R      : natural := 12;  -- address width (read operation) 
    Nbits_data_R      : natural := 32); -- data width (read operation)
  port(
    -------------------------------
    ------ General Interface ------
    wr_clk            : in std_logic; -- regFile wr_clk + mem element wr_clk
    rd_clk            : in std_logic; -- regFile wr_clk + mem element rd_clk
    rst               : in std_logic; -- reset
    -------------------------------
    -- Register File Interface ----
    -- write
    wrEn_fromSCAX     : in  std_logic;                                 -- write-pulse the memDin into the mem ctrl
    wr_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_W-1 downto 0); -- addr to write into
    din_fromSCAX      : in  std_logic_vector(Nbits_data_W-1 downto 0); -- data to write
    wr_addr_toSCAX    : out std_logic_vector(Nbits_addr_W-1 downto 0); -- current addr (back to scax)
    -- read
    rdEn_fromSCAX     : in  std_logic;                                 -- read-pulse the memDout into the mem ctrl
    rd_addr_fromSCAX  : in  std_logic_vector(Nbits_addr_R-1 downto 0); -- addr to read from
    dout_toSCAX       : out std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read (back to scax)
    rd_addr_toSCAX    : out std_logic_vector(Nbits_addr_R-1 downto 0); -- current addr (back to scax)
    -------------------------------
    -- Memory Element Interface ---
    -- write
    wrEn_toMem        : out std_logic;                                 -- to ram/fifo wr_en
    wr_addr_toMem     : out std_logic_vector(Nbits_addr_W-1 downto 0); -- to ram/fifo addr
    din_toMem         : out std_logic_vector(Nbits_data_W-1 downto 0); -- to ram/fifo din
    --read
    dout_fromMem      : in  std_logic_vector(Nbits_data_R-1 downto 0); -- data that were read
    rdEn_toMem        : out std_logic;
    rd_addr_toMem     : out std_logic_vector(Nbits_addr_R-1 downto 0) -- to ram/fifo addr
  ); 
end component; 

component blk_mem_test
  port(
    clka  : in  std_logic;
    wea   : in  std_logic_vector(0 downto 0);
    addra : in  std_logic_vector(11 downto 0);
    dina  : in  std_logic_vector(31 downto 0);
    clkb  : in  std_logic;
    enb   : in  std_logic;
    addrb : in  std_logic_vector(11 downto 0);
    doutb : out std_logic_vector(31 downto 0)
  );
end component;

  signal  wrEn_toMem        : std_logic := '0';
  signal  wr_addr_toMem     : std_logic_vector(11 downto 0) := (others => '0');
  signal  din_toMem         : std_logic_vector(31 downto 0) := (others => '0');
  signal  dout_fromMem      : std_logic_vector(31 downto 0) := (others => '0');
  signal  rdEn_toMem        : std_logic := '0';
  signal  rd_addr_toMem     : std_logic_vector(11 downto 0) := (others => '0');
  
begin

gen_loop: for I in 0 to 31 generate
dummy_proc: process(clk)
begin
  if(rising_edge(clk))then
    -- auto-gen regFile
    --regs_UFLtoSCAX.MM_addc_addc_packet_BC_offset_0003(I) <= regs_SCAXtoUFL.MM_addc_addc_packet_BC_offset_0003(I);

    regs_UFLtoSCAX.master01_dummy32_3ff <= x"c0cac01a";
    regs_UFLtoSCAX.master01_dummy32_3ab <= regs_SCAXtoUFL.master01_dummy32_3ff;

  end if;
end process;

end generate gen_loop;

scax_mem_ctrl_inst: scax_mem_ctrl
  generic map(
    Nbits_addr_W      => 12,
    Nbits_data_W      => 32,
    Nbits_addr_R      => 12,
    Nbits_data_R      => 32)
  port map(
    -------------------------------
    ------ General Interface ------
    wr_clk            => clk,
    rd_clk            => clk,
    rst               => rst,
    -------------------------------
    -- Register File Interface ----
    -- write
    wrEn_fromSCAX     => regs_SCAXtoUFL.master01_wrEn_003,
    wr_addr_fromSCAX  => regs_SCAXtoUFL.master01_wrAddr12_002,
    din_fromSCAX      => regs_SCAXtoUFL.master01_wrData32_003,
    wr_addr_toSCAX    => regs_UFLtoSCAX.master01_wrAddr12_002,
    -- read
    rdEn_fromSCAX     => regs_SCAXtoUFL.master01_rdEn_001,
    rd_addr_fromSCAX  => regs_SCAXtoUFL.master01_rdAddr12_000,
    dout_toSCAX       => regs_UFLtoSCAX.master01_rdData32_001,
    rd_addr_toSCAX    => regs_UFLtoSCAX.master01_rdAddr12_000,
    -------------------------------
    -- Memory Element Interface ---
    -- write
    wrEn_toMem        => wrEn_toMem,
    wr_addr_toMem     => wr_addr_toMem,
    din_toMem         => din_toMem,
    --read
    dout_fromMem      => dout_fromMem,
    rdEn_toMem        => rdEn_toMem,
    rd_addr_toMem     => rd_addr_toMem
  ); 

  blk_mem_test_inst: blk_mem_test
  port map(
    clka    => clk,
    wea(0)  => wrEn_toMem,
    addra   => wr_addr_toMem,
    dina    => din_toMem,
    clkb    => clk,
    enb     => rdEn_toMem,
    addrb   => rd_addr_toMem,
    doutb   => dout_fromMem
  );
  
end RTL;